---
title: How to install Monit 5 on Fedora 36
author: Kipkoech Sang
type: post
date: 2022-08-11T08:05:30+00:00
url: /2022/08/11/how-to-install-monit-5-on-fedora-36/
categories:
  - Uncategorized

---
In this tutorial, we are going to learn how to install Monit 5 on Fedora 36.

<a href="https://en.wikipedia.org/wiki/Monit" target="_blank" rel="noreferrer noopener">Monit</a> is a small open source utility for monitoring and managing Unix systems. It performs automatic maintenance whenever there is downtime in your system.

## Uses of Monit. {.wp-block-heading}

Monit can have many use cases because of its versatility. Let’s look at the following:

  * Monit can be used to monitor files and directories, Monit monitor this system for any changes such as file timestamp, security changes or file size changes, etc.
  * Monit acts proactively, whereby in case of an error in the system functionality, Monit can send an alert to inform that such a process wasn’t complete.
  * Monit can be used to test programs and scripts such as cron jobs do. You can monitor and check the scripts you want to use in any scenario.
  * We use Monit to monitor any daemon processes or any other process running on the localhost such as sshd, Nginx, or MySQL processes.
  * Monit can be used to monitor general system functionality such as CPU and RAM usage.
  * It can be used to monitor network connections on various servers because Monit has built-in tests for Internet protocols such as SMTP, and HTTP.

### How to Install Monit 5 on Fedora 36 {.wp-block-heading}

### 1. Update system repositories. {.wp-block-heading}

Before you begin any installation, make sure you update all the system packages in order to make them up to date. To update Fedora use `<mark style="background-color:#abb8c3" class="has-inline-color">sudo dnf update</mark>`.

<pre class="wp-block-code"><code>$ sudo dnf update</code></pre>

Follow the prompts in order to complete updates successfully.

### 2. Install Monit 5 on Fedora 36 {.wp-block-heading}

Monit is readily available on the Fedora packages, so to install you use the following command `<mark style="background-color:#abb8c3" class="has-inline-color">sudo dnf install monit</mark>`.

<pre class="wp-block-code"><code>$ sudo dnf install monit </code></pre>

You should be in a position to see the following output.

<pre class="wp-block-code"><code>Fedora Modular 36 - x86_64 - Updates             12 kB/s |  15 kB     00:01    
Dependencies resolved.
================================================================================
 Package        Architecture    Version                  Repository        Size
================================================================================
Installing:
 &lt;strong>monit&lt;/strong>          x86_64          5.32.0-1.fc36            updates          391 k

Transaction Summary
================================================================================
Install  1 Package

Total download size: 391 k
Installed size: 990 k
Is this ok &#91;y/N]: y
Downloading Packages:
monit-5.32.0-1.fc36.x86_64.rpm                  949 kB/s | 391 kB     00:00    
--------------------------------------------------------------------------------
Total                                           619 kB/s | 391 kB     00:00     
Running transaction check
Transaction check succeeded.
Running transaction test
Transaction test succeeded.
Running transaction
  Preparing        :                                                        1/1 
  Installing       : monit-5.32.0-1.fc36.x86_64                             1/1 
  Running scriptlet: monit-5.32.0-1.fc36.x86_64                             1/1 
  Verifying        : monit-5.32.0-1.fc36.x86_64                             1/1 

Installed:
  monit-5.32.0-1.fc36.x86_64                                                    

Complete!</code></pre>

### 3. Configure Monit service {.wp-block-heading}

To configure Monit to start on boot we use the following command `<mark style="background-color:#abb8c3" class="has-inline-color">sudo systemctl enable monit</mark>`

<pre class="wp-block-code"><code>$ sudo systemctl enable monit</code></pre>

To start Monit immediately we use the following command.

<pre class="wp-block-code"><code>$ sudo systemctl start monit </code></pre>

To check the status of Monit we use the following command.

<pre class="wp-block-code"><code>$ sudo systemctl status monit 
● monit.service - Pro-active monitoring utility for unix systems
     Loaded: loaded (/usr/lib/systemd/system/monit.service; enabled; vendor pre>
     Active: active (running) since Thu 2022-08-11 08:00:10 UTC; 12s ago
       Docs: man:monit(1)
             https:&#47;&#47;mmonit.com/wiki/Monit/HowTo
   Main PID: 15788 (monit)
      Tasks: 2 (limit: 1113)
     Memory: 2.0M
        CPU: 29ms
     CGroup: /system.slice/monit.service
             └─ 15788 /usr/bin/monit -I

Aug 11 08:00:10 localhost.localdomain systemd&#91;1]: Started monit.service - Pro-a>
Aug 11 08:00:10 localhost.localdomain monit&#91;15788]:  New Monit id: d45a290ce0d6>
                                                     Stored in '/root/.monit.id'
Aug 11 08:00:10 localhost.localdomain monit&#91;15788]: Starting Monit 5.32.0 daemo>
Aug 11 08:00:10 localhost.localdomain monit&#91;15788]: 'localhost.localdomain' Mon></code></pre>

There are packages installed with Monit automatically. To check those files you can use the following command `<mark style="background-color:#abb8c3" class="has-inline-color">rpm -ql monit</mark>`.

<pre class="wp-block-code"><code>$ sudo rpm -ql monit
/etc/monit.d
/etc/monitrc
/usr/bin/monit
/usr/lib/.build-id
/usr/lib/.build-id/9a
/usr/lib/.build-id/9a/670fe3396cb159ea1c384f1f673a420b3691ed
/usr/lib/systemd/system/monit.service
/usr/share/doc/monit
/usr/share/doc/monit/CHANGES
/usr/share/doc/monit/COPYING
/usr/share/man/man1/monit.1.gz</code></pre>

NB: Prefix sudo whenever you are working as a regular user.

## Conclusion {.wp-block-heading}

We have successfully installed and configured Monit on Ubuntu 22.04. For further reference check the&nbsp;<a href="https://mmonit.com/wiki/" target="_blank" rel="noreferrer noopener">Monit documentation</a>.