---
title: How to install Node Js on CentOS 8 Stream
author: Kipkoech Sang
type: post
date: 2022-01-20T11:27:29+00:00
url: /2022/01/20/how-to-install-node-js-on-centos-8-stream/
rank_math_news_sitemap_robots:
  - index
rank_math_robots:
  - 'a:1:{i:0;s:5:"index";}'
rank_math_analytic_object_id:
  - 28
rank_math_internal_links_processed:
  - 1
categories:
  - Linux

---
In today’s guide, I am going to take you through the installation of node js on CentOS 8 Stream.

Node.js is an open-source cross-platform, backend javascript runtime environment that runs on the V8 engine and executes javascript code outside of a web browser.

A Node.js app runs in a single process, without creating a new thread for every request. It provides a set of asynchronous I/O primitives in its standard library that prevent javascript code from blocking and generally, libraries from node.js are written using non-blocking paradigms, making blocking behavior the exceptions rather than the norm.

When Node.js performs an I/O operation, like reading from the network, accessing a database or the filesystem, instead of blocking the thread and wasting CPU cycles waiting, Node.js will resume the operations when the response comes back. This allows Node.js to handle thousands of concurrent connections with a single server without introducing the burden of managing thread concurrency, which could be a significant source of bugs.

**[nvm][1]**&nbsp;is used to run node.js packages. It allows you to easily switch node.js versions and install new versions to try and easily roll back if something went wrong.

When Node.js is installed, you will have access to the node executable program in the command line.

## Node Js best proactices  {.wp-block-heading}

  * Structure your code by components. This will help alot while incorporating new features into your system.
  * Keep the web layer within their boundaries. Each component must have layers which is a dedicated object for the web, logic and data access.
  * Wrap common utilities on an NPM such as encryption.
  * Separate your app and the server platforms
  * Use asy-wait for error handling issues.
  * Handle errors centrally, not within a middleware.
  * Document API errors using Swagger or GraphQL
  * Use ESlint as a linting tool
  * Use specific Node js plugins
  * Separate your statements properly.

## Related Articles  {.wp-block-heading}

  * [How to install Node.js 17 on Ubuntu 20.04][2]
  * [How to install Node.js on Fedora 35][3]

## Prerequisites {.wp-block-heading}

  1. Have a user with sudo privileges
  2. Have a CentOS 9 stream up and running.
  3. Have basic terminal knowledge.

## Installing Node js on CentOS 8 stream {.wp-block-heading}

### 1. Run System updates  {.wp-block-heading}

To begin with, we are going to run system updates in order to make our repositories up to date. 

<pre class="wp-block-code"><code>$ sudo dnf update -y</code></pre>

### 2. List available Node Js stream 8 modules {.wp-block-heading}

To list the available modules on CentOS 8 stream use the following command:

<pre class="wp-block-code"><code>$ sudo dnf module list nodejs
# output
Last metadata expiration check: 0:01:37 ago on Thu 20 Jan 2022 10:41:19 AM UTC.
CentOS Stream 8 - AppStream
Name                 Stream                  Profiles                                           Summary                        
nodejs               10 &#91;d]&#91;e]               common &#91;d], development, minimal, s2i              Javascript runtime             
nodejs               12                      common &#91;d], development, minimal, s2i              Javascript runtime             
nodejs               14                      common &#91;d], development, minimal, s2i              Javascript runtime             
nodejs               16                      common, development, minimal, s2i                  Javascript runtime             

Hint: &#91;d]efault, &#91;e]nabled, &#91;x]disabled, &#91;i]nstalled</code></pre>

### 3. Install Node js on CentOS 8 Stream {.wp-block-heading}

Node.js is available in the stream 9 repositories, It&#8217;s not like other retired distributions. So to install Node.js we will run the following command. 

<pre class="wp-block-code"><code>$ sudo dnf install nodejs</code></pre>

You will see the following output

<pre class="wp-block-code"><code>output
Dependencies resolved.
===============================================================================================================================
 Package                   Architecture    Version                                                    Repository          Size
===============================================================================================================================
Installing:
 nodejs                    x86_64          1:10.23.1-1.module_el8.4.0+645+9ce14ba2                    appstream          8.9 M
Installing dependencies:
 npm                       x86_64          1:6.14.10-1.10.23.1.1.module_el8.4.0+645+9ce14ba2          appstream          3.7 M
Installing weak dependencies:
 nodejs-full-i18n          x86_64          1:10.23.1-1.module_el8.4.0+645+9ce14ba2                    appstream          7.3 M
Enabling module streams:
 nodejs                                    10                                                                                 

Transaction Summary
===============================================================================================================================
Install  3 Packages

Total download size: 20 M
Installed size: 71 M
Is this ok &#91;y/N]: y
....
Running transaction
  Running scriptlet: npm-1:6.14.10-1.10.23.1.1.module_el8.4.0+645+9ce14ba2.x86_64                                          1/1 
  Preparing        :                                                                                                       1/1 
  Installing       : nodejs-full-i18n-1:10.23.1-1.module_el8.4.0+645+9ce14ba2.x86_64                                       1/3 
  Installing       : npm-1:6.14.10-1.10.23.1.1.module_el8.4.0+645+9ce14ba2.x86_64                                          2/3 
  Installing       : nodejs-1:10.23.1-1.module_el8.4.0+645+9ce14ba2.x86_64                                                 3/3 
  Running scriptlet: nodejs-1:10.23.1-1.module_el8.4.0+645+9ce14ba2.x86_64                                                 3/3 
  Verifying        : nodejs-1:10.23.1-1.module_el8.4.0+645+9ce14ba2.x86_64                                                 1/3 
  Verifying        : nodejs-full-i18n-1:10.23.1-1.module_el8.4.0+645+9ce14ba2.x86_64                                       2/3 
  Verifying        : npm-1:6.14.10-1.10.23.1.1.module_el8.4.0+645+9ce14ba2.x86_64                                          3/3 

Installed:
  nodejs-1:10.23.1-1.module_el8.4.0+645+9ce14ba2.x86_64        nodejs-full-i18n-1:10.23.1-1.module_el8.4.0+645+9ce14ba2.x86_64
  npm-1:6.14.10-1.10.23.1.1.module_el8.4.0+645+9ce14ba2.x86_64</code></pre>

Press y to allow the installation to continue.

To check the node version do the following:

<pre class="wp-block-code"><code>$ node -v
v10.23.1</code></pre>

## Conclusion {.wp-block-heading}

Congratulation you have installed Nodejs on CentOS 8 stream. Incase of any issue please feel free to contact us for assistance.

 [1]: https://github.com/nvm-sh/nvm
 [2]: https://nextgentips.com/2021/12/26/how-to-install-node-js-17-on-ubuntu-20-04/
 [3]: https://nextgentips.com/2021/12/02/how-to-install-node-js-on-fedora-35/