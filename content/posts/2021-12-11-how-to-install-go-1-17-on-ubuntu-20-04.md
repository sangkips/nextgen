---
title: How to install Go 1.17 on Ubuntu 20.04|22.04
author: Kipkoech Sang
type: post
date: 2021-12-11T19:18:07+00:00
url: /2021/12/11/how-to-install-go-1-17-on-ubuntu-20-04/
rank_math_primary_category:
  - 36
rank_math_description:
  - Golang is an open-source language that is easy to learn and use. Go 1.17 is reliable, builds fast, and efficient software that scales fast.
rank_math_focus_keyword:
  - go 1.17
rank_math_news_sitemap_robots:
  - index
rank_math_robots:
  - 'a:1:{i:0;s:5:"index";}'
rank_math_analytic_object_id:
  - 67
rank_math_internal_links_processed:
  - 1
rank_math_seo_score:
  - 68
categories:
  - Programming

---
In this tutorial, we are going to explore how to install Go 1.17 on Ubuntu 20.04|22.04.

[Golang][1] is an open-source programming language that is easy to learn and use. It is built-in concurrency and has a robust standard library. It is reliable, builds fast, and efficient software that scales fast.

Its concurrency mechanisms make it easy to write programs that get the most out of multicore and networked machines, while its novel-type systems enable flexible and modular program constructions. 

Go compiles quickly to machine code and has the convenience of garbage collection and the power of run-time reflection.

## Related Articles  {#related-articles.wp-block-heading}

  * [How to install Go 1.18 on Rocky Linux/AlmaLinux][2]
  * [How to install go 1.18 on Ubuntu 20.04][3]
  * [How to install Go 1.18 on Fedora 35][4]

## Installing Go 1.17 on Ubuntu 20.04 {#installing-go-1-17-on-ubuntu-20-04.wp-block-heading}

### 1. Run system updates  {#1-run-system-updates.wp-block-heading}

To begin with, we need to run system updates in order to make our current repositories up to date. To do so we need to run the following command on our terminal.

<pre class="wp-block-code"><code>$ sudo apt update && apt upgrade -y</code></pre>

When both upgrades and updates are complete, go ahead to download go from its download page.

### 2. Download Go 1.17 {#2-download-go-1-17.wp-block-heading}

To download go, move into the [go download page][5] and click download. When the download is complete go to the download page and extract.

When the download is complete, now move ahead and install go onto your system.

### 3. Install Go 1.17 on Ubuntu 20.04 {#3-install-go-1-17-on-ubuntu-20-04.wp-block-heading}

For us to have a working go installation, we need to extract the archive downloaded into usr/local, creating a Go tree on /usr/local/go. Remove the previous go modules before proceeding. Use the following command to perform this operation.

<pre class="wp-block-code"><code>$ tar -C /usr/local -xzf go1.17.5.linux-amd64.tar.gz</code></pre>

Now that we have extracted go, we now need to create PATH so that we can tell where we will find our Go modules whenever we want to access them.

Add /usr/local/go/bin to the PATH environment variable. 

On your $HOME/.profile add the following line:

<pre class="wp-block-code"><code>$ export PATH=$PATH:/usr/local/go/bin</code></pre>

For the above command to be applied system-wide make sure to add it to /etc/profile.

To apply the changes immediately, run source $HOME/.profile

<pre class="wp-block-code"><code>$ source $HOME/.profile</code></pre>

To verify the go version run the following command

<pre class="wp-block-code"><code>$ go version
go version go1.17.5 linux/amd64</code></pre>

<pre class="wp-block-code"><code>$ go env
GO111MODULE=""
GOARCH="amd64"
GOBIN=""
GOCACHE="/root/.cache/go-build"
GOENV="/root/.config/go/env"
GOEXE=""
GOEXPERIMENT=""
GOFLAGS=""
GOHOSTARCH="amd64"
GOHOSTOS="linux"
GOINSECURE=""
GOMODCACHE="/root/go/pkg/mod"
GONOPROXY=""
GONOSUMDB=""
GOOS="linux"
GOPATH="/root/go"
GOPRIVATE=""
GOPROXY="https://proxy.golang.org,direct"
GOROOT="/usr/local/go"
GOSUMDB="sum.golang.org"
GOTMPDIR=""
GOTOOLDIR="/usr/local/go/pkg/tool/linux_amd64"
GOVCS=""
GOVERSION="go1.17.5"
GCCGO="gccgo"
AR="ar"
CC="gcc"
CXX="g++"
CGO_ENABLED="1"
GOMOD="/dev/null"
CGO_CFLAGS="-g -O2"
CGO_CPPFLAGS=""
CGO_CXXFLAGS="-g -O2"
CGO_FFLAGS="-g -O2"
CGO_LDFLAGS="-g -O2"
PKG_CONFIG="pkg-config"
GOGCCFLAGS="-fPIC -m64 -pthread -fmessage-length=0 -fdebug-prefix-map=/tmp/go-build452429440=/tmp/go-build -gno-record-gcc-switches"</code></pre>

### 4. Write hello world sample program. {#4-write-hello-world-sample-program.wp-block-heading}

To write our first program do the following:

Move to your home directory and create a hello directory

<pre class="wp-block-code"><code>$ cd $HOME
$ mkdir hello
$ cd hello</code></pre>

Enable dependency tracking for your code. To enable dependency tracking, create a go.mod module. This module stays with your code. To enable it run the _**go mod init**_. Run the following to enable dependency tracking.

<pre class="wp-block-code"><code>$ go mod init nextgentips/hello
go: creating new go.mod: module nextgentips/hello</code></pre>

Open your preferred text editor and write the following code

<pre class="wp-block-code"><code>Package main
import 'fmt'

func main() {
         fmt.Println("Hello, world")
}
</code></pre>

Run your code with the following command.

<pre class="wp-block-code"><code>$ go run
Hello, world</code></pre>

## Conclusion. {#conclusion.wp-block-heading}

In this guide, we have learned how to install and set up the go path. Happy coding!

### Best Golang Books {#best-golang-books.wp-block-heading}

If you want to learn more about Go programming read this book.

  * <a title="Learn Go" href="https://amzn.to/3AsqRMc" target="_blank" rel="noreferrer noopener">Learn Go</a>
  * <a title="Learning Go: An Idiomatic Approach to Real-World Go Programming" href="https://amzn.to/3nRwDlg" target="_blank" rel="noreferrer noopener">Learning Go: An Idiomatic Approach to Real-World Go Programming</a>
  * <a href="https://www.amazon.com/gp/product/B099TL8WLH/ref=as_li_tl?ie=UTF8&camp=1789&creative=9325&creativeASIN=B099TL8WLH&linkCode=as2&tag=nextgentips-20&linkId=a364fb35f897974e51646a6cfd9f6ac6" target="_blank" rel="noreferrer noopener">Go For Beginners : A Genius Guide to Go Programing</a>

 [1]: https://go.dev/doc/
 [2]: https://nextgentips.com/2021/12/28/how-to-install-go-1-18-on-rocky-linux-almalinux/
 [3]: https://nextgentips.com/2021/12/23/how-to-install-go-1-18-on-ubuntu-20-04/
 [4]: https://nextgentips.com/2021/12/21/how-to-install-go-1-18-on-fedora-35/
 [5]: https://go.dev/doc/install