---
title: How to install Node.js 17 on Ubuntu 20.04
author: Kipkoech Sang
type: post
date: 2021-12-26T13:20:09+00:00
url: /2021/12/26/how-to-install-node-js-17-on-ubuntu-20-04/
rank_math_news_sitemap_robots:
  - index
rank_math_robots:
  - 'a:1:{i:0;s:5:"index";}'
rank_math_analytic_object_id:
  - 51
rank_math_internal_links_processed:
  - 1
categories:
  - Programming

---
Node.js is an open-source cross-platform, backend javascript runtime environment that runs on the V8 engine and executes javascript code outside of a web browser.

A Node.js app runs in a single process, without creating a new thread for every request. It provides a set of asynchronous I/O primitives in its standard library that prevent javascript code from blocking and generally, libraries from node.js are written using non-blocking paradigms, making blocking behavior the exceptions rather than the norm.

When Node.js performs an I/O operation, like reading from the network, accessing a database or the filesystem, instead of blocking the thread and wasting CPU cycles waiting, Node.js will resume the operations when the response comes back. This allows Node.js to handle thousands of concurrent connections with a single server without introducing the burden of managing thread concurrency, which could be a significant source of bugs.

**[nvm][1]** is used to run node.js packages. It allows you to easily switch node.js versions and install new versions to try and easily roll back if something went wrong.

## Related Articles  {.wp-block-heading}

  * [How to install Node.js on Fedora 35][2]

## Prerequisites  {.wp-block-heading}

To install nodejs you need to have the following:

  * You are running and Ubuntu 20.04 server or desktop
  * Have a user with sudo privileges 
  * Basic knowledge of the terminal.

We are going to use two methods to install Node.js 

  * Installing using Node Version Manager
  * Using Node.js PPA

## Installing Node.js using Node version Manager {.wp-block-heading}

## Run System Updates  {.wp-block-heading}

To begin our installation, we need to run system-wide updates in order to update our repositories. Open your terminal and type the following command;

<pre class="wp-block-code"><code>$ sudo apt update && apt upgrade </code></pre>

Using NVM allows you to use different versions of node.js in your machine. To install using this method we need to get the command from the [github projects site.][1] Get the script and use to install node.js.

<pre class="wp-block-code"><code>$ wget -qO- https://raw.githubusercontent.com/nvm-sh/nvm/v0.39.1/install.sh | bash</code></pre>

You will get the following output.

<pre class="wp-block-code"><code>Output
=> Downloading nvm from git to '/root/.nvm'
=> Cloning into '/root/.nvm'...
remote: Enumerating objects: 353, done.
remote: Counting objects: 100% (353/353), done.
remote: Compressing objects: 100% (301/301), done.
remote: Total 353 (delta 40), reused 155 (delta 27), pack-reused 0
Receiving objects: 100% (353/353), 213.74 KiB | 9.71 MiB/s, done.
Resolving deltas: 100% (40/40), done.
* (HEAD detached at FETCH_HEAD)
  master
=> Compressing and cleaning up git repository

=> Appending nvm source string to /root/.bashrc
=> Appending bash_completion source string to /root/.bashrc
=> Close and reopen your terminal to start using nvm or run the following to use it now:

export NVM_DIR="$HOME/.nvm"
&#91; -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh"  # This loads nvm
&#91; -s "$NVM_DIR/bash_completion" ] && \. "$NVM_DIR/bash_completion"  # This loads nvm bash_completion</code></pre>

We need to source the script for the changes to take effect.

<pre class="wp-block-code"><code>$ source ~/.bashrc</code></pre>

Lets now check the versions of nodejs available from the remote source. Use the following command;

<pre class="wp-block-code"><code>$ nvm list-remote</code></pre>

You will get along list of remote nodejs. Sample output will look like this;

<pre class="wp-block-code"><code>Sample output
 ...
v16.10.0
       v16.11.0
       v16.11.1
       v16.12.0
       v16.13.0   (LTS: Gallium)
       v16.13.1   (Latest LTS: Gallium)
        v17.0.0
        v17.0.1
        v17.1.0
        v17.2.0
        v17.3.0</code></pre>

So to install the latest version you use this command;

<pre class="wp-block-code"><code>$ nvm install v17.3.0</code></pre>

This command will download the nodejs version 17.3.0 into our system.

<pre class="wp-block-code"><code>Output
Downloading and installing node v17.3.0...
Downloading https://nodejs.org/dist/v17.3.0/node-v17.3.0-linux-x64.tar.xz...
########################################################################################################## 100.0%
Computing checksum with sha256sum
Checksums matched!
Now using node v17.3.0 (npm v8.3.0)
Creating default alias: default -> v17.3.0</code></pre>

In order to verify your installation check with **node -v** command.

<pre class="wp-block-code"><code>$ node --version
v17.3.0</code></pre>

## Installing Node.js using PPA repository {.wp-block-heading}

We are going to install using PPA maintained by NodeSource community. 

Lets begin by installing the PPA into our system. Go to [NodeSource page][3] to get the available versions of node you will install into your system.

<pre class="wp-block-code"><code>$ curl -fsSL https://deb.nodesource.com/setup_17.x | sudo -E bash -</code></pre>

Using script is always a good thing because it makes your work very easy. You can check the output of this script. You don&#8217;t have to do anything at all.

<pre class="wp-block-code"><code>Output
## Installing the NodeSource Node.js 17.x repo...


## Populating apt-get cache...

+ apt-get update
Hit:1 http://security.ubuntu.com/ubuntu focal-security InRelease
Hit:2 http://mirrors.digitalocean.com/ubuntu focal InRelease                                                    
Hit:3 http://mirrors.digitalocean.com/ubuntu focal-updates InRelease                                            
Hit:4 https://repos-droplet.digitalocean.com/apt/droplet-agent main InRelease
Hit:5 http://mirrors.digitalocean.com/ubuntu focal-backports InRelease
Reading package lists... Done

## Confirming "focal" is supported...

+ curl -sLf -o /dev/null 'https://deb.nodesource.com/node_17.x/dists/focal/Release'

## Adding the NodeSource signing key to your keyring...

+ curl -s https://deb.nodesource.com/gpgkey/nodesource.gpg.key | gpg --dearmor | tee /usr/share/keyrings/nodesource.gpg >/dev/null

## Creating apt sources list file for the NodeSource Node.js 17.x repo...

+ echo 'deb &#91;signed-by=/usr/share/keyrings/nodesource.gpg] https://deb.nodesource.com/node_17.x focal main' > /etc/apt/sources.list.d/nodesource.list
+ echo 'deb-src &#91;signed-by=/usr/share/keyrings/nodesource.gpg] https://deb.nodesource.com/node_17.x focal main' >> /etc/apt/sources.list.d/nodesource.list

## Running `apt-get update` for you...

+ apt-get update
Hit:1 http://security.ubuntu.com/ubuntu focal-security InRelease
Hit:2 http://mirrors.digitalocean.com/ubuntu focal InRelease                                     
Get:3 https://deb.nodesource.com/node_17.x focal InRelease &#91;4583 B]                              
Hit:4 https://repos-droplet.digitalocean.com/apt/droplet-agent main InRelease
Hit:5 http://mirrors.digitalocean.com/ubuntu focal-updates InRelease
Hit:6 http://mirrors.digitalocean.com/ubuntu focal-backports InRelease
Get:7 https://deb.nodesource.com/node_17.x focal/main amd64 Packages &#91;772 B]
Fetched 5355 B in 1s (5743 B/s)
Reading package lists... Done

## Run `sudo apt-get install -y nodejs` to install Node.js 17.x and npm
## You may also need development tools to build native addons:
     sudo apt-get install gcc g++ make
## To install the Yarn package manager, run:
     curl -sL https://dl.yarnpkg.com/debian/pubkey.gpg | gpg --dearmor | sudo tee /usr/share/keyrings/yarnkey.gpg >/dev/null
     echo "deb &#91;signed-by=/usr/share/keyrings/yarnkey.gpg] https://dl.yarnpkg.com/debian stable main" | sudo tee /etc/apt/sources.list.d/yarn.list
     sudo apt-get update && sudo apt-get install yarn</code></pre>

## Install Node.js on Ubuntu 20.04 {.wp-block-heading}

Having pulled the script, its now time we do the installation. Open your terminal and type this command.

<pre class="wp-block-code"><code>$ sudo apt install nodejs</code></pre>

Sample output.

<pre class="wp-block-code"><code>Output
The following NEW packages will be installed:
  nodejs
0 upgraded, 1 newly installed, 0 to remove and 0 not upgraded.
Need to get 26.9 MB of archives.
After this operation, 180 MB of additional disk space will be used.
Get:1 https://deb.nodesource.com/node_17.x focal/main amd64 nodejs amd64 17.3.0-deb-1nodesource1 &#91;26.9 MB]
Fetched 26.9 MB in 0s (75.0 MB/s)</code></pre>

Check the node version with the following command;

<pre class="wp-block-code"><code>$ node --version
v17.3.0</code></pre>

## Conclusion {.wp-block-heading}

In this tutorial we have successfully installed node.js using NodeSource PPA and using Node Version Manager on our Ubuntu 20.04. Congratulations and continue following our site for more tutorials.

 [1]: https://github.com/nvm-sh/nvm
 [2]: https://nextgentips.com/2021/12/02/how-to-install-node-js-on-fedora-35/
 [3]: https://github.com/nodesource/distributions/blob/master/README.md