---
title: How to install Docker-CE on Ubuntu 22.04
author: Kipkoech Sang
type: post
date: 2022-05-09T07:40:17+00:00
url: /2022/05/09/how-to-install-docker-ce-on-ubuntu-22-04/
rank_math_seo_score:
  - 21
rank_math_primary_category:
  - 62
rank_math_focus_keyword:
  - install docker on Ubuntu 22.04
rank_math_internal_links_processed:
  - 1
rank_math_analytic_object_id:
  - 175
categories:
  - Containers

---
Docker is a set of platform-as-a-service products that uses OS-level virtualization to deliver software in packages called containers. Containers are usually isolated from one another and bundled with their own software libraries and configuration files, they can communicate with each other through well-defined channels.

Docker makes it possible to get more apps running on the same old servers and also makes it easy to package and ship programs. In this tutorial am going to show you how you can install Docker-CE on Ubuntu 20.04.

## Related Articles {.wp-block-heading}

  * [How to install and use Docker-ce on Fedora 35][1]
  * [How to install Docker on Arch Linux][2]
  * [How to install Docker-ce on Ubuntu 21.10][3]
  * [How to install and configure Docker-CE on Ubuntu 20.04][4]

## Prerequisites {.wp-block-heading}

  * Make sure you have a user with sudo privileges
  * Have Ubuntu 22.04 server up and running
  * Have basic knowledge about running commands on a shell

## Install Docker-ce on Ubuntu 22.04 {.wp-block-heading}

### 1. Update system repositories  {.wp-block-heading}

Let&#8217;s begin by updating our system repositories to make them up to date.

<pre class="wp-block-code"><code>sudo apt update && apt upgrade -y</code></pre>

When both updates and upgrades are complete, we can go ahead and uninstall old Docker versions.

### 2. Uninstall old versions of Docker  {.wp-block-heading}

Old versions of Docker were called docker, docker.io or docker-engine. Uninstall them before installing the new one.

<pre class="wp-block-code"><code>sudo apt-get remove docker docker-engine docker.io containerd runc</code></pre>

If they were not installed then you will see an output like this:

<pre class="wp-block-code"><code># output
Reading package lists... Done
Building dependency tree... Done
Reading state information... Done
Package 'docker-engine' is not installed, so not removed
Package 'containerd' is not installed, so not removed
Package 'runc' is not installed, so not removed
Package 'docker' is not installed, so not removed
Package 'docker.io' is not installed, so not removed
The following packages were automatically installed and are no longer required:
  docker-ce-rootless-extras docker-scan-plugin libslirp0 pigz slirp4netns
Use 'sudo apt autoremove' to remove them.
0 upgraded, 0 newly installed, 0 to remove and 0 not upgraded.</code></pre>

### 3. Installing Docker on Ubuntu 22.04 {.wp-block-heading}

There are three methods we can install Docker, 

  * Install using the DEB repository 
  * Install from the packages 
  * Install using a convenient script

#### Install using .deb Repository {.wp-block-heading}

We are installing docker here for the first time, so we need to set up the Docker repository in our system.

To set up the repository first install required dependencies such as, ca-certificates, curl, GnuPG and lsb-release.

<pre class="wp-block-code"><code>sudo apt install ca-certificates curl gnupg lsb-release</code></pre>

The output will be as follows:

<pre class="wp-block-code"><code>Reading package lists... Done
Building dependency tree... Done
Reading state information... Done
&lt;strong>ca-certificates is already the newest version (20211016).
gnupg is already the newest version (2.2.27-3ubuntu2).
gnupg set to manually installed.
lsb-release is already the newest version (11.1.0ubuntu4).
lsb-release set to manually installed.
curl is already the newest version (7.81.0-1ubuntu1.1)&lt;/strong>.
The following packages were automatically installed and are no longer required:
  docker-ce-rootless-extras docker-scan-plugin libslirp0 pigz slirp4netns
Use 'sudo apt autoremove' to remove them.
0 upgraded, 0 newly installed, 0 to remove and 0 not upgraded.</code></pre>

Then the next step is to install GPG key to sign the docker images being installed. 

<pre class="wp-block-code"><code>curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo gpg --dearmor -o /usr/share/keyrings/docker-archive-keyring.gpg</code></pre>

The next step is to set up a **stable, nightly,** or **test** repository depending on what you want to install.

<pre class="wp-block-code"><code>echo \
  "deb &#91;arch=$(dpkg --print-architecture) signed-by=/usr/share/keyrings/docker-archive-keyring.gpg] https://download.docker.com/linux/ubuntu \
  $(lsb_release -cs) &lt;strong>stable&lt;/strong>" | sudo tee /etc/apt/sources.list.d/docker.list > /dev/null</code></pre>

Lastly, let&#8217;s install the Docker engine. To check the available versions we can use the **apt-cache madison docker-ce command.**

<pre class="wp-block-code"><code>$ sudo apt-cache madison docker-ce
docker-ce | 5:20.10.15~3-0~ubuntu-jammy | https://download.docker.com/linux/ubuntu jammy/stable amd64 Packages
 docker-ce | 5:20.10.14~3-0~ubuntu-jammy | https://download.docker.com/linux/ubuntu jammy/stable amd64 Packages
 docker-ce | 5:20.10.13~3-0~ubuntu-jammy | https://download.docker.com/linux/ubuntu jammy/stable amd64 Packages</code></pre>

#### Install Docker-Engine {.wp-block-heading}

Before we can install the Docker engine let&#8217;s update our repositories one more time.

<pre class="wp-block-code"><code>$ sudo apt update
$ sudo apt-get install docker-ce docker-ce-cli containerd.io docker-compose-plugin</code></pre>

### 4. Enable Docker {.wp-block-heading}

We need to enable docker-ce to start on reboot, in order to avoid starting every time you boot your machine.

<pre class="wp-block-code"><code>sudo systemctl enable docker</code></pre>

### 5. Start Docker service {.wp-block-heading}

You are supposed to start the docker service to run containerd environment. To start, use the following command.

<pre class="wp-block-code"><code>sudo systemctl start docker</code></pre>

Lastly, we can check the status of docker to see if it is running.

### 6. Check the status of Docker {.wp-block-heading}

Check if Docker is running with the following command on your terminal.

<pre class="wp-block-code"><code>$ sudo systemctl status docker
● docker.service - Docker Application Container Engine
     Loaded: loaded (/lib/systemd/system/docker.service; enabled; vendor preset>
     Active: active (running) since Mon 2022-05-09 09:45:15 EAT; 8min ago
TriggeredBy: ● docker.socket
       Docs: https://docs.docker.com
   Main PID: 9636 (dockerd)
      Tasks: 10
     Memory: 30.5M
        CPU: 477ms
     CGroup: /system.slice/docker.service
             └─9636 /usr/bin/dockerd -H fd:// --containerd=/run/containerd/cont>

May 09 09:45:14 zx-pc dockerd&#91;9636]: time="2022-05-09T09:45:14.783146457+03:00">
May 09 09:45:14 zx-pc dockerd&#91;9636]: time="2022-05-09T09:45:14.783171530+03:00"></code></pre>

If you got status active, then your docker service is running as expected, if not probably you haven’t started the service.

### 7. Test Docker {.wp-block-heading}

Let’s test our docker to see if it is running. Test using hello-world container. This will pull hello-world image from docker hub

<pre class="wp-block-code"><code>$ sudo docker run hello-world
Unable to find image 'hello-world:latest' locally
latest: Pulling from library/hello-world
2db29710123e: Pull complete 
Digest: sha256:10d7d58d5ebd2a652f4d93fdd86da8f265f5318c6a73cc5b6a9798ff6d2b2e67
Status: Downloaded newer image for hello-world:latest

&lt;strong>Hello from Docker!
This message shows that your installation appears to be working correctly.

To generate this message, Docker took the following steps:
 1. The Docker client contacted the Docker daemon.
 2. The Docker daemon pulled the "hello-world" image from the Docker Hub.
    (amd64)
 3. The Docker daemon created a new container from that image which runs the
    executable that produces the output you are currently reading.
 4. The Docker daemon streamed that output to the Docker client, which sent it
    to your terminal.

To try something more ambitious, you can run an Ubuntu container with:
 $ docker run -it ubuntu bash&lt;/strong>

Share images, automate workflows, and more with a free Docker ID:
 https:&#47;&#47;hub.docker.com/

For more examples and ideas, visit:
 https://docs.docker.com/get-started/</code></pre>

We can try to install ubuntu like this **docker run -it ubuntu bash**

<pre class="wp-block-code"><code>$ sudo docker run -it ubuntu bash
Unable to find image 'ubuntu:latest' locally
latest: Pulling from library/ubuntu
125a6e411906: Pull complete 
Digest: sha256:26c68657ccce2cb0a31b330cb0be2b5e108d467f641c62e13ab40cbec258c68d
Status: Downloaded newer image for ubuntu:latest
root@c867239ffe6a:/# </code></pre>

You now have running ubuntu inside a docker container.

## Install Docker using a convenient script {.wp-block-heading}

Using this method is not recommended for the production environment. Use it only when doing some tests.

To download the script run the following:

<pre class="wp-block-code"><code>curl -fsSL https://get.docker.com -o get-docker.sh</code></pre>

Then execute the script with this command:

<pre class="wp-block-code"><code>sudo sh get-docker.sh</code></pre>

## Install Docker from the Package {.wp-block-heading}

To install Docker from the package, go to <a href="https://download.docker.com/linux/ubuntu/dists/" target="_blank" rel="noreferrer noopener">docker distros page</a> and chose Ubuntu distribution, then choose if you want stable, nightly or test version. 

Download the package 

<pre class="wp-block-code"><code>wget https://download.docker.com/linux/ubuntu/dists/jammy/stable/Contents-amd64</code></pre>

Run the installation using the following command:

<pre class="wp-block-code"><code>sudo dpkg -i /path/to/package.deb</code></pre>

## Uninstalling Docker {.wp-block-heading}

Whenever you want to uninstall docker from your machine, use the following command to completely remove the installation.

<pre class="wp-block-code"><code>sudo apt-get purge docker-ce docker-ce-cli containerd.io docker-compose-plugin</code></pre>

Then you need to delete all the images, volumes, and containers with these commands 

<pre class="wp-block-code"><code>$ sudo rm -rf /var/lib/docker
$ sudo rm -rf /var/lib/containerd</code></pre>

## Conclusion {.wp-block-heading}

From here you can see our Docker is running as expected, Learn other docker commands to be well conversant with docker. <a href="https://docs.docker.com/engine/install/" target="_blank" rel="noreferrer noopener">Docker Documentation </a>has a tone of information.

 [1]: https://nextgentips.com/2021/11/22/how-to-install-and-use-docker-ce-on-fedora-35/
 [2]: https://nextgentips.com/2021/09/30/how-to-install-docker-on-arch-linux/
 [3]: https://nextgentips.com/2021/11/27/how-to-install-docker-ce-on-ubuntu-21-10/
 [4]: https://nextgentips.com/2022/01/01/how-to-install-and-configure-docker-ce-on-ubuntu-20-04/