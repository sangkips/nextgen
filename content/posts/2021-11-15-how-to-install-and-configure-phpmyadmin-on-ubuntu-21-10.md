---
title: How to install and configure PhPMyAdmin on Ubuntu 21.10
author: Kipkoech Sang
type: post
date: 2021-11-15T19:01:39+00:00
url: /2021/11/15/how-to-install-and-configure-phpmyadmin-on-ubuntu-21-10/
ocean_gallery_link_images:
  - off
ocean_disable_margins:
  - enable
ocean_display_top_bar:
  - default
ocean_display_header:
  - default
ocean_disable_title:
  - default
ocean_disable_heading:
  - default
ocean_disable_breadcrumbs:
  - default
ocean_display_footer_widgets:
  - default
ocean_display_footer_bottom:
  - default
ocean_link_format_target:
  - self
ocean_quote_format_link:
  - post
entry_views:
  - 1
view_ip:
  - 'a:1:{i:0;s:14:"54.242.182.186";}'
rank_math_news_sitemap_robots:
  - index
rank_math_robots:
  - 'a:1:{i:0;s:5:"index";}'
rank_math_analytic_object_id:
  - 101
rank_math_internal_links_processed:
  - 1
categories:
  - Database

---
In this tutorial guide we will be learning how to install and configure PHPMyAdmin on Ubuntu 21.10 9 (Impish indri).

PHPMyAdmin is free and open source administration tool for MySQL and Mariadb database server. PHPMyAdmin assist users who are not well conversant with the command line because PHPMyAdmin is user friendly because of its user interface presence. Still you can perform database tasks such as creating users, running transactions, creating databases etc.

## Related Articles  {.wp-block-heading}

[How to Install Apache, MYSQL, PHP on Ubuntu 20.04][1]

[How to setup Ubuntu 20.04 Server for the first time][2]

## Prerequisites  {.wp-block-heading}

  1. Have a user with sudo privileges
  2. Ubuntu server up and running 
  3. Strong internet connections 

## Table of Contents  {.wp-block-heading}

  1. Update your system repositories 
  2. Install MySQL, Apache and PHP (LAMP)
  3. Install PHPMyAdmin
  4. Configure password for MySQL

## 1. Update System Repositories  {.wp-block-heading}

The first thing you must always do to any Linux operating system is to update its repositories to reflect on daily changes done to the system. Let us update ours and move on to the next step.

<pre class="wp-block-code"><code>$ sudo apt update
$ sudo apt upgrade -y</code></pre>

When both update and upgrade is complete, we can move to install LAMP stack on our system. Let&#8217;s begin

## 2. Install Linux, Apache, MySQL and PHP (LAMP) {.wp-block-heading}

LAMP stand for Linux, Apache, MySQL and PHP. It is installed together to help servers host dynamic websites written in PHP. 

### Install Apache2 {.wp-block-heading}

Let&#8217;s begin with Apache webserver. Use the following to install Apache and update our firewall rules to allow Http traffic pass.

<pre class="wp-block-code"><code>$ sudo apt install apache2 -y</code></pre>

We are installing Apache because PHPMyAdmin requires it to run its operations. When installation is complete then we can move to adjust our firewall settings to allow HTTP traffic accordingly. Let us check the status of our firewall with the following command;

<pre class="wp-block-code"><code>$ sudo ufw app list</code></pre>

You will see the following output 

<pre class="wp-block-code"><code># 
Available applications:
  Apache
  Apache Full
  Apache Secure
  OpenSSH</code></pre>

Allow only Apache traffic on port 80. Do the following in order to allow traffic from Apache.

<pre class="wp-block-code"><code>$ sudo ufw allow in 'Apache'</code></pre>

Now we can check the status to verify that indeed Apache had been allowed 

<pre class="wp-block-code"><code>$ sudo ufw status</code></pre>

I am getting the status that it is inactive, if in case you get this error please enable the ufw like this;

<pre class="wp-block-code"><code>$ ufw enable</code></pre>

Check the status again, this time round it will show the following on your terminal;

<pre class="wp-block-code"><code>$ Status: active

To                         Action      From
--                         ------      ----
Apache                     ALLOW       Anywhere                  
Apache (v6)                ALLOW       Anywhere (v6)             
</code></pre>

You can see that Apache is now allowed to pass traffic. Please check the status by tying the following on the browser.

<pre class="wp-block-code"><code># http://&lt;Your_IP_Address></code></pre>

If you get an Apache figure on your browser you are good to go.

### Install MySQL {.wp-block-heading}

Installing MySQL is straight forward, use the following code on your terminal;

<pre class="wp-block-code"><code>$ sudo apt install mysql-server</code></pre>

When installation is complete do secure mysql with the folllowing code;

<pre class="wp-block-code"><code>$ sudo mysql_secure_installation</code></pre>

This will ask you to confirm different prompts. Confirm to be sure that MySQL is installed;

<pre class="wp-block-code"><code>$ sudo mysql</code></pre>

### Installing PHP {.wp-block-heading}

To run PHP it depends on different dependencies like **php-mysql** for connection with mysql database and **libapache2-mod-php**. To install this dependencies we do the following;

<pre class="wp-block-code"><code>$ sudo apt install php libapache2-mod-php php-mysql</code></pre>

Confirm if indeed PHP installation was successful with the following command;

<pre class="wp-block-code"><code>php --version</code></pre>

You will get the following output.

<pre class="wp-block-code"><code>PHP 8.0.8 (cli) (built: Oct 26 2021 11:42:42) ( NTS )
Copyright (c) The PHP Group
Zend Engine v4.0.8, Copyright (c) Zend Technologies
    with Zend OPcache v8.0.8, Copyright (c), by Zend Technologies</code></pre>

Having completed the following we need now to install PHPMyAdmin

## 3. Install PHPMyAdmin  {.wp-block-heading}

We can install PHPMyAdmin now but first you need to install the following dependencies;

<pre class="wp-block-code"><code>$ sudo apt install phpmyadmin php-mbstring php-zip php-gd php-json php-curl</code></pre>

Press Y to allow installation to continue. Select Apache and press enter<figure class="wp-block-image size-large">

<img decoding="async" loading="lazy" width="810" height="583" src="https://i0.wp.com/nextgentips.com/wp-content/uploads/2021/11/Screenshot-from-2021-11-15-21-37-52.png?resize=810%2C583&#038;ssl=1" alt="PHPMyAdmin configuration" class="wp-image-605" srcset="https://i0.wp.com/nextgentips.com/wp-content/uploads/2021/11/Screenshot-from-2021-11-15-21-37-52.png?resize=1024%2C737&ssl=1 1024w, https://i0.wp.com/nextgentips.com/wp-content/uploads/2021/11/Screenshot-from-2021-11-15-21-37-52.png?resize=300%2C216&ssl=1 300w, https://i0.wp.com/nextgentips.com/wp-content/uploads/2021/11/Screenshot-from-2021-11-15-21-37-52.png?resize=768%2C553&ssl=1 768w, https://i0.wp.com/nextgentips.com/wp-content/uploads/2021/11/Screenshot-from-2021-11-15-21-37-52.png?w=1054&ssl=1 1054w" sizes="(max-width: 810px) 100vw, 810px" data-recalc-dims="1" /> <figcaption>PHPMyAdmin configuration</figcaption></figure> 

Next choose password for your phpmyadmin and press enter.

After installation is complete you can restart the Apache2 server for the changes to take place.

<pre class="wp-block-code"><code>$ sudo systemctl restart apache2</code></pre>

Lastly check your installation if it is going through with the following command;

<pre class="wp-block-code"><code># http://&lt;Your_IP_Address>/phpmyadmin</code></pre>

## Conclusion {.wp-block-heading}

Congratulation you have installed PHPMyAdmin on your system. You can now continue administering database with the help of user interface. Check [PHPmyAdmin documentation][3] for further information.

 [1]: https://nextgentips.com/2021/10/11/how-to-install-apache-mysql-php-on-ubuntu-20-04/
 [2]: https://nextgentips.com/2021/10/10/initial-server-setup-for-ubuntu-20-04/
 [3]: https://www.phpmyadmin.net/