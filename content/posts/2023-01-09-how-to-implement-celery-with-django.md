---
title: How to implement Celery with Django
author: Kipkoech Sang
type: post
date: 2023-01-09T14:45:49+00:00
url: /2023/01/09/how-to-implement-celery-with-django/
categories:
  - Linux

---
### What is Celery? {.wp-block-heading}

Perhaps you might be wondering like me what the heck is Celery? Let&#8217;s begin by first understanding what Celery is. Celery is an open-source asynchronous **task queue** that is based on distributed message passing. And what is a task queue? A task queue&#8217;s input is a unit of work called a task. They are used as a mechanism to distribute work across threads or machines.

Celery communicates via messages using a broker such as Rabbitmq, or Redis. Broker mediates between clients and workers. A client initiates a task by adding a message to the queue then the broker delivers that message to the worker.

### Why Celery? {.wp-block-heading}

We use Celery because of what it offers to the end users. People don&#8217;t have to wait for a transaction to complete but instead, Celery will take care of all the background tasks without you noticing the delays that the system might be experiencing. Here are a few reasons for using Celery:

  * It&#8217;s simple. Celery doesn&#8217;t need configuration files, and therefore, becomes easy to use and maintain.
  * It&#8217;s highly available, in case of failure it will automatically restart
  * It&#8217;s flexible, and every part of Celery can be extended or used on its own such as extending the serializer, longing, broker transports, etc.
  * It&#8217;s very fast and can process a million tasks within minutes.
  * It supports concurrency such as multithreading, single threading, or multiprocessing.
  * It supports serialization and brokers such as <a href="https://www.rabbitmq.com/" target="_blank" rel="noopener" title="">RabbitMQ</a>, <a href="https://redis.io/" target="_blank" rel="noopener" title="">Redis</a>, <a href="https://aws.amazon.com/sqs/" target="_blank" rel="noopener" title="">Amazon SQS</a>

### How to implement Celery with Django application {.wp-block-heading}

To begin implementing Celery in Django application. we first need to install Celery and RabbitMQ or Redis as brokers. So let&#8217;s begin.

In this example, I will be using Redis as a message broker and also as a database backend.

We are going to use the docker container to run our application because I love to do things inside a docker container.

To start building, create a skeleton project, and add Redis, celery, Django, pytest-django, pytest to the requirements.txt file.

<pre class="wp-block-code"><code>django==4.1
celery==5.2.7
redis==4.4.0
gunicorn==20.1.0
whitenoise==6.2.0
pytest-django==4.5.2
black==22.12.0
pytest==7.2.0</code></pre>

The second thing is to create a Dockerfile and docker-compose file respectively. Add the following content to Dockerfile.

<pre class="wp-block-code"><code># Python base image. Get this from dockerhub
FROM python:3.11-slim

# Set environment variables
ENV PYTHONUNBUFFERED=1

# Set your working directory
WORKDIR /usr/src/app

# Install dependencies required by the image
RUN apt update && apt install -y g++ libpq-dev gcc musl-dev

# Allow docker to cache installed dependencies between builds 
COPY requirements.txt .
RUN python3 -m pip install -r requirements.txt --no-cache-dir

# Copy and mount the project to the working directory
COPY . .

# Script to run given instruction eg running production server.
CMD &#91;"./run.sh"]</code></pre>

Also, add these contents to the docker-compose file

<pre class="wp-block-code"><code>#docker-compose.yml
version: '3.9'

services:
  web:
    build: .
    command: python manage.py runserver 0.0.0.0:8000
    volumes:
      - .:/usr/src/app/
    ports:
      - 1337:8000
    environment:
      - DEBUG=1
      - SECRET_KEY=mySecretkey456
      - DJANGO_ALLOWED_HOSTS=localhost 127.0.0.1 &#91;::1]
      - CELERY_BROKER=redis://redis:6379/0
      - CELERY_BACKEND=redis://redis:6379/0
    depends_on:
      - redis

  celery:
    build: .
    command: celery --app=core worker --loglevel=info --logfile=logs/celery.log
    volumes:
      - .:/usr/src/app
    environment:
      - DEBUG=1
      - SECRET_KEY=mySecretkey123
      - DJANGO_ALLOWED_HOSTS=localhost 127.0.0.1 &#91;::1]
      - CELERY_BROKER=redis://redis:6379/0
      - CELERY_BACKEND=redis://redis:6379/0
    depends_on:
      - web
      - redis

  redis:
    image: redis:7-alpine</code></pre>

The third thing to do is to create a Django project, let&#8217;s call our project main. Use the following code

<pre class="wp-block-code"><code>django-admin startapp main .</code></pre>

The fourth thin to do is to create a script that can run in order to collect static and run the Gunicorn server. create a `run.sh` and place the following code.

<pre class="wp-block-code"><code>#!/bin/bash

python manage.py collectstatic --no-input

exec gunicorn --bind 0.0.0.0:8000 main.wsgi:application -w 2</code></pre>

The fifth thing to do is to create a Django app, called review 

<pre class="wp-block-code"><code>docker-compose run web /usr/local/bin/django-admin startapp review</code></pre>

You can add tests to our project, I am going to create pytest for this project. Create a tests folder in our app folder and add pytest, pytest-django to the requirements. Then create a `pytest.ini` file and add the following content.

<pre class="wp-block-code"><code>&#91;pytest]
DJANGO_SETTINGS_MODULE = main.settings

python_files = tests.py test_*.py *_tests.py</code></pre>

The last thing here is to power up our container image. To run use `<mark style="background-color:#abb8c3" class="has-inline-color">docker-compose up --build</mark>`

<pre class="wp-block-code"><code>docker-compose up --build</code></pre>

This will pull the Redis image and build our python 3.11 image. There is so much going on here. To learn more about Docker check this <a href="https://www.docker.com/101-tutorial/" target="_blank" rel="noopener" title="">Docker 101 tutorial</a>

### Adding and configuring Celery inside Django {.wp-block-heading}

When the Django project setup is over, we can move ahead and add Celery-related settings to the Django project, this will create a connection between the Django app and the task queue.

So inside our Django project create a file and name it `<mark style="background-color:#abb8c3" class="has-inline-color">celery.py</mark>`

<pre class="wp-block-code"><code>import os

from celery import Celery


os.environ.setdefault("DJANGO_SETTINGS_MODULE", "main.settings")
app = Celery("main")
app.config_from_object("django.conf:settings", namespace="CELERY") 
app.autodiscover_tasks()// </code></pre>

  * The first thing we did here is to let Celery know how to find the Django project with the help of `<mark style="background-color:#abb8c3" class="has-inline-color">DJANGO_SETTINGS_MODULE", "main.settings</mark>`
  * The second thing is to create a Celery instance and instantiate it with an app.
  * The third thing is to make sure that we prevent any crush from happening with the use of `<mark style="background-color:#abb8c3" class="has-inline-color">namespace="CELERY"</mark>`
  * The last thing is to tell celery to look for all celery-related tasks.

For celery.py to start fetching the necessary Celery settings, we need to tell Celery about Redis as a broker and as a db backend. Add the following code to the bottom of the `settings.py`.

<pre class="wp-block-code"><code>CELERY_BROKER_URL = os.environ.get("CELERY_BROKER", "redis://redis:6379/0")
CELERY_RESULT_BACKEND = os.environ.get("CELERY_BROKER", "redis://redis:6379/0")</code></pre>

The next thing to update `__init__.py` in our main Django project. add the following code

<pre class="wp-block-code"><code>from .celery import app as celery_app

__all__ = ('celery_app',)</code></pre>

This code will make sure the app is always imported when Django starts so that shared_task will use this app.

Now you can then power up your container and you will see that celery is up and running with a few warnings which I will optimize later.<figure class="wp-block-image size-large">

<img decoding="async" loading="lazy" width="810" height="403" src="https://i0.wp.com/nextgentips.com/wp-content/uploads/2023/01/Screenshot-from-2023-01-09-17-05-48.png?resize=810%2C403&#038;ssl=1" alt="Nextgentips: Celery status" class="wp-image-1706" srcset="https://i0.wp.com/nextgentips.com/wp-content/uploads/2023/01/Screenshot-from-2023-01-09-17-05-48.png?resize=1024%2C510&ssl=1 1024w, https://i0.wp.com/nextgentips.com/wp-content/uploads/2023/01/Screenshot-from-2023-01-09-17-05-48.png?resize=300%2C150&ssl=1 300w, https://i0.wp.com/nextgentips.com/wp-content/uploads/2023/01/Screenshot-from-2023-01-09-17-05-48.png?resize=768%2C383&ssl=1 768w, https://i0.wp.com/nextgentips.com/wp-content/uploads/2023/01/Screenshot-from-2023-01-09-17-05-48.png?w=1346&ssl=1 1346w" sizes="(max-width: 810px) 100vw, 810px" data-recalc-dims="1" /> <figcaption class="wp-element-caption">Nextgentips: Celery status</figcaption></figure> 

That is it for the Celery configuration, in the next post I will show you how to handle workloads Asynchronously and start writing tests for our project. For now feel free to shoot any issue related to Celery, Redis, and also RabbitMQ. Get the code <a href="https://github.com/sangkips/CeleryDjango" target="_blank" rel="noopener" title="">here</a>