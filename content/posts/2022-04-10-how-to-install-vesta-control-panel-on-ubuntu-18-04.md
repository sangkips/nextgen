---
title: How to install Vesta Control Panel on Ubuntu 18.04
author: Kipkoech Sang
type: post
date: 2022-04-10T07:19:49+00:00
url: /2022/04/10/how-to-install-vesta-control-panel-on-ubuntu-18-04/
rank_math_seo_score:
  - 68
rank_math_focus_keyword:
  - vesta control panel
rank_math_internal_links_processed:
  - 1
rank_math_analytic_object_id:
  - 164
rank_math_primary_category:
  - 6
categories:
  - Linux

---
Vesta Control Panel is a simple and clever web hosting control panel. It has the fine touch of Softaculus auto installer that is able to install more than 439 apps with one click. It also supports a great ton of features such as a built-in command-line interface, and Softaculus which you can focus on using the apps rather than installing. Has support for commercial plugins such as SFTP Chroot.

In this tutorial, I will take you through the installation and configuration of the Vesta control panel step by step. 

## Why Vesta Control Panel {.wp-block-heading}

Maybe you are wondering why Vesta control panel and not others. Vesta offers the following advantages.

  * Vesta web interface is extremely first couple with its first keyboard shortcuts.
  * Sites powered by vesta are fast because vesta delivers optimized configurations for low, medium, and high RAM server types. 
  * Vesta requires low resources and still runs fast. 512 Mb RAM, 20Gb hard disk, and 1Ghz CPU are enough to power Vesta resources.
  * It has a built-in firewall that resolves all common issues.
  * It has supported internationalization. It supports more than 25 languages and regions around the globe.
  * It provides monitoring and logging of server requests.
  * It has an out-of-the-box backup system implemented by daily cron jobs.

## Install Vesta Control Panel on Ubuntu 18.04 {.wp-block-heading}

### 1. Connect to your server {.wp-block-heading}

First, we need to connect to our Ubuntu server via ssh console like this 

<pre class="wp-block-code"><code>ssh root@&lt;Ip-Address&gt;</code></pre>

### 2. Update system repositories.  {.wp-block-heading}

Secondly, we need to update our system repositories in order to make them up to date. This will ensure that we don&#8217;t run into errors later during our installation.

<pre class="wp-block-code"><code>sudo apt update && apt upgrade -y</code></pre>

### 3. Download Vesta Control Panel {.wp-block-heading}

We are going to use the vesta installation script to run our installation. This is a very convenient method as all the vesta requirements are included in the script.

<pre class="wp-block-code"><code>sudo curl -O http://vestacp.com/pub/vst-install.sh</code></pre>

Then to run the script in order to install Vesta use the following command.

<pre class="wp-block-code"><code>&lt;strong>bash vst-install.sh&lt;/strong>

Error: group admin exists

Please remove admin group before proceeding.
If you want to do it automatically run installer with -f option:
Example: bash vst-install.sh --force</code></pre>

If the installation fails for some reason like the one shown above, you can do &#8211;force installation like this.

<pre class="wp-block-code"><code>bash vst-install.sh --force </code></pre>

You will get an output like this:

<pre class="wp-block-code"><code>#output
_|      _|  _|_|_|_|    _|_|_|  _|_|_|_|_|    _|_|
 _|      _|  _|        _|            _|      _|    _|
 _|      _|  _|_|_|      _|_|        _|      _|_|_|_|
   _|  _|    _|              _|      _|      _|    _|
     _|      _|_|_|_|  _|_|_|        _|      _|    _|

                                  Vesta Control Panel



The following software will be installed on your system:
   - Nginx Web Server
   - Apache Web Server (as backend)
   - Bind DNS Server
   - Exim Mail Server
   - Dovecot POP3/IMAP Server
   - MySQL Database Server
   - Vsftpd FTP Server
   - Softaculous Plugin
   - Iptables Firewall + Fail2Ban


Would you like to continue &#91;y/n]: </code></pre>

Press Y to allow the installation to continue. You need to provide your admin email and FQDN for the installation to continue.

<pre class="wp-block-code"><code>&lt;strong>Please enter admin email address: next@gmail.com
Please enter Vesta port number (press enter for 8083): 
Please enter FQDN hostname &#91;ubuntu]: next-example.com
Installation backup directory: /root/vst_install_backups/1649527203
&lt;/strong>



Installation will take about 15 minutes ...

Reading package lists... Done
Building dependency tree       
Reading state information... Done
Calculating upgrade... Done
The following package was automatically installed and is no longer required:
  libfwupdplugin1
Use 'apt autoremove' to remove it.
0 upgraded, 0 newly installed, 0 to remove and 0 not upgraded.
--2022-04-09 18:00:09--  http://nginx.org/keys/nginx_signing.key
Resolving nginx.org (nginx.org)... 52.58.199.22, 3.125.197.172, 2a05:d014:edb:5702::6, ...
Connecting to nginx.org (nginx.org)|52.58.199.22|:80... connected.
HTTP request sent, awaiting response... 200 OK
Length: 1561 (1.5K) &#91;application/octet-stream]
Saving to: ‘/tmp/nginx_signing.key’</code></pre>

when don with installation visit **http://<ip-address>/8083** on your terminal to access its interface. Use the credentials given.

<pre class="wp-block-code"><code>Thank you for using Softaculous
=======================================================

 _|      _|  _|_|_|_|    _|_|_|  _|_|_|_|_|    _|_|   
 _|      _|  _|        _|            _|      _|    _| 
 _|      _|  _|_|_|      _|_|        _|      _|_|_|_| 
   _|  _|    _|              _|      _|      _|    _| 
     _|      _|_|_|_|  _|_|_|        _|      _|    _| 


Congratulations, you have just successfully installed Vesta Control Panel

    https:&#47;&#47;138.68.177.91:8083
    username: admin
    password: knCoRJpPqb

We hope that you enjoy your installation of Vesta. Please feel free to contact us anytime if you have any questions.
Thank you.

--
Sincerely yours
vestacp.com team</code></pre><figure class="wp-block-image size-full">

<img decoding="async" loading="lazy" width="807" height="582" src="https://i0.wp.com/nextgentips.com/wp-content/uploads/2022/04/Screenshot-from-2022-04-10-10-11-14-1.png?resize=807%2C582&#038;ssl=1" alt="Vesta CP login" class="wp-image-1289" srcset="https://i0.wp.com/nextgentips.com/wp-content/uploads/2022/04/Screenshot-from-2022-04-10-10-11-14-1.png?w=807&ssl=1 807w, https://i0.wp.com/nextgentips.com/wp-content/uploads/2022/04/Screenshot-from-2022-04-10-10-11-14-1.png?resize=300%2C216&ssl=1 300w, https://i0.wp.com/nextgentips.com/wp-content/uploads/2022/04/Screenshot-from-2022-04-10-10-11-14-1.png?resize=768%2C554&ssl=1 768w" sizes="(max-width: 807px) 100vw, 807px" data-recalc-dims="1" /> <figcaption>Vesta CP login</figcaption></figure> <figure class="wp-block-image size-large is-resized"><img decoding="async" loading="lazy" src="https://i0.wp.com/nextgentips.com/wp-content/uploads/2022/04/Screenshot-from-2022-04-10-10-11-41.png?resize=810%2C463&#038;ssl=1" alt="Vesta CP Dashboard" class="wp-image-1290" width="810" height="463" srcset="https://i0.wp.com/nextgentips.com/wp-content/uploads/2022/04/Screenshot-from-2022-04-10-10-11-41.png?resize=1024%2C586&ssl=1 1024w, https://i0.wp.com/nextgentips.com/wp-content/uploads/2022/04/Screenshot-from-2022-04-10-10-11-41.png?resize=300%2C172&ssl=1 300w, https://i0.wp.com/nextgentips.com/wp-content/uploads/2022/04/Screenshot-from-2022-04-10-10-11-41.png?resize=768%2C439&ssl=1 768w, https://i0.wp.com/nextgentips.com/wp-content/uploads/2022/04/Screenshot-from-2022-04-10-10-11-41.png?w=1080&ssl=1 1080w" sizes="(max-width: 810px) 100vw, 810px" data-recalc-dims="1" /><figcaption>Vesta CP Dashboard</figcaption></figure> 

### 4. Uninstall Vesta Control Panel {.wp-block-heading}

When you want to uninstall Vesta CP the first thing to do is to stop vesta services like this;

<pre class="wp-block-code"><code>service vesta stop</code></pre>

After you have stopped the service then proceed to remove the installation like this:

<pre class="wp-block-code"><code>sudo apt remove vesta*
rm -f /etc/apt/sources.list.d/vesta.list</code></pre>

Lastly, remove the data directory and its associated cron jobs.

<pre class="wp-block-code"><code>rm -rf /usr/local/vesta</code></pre>