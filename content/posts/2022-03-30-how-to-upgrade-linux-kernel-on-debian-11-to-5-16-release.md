---
title: How to upgrade Linux Kernel on Debian 11 to 5.16 Release
author: Kipkoech Sang
type: post
date: 2022-03-30T10:03:22+00:00
url: /2022/03/30/how-to-upgrade-linux-kernel-on-debian-11-to-5-16-release/
rank_math_seo_score:
  - 12
rank_math_internal_links_processed:
  - 1
rank_math_primary_category:
  - 6
rank_math_analytic_object_id:
  - 162
categories:
  - Linux

---
In this tutorial, we are going to learn how to upgrade Linux Kernel to 5.16 mainline release on Debian 11.

<a href="https://www.kernel.org/" target="_blank" rel="noreferrer noopener">Linux Kernel</a>&nbsp;is a&nbsp;**free and open-source, monolithic, modular, multitasking Unix-like operating system**.&nbsp;**It is the main component of a Linux operating system and is the core interface between the computer’s hardware and its processes. It makes communication possible between computer hardware and processes running on it and it manages resources effectively.**

Linux 5.17 mainline was released recently by&nbsp;**Linux Torvalds**&nbsp;with better new features to try out. The mainline tree is maintained by Linus Torvalds and It is where all new features are added and releases always come from.

## Notable features on Linux Kernel 5.16 {.wp-block-heading}

  * Introduction of new AMD P-state subsystem for future AMD CPUs that provide a performance boost.
  * Introduction of page-table check feature to better protect the GNU/Linux system from threats.
  * It introduces boot time memtest memory tester to the mk68k architecture.
  * introduces an alternative way to implement loops in BPF programs.
  * It adds support for tracking forced-idle time to the core scheduling feature and adds support to offloading traffic-control actions on network devices.

## Upgrade Linux Kernel to 5.16 release {#upgrade-linux-kernel-to-5-16-release.wp-block-heading}

### 1. Run system update {#1-run-system-update.wp-block-heading}

The first thing to do is to run system updates on our Debian 11 server. Use the following command on your terminal.

<pre class="wp-block-code"><code>$ sudo apt update && apt upgrade -y</code></pre>

When upgrades and updates are complete, we can now begin to download headers, modules, and images.

Before we can proceed let&#8217;s check the Linux kernel we are having with the following command.

<pre class="wp-block-code"><code>uname -r
5.10.0-11-amd64</code></pre>

The best way to upgrade Linux Kernel in Debian is to install one of the supplied Linux kernel from the apt repository.

To check the available Linux Kernel on your repository, use the following command.

<pre class="wp-block-code"><code>sudo apt-cache search linux-image</code></pre>

Then you will get the following output.

<pre class="wp-block-code"><code># Sample output
linux-image-5.15.0-0.bpo.3-rt-amd64 - Linux 5.15 for 64-bit PCs, PREEMPT_RT (signed)
linux-image-5.16.0-0.bpo.3-amd64 - Linux 5.16 for 64-bit PCs (signed)
linux-image-5.16.0-0.bpo.3-cloud-amd64 - Linux 5.16 for x86-64 cloud (signed)
linux-image-5.16.0-0.bpo.3-rt-amd64 - Linux 5.16 for 64-bit PCs, PREEMPT_RT (signed)
linux-image-5.16.0-0.bpo.4-amd64 - Linux 5.16 for 64-bit PCs (signed)
linux-image-5.16.0-0.bpo.4-cloud-amd64 - Linux 5.16 for x86-64 cloud (signed)
linux-image-5.16.0-0.bpo.4-rt-amd64 - Linux 5.16 for 64-bit PCs, PREEMPT_RT (signed)
&lt;strong>linux-image-unsigned-5.17.0-051700-generic - Linux kernel image for version 5.17.0 on 64 bit x86 SMP&lt;/strong></code></pre>

We are going to install Linux kernel image 5.17 unsigned.

### 2. Install Linux Kernel 5.16 on Debian 11 Bullseye. {.wp-block-heading}

Before running the installation, make sure you read all the prompts during installation so that you can not run into problems later on. 

To install the image run the following command.

<pre class="wp-block-code"><code>sudo apt install linux-image-5.16.0-0.bpo.3-amd64</code></pre>

See the sample output below.

<pre class="wp-block-code"><code># output
Reading package lists... Done
Building dependency tree... Done
Reading state information... Done
Suggested packages:
  linux-doc-5.16 debian-kernel-handbook grub-pc | grub-efi-amd64 | extlinux
The following NEW packages will be installed:
  linux-image-5.16.0-0.bpo.3-amd64
0 upgraded, 1 newly installed, 0 to remove and 0 not upgraded.
Need to get 67.9 MB of archives.
After this operation, 452 MB of additional disk space will be used.
Get:1 http://deb.debian.org/debian bullseye-backports/main amd64 linux-image-5.16.0-0.bpo.3-amd64 amd64 5.16.11-1~bpo11+1 &#91;67.9 MB]
Fetched 67.9 MB in 1s (123 MB/s)                             
Selecting previously unselected package linux-image-5.16.0-0.bpo.3-amd64.
(Reading database ... 29865 files and directories currently installed.)
Preparing to unpack .../linux-image-5.16.0-0.bpo.3-amd64_5.16.11-1~bpo11+1_amd64.deb ...
Unpacking linux-image-5.16.0-0.bpo.3-amd64 (5.16.11-1~bpo11+1) ...
Setting up linux-image-5.16.0-0.bpo.3-amd64 (5.16.11-1~bpo11+1) ...
/etc/kernel/postinst.d/initramfs-tools:
update-initramfs: Generating /boot/initrd.img-5.16.0-0.bpo.3-amd64
/etc/kernel/postinst.d/zz-update-grub:
Generating grub configuration file ...
Found linux image: /boot/vmlinuz-5.16.0-0.bpo.3-amd64
Found initrd image: /boot/initrd.img-5.16.0-0.bpo.3-amd64
Found linux image: /boot/vmlinuz-5.10.0-13-amd64
Found initrd image: /boot/initrd.img-5.10.0-13-amd64
Found linux image: /boot/vmlinuz-5.10.0-11-amd64
Found initrd image: /boot/initrd.img-5.10.0-11-amd64
done</code></pre>

Then you need to reboot your system for the changes to take effect.

<pre class="wp-block-code"><code>sudo reboot -n</code></pre>

Now you can check the Linux Kernel again

<pre class="wp-block-code"><code>uname -r
5.16.0-0.bpo.3-amd64</code></pre>

## Conclusion {.wp-block-heading}

Congratulations you have successfully upgraded your Debian kernel to 5.16.0-0.bpo.3-amd64 release.