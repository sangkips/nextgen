---
title: How to Install PHP 8 on Ubuntu 21.04
author: Kipkoech Sang
type: post
date: 2021-10-19T12:41:32+00:00
url: /2021/10/19/how-to-install-php-8-on-ubuntu-21-04/
ocean_gallery_link_images:
  - off
ocean_disable_margins:
  - enable
ocean_display_top_bar:
  - default
ocean_display_header:
  - default
ocean_disable_title:
  - default
ocean_disable_heading:
  - default
ocean_disable_breadcrumbs:
  - default
ocean_display_footer_widgets:
  - default
ocean_display_footer_bottom:
  - default
ocean_link_format_target:
  - self
ocean_quote_format_link:
  - post
rank_math_news_sitemap_robots:
  - index
rank_math_robots:
  - 'a:1:{i:0;s:5:"index";}'
rank_math_analytic_object_id:
  - 131
post_view:
  - 1
is_post_view:
  - 'a:1:{i:0;s:27:"74.125.150.10, 99.82.171.41";}'
rank_math_internal_links_processed:
  - 1
categories:
  - Database

---
In this article we are going to learn how to install PHP 8 on Ubuntu 21.04. Are you wondering what PHP stands for? Worry no more, PHP is an acronym which stands for Hypertext Preprocessor. PHP is a general purpose scripting language suitable for web development. Most websites out there are programmed using PHP language because it is: 

  * Fast. PHP is fast because it runs on its own memory space. Also PHP has Zend engine that parses PHP code and turns it into opcodes which are then interpreted.
  * Flexible. PHP is flexible because it allows to almost all databases. MySQL being the de-facto database to connect to because it is open source. 
  * Free and open source 
  * PHP is forgiving. Why do we say PHP is forgiving? It is forgiving meaning its learning it is not so hard therefore suitable to almost all programmers starting out.
  * PHP supports major communication protocols i.e LDAP, IMAP, POP3.

## Prerequisites for Installing PHP {.wp-block-heading}

  * Before you can use PHP you must have web server installed. Apache web server is the most widely used with PHP.
  * Database must be there for PHP to run. It works well on all databases such Oracle, MySQL, PostgreSQL.
  * PHP parser must be there. What is PHP parser if you can ask? PHP Parser is a library that takes a source code written in PHP, passes it through a lexical analyzer and creates its respective syntax tree. 

So back to our PHP 8 installation. PHP has evolved since its inception from PHP3 to now PHP8. Along the way many features have been added and through this we are still seeing its evolution. 

PHP 8.0 is a major update release version. It contains many new features and optimizations e.g

  * Named arguments. Named arguments allow you to pass input data into a function based on their argument name instead of the argument order. You only specify required parameters skipping optional ones.
  * Union types. Union types accepts values of multiple data types rather than a single one. 
  * Attributes also know as annotations is to add meta data to classes, methods, variables in a structured way.
  * Constructor property promotion helps to reduce a lot of boilerplate code while constructing simple objects. 
  * Null safe operators provides operational chaining feature. While evaluating the elements, if one chain element fails then the entire chain of execution will fail. 
  * JIT provides better performance for numerical calculations.
  * Error handling gives a way of showing where error occurs in the code. Whenever there is an error in the code its just handled by giving type of error message.

## Installing PHP 8 {.wp-block-heading}

We are going to use Ondrej sury PPA repository to install PHP 8 on our system. To install a package from Personal package archive you need to tell Ubuntu where to find the PPA. 

First let us update our system with the following command:

<pre class="wp-block-code"><code>$ sudo apt update</code></pre>

After update is complete we can now add our PPA repository to our Ubuntu system with the following command:

<pre class="wp-block-code"><code>$ sudo add-apt-repository ppa:ondrej/php</code></pre>

Press Enter when prompted to allow installation of the PPA package from Ondrej/php.

<pre class="wp-block-code"><code>Output
$ sudo add-apt-repository ppa:ondrej/php
&#91;sudo] password for nextgentips: 
 Co-installable PHP versions: PHP 5.6, PHP 7.x and most requested extensions are included. Only Supported Versions of PHP (http://php.net/supported-versions.php) for Supported Ubuntu Releases (https://wiki.ubuntu.com/Releases) are provided. Don't ask for end-of-life PHP versions or Ubuntu release, they won't be provided.

Debian oldstable and stable packages are provided as well: https://deb.sury.org/#debian-dpa

You can get more information about the packages at https://deb.sury.org

IMPORTANT: The &lt;foo&gt;-backports is now required on older Ubuntu releases.

BUGS&FEATURES: This PPA now has a issue tracker:
https:&#47;&#47;deb.sury.org/#bug-reporting

CAVEATS:
1. If you are using php-gearman, you need to add ppa:ondrej/pkg-gearman
2. If you are using apache2, you are advised to add ppa:ondrej/apache2
3. If you are using nginx, you are advised to add ppa:ondrej/nginx-mainline
&nbsp;&nbsp;&nbsp;or ppa:ondrej/nginx

PLEASE READ: If you like my work and want to give me a little motivation, please consider donating regularly: https://donate.sury.org/

WARNING: add-apt-repository is broken with non-UTF-8 locales, see
https://github.com/oerdnj/deb.sury.org/issues/56 for workaround:

# LC_ALL=C.UTF-8 add-apt-repository ppa:ondrej/php
 More info: https://launchpad.net/~ondrej/+archive/ubuntu/php
Press &#91;ENTER] to continue or Ctrl-c to cancel adding it.
</code></pre>

When its complete it should show the following screenshot

<pre class="wp-block-code"><code>Hit:1 http://dl.google.com/linux/chrome/deb stable InRelease                   
Get:2 http://ppa.launchpad.net/ondrej/php/ubuntu focal InRelease &#91;23.9 kB]     
Hit:3 http://ke.archive.ubuntu.com/ubuntu focal InRelease                      
Get:4 http://ke.archive.ubuntu.com/ubuntu focal-updates InRelease &#91;114 kB]     
Get:5 http://ke.archive.ubuntu.com/ubuntu focal-backports InRelease &#91;101 kB]   
Get:6 http://security.ubuntu.com/ubuntu focal-security InRelease &#91;114 kB]      
Get:7 http://ke.archive.ubuntu.com/ubuntu focal-updates/main amd64 Packages &#91;1,294 kB]
Get:8 http://ke.archive.ubuntu.com/ubuntu focal-updates/main i386 Packages &#91;550 kB]
Get:9 http://ke.archive.ubuntu.com/ubuntu focal-updates/main amd64 DEP-11 Metadata &#91;283 kB]
Get:10 http://ke.archive.ubuntu.com/ubuntu focal-updates/restricted amd64 Packages &#91;522 kB]
Get:11 http://ke.archive.ubuntu.com/ubuntu focal-updates/restricted i386 Packages &#91;19.4 kB]
Get:12 http://ke.archive.ubuntu.com/ubuntu focal-updates/universe i386 Packages &#91;642 kB]
Get:13 http://ke.archive.ubuntu.com/ubuntu focal-updates/universe amd64 Packages &#91;867 kB]
Get:14 http://ke.archive.ubuntu.com/ubuntu focal-updates/universe amd64 DEP-11 Metadata &#91;362 kB]
Get:15 http://ke.archive.ubuntu.com/ubuntu focal-updates/multiverse amd64 DEP-11 Metadata &#91;940 B]
Get:16 http://ke.archive.ubuntu.com/ubuntu focal-backports/universe amd64 DEP-11 Metadata &#91;10.4 kB]
Hit:17 https://apache.jfrog.io/artifactory/couchdb-deb focal InRelease         
Get:18 http://security.ubuntu.com/ubuntu focal-security/main amd64 DEP-11 Metadata &#91;29.0 kB]
Get:19 http://security.ubuntu.com/ubuntu focal-security/universe amd64 DEP-11 Metadata &#91;63.5 kB]
Get:20 http://security.ubuntu.com/ubuntu focal-security/multiverse amd64 DEP-11 Metadata &#91;2,464 B]
Get:21 http://ppa.launchpad.net/ondrej/php/ubuntu focal/main amd64 Packages &#91;96.0 kB]
Get:22 http://ppa.launchpad.net/ondrej/php/ubuntu focal/main i386 Packages &#91;33.8 kB]
Fetched 5,128 kB in 7s (687 kB/s)                                              
Reading package lists... Done
</code></pre>

Update the system when complete for the changes to take effect.

Now we can install PHP 8.0 after all updates are complete. Use the following command to install PHP 8.

<pre class="wp-block-code"><code>$ sudo apt install php8.0</code></pre>

<pre class="wp-block-code"><code>$ sudo apt update</code></pre>

After installation is complete we can confirm the PHP version to confirm if PHP 8 was actually installed.

<pre class="wp-block-code"><code>$ php -v</code></pre>

<pre class="wp-block-code"><code>output
$ php -v
PHP 8.0.11 (cli) (built: Sep 23 2021 21:26:24) ( NTS )
Copyright (c) The PHP Group
Zend Engine v4.0.11, Copyright (c) Zend Technologies
    with Zend OPcache v8.0.11, Copyright (c), by Zend Technologies</code></pre>

## Conclusion {.wp-block-heading}

We have learned how to install the current version of PHP 8 on our Ubuntu 21.04, next you can check more on the [Documentation][1] for detailed features of PHP 8. Read more about PHP 8 PPA [here][2].

 [1]: https://www.php.net/releases/8.0/en.php
 [2]: https://launchpad.net/~ondrej/+archive/ubuntu/php