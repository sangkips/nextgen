---
title: How to Install OpenLiteSpeed on Rocky Linux/AlmaLinux
author: Kipkoech Sang
type: post
date: 2021-11-12T14:06:29+00:00
url: /2021/11/12/how-to-install-openlitespeed-on-rocky-linux-almalinux/
apvc_active_counter:
  - Yes
count_start_from:
  - 1
ocean_gallery_link_images:
  - off
ocean_disable_margins:
  - enable
ocean_display_top_bar:
  - default
ocean_display_header:
  - default
ocean_disable_title:
  - default
ocean_disable_heading:
  - default
ocean_disable_breadcrumbs:
  - default
ocean_display_footer_widgets:
  - default
ocean_display_footer_bottom:
  - default
ocean_link_format_target:
  - self
ocean_quote_format_link:
  - post
rank_math_news_sitemap_robots:
  - index
rank_math_robots:
  - 'a:1:{i:0;s:5:"index";}'
rank_math_analytic_object_id:
  - 104
rank_math_internal_links_processed:
  - 1
categories:
  - Monitoring

---
In this tutorial guide we will learn how to install Openlitespeed server on Rocky Linux/Alma Linux

Openlitespeed is an easy to use open source web server. It offers unbeatable features and performance to your website along with top notch security. The server understands all the Apache rewrite rules and has intelligent cache acceleration features that let you implement the fastest caching on your server.

## Prerequisites  {.wp-block-heading}

  * Rocky Linux 8 Server
  * User account with sudo privileges 
  * Strong internet connection

## Table of Contents  {.wp-block-heading}

  1. Update our System
  2. Add OpenLiteSpeed to Rocky Linux 8 to the repository
  3. Install OpenLiteSpeed on Rocky Linux
  4. Install PHP
  5. Install and confiogure MySQL
  6. Set up OpenLiteSpeed login Admin credentials 
  7. Test OpenLiteSpeed

## Related Articles  {.wp-block-heading}

  * [How to install aapanel 6 on Fedora 34][1]

## 1. Update Rocky Linux Repositories  {.wp-block-heading}

First we need to update our system so that our repositories are up to date. This is the first step whenever you are doing any installation in the system. Type the following command into our terminal.

<pre class="wp-block-code"><code>$ sudo dnf update -y</code></pre>

When the update is complete proceed to add OpenLiteSpeed to repository.

## 2. Add OpenLiteSpeed to EPEL repository {.wp-block-heading}

To add OpenLiteSpeed into our repository in Rocky 8 Linux, type the following into our terminal;

<pre class="wp-block-code"><code>$ sudo dnf install epel-release</code></pre>

Sample output

<pre class="wp-block-code"><code>....
Running transaction check
Transaction check succeeded.
Running transaction test
Transaction test succeeded.
Running transaction
  Preparing        :                                                                                         1/1 
  Installing       : epel-release-8-13.el8.noarch                                                            1/1 
  Running scriptlet: epel-release-8-13.el8.noarch                                                            1/1 
  Verifying        : epel-release-8-13.el8.noarch                                                            1/1 

Installed:
  epel-release-8-13.el8.noarch                                                                                   

Complete!</code></pre>

Add OpenLiteSpeed to our repository

<pre class="wp-block-code"><code>$ sudo rpm -Uvh http://rpms.litespeedtech.com/centos/litespeed-repo-1.1-1.el8.noarch.rpm

Retrieving http://rpms.litespeedtech.com/centos/litespeed-repo-1.1-1.el8.noarch.rpm
Verifying...                          ################################# &#91;100%]
Preparing...                          ################################# &#91;100%]
Updating / installing...
   1:litespeed-repo-1.2-1.el8         ################################# &#91;100%]</code></pre>

## 3. Install OpenLiteSpeed on Rocky Linux/AlmaLinux  {.wp-block-heading}

Now that we have added the OpenLiteSpeed Repository to our system, we can now install OpenLiteSpeed with the following command:

<pre class="wp-block-code"><code>$ sudo dnf install openlitespeed</code></pre>

After the installation is complete, you check the status of OpenLiteSpeed server with the following command;

<pre class="wp-block-code"><code>$ # sudo systemctl status lsws
● lshttpd.service - OpenLiteSpeed HTTP Server
   Loaded: loaded (/usr/lib/systemd/system/lshttpd.service; enabled; vendor preset: disabled)
   Active: active (running) since Fri 2021-11-12 12:22:52 UTC; 1min 41s ago
  Process: 63411 ExecStart=/usr/local/lsws/bin/lswsctrl start (code=exited, status=0/SUCCESS)
 Main PID: 63435 (litespeed)
   CGroup: /system.slice/lshttpd.service
           ├─63435 openlitespeed (lshttpd - main)
           ├─63439 openlitespeed (lscgid)
           └─63452 openlitespeed (lshttpd - #01)

Nov 12 12:22:49 rockylinux systemd&#91;1]: Starting OpenLiteSpeed HTTP Server...
Nov 12 12:22:50 rockylinux lswsctrl&#91;63411]: &#91;OK] litespeed: pid=63435.
Nov 12 12:22:52 rockylinux systemd&#91;1]: Started OpenLiteSpeed HTTP Server.</code></pre>

It must show the status as active for us to continue with this tutorial. If not start your server first with the following command

<pre class="wp-block-code"><code>$ /usr/local/lsws/bin/lswsctrl start.</code></pre>

## 4. Install PHP for OpenLiteSpeed  {.wp-block-heading}

PHP is not embedded in OpenLiteSpeed as is the case with Apache. OpenLiteSpeed delegates PHP execution to external PHP executables. This will allows for better security, stability and handling of multiple connections.

Begin installation of PHP74 with the following command

<pre class="wp-block-code"><code>$ sudo dnf -y install lsphp74 lsphp74-common lsphp74-mysqlnd lsphp74-gd lsphp74-process lsphp74-mbstring lsphp74-xml lsphp74-mcrypt lsphp74-pdo lsphp74-imap lsphp74-soap lsphp74-bcmath</code></pre>

Once the installation of PHP74 is complete, we can now move to install MySQL server

## 5. Install MySQL server  {.wp-block-heading}

You can run the following command to install MySQL server

<pre class="wp-block-code"><code>$ sudo dnf install mysql mysql-server</code></pre>

Run secure installation script to make MyQL safe, but first you need to start mysqld service;

<pre class="wp-block-code"><code>$ sudo systemctl start mysqld
$ sudo systemctl enable mysqld</code></pre>

Check the status now with the following command;

<pre class="wp-block-code"><code>$ sudo systemctl status mysqld</code></pre>

Sample Output

<pre class="wp-block-code"><code>● mysqld.service - MySQL 8.0 database server
   Loaded: loaded (/usr/lib/systemd/system/mysqld.service; enabled; vendor preset: disabled)
   Active: active (running) since Fri 2021-11-12 12:56:55 UTC; 2min 48s ago
 Main PID: 66270 (mysqld)
   Status: "Server is operational"
    Tasks: 37 (limit: 11189)
   Memory: 458.0M
   CGroup: /system.slice/mysqld.service
           └─66270 /usr/libexec/mysqld --basedir=/usr

Nov 12 12:56:47 rockylinux systemd&#91;1]: Starting MySQL 8.0 database server...
Nov 12 12:56:47 rockylinux mysql-prepare-db-dir&#91;66189]: Initializing MySQL database
Nov 12 12:56:55 rockylinux systemd&#91;1]: Started MySQL 8.0 database server.</code></pre>

It is now time to run our secure installation, do the following;

<pre class="wp-block-code"><code>$ sudo mysql_secure_installation</code></pre>

For easy logging into the system it is better you do not set validate password. Type N and the consequent prompts you can type Y to allow installation to continue.

When it is complete we can now move forward to set up OpenLiteSpeed administration panel. 

## 5. Configure OpenLiteSpeed Admin panel {.wp-block-heading}

To change the default password in the OpenLiteSpeed dashboard, we need to run the following command;

<pre class="wp-block-code"><code>$ /usr/local/lsws/admin/misc/admpass.sh</code></pre>

Sample output

<pre class="wp-block-code"><code># 
Please specify the user name of administrator.
This is the user name required to login the administration Web interface.

User name &#91;admin]: admin

Please specify the administrator's password.
This is the password required to login the administration Web interface.

Password: 
Retype password: 
Administrator's username/password is updated successfully!</code></pre>

We have successfuly added the administrator, it is now good we configure firewall to allow OpenLiteSpeed run successfully.

## 6. Configure Firewall Settings  {.wp-block-heading}

OpenLiteSpeed listens on port 8088 and 7080, so we must allow this ports access. To do so we can do the following;

<pre class="wp-block-code"><code>$ firewall-cmd --zone=public --permanent --add-port=8088/tcp</code></pre>

Do the same for port 7080

<pre class="wp-block-code"><code>$ firewall-cmd --zone=public --permanent --add-port=7080/tcp</code></pre>

Then you need to reload the system for changes to take effect

<pre class="wp-block-code"><code>$ sudo firewall-cmd --reload</code></pre>

If incase you run into issues with the following please ensure that f**irewalld** is up and running. Run the folllowing command to enable it.

<pre class="wp-block-code"><code>$ systemctl enable --now firewalld</code></pre>

Lastly we can test our OpenLiteSpeed to see if it is running with the following on your prefered browser;

<pre class="wp-block-code"><code>http://&lt;Your_IP_address:8088></code></pre><figure class="wp-block-image size-large">

<img decoding="async" loading="lazy" width="810" height="464" src="https://i0.wp.com/nextgentips.com/wp-content/uploads/2021/11/Screenshot-from-2021-11-12-16-34-12.png?resize=810%2C464&#038;ssl=1" alt="openLiteSpeed on port 8088" class="wp-image-583" srcset="https://i0.wp.com/nextgentips.com/wp-content/uploads/2021/11/Screenshot-from-2021-11-12-16-34-12.png?resize=1024%2C586&ssl=1 1024w, https://i0.wp.com/nextgentips.com/wp-content/uploads/2021/11/Screenshot-from-2021-11-12-16-34-12.png?resize=300%2C172&ssl=1 300w, https://i0.wp.com/nextgentips.com/wp-content/uploads/2021/11/Screenshot-from-2021-11-12-16-34-12.png?resize=768%2C440&ssl=1 768w, https://i0.wp.com/nextgentips.com/wp-content/uploads/2021/11/Screenshot-from-2021-11-12-16-34-12.png?w=1294&ssl=1 1294w" sizes="(max-width: 810px) 100vw, 810px" data-recalc-dims="1" /> <figcaption>OpenLiteSpeed on port 8088</figcaption></figure> 

To access the admin site we need to use port 7080 like this 

<pre class="wp-block-code"><code>http://&lt;Your_IP_address:7080></code></pre><figure class="wp-block-image size-large">

<img decoding="async" loading="lazy" width="810" height="464" src="https://i0.wp.com/nextgentips.com/wp-content/uploads/2021/11/Screenshot-from-2021-11-12-16-53-07.png?resize=810%2C464&#038;ssl=1" alt="OpenLiteSpeed login screen" class="wp-image-584" srcset="https://i0.wp.com/nextgentips.com/wp-content/uploads/2021/11/Screenshot-from-2021-11-12-16-53-07.png?resize=1024%2C586&ssl=1 1024w, https://i0.wp.com/nextgentips.com/wp-content/uploads/2021/11/Screenshot-from-2021-11-12-16-53-07.png?resize=300%2C172&ssl=1 300w, https://i0.wp.com/nextgentips.com/wp-content/uploads/2021/11/Screenshot-from-2021-11-12-16-53-07.png?resize=768%2C440&ssl=1 768w, https://i0.wp.com/nextgentips.com/wp-content/uploads/2021/11/Screenshot-from-2021-11-12-16-53-07.png?w=1294&ssl=1 1294w" sizes="(max-width: 810px) 100vw, 810px" data-recalc-dims="1" /> <figcaption>openLiteSpeed login screen</figcaption></figure> <figure class="wp-block-image size-large"><img decoding="async" loading="lazy" width="810" height="464" src="https://i0.wp.com/nextgentips.com/wp-content/uploads/2021/11/Screenshot-from-2021-11-12-16-53-27.png?resize=810%2C464&#038;ssl=1" alt="OpenLiteSpeed Dashboard " class="wp-image-585" srcset="https://i0.wp.com/nextgentips.com/wp-content/uploads/2021/11/Screenshot-from-2021-11-12-16-53-27.png?resize=1024%2C586&ssl=1 1024w, https://i0.wp.com/nextgentips.com/wp-content/uploads/2021/11/Screenshot-from-2021-11-12-16-53-27.png?resize=300%2C172&ssl=1 300w, https://i0.wp.com/nextgentips.com/wp-content/uploads/2021/11/Screenshot-from-2021-11-12-16-53-27.png?resize=768%2C440&ssl=1 768w, https://i0.wp.com/nextgentips.com/wp-content/uploads/2021/11/Screenshot-from-2021-11-12-16-53-27.png?w=1294&ssl=1 1294w" sizes="(max-width: 810px) 100vw, 810px" data-recalc-dims="1" /><figcaption>OpenLiteSpeed Dashboard </figcaption></figure> 

## Conclusion {.wp-block-heading}

I am glad you have learned how to install OpenLiteSpeed on Rocky Linux/Alma Linux. Continue playing around with the configuration to learn more. In case of problem don not hesitate to contact us or consult [OpenLiteSpeed Documentation][2]

 [1]: https://nextgentips.com/2021/11/05/how-to-install-aapanel-6-on-fedora-34/
 [2]: https://openlitespeed.org/#install