---
title: The Top Docker Commands you must know
author: Kipkoech Sang
type: post
date: 2023-01-26T07:14:03+00:00
url: /2023/01/26/the-top-docker-commands-you-must-know/
categories:
  - Containers

---
## What is Docker {.wp-block-heading}

**Docker is a set of platform-as-a-service products that uses OS-level virtualization to deliver software in packages called containers**. Containers are usually isolated from one another and bundled with their own software libraries and configuration files, they can communicate with each other through well-defined channels.

## Benefits of Docker  {.wp-block-heading}

  * Docker makes it possible to get more apps running on the same old servers and also makes it easy to package and ship programs.
  * It allows users to package an application with all of its dependencies into a standard unit for software development. 
  * Containers don&#8217;t have a high overhead i.e it doesn&#8217;t consume more resources such as computing power, memory, and storage, unlike virtual machines.

To install Docker in your system refer to this article <a href="https://nextgentips.com/2022/05/09/how-to-install-docker-ce-on-ubuntu-22-04/" target="_blank" rel="noopener" title="">How to install Docker-CE on Ubuntu 22.04</a>. After you have installed it and Docker is up and running come back and continue.

## Docker commands to use on a daily basis {.wp-block-heading}

### Docker Run {.wp-block-heading}

Before you can continue, make sure that Docker is up and running, you can use the following command to check if it is running `<mark style="background-color:#abb8c3" class="has-inline-color">docker run hello-world</mark>` 

<pre class="wp-block-code"><code>$ docker run hello-world

Unable to find image 'hello-world:latest' locally
latest: Pulling from library/hello-world
2db29710123e: Pull complete 
Digest: sha256:aa0cc8055b82dc2509bed2e19b275c8f463506616377219d9642221ab53cf9fe
Status: Downloaded newer image for hello-world:latest

Hello from Docker!
This message shows that your installation appears to be working correctly.</code></pre>

From the above, we can see that Docker is up and running. You have seen how to use `<mark style="background-color:#abb8c3" class="has-inline-color">docker run</mark>` command, whenever you run this command, the Docker client first finds the image in the Docker registry and then loads the container, and lastly runs it.

### Docker Pull {.wp-block-heading}

Docker pull downloads images from the Docker registry and saves them on our local machine. I know you might be wondering what might be docker images, give me a second I will explain to you in a moment. I want to use an example of a Redis image here, you can use anything you want such as PostgreSQL, Mysql, ubuntu, python images, etc. To pull Redis from the registry we use `<mark style="background-color:#abb8c3" class="has-inline-color">docker pull redis</mark>`

<pre class="wp-block-code"><code>$ &lt;strong>docker pull redis&lt;/strong>
Using default tag: latest
latest: Pulling from library/redis
8740c948ffd4: Already exists 
a2271c958e57: Pull complete 
495af11a3eac: Pull complete 
18b045ddb54d: Pull complete 
f49c2d6d086c: Pull complete 
14ed0c386119: Pull complete 
Digest: sha256:325d5a448d8f6c1d30a0a0fb26090343279d4cf23258b26b1745862f332e9479
Status: Downloaded newer image for redis:latest
docker.io/library/redis:latest</code></pre>

So now Redis has been downloaded into our system, you can check its existence now with the next command.

### Docker Images {.wp-block-heading}

A docker image is a read-only template that contains a set of instructions for creating a container. From our example, Redis is built from the instructions given by its creator. Let&#8217;s see how to check the existence of images from our local system.

<pre class="wp-block-code"><code>docker images</code></pre>

Here is the screenshot of our image Redis, it has the name, tag, image id, created day, and size.<figure class="wp-block-image size-full">

<img decoding="async" loading="lazy" width="650" height="72" src="https://i0.wp.com/nextgentips.com/wp-content/uploads/2023/01/Screenshot-from-2023-01-26-08-23-41.png?resize=650%2C72&#038;ssl=1" alt="nextgentips: docker images" class="wp-image-1713" srcset="https://i0.wp.com/nextgentips.com/wp-content/uploads/2023/01/Screenshot-from-2023-01-26-08-23-41.png?w=650&ssl=1 650w, https://i0.wp.com/nextgentips.com/wp-content/uploads/2023/01/Screenshot-from-2023-01-26-08-23-41.png?resize=300%2C33&ssl=1 300w" sizes="(max-width: 650px) 100vw, 650px" data-recalc-dims="1" /> <figcaption class="wp-element-caption">nextgentips: docker images</figcaption></figure> 

To use this image, you <mark style="background-color:#abb8c3" class="has-inline-color">`docker run` </mark>command as we have stated above. So let&#8217;s run this image and see what happens.

<pre class="wp-block-code"><code>$ &lt;strong>docker run redis&lt;/strong>
1:C 26 Jan 2023 05:28:46.464 # oO0OoO0OoO0Oo Redis is starting oO0OoO0OoO0Oo
1:C 26 Jan 2023 05:28:46.464 # Redis version=7.0.8, bits=64, commit=00000000, modified=0, pid=1, just started
1:C 26 Jan 2023 05:28:46.464 # Warning: no config file specified, using the default config. In order to specify a config file use redis-server /path/to/redis.conf
1:M 26 Jan 2023 05:28:46.465 * monotonic clock: POSIX clock_gettime
1:M 26 Jan 2023 05:28:46.465 * Running mode=standalone, port=6379.
1:M 26 Jan 2023 05:28:46.465 # Server initialized
1:M 26 Jan 2023 05:28:46.465 # WARNING Memory overcommit must be enabled! Without it, a background save or replication may fail under low memory condition. Being disabled, it can can also cause failures without low memory condition, see https://github.com/jemalloc/jemalloc/issues/1328. To fix this issue add 'vm.overcommit_memory = 1' to /etc/sysctl.conf and then reboot or run the command 'sysctl vm.overcommit_memory=1' for this to take effect.
&lt;strong>1:M 26 Jan 2023 05:28:46.466 * Ready to accept connections&lt;/strong>
</code></pre>

From the above output, take note of `<mark style="background-color:#abb8c3" class="has-inline-color">Ready to accept connections</mark>`, It tells us that the image runs successfully and is ready to be used, Redis act as a cache, database as well as message broker. So am not going to tackle the ins and out of Redis.

### Docker ps {.wp-block-heading}

Whenever you want to check the running containers, you use <mark style="background-color:#abb8c3" class="has-inline-color">`docker ps` </mark>command, it&#8217;s a very handy command when it comes to docker. So let&#8217;s check if Redis is up and running:

<pre class="wp-block-code"><code>docker ps</code></pre><figure class="wp-block-image size-full">

<img decoding="async" loading="lazy" width="810" height="59" src="https://i0.wp.com/nextgentips.com/wp-content/uploads/2023/01/Screenshot-from-2023-01-26-08-36-39.png?resize=810%2C59&#038;ssl=1" alt="nextgentips: docker ps" class="wp-image-1714" srcset="https://i0.wp.com/nextgentips.com/wp-content/uploads/2023/01/Screenshot-from-2023-01-26-08-36-39.png?w=989&ssl=1 989w, https://i0.wp.com/nextgentips.com/wp-content/uploads/2023/01/Screenshot-from-2023-01-26-08-36-39.png?resize=300%2C22&ssl=1 300w, https://i0.wp.com/nextgentips.com/wp-content/uploads/2023/01/Screenshot-from-2023-01-26-08-36-39.png?resize=768%2C56&ssl=1 768w" sizes="(max-width: 810px) 100vw, 810px" data-recalc-dims="1" /> <figcaption class="wp-element-caption">nextgentips: docker ps</figcaption></figure> 

From the output, you can see that Redis is up and running with status and also given ports. So Redis is running out of the box, we didn&#8217;t hustle with the installation, so this is the benefit of running docker containers.

Whenever you want to remove the containers, always stop them first with `<mark style="background-color:#abb8c3" class="has-inline-color">docker stop <id></mark>` 

<pre class="wp-block-code"><code>docker stop &lt;id></code></pre>

But sometimes you have many running containers which you don&#8217;t need anymore, you can forcibly stop them and remove them all at once with the following command.

<pre class="wp-block-code"><code>$ docker rm -f $(docker ps -aq)   </code></pre>

  * -f means force removal
  * $(docker ps -aq) means to list all the containers with their container IDs only 

If you run `docker ps -aq` will list all the container IDs 

<pre class="wp-block-code"><code>$ docker ps -aq
a9c86577f93a
12f7db5f5fae
cbc2e9d4e33a
7a66698931ba
29e912cc3aec
9a8e061ce199
90f95caf8834</code></pre>

### Docker volumes {.wp-block-heading}

Volumes are used in persisting data generated by and used by the Docker containers. So when we run our Redis above data is persisted and whenever we want to run Redis those data are used without necessarily pulling the new Redis image. To check running volumes we use `<mark style="background-color:#abb8c3" class="has-inline-color">docker volumes ls</mark>`

<pre class="wp-block-code"><code>$ docker volumes ls
DRIVER    VOLUME NAME
local     3c5bb87879555163a6ab96ab12ba40f3de5a9182f4fb8024f13ed62554e887a4
local     339040a520a17349a1a834a72e73b4dcdb915dacaf327a3d2391c52ad35ce6bc
local     cff002a049f37e9e3458c8fed4502197df5f636a959293945d24da2b7ca98e38</code></pre>

If you want to remove a certain volume use `<mark style="background-color:#abb8c3" class="has-inline-color">docker volume rm <volume-name></mark>`. Let&#8217;s remove the following volume

<pre class="wp-block-code"><code>docker volume rm 3c5bb87879555163a6ab96ab12ba40f3de5a9182f4fb8024f13ed62554e887a4</code></pre>

You will get the name as the output meaning it&#8217;s successfully removed.

Sometimes you have very many running volumes and you want to remove them all then use the following command.

<pre class="wp-block-code"><code>$ docker volume rm -f $(docker volume ls)  </code></pre>

  * -f means you want to forcibly remove the volumes
  * $(docker volume ls) means list all the volumes 

### Docker build {.wp-block-heading}

To create an image from the specification given in a template, use `<mark style="background-color:#abb8c3" class="has-inline-color">docker build . -t <username>/<image_name>:<version></mark>`

<pre class="wp-block-code"><code>&lt;code>&lt;mark style="background-color:#ffffff" class="has-inline-color">docker build . -t &lt;username>/&lt;image_name>:&lt;version>&lt;/mark></code>&lt;/code></pre>

### Docker push {.wp-block-heading}

It&#8217;s always advisable to save your images, so that whenever you want others to use they can easily pull them from the registries such as DockerHub. I know windows and Mac users have Docker Desktop that can save the images automatically but for Linux, you have to push the images. 

<pre class="wp-block-code"><code>docker push &lt;username>/&lt;image_name>:&lt;version> </code></pre>

### Docker prune {.wp-block-heading}

Let&#8217;s say you no longer need certain containers, images, networks, and volumes then you can remove them with `<mark style="background-color:#abb8c3" class="has-inline-color">docker system prune</mark>`. This will remove all the images, volumes, and networks except the volumes because it stores important data. Whenever you want to remove volumes prepend `<mark style="background-color:#abb8c3" class="has-inline-color">--volumes</mark>` to the command.

<pre class="wp-block-code"><code>$ &lt;strong>docker system prune&lt;/strong>

WARNING! This will remove:
  - all stopped containers
  - all networks not used by at least one container
  - all dangling images
  - all dangling build cache

Are you sure you want to continue? &#91;y/N]


# with volumes
$ &lt;strong>docker system prune --volumes&lt;/strong>
WARNING! This will remove:
  - all stopped containers
  - all networks not used by at least one container
  - all volumes not used by at least one container
  - all dangling images
  - all dangling build cache

Are you sure you want to continue? &#91;y/N] </code></pre>

Click yes if you want to remove it and No if not.

### Docker rmi {.wp-block-heading}

When you want to remove a certain image use `docker rmi` command, this will remove one image at a time and only if the image isn&#8217;t running. You will get an error whenever you are removing a running image.

<pre class="wp-block-code"><code>$ &lt;strong>docker rmi redis&lt;/strong>
Error response from daemon: conflict: unable to remove repository reference "redis" (must force) - container 12f7db5f5fae is using its referenced image 19c51d4327cf</code></pre>

I am getting an error here because Redis is running, whenever you want to force prepend -f to force remove.

<pre class="wp-block-code"><code>$ &lt;strong>docker rmi -f redis&lt;/strong>
Untagged: redis:latest
Untagged: redis@sha256:325d5a448d8f6c1d30a0a0fb26090343279d4cf23258b26b1745862f332e9479</code></pre>

But this is not recommended you can stop the running container first and then initiate removal. To stop a container run `<mark style="background-color:#abb8c3" class="has-inline-color">docker stop 0435d9faf66f</mark>`

<pre class="wp-block-code"><code>$ docker stop 0435d9faf66f </code></pre>

**0435d9faf66f** this is the image ID, it&#8217;s always good to reference the containers with their IDs because if you use an image name you might remove the wrong container.

To remove all the images at once then we use `<mark style="background-color:#abb8c3" class="has-inline-color">docker rmi -f $(docker images -aq)</mark>`

<pre class="wp-block-code"><code>$ docker rmi -f $(docker images -aq) </code></pre>

  * -f means force removal
  * $(docker images -aq) means to list all the images specifying the image IDs only.

## Conclusion {.wp-block-heading}

Here we have learned various useful Docker commands one should learn in order to become Docker-ready. I hope it&#8217;s been informative and it opens up your mind. For further reading on docker check [Docker documentation.][1]

 [1]: https://docs.docker.com/engine/reference/commandline/docker/ ""