---
title: Learn Kubernetes basics
author: Kipkoech Sang
type: post
date: 2021-12-09T19:01:59+00:00
url: /2021/12/09/learn-kubernetes-basics/
entry_views:
  - 1
view_ip:
  - 'a:1:{i:0;s:13:"66.249.75.118";}'
rank_math_news_sitemap_robots:
  - index
rank_math_robots:
  - 'a:1:{i:0;s:5:"index";}'
rank_math_analytic_object_id:
  - 70
rank_math_internal_links_processed:
  - 1
post_view:
  - 1
is_post_view:
  - 'a:1:{i:0;s:28:"5.255.253.115, 99.82.162.201";}'
categories:
  - Kubernetes

---
[Kubernetes][1] is an open-source container orchestration system for automating computer application deployment, scaling, and management. 

Kubernetes is a portable, extensible, open-source platform for managing containerized workloads and services that facilitates both declarative configuration and automation.

## What Kubernetes provides {.wp-block-heading}

  * **Service discovery and load balancing**. Kubernetes is in a position to expose a container using DNS or IP address. If traffic to a container is high, Kubernetes is able to do load balancing and distribute network the network traffic so that deployment is stable.
  * **Storage orchestration**. Kubernetes allows one to mount storage automatically of your choice.
  * **Automated rollouts and rollbacks**. You can automate Kubernetes to create new containers for your environment, and also you can remove existing containers and adopt all their resources to the new container.
  * **Self-healing**. Kubernetes restart Containers that fail, replace and kill containers that aren&#8217;t responsive
  * **Automatic bin packing**. You provide kubernetes with cluster of nodes that it can use to run containerized tasks. You tell kubernetes how much CPU and memory each container needs. Kubernetes can fit containers into your nodes to make the best use of your resources.
  * **Secret and configuration management**. Kubernetes lets you manage and store sensitive information such as passwords. OAuth tokens and ssh keys. You can deploy and update secret and applications configurations without rebuilding your container images and without exposing secrets in your stack configurations. 
  * Kubernets components Nodes: Pods Control plane

## Kubernetes Components {.wp-block-heading}

  * **Nodes:** A Kubernetes cluster consists of a set of worker machines called nodes, that run containerized applications.
  * **Pods**: These are components of the application workload. A Pod represents a set of running containers in your cluster.
  * **Control plane** is a container orchastration layer that exposes the API and interfaces to define, deploy, and manage the life cycle of containers. 

## Container orchestration {.wp-block-heading}

Container orchestration is the management of individual web servers operating in containers through virtual partitions on data center hardware. It is a means of maintaining the elastic framework for web servers in a data center on an automated basis in production. 

This is how orchestration works. Have an orchestration tool like Kubernetes, you will have to describe your configuration in a YAML or JSON file. This configuration file is where you tell your orchestration tool where to gather container images, establish networking between containers, how to mount storage volumes and where to store logs.

Containers are deployed onto hosts in replicated groups. When you want to deploy the new container into the cluster, container orchestration tools such as Kubernetes or Docker swam schedule the deployment and look for the best appropriate host to place the container.

Once the container is running on the host, it&#8217;s time for the orchestration tool to manage its lifecycle using the YAML file specifications.

Next, we will explore [Kubernetes Architecture][2].

 [1]: https://kubernetes.io/
 [2]: https://nextgentips.com/2021/12/10/kubernetes-architecture/