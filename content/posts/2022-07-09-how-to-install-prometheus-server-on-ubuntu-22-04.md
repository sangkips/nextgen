---
title: How to install Prometheus Server on Ubuntu 22.04
author: Kipkoech Sang
type: post
date: 2022-07-09T06:51:18+00:00
url: /2022/07/09/how-to-install-prometheus-server-on-ubuntu-22-04/
categories:
  - Uncategorized

---
In this guide, we will learn how to install the Prometheus server on Ubuntu 22.04. **Prometheus is an open-source system monitoring and alerting toolkit. Prometheus collects and stores its metrics as time-series data. Metrics information is stored with the timestamp at which it was recorded, alongside optional key-value pairs called labels.**

And now you are wondering what are metrics? Metrics are numeric measurements and time-series mean that the changes are recorded over time.

## Install Prometheus on Ubuntu 22.04 {.wp-block-heading}

### 1. Update the system repositories. {.wp-block-heading}

Let us update the system first in order to make the repositories up to date.

<pre class="wp-block-code"><code>$ sudo apt update && apt upgrade -y</code></pre>

### 2. Download and install Prometheus {.wp-block-heading}

Download the latest release of Prometheus into your system from the Prometheus <a href="https://prometheus.io/download/" target="_blank" rel="noreferrer noopener">download page</a>.

<pre class="wp-block-code"><code>$ wget https://github.com/prometheus/prometheus/releases/download/v2.37.0-rc.0/prometheus-2.37.0-rc.0.linux-amd64.tar.gz</code></pre>

After the download is complete, the next step is to extract the archive:

<pre class="wp-block-code"><code>$ tar xvfz prometheus-*.tar.gz</code></pre>

For convenience, we can change the directory for the extracted archive

<pre class="wp-block-code"><code>$ cd prometheus-*</code></pre>

From there we can create a configuration file directory

<pre class="wp-block-code"><code>$ sudo mkdir -p /etc/prometheus</code></pre>

Then we can create the data directory

<pre class="wp-block-code"><code>$ sudo mkdir -p /var/lib/prometheus</code></pre>

Let’s now move the binary files Prometheus and promtool to `<mark style="background-color:#abb8c3" class="has-inline-color">/usr/local/bin/</mark>`

Use the following code snippet 

<pre class="wp-block-code"><code>$ sudo mv prometheus promtool /usr/local/bin/</code></pre>

Let&#8217;s use a Prometheus.yaml file that we can use as an example to see how Prometheus monitors your infrastructure. This file will only monitor itself for now.

<pre class="wp-block-code"><code>$ sudo vi prometheus.yml</code></pre>

you will see the following contents from prometheus.yaml file

<pre class="wp-block-code"><code>global:
  scrape_interval:     15s
  external_labels:
    monitor: 'codelab-monitor'
scrape_configs:
   scraped from this config.
  - job_name: 'prometheus'
    scrape_interval: 5s

    static_configs:
      - targets: &#91;'localhost:9090']</code></pre>

Move the template configuration file&nbsp;`prometheus.yml`&nbsp;to&nbsp;`/etc/prometheus/`&nbsp;directory

<pre class="wp-block-code"><code>$ sudo mv prometheus.yml /etc/prometheus/prometheus.yml</code></pre>

We have installed Prometheus. Let&#8217;s verify the version

<pre class="wp-block-code"><code>$ prometheus --version
prometheus, version 2.37.0-rc.0 (branch: HEAD, revision: 2479fb42f0a9bb45c2e82f11efc73c0a75fc492c)
  build user:       root@b82e7804c2b2
  build date:       20220705-13:33:30
  go version:       go1.18.3
  platform:         linux/amd64
</code></pre>

### 3. Create Prometheus System Group {.wp-block-heading}

The following code can help create the Prometheus system group.

<pre class="wp-block-code"><code>$ sudo groupadd --system prometheus</code></pre>

We can then create a Prometheus system user and assign the primary group created

<pre class="wp-block-code"><code>$ sudo useradd -s /sbin/nologin --system -g prometheus prometheus</code></pre>

Set the ownership of Prometheus files and data directories to the Prometheus group and user.

<pre class="wp-block-code"><code>$ sudo chown -R prometheus:prometheus /etc/prometheus/  /var/lib/prometheus/

$ sudo chmod -R 775 /etc/prometheus/ /var/lib/prometheus/</code></pre>

Lastly, we can create a systemd service to run at boot time

<pre class="wp-block-code"><code>$ sudo nano /etc/systemd/system/prometheus.service</code></pre>

Add the following content to prometheus.service

<pre class="wp-block-code"><code>&#91;Unit]
Description=Prometheus
Wants=network-online.target
After=network-online.target

&#91;Service]
User=prometheus
Group=prometheus
Restart=always
Type=simple
ExecStart=/usr/local/bin/prometheus \
    --config.file=/etc/prometheus/prometheus.yml \
    --storage.tsdb.path=/var/lib/prometheus/ \
    --web.console.templates=/etc/prometheus/consoles \
    --web.console.libraries=/etc/prometheus/console_libraries \
    --web.listen-address=0.0.0.0:9090

&#91;Install]
WantedBy=multi-user.target</code></pre>

Start the Prometheus service

<pre class="wp-block-code"><code>$ sudo systemctl start prometheus</code></pre>

Now we can check if our Prometheus is up and running

<pre class="wp-block-code"><code>$ sudo systemctl status prometheus</code></pre>

<pre class="wp-block-code"><code>● prometheus.service - Prometheus
     Loaded: loaded (/etc/systemd/system/prometheus.service; disabled; vendor p>
     Active: active (running) since Sat 2022-07-09 06:36:04 UTC; 32s ago
   Main PID: 13447 (prometheus)
      Tasks: 6 (limit: 1033)
     Memory: 17.9M
        CPU: 86ms
     CGroup: /system.slice/prometheus.service
             └─13447 /usr/local/bin/prometheus --config.file=/etc/prometheus/pr>

Jul 09 06:36:04 localhost prometheus&#91;13447]: ts=2022-07-09T06:36:04.386Z caller>
Jul 09 06:36:04 localhost prometheus&#91;13447]: ts=2022-07-09T06:36:04.389Z caller>
Jul 09 06:36:04 localhost prometheus&#91;13447]: ts=2022-07-09T06:36:04.390Z caller>
Jul 09 06:36:04 localhost prometheus&#91;13447]: ts=2022-07-09T06:36:04.390Z caller>
Jul 09 06:36:04 localhost prometheus&#91;13447]: ts=2022-07-09T06:36:04.392Z caller>
Jul 09 06:36:04 localhost prometheus&#91;13447]: ts=2022-07-09T06:36:04.392Z caller>
Jul 09 06:36:04 localhost prometheus&#91;13447]: ts=2022-07-09T06:36:04.393Z caller>
Jul 09 06:36:04 localhost prometheus&#91;13447]: ts=2022-07-09T06:36:04.394Z caller>
Jul 09 06:36:04 localhost prometheus&#91;13447]: ts=2022-07-09T06:36:04.394Z caller>
Jul 09 06:36:04 localhost prometheus&#91;13447]: ts=2022-07-09T06:36:04.395Z caller>
lines 1-20/20 (END)</code></pre>

Add firewall rules to allow Prometheus to run on TCP port 9090

<pre class="wp-block-code"><code>$ ufw allow 9090/tcp</code></pre>

Access your server from the browser.

<pre class="wp-block-code"><code>http://&lt;ip address&gt;or&lt;hostname&gt;:9090</code></pre><figure class="wp-block-image size-large">

<img decoding="async" loading="lazy" width="810" height="276" src="https://i0.wp.com/nextgentips.com/wp-content/uploads/2022/07/Screenshot-from-2022-07-09-09-43-27.png?resize=810%2C276&#038;ssl=1" alt="nextgentips: Prometheus dashboard" class="wp-image-1485" srcset="https://i0.wp.com/nextgentips.com/wp-content/uploads/2022/07/Screenshot-from-2022-07-09-09-43-27.png?resize=1024%2C349&ssl=1 1024w, https://i0.wp.com/nextgentips.com/wp-content/uploads/2022/07/Screenshot-from-2022-07-09-09-43-27.png?resize=300%2C102&ssl=1 300w, https://i0.wp.com/nextgentips.com/wp-content/uploads/2022/07/Screenshot-from-2022-07-09-09-43-27.png?resize=768%2C262&ssl=1 768w, https://i0.wp.com/nextgentips.com/wp-content/uploads/2022/07/Screenshot-from-2022-07-09-09-43-27.png?w=1295&ssl=1 1295w" sizes="(max-width: 810px) 100vw, 810px" data-recalc-dims="1" /> <figcaption>nextgentips: Prometheus dashboard</figcaption></figure> 



## Conclusion {.wp-block-heading}

Congratulations! you have successfully installed Prometheus on Ubuntu 22.04. To read more consult&nbsp;[Prometheus documentation][1].

 [1]: https://prometheus.io/docs/prometheus/latest/getting_started/