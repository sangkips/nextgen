---
title: How to install Terraform on Alma Linux 9
author: Kipkoech Sang
type: post
date: 2022-07-06T15:19:40+00:00
url: /2022/07/06/how-to-install-terraform-on-alma-linux-9/
categories:
  - Automation

---
In this article, we are going to learn how to install Terraform on Alma Linux 9.

Terraform is an open-source infrastructure as code software tool that provides a consistent CLI workflow to manage hundreds of cloud services. Terraform codifies cloud APIs into declarative configuration files.

Infrastructure as code (IAC) tools allow you to manage your infrastructure with a configuration file rather than through a graphical interface. IAC allows you to build, change and manage your infrastructure in a safe, consistent, and repeatable way by defining configurations that you can version, reuse and share.

## Advantages of Using Terraform {.wp-block-heading}

  * Terraform can manage infrastructure on multiple cloud platforms.
  * The human-readable form helps you write infrastructure code quickly.
  * Terraforms state helps you to track resource changes throughout your deployments.
  * You can commit your configurations to version control to safely collaborate on infrastructure.

## Install Terraform on Alma Linux 9. {.wp-block-heading}

### 1. Update system repositories {.wp-block-heading}

Before beginning the installation, it is advisable to update your repositories in order to make them up to date.

<pre class="wp-block-code"><code>sudo dnf update -y</code></pre>

After the updates are complete, you need to add the yum-utils package manager to your system first. Yum-utils allows one to add functions without modifying yum-utils code.

<pre class="wp-block-code"><code>sudo yum install yum-utils -y</code></pre>

The sample output will look like this.

<pre class="wp-block-code"><code>&#91;root@localhost ~]# sudo yum install yum-utils -y
Last metadata expiration check: 0:06:57 ago on Wed 06 Jul 2022 02:58:44 PM UTC.
Dependencies resolved.
================================================================================
 Package           Architecture   Version                  Repository      Size
================================================================================
Installing:
 yum-utils         noarch         4.0.24-4.el9_0           baseos          36 k

Transaction Summary
================================================================================
Install  1 Package

Total download size: 36 k
Installed size: 23 k
Downloading Packages:
yum-utils-4.0.24-4.el9_0.noarch.rpm             2.7 MB/s |  36 kB     00:00    
--------------------------------------------------------------------------------
Total                                            71 kB/s |  36 kB     00:00     
Running transaction check
Transaction check succeeded.
Running transaction test
Transaction test succeeded.
Running transaction
  Preparing        :                                                        1/1 
  Installing       : yum-utils-4.0.24-4.el9_0.noarch                        1/1 
  Running scriptlet: yum-utils-4.0.24-4.el9_0.noarch                        1/1 
  Verifying        : yum-utils-4.0.24-4.el9_0.noarch                        1/1 

Installed:
  yum-utils-4.0.24-4.el9_0.noarch                                               

Complete!
</code></pre>

### 2. Add Hashicorp repository  {.wp-block-heading}

Now we need to tell our system where to get installation packages from and that is by adding the Hashicorp repository to our system repositories. To add repositories, we need to use the following command.

<pre class="wp-block-code"><code>sudo yum-config-manager --add-repo https://rpm.releases.hashicorp.com/RHEL/hashicorp.repo</code></pre>

In order to check whether the Hashicorp repository has been added, use the following command.

<pre class="wp-block-code"><code>sudo dnf repolist</code></pre>

You should see something like this:

<pre class="wp-block-code"><code>repo id                         repo name
appstream                       AlmaLinux 9 - AppStream
baseos                          AlmaLinux 9 - BaseOS
extras                          AlmaLinux 9 - Extras
hashicorp                       Hashicorp Stable - x86_64</code></pre>

### 3. Install Terraform on Alma Linux 9 {.wp-block-heading}

After everything is set, we can now install Terraform with the following command.

<pre class="wp-block-code"><code>sudo dnf install terraform -y</code></pre>

The output should look like this.

<pre class="wp-block-code"><code>Hashicorp Stable - x86_64                       690 kB/s | 704 kB     00:01    
Dependencies resolved.
================================================================================
 Package            Architecture    Version            Repository          Size
================================================================================
Installing:
 terraform          x86_64          1.2.4-1            hashicorp           13 M

Transaction Summary
================================================================================
Install  1 Package

Total download size: 13 M
Installed size: 60 M
Downloading Packages:
terraform-1.2.4-1.x86_64.rpm                     11 MB/s |  13 MB     00:01    
--------------------------------------------------------------------------------
Total                                            11 MB/s |  13 MB     00:01     
Hashicorp Stable - x86_64                        20 kB/s | 3.1 kB     00:00    
Importing GPG key 0xA3219F7B:
 Userid     : "HashiCorp Security (HashiCorp Package Signing) &lt;security+packaging@hashicorp.com>"
 Fingerprint: E8A0 32E0 94D8 EB4E A189 D270 DA41 8C88 A321 9F7B
 From       : https://rpm.releases.hashicorp.com/gpg
Key imported successfully
Running transaction check
Transaction check succeeded.
Running transaction test
Transaction test succeeded.
Running transaction
  Preparing        :                                                        1/1 
  Installing       : terraform-1.2.4-1.x86_64                               1/1 
  Verifying        : terraform-1.2.4-1.x86_64                               1/1 

Installed:
  terraform-1.2.4-1.x86_64                                                      

Complete!</code></pre>

To verify if terraform was successfully installed, use the following command.

<pre class="wp-block-code"><code>$ terraform --version
Terraform v1.2.4
on linux_amd64</code></pre>

## Conclusion {.wp-block-heading}

We have successfully installed Terraform on Alma Linux 9. Continue learning more about Terraform using Its rich [documentation.][1]

 [1]: https://learn.hashicorp.com/collections/terraform/docker-get-started