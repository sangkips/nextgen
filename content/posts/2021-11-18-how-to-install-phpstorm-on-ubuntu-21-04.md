---
title: How to install PHPStorm on Ubuntu 21.04
author: Kipkoech Sang
type: post
date: 2021-11-18T18:55:32+00:00
url: /2021/11/18/how-to-install-phpstorm-on-ubuntu-21-04/
rank_math_news_sitemap_robots:
  - index
rank_math_robots:
  - 'a:1:{i:0;s:5:"index";}'
rank_math_analytic_object_id:
  - 96
rank_math_internal_links_processed:
  - 1
categories:
  - Linux

---
In this tutorial guide I will take you through the installation of PHPStorm on Ubuntu 21.04.

PHPStorm is a proprietary, cross-platform IDE for PHP. It provides an editor for PHP, Javascript and HTML with on-the-fly code analysis, error prevention and automated refactoring for PHP and Javascript code. 

## Features of PHPStorm {.wp-block-heading}

PHPStorm is well known for the following features:

  * It has intelligent coding assistance, it provides on-the-fly error prevention, auto-completion and code refactoring.
  * PHPStorm provides a powerful built-in tools for debugging, testing and profiling your applications. 
  * PHPStorm supports all cutting edge web development including SASS, SCSS, LESS, Coffeescript etc.
  * PHPStorm is built on top of open source intellij platform which has been in development for 15 years.

## New Features in PHPStorm 2021.2 {.wp-block-heading}

  * PHPStorm 2021.2 introduces support for generics in PHP. Use of generics via annotations make code safer and more predictable when working with collections, containers etc.
  * One of the most highly anticipated features of PHP 8.1 release is enums. An enum in PHP contain a set of cases, can have methods and can be used as a typed hint.
  * It has introduced support for array shapes. It support code completion for such annotated arrays reducing time spend on routine typing and protecting you from mistakes.
  * It has come with improved extract method refactoring which allows you to split big methods into smaller units and also help eliminate duplicate codes.
  * The release comes with a bunch of inspections to reduce the time spend looking for bugs and fixing them.
  * Now you can run tests before commit 

You don&#8217;t have to install java to run PHPStorm because jetbrains runtime is bundled with IDE.

## Related Articles  {.wp-block-heading}

  1. [How to install Visual Studio Code on Ubuntu 21.10][1]
  2. [How to install Visual Studio Code on Ubuntu 21.04][2]

## Prerequisites  {.wp-block-heading}

  1. Ubuntu 21.04 desktop
  2. User with sudo privileges 
  3. Access to internet

## Table of Contents {.wp-block-heading}

  1. Run updates for Ubuntu 21.04 repositories 
  2. Install PHPStorm as snap package

## 1. Run Ubuntu 21.04 updates {.wp-block-heading}

The first thing we can do is to install recent updates into our Ubuntu 21.04 repositories.

<pre class="wp-block-code"><code>$ sudo apt update
$ sudo apt upgrade -y</code></pre>

After the update and upgrade is complete, let&#8217;s move to install PHPStorm using snap.

## 2. Install PHPStorm using snap package {.wp-block-heading}

I choose to use snap to install PHPStorm because snaps update automatically, PHPStorm will be up to date every time. Use the following to install to install latest version of PHPStorm.

<pre class="wp-block-code"><code>$ sudo snap install phpstorm --classic
phpstorm 2021.2.3 from jetbrains✓ installed</code></pre>

You can now launch PHPStorm by typing **phpstorm** in terminal

Lastly we can learn how to uninstall PHPStorm 

## 3. Uninstall PHPStorm {.wp-block-heading}

When you no longer need PHPStorm you can uninstall the package using the following command:

<pre class="wp-block-code"><code>$ sudo snap remove phpstorm
phpstorm removed</code></pre>

## 4. Conclusion {.wp-block-heading}

We have successfully installed PHPStorm using snap package because it is the best if you do not want to run updates every time. In case of any difficulty you can consult [PHPStorm Documentation][3].

 [1]: https://nextgentips.com/2021/11/06/how-to-install-visual-studio-code-on-ubuntu-21-10/
 [2]: https://nextgentips.com/2021/10/24/how-to-install-visual-studio-code-on-ubuntu-21-04/
 [3]: https://www.jetbrains.com/help/phpstorm/installation-guide.html#snap