---
title: How to install Apache Cassandra 4 on Ubuntu 22.04
author: Kipkoech Sang
type: post
date: 2022-07-07T09:44:09+00:00
url: /2022/07/07/how-to-install-apache-cassandra-4-on-ubuntu-22-04/
categories:
  - Database

---
In this tutorial, we are going to explore how to install Apache Cassandra 4 on our Ubuntu 22.04 server. <a href="https://cassandra.apache.org/_/index.html" target="_blank" rel="noreferrer noopener">Apache Cassandra</a> is an open-source, free, NoSQL database used to store large data. Its linear scalability and proven fault tolerance on commodity hardware or cloud infrastructure make it the perfect platform for mission-critical data. 

NoSQL databases are lightweight, open-source, non-relational, and largely distributed. It has the following strengths:

  * It has horizontal scalability. Cassandra allows for the seamless addition of nodes. A node represents an instance of Cassandra. These nodes do communicate with each other through a protocol called&nbsp;gossip. Gossip is a peer-to-peer communication protocol in which nodes periodically exchange communication about themselves and other nodes they know about.
  * It contains distributed architecture. Distributed means that Cassandra can run on multiple machines while appearing to users as a unified whole. Cassandra databases easily scale when an application is under stress, this prevents data loss from any given data center’s hardware failure.
  * It has a flexible approach to the schema definition. A schema once defined its columns for a table while inserting data in every row, all columns must at least be filled with null value but for Cassandra column families are defined the columns are not. You can add any column to any column family at any time.

Apache Cassandra is suitable for high-volume data types. With the advent of Big data, Cassandra has become the solution to issues that SQL databases were not able to solve.

## Install Cassandra 4 on Ubuntu 22.04 {.wp-block-heading}

### 1. Run system updates  {.wp-block-heading}

Before beginning the installation of Cassandra 4, update your system repositories in order to make them up to date.

<pre class="wp-block-code"><code>sudo apt update && apt upgrade -y</code></pre>

### 2. Install Java {.wp-block-heading}

Ensure java 8 or above is installed because Cassandra depends on Java to run. So let us check if we have java in our system with the following command on our terminal.

<pre class="wp-block-code"><code>$ java version</code></pre>

You should the following output if Java is not installed.

<pre class="wp-block-code"><code>Command 'java' not found, but can be installed with:
apt install openjdk-11-jre-headless  # version 11.0.15+10-0ubuntu0.22.04.1, or
apt install default-jre              # version 2:1.11-72build2
apt install openjdk-17-jre-headless  # version 17.0.3+7-0ubuntu0.22.04.1
apt install openjdk-18-jre-headless  # version 18~36ea-1
apt install openjdk-8-jre-headless   # version 8u312-b07-0ubuntu1</code></pre>

If Java 8 is not installed in your system, Let’s go ahead and install it. The following command can install Java 8 on your system.

<pre class="wp-block-code"><code>$ sudo apt install openjdk-8-jre-headless</code></pre>

It will start installing many dependencies as you can from the output shown below

<pre class="wp-block-code"><code>#output
Reading package lists... Done
Building dependency tree... Done
Reading state information... Done
The following additional packages will be installed:
  ca-certificates-java fontconfig-config fonts-dejavu-core java-common
  libavahi-client3 libavahi-common-data libavahi-common3 libcups2
  libfontconfig1 libjpeg-turbo8 libjpeg8 liblcms2-2 libpcsclite1 libxi6
  libxrender1 libxtst6 x11-common
Suggested packages:
  default-jre cups-common liblcms2-utils pcscd libnss-mdns fonts-dejavu-extra
  fonts-ipafont-gothic fonts-ipafont-mincho fonts-wqy-microhei
  fonts-wqy-zenhei fonts-indic
The following NEW packages will be installed:
  ca-certificates-java fontconfig-config fonts-dejavu-core java-common
  libavahi-client3 libavahi-common-data libavahi-common3 libcups2
  libfontconfig1 libjpeg-turbo8 libjpeg8 liblcms2-2 libpcsclite1 libxi6
  libxrender1 libxtst6 openjdk-8-jre-headless x11-common
0 upgraded, 18 newly installed, 0 to remove and 0 not upgraded.
Need to get 32.7 MB of archives.
After this operation, 110 MB of additional disk space will be used.
Do you want to continue? &#91;Y/n] </code></pre>

Now we can check if Java 8 was installed successfully installed.

<pre class="wp-block-code"><code>$ java -version
openjdk version "1.8.0_312"
OpenJDK Runtime Environment (build 1.8.0_312-8u312-b07-0ubuntu1-b07)
OpenJDK 64-Bit Server VM (build 25.312-b07, mixed mode)</code></pre>

### 3. Install Apache Cassandra. {.wp-block-heading}

To ensure packages are accessible via HTTPS, the apt-transport-HTTPS Package should be installed. This APT transport allows the use of repositories accessed via HTTP Secure protocol. Use the following command to do so

<pre class="wp-block-code"><code>$ sudo apt install apt-transport-https</code></pre>

From the output below apt-transport-HTTPS is there by default.

### 4. Add Public GPG key for Apache Cassandra  {.wp-block-heading}

To communicate with other protocols you must exchange public keys. Add the Apache Cassandra repository keys to the list of trusted keys on the server:

<pre class="wp-block-code"><code>$ wget -q -O - https://www.apache.org/dist/cassandra/KEYS | sudo apt-key add -</code></pre>

We can now go ahead and add Cassandra&#8217;s repository to Cassandra.sources.list files with the following command. You will realize we are using&nbsp;**40x**. This is a corresponding distribution name.

<pre class="wp-block-code"><code>$ sudo sh -c 'echo "deb http://www.apache.org/dist/cassandra/debian 40x main" &gt; /etc/apt/sources.list.d/cassandra.list'</code></pre>

Before we can install Apache Cassandra, run an update to update all the repositories first.

<pre class="wp-block-code"><code>$ sudo apt update
$ sudo apt install cassandra</code></pre>

You should in a position to see the following output.

<pre class="wp-block-code"><code>Reading package lists... Done
Building dependency tree... Done
Reading state information... Done
Suggested packages:
  cassandra-tools
The following NEW packages will be installed:
  cassandra
0 upgraded, 1 newly installed, 0 to remove and 0 not upgraded.
Need to get 47.5 MB of archives.
After this operation, 58.8 MB of additional disk space will be used.
Get:1 https://downloads.apache.org/cassandra/debian 40x/main amd64 cassandra all 4.0.4 &#91;47.5 MB]
Fetched 47.5 MB in 4s (13.3 MB/s)    
Selecting previously unselected package cassandra.
(Reading database ... 73903 files and directories currently installed.)
Preparing to unpack .../cassandra_4.0.4_all.deb ...
Unpacking cassandra (4.0.4) ...
Setting up cassandra (4.0.4) ...
Adding group `cassandra' (GID 117) ...
Done.</code></pre>

Monitor the progress of Cassandras startup with the following command.

<pre class="wp-block-code"><code>tail -f logs/system.log</code></pre>

Cassandra is ready when you see the following output.

<pre class="wp-block-code"><code></code></pre>

You can also check the status of Cassandra with the following command.

<pre class="wp-block-code"><code>$ sudo systemctl status cassandra
● cassandra.service - LSB: distributed storage system for structured data
     Loaded: loaded (/etc/init.d/cassandra; generated)
     Active: active (running) since Thu 2022-07-07 09:33:52 UTC; 3min 12s ago
       Docs: man:systemd-sysv-generator(8)
    Process: 5294 ExecStart=/etc/init.d/cassandra start (code=exited, status=0/&gt;
      Tasks: 44 (limit: 1033)
     Memory: 714.0M
        CPU: 21.527s
     CGroup: /system.slice/cassandra.service
             └─5405 /usr/bin/java -ea -da:net.openhft... -XX:+UseThreadPrioriti&gt;

Jul 07 09:33:52 localhost systemd&#91;1]: Starting LSB: distributed storage system &gt;
Jul 07 09:33:52 localhost systemd&#91;1]: Started LSB: distributed storage system f&gt;
lines 1-13/13 (END)</code></pre>

The status bar should show UN meaning Up and Normal.

Connect to the database with the following command:

<pre class="wp-block-code"><code>$ cqlsh
Connected to Test Cluster at 127.0.0.1:9042
&#91;cqlsh 6.0.0 | Cassandra 4.0.4 | CQL spec 3.4.5 | Native protocol v5]
Use HELP for help.
cqlsh&gt; </code></pre>

You can read more on these related articles 

  * <a href="https://nextgentips.com/2021/10/28/how-to-install-apache-cassandra-4-on-ubuntu-21-10/" target="_blank" rel="noreferrer noopener">How to Install Apache Cassandra 4 on Ubuntu 21.10</a>