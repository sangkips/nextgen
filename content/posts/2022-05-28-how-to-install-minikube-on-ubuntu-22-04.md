---
title: How to install Minikube on Ubuntu 22.04
author: Kipkoech Sang
type: post
date: 2022-05-28T13:11:08+00:00
url: /2022/05/28/how-to-install-minikube-on-ubuntu-22-04/
rank_math_seo_score:
  - 21
rank_math_primary_category:
  - 38
rank_math_internal_links_processed:
  - 1
rank_math_analytic_object_id:
  - 182
categories:
  - Kubernetes

---
<a href="https://minikube.sigs.k8s.io/docs/" target="_blank" rel="noreferrer noopener">Minikube</a>&nbsp;is a tool that makes it easy to run Kubernetes locally. Minikube runs a single-node Kubernetes cluster inside a VM on your local machine. Kubernetes quickly set up a local Kubernetes cluster on Linux, Windows, and macOS.

It is always advisable before diving into Kubernetes, you will need a minimal Kubernetes setup. Such a setup should spin up fast and integrate well with other tools.

In his tutorial, we are going to learn how to install Minikube on Ubuntu 22.04. I am going to use Docker as a containerization platform. You can either VirtualBox, Podman, KVM, etc.

Minikube is the best fit because of the following:

  * It runs on Windows, Linux, and macOS.
  * It supports the latest Kubernetes release
  * It supports multiple container runtimes i.e Containerd, KVM, Docker, Podman, etc
  * It has supports for advanced features such as Load balancing, Featuregates, and filesystems mounts.
  * It has support for Addons. Addons is a marketplace for developers to share configurations for running services on Minikube.
  * It supports CI environments

## Prerequisites {.wp-block-heading}

To run Kubernetes effectively you need to allocate the following to Minikube on your system.

  * 2 CPUs or more
  * 2 GB of memory
  * 20 GB or more of free disk space
  * Reliable internet connections.
  * Container or virtual machine manager such as Docker, KVM, Podman, Virtual Box etc.

## Related Articles {.wp-block-heading}

  * [How to install and configure Minikube on Ubuntu 21.10][1]
  * [How to install and use Minikube on Fedora 35][2]

## Install Minikube on Ubuntu 22.04 {.wp-block-heading}

### 1. Update system repositories {.wp-block-heading}

The first thing we need to do is to update our system repositories in order to make them up to date. Run the following command on your terminal to update our system.

<pre class="wp-block-code"><code>$ sudo apt update && apt upgrade -y</code></pre>

### 2. Install Docker on Ubuntu 20.04 {.wp-block-heading}

As I said from the beginning, we are going to use Docker. Check this article on <a href="https://nextgentips.com/2022/01/01/how-to-install-and-configure-docker-ce-on-ubuntu-20-04/" target="_blank" rel="noreferrer noopener"><a href="https://nextgentips.com/2022/05/09/how-to-install-docker-ce-on-ubuntu-22-04/">How to install Docker-CE on Ubuntu 22.04</a></a>

You can install Docker using the following command on your terminal.

<pre class="wp-block-code"><code>$ sudo apt install docker.io</code></pre>

You need to enable and start docker so that it will start every time you boot your system.

<pre class="wp-block-code"><code># Enable docker
$ sudo systemctl enable docker
# Start docker
$sudo systemctl start docker 
# Check docker status
$ sudo systemctl status docker  </code></pre>

<pre class="wp-block-code"><code># Docker Status 
● docker.service - Docker Application Container Engine
     Loaded: loaded (/lib/systemd/system/docker.service; enabled; vendor preset: enabled)
     Active: active (running) since Sun 2022-01-23 15:32:26 UTC; 5min ago
TriggeredBy: ● docker.socket
       Docs: https://docs.docker.com
   Main PID: 8858 (dockerd)
      Tasks: 10
     Memory: 41.8M
     CGroup: /system.slice/docker.service
             └─8858 /usr/bin/dockerd -H fd:// --containerd=/run/containerd/containerd.sock</code></pre>

Once Docker is installed, we need to add a user into our system because Docker runs in rootless mode.

**Add a user to Docker** with the following command.

<pre class="wp-block-code"><code># add user and add to docker group
$ sudo usermod -aG docker $USER && newgrp docker</code></pre>

When Docker installation is complete then it’s time to install Minikube.

### 3. Install Minikube on Ubuntu 20.04 {.wp-block-heading}

In order to install the latest version release of Minikube, we are going to download it using the curl command.

<pre class="wp-block-code"><code>$ curl -LO https://storage.googleapis.com/minikube/releases/latest/minikube_latest_amd64.deb
sudo dpkg -i minikube_latest_amd64.deb</code></pre>

### 4. Start Minikube {.wp-block-heading}

To start using Minikube we need to start it first with the following command:

<pre class="wp-block-code"><code>$ minikube start</code></pre>

You will see the following output

<pre class="wp-block-code"><code># output
😄  minikube v1.25.2 on Ubuntu 22.04
✨  Automatically selected the docker driver. Other choices: none, ssh
👍  Starting control plane node minikube in cluster minikube
🚜  Pulling base image ...
💾  Downloading Kubernetes v1.23.3 preload ...
    > preloaded-images-k8s-v17-v1...: 505.68 MiB / 505.68 MiB  100.00% 1.20 MiB
    > gcr.io/k8s-minikube/kicbase: 379.06 MiB / 379.06 MiB  100.00% 911.18 KiB 
🔥  Creating docker container (CPUs=2, Memory=2200MB) ...
🐳  Preparing Kubernetes v1.23.3 on Docker 20.10.12 ...
    ▪ kubelet.housekeeping-interval=5m
    ▪ Generating certificates and keys ...
    ▪ Booting up control plane ...
    ▪ Configuring RBAC rules ...
🔎  Verifying Kubernetes components...
    ▪ Using image gcr.io/k8s-minikube/storage-provisioner:v5
🌟  Enabled addons: storage-provisioner, default-storageclass
💡  kubectl not found. If you need it, try: 'minikube kubectl -- get pods -A'
🏄  Done! kubectl is now configured to use "minikube" cluster and "default" namespace by default</code></pre>

We can check minikube version with the following command:

<pre class="wp-block-code"><code>$ minikube version
minikube version: v1.25.2
commit: 362d5fdc0a3dbee389b3d3f1034e8023e72bd3a7</code></pre>

### 5. Install Kubectl on Ubuntu 20.04 {.wp-block-heading}

Let&#8217;s install kubectl using binary install. We are going to use the curl command.

<pre class="wp-block-code"><code>$ curl -LO "https://dl.k8s.io/release/$(curl -L -s https://dl.k8s.io/release/stable.txt)/bin/linux/amd64/kubectl"</code></pre>

Download the following checksum to validate our install

<pre class="wp-block-code"><code>$ curl -LO "https://dl.k8s.io/$(curl -L -s https://dl.k8s.io/release/stable.txt)/bin/linux/amd64/kubectl.sha256"</code></pre>

Then when installation is complete validate with the following command.

<pre class="wp-block-code"><code>$ echo "$(&lt;kubectl.sha256)  kubectl" | sha256sum --check</code></pre>

If valid you will get **kubectl ok** as the output.

**Install Kubectl**

<pre class="wp-block-code"><code>$ sudo install -o root -g root -m 0755 kubectl /usr/local/bin/kubectl</code></pre>

If you ls you will notice that kubectl changes color meaning it’s now executable.

Check if t=Kubectl is installed properly.

<pre class="wp-block-code"><code>$ kubectl version --short 
Flag --short has been deprecated, and will be removed in the future. The --short output will become the default.
Client Version: v1.24.1
Kustomize Version: v4.5.4
Server Version: v1.23.3</code></pre>

Check **kubectl cluster** information with the following command;

<pre class="wp-block-code"><code>$ kubectl cluster-info</code></pre>

If its working correctly then we will see a URL being shown

<pre class="wp-block-code"><code># output
Kubernetes control plane is running at https://192.168.49.2:8443
CoreDNS is running at https://192.168.49.2:8443/api/v1/namespaces/kube-system/services/kube-dns:dns/proxy

To further debug and diagnose cluster problems, use 'kubectl cluster-info dump'.</code></pre>

To check configuration use **kubectl config view**

<pre class="wp-block-code"><code>$ kubectl config view </code></pre>

You will get a very nice json file like the one shown below.

<pre class="wp-block-code"><code># output
apiVersion: v1
clusters:
- cluster:
    certificate-authority: /home/nextgen/.minikube/ca.crt
    extensions:
    - extension:
        last-update: Sat, 28 May 2022 13:40:38 EAT
        provider: minikube.sigs.k8s.io
        version: v1.25.2
      name: cluster_info
    server: https://192.168.49.2:8443
  name: minikube
contexts:
- context:
    cluster: minikube
    extensions:
    - extension:
        last-update: Sat, 28 May 2022 13:40:38 EAT
        provider: minikube.sigs.k8s.io
        version: v1.25.2
      name: context_info
    namespace: default
    user: minikube
  name: minikube
current-context: minikube
kind: Config
preferences: {}
users:
- name: minikube
  user:
    client-certificate: /home/nextgen/.minikube/profiles/minikube/client.crt
    client-key: /home/nextgen/.minikube/profiles/minikube/client.key</code></pre>

In order to access your cluster using kubectl use the following command.

<pre class="wp-block-code"><code>$ kubectl get po -A
NAMESPACE     NAME                               READY   STATUS    RESTARTS      AGE
kube-system   coredns-64897985d-hgvbk            1/1     Running   0             30m
kube-system   etcd-minikube                      1/1     Running   0             30m
kube-system   kube-apiserver-minikube            1/1     Running   0             30m
kube-system   kube-controller-manager-minikube   1/1     Running   0             30m
kube-system   kube-proxy-6c48r                   1/1     Running   0             30m
kube-system   kube-scheduler-minikube            1/1     Running   0             30m
kube-system   storage-provisioner                1/1     Running   1 (29m ago)   30m
</code></pre>

### 6. Enable Kubernetes Dashboard {.wp-block-heading}

Kubernetes comes with a dashboard where you will manage your clusters. To enable the dashboard, use the following command:

<pre class="wp-block-code"><code>$ minikube dashboard</code></pre>

Running the following command will get us the following output.

<pre class="wp-block-code"><code>#output
🔌  Enabling dashboard ...
    ▪ Using image kubernetesui/dashboard:v2.3.1
    ▪ Using image kubernetesui/metrics-scraper:v1.0.7
🤔  Verifying dashboard health ...
🚀  Launching proxy ...
🤔  Verifying proxy health ...
🎉  Opening http://127.0.0.1:35251/api/v1/namespaces/kubernetes-dashboard/services/http:kubernetes-dashboard:/proxy/ in your default browser...</code></pre>

It will then open your dashboard on your default browser. You can now interact easily with your deployments.

Whenever you don&#8217;t want to start minikube dashboard on your browser, you can prefix with &#8211;url

<pre class="wp-block-code"><code>minikube dashboard --url</code></pre>

### 7. Create a deployment and a service  {.wp-block-heading}

A service is an abstraction that defines a logical set of Pods and a policy by which to access them. Pods are the smallest execution unit in Kubernetes. It encapsulates one or more applications. Kubernetes Pods are created and destroyed to match the desired state of your cluster. Pods are only accessible by its internal IP address within the kubernetes cluster.

Let&#8217;s make an hello-node. Follow the following steps.

#### Create deployment  {.wp-block-heading}

To create a deployment we use **kubectl create** command 

<pre class="wp-block-code"><code>$ kubectl create deployment hello-node --image=k8s.gcr.io/echoserver:1.4</code></pre>

Sample output will look like this 

<pre class="wp-block-code"><code> > kubectl.sha256: 64 B / 64 B &#91;--------------------------] 100.00% ? p/s 0s
    > kubectl: 30.80 KiB / 44.43 MiB &#91;>_________________________] 0.07% ? p/s     > kubectl: 46.80 KiB / 44.43 MiB &#91;>_________________________] 0.10% ? p/s     > kubectl: 78.79 KiB / 44.43 MiB &#91;>_________________________] 0.17% ? p/s     > kubectl: 286.79 KiB / 44.43 MiB &#91;>_______] 0.63% 426.90 KiB p/s ETA 1m45    > kubectl: 558.79 KiB / 44.43 MiB &#91;>_______] 1.23% 426.90 KiB p/s ETA 1m45    > kubectl: 894.79 KiB / 44.43 MiB &#91;>_______] 1.97% 426.90 KiB p/s ETA 1m44    > kubectl: 1.61 MiB / 44.43 MiB &#91;>_________] 3.62% 545.52 KiB p/s ETA 1m20    > kubectl: 1.61 MiB / 44.43 MiB &#91;>_________] 3.62% 545.52 KiB p/s ETA 1m20</code></pre>

To view the deployments, we use **kubectl get deployments**

<pre class="wp-block-code"><code>kubectl get deployments</code></pre>

The sample output will look like this.

<pre class="wp-block-code"><code>NAME         READY   UP-TO-DATE   AVAILABLE   AGE
hello-node   1/1     1            1           115s</code></pre>

To view the pods use the kubectl get pods command.

<pre class="wp-block-code"><code>kubectl get pods </code></pre>

The sample output will look like this 

<pre class="wp-block-code"><code>NAME                          READY   STATUS    RESTARTS   AGE
hello-node-6b89d599b9-mlv7q   1/1     Running   0          5m23s</code></pre>

After we have created deployment we need to expose the Pod to the public internet with **kubectl expose** command.

<pre class="wp-block-code"><code>kubectl expose deployment hello-node &lt;strong>--type=LoadBalancer&lt;/strong> --port=8080</code></pre>

**&#8211;type=LoadBalancer** expose the service outside the cluster

You will get an output **service/hello-node exposed**.

To view the services you have created we use **kubectl get services** command.

<pre class="wp-block-code"><code>kubectl get services </code></pre>

This is the output you will get.

<pre class="wp-block-code"><code>NAME         TYPE           CLUSTER-IP    EXTERNAL-IP   PORT(S)          AGE
hello-node   LoadBalancer   10.97.64.87   &lt;pending>     8080:30179/TCP   33m
kubernetes   ClusterIP      10.96.0.1     &lt;none>        443/TCP          133m</code></pre>

To stop minikube virtual machine we use **minikube stop**

<pre class="wp-block-code"><code>minikube stop</code></pre>

To delete minikube virtual machine use **minikube delete**

<pre class="wp-block-code"><code>minikube delete</code></pre>

To stop and delete the services we have created above we use the following command.

<pre class="wp-block-code"><code>$ kubectl delete service hello-node
$ kubectl delete deployment hello-node</code></pre>

## Conclusion {.wp-block-heading}

We have successfully installed Minikube on Ubuntu 22.04. If you face any challenges do give us a comment and we will be happy to assist.

 [1]: https://nextgentips.com/2021/12/24/how-to-install-and-configure-minikube-on-ubuntu-21-10/
 [2]: https://nextgentips.com/2021/12/18/how-to-install-and-configure-minikube-on-fedora-35/