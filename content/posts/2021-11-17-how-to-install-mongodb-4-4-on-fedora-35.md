---
title: How to install MongoDB 4.4 on Fedora 35
author: Kipkoech Sang
type: post
date: 2021-11-17T09:28:18+00:00
url: /2021/11/17/how-to-install-mongodb-4-4-on-fedora-35/
rank_math_news_sitemap_robots:
  - index
rank_math_robots:
  - 'a:1:{i:0;s:5:"index";}'
rank_math_analytic_object_id:
  - 99
rank_math_internal_links_processed:
  - 1
categories:
  - Database

---
In this tutorial we are going to explore how to install MongoDB on fedora 35.

MongoDB is a free and open source document database designed for ease of application development and scaling.

Every record in a mongoDB document, which is a data structure composed of field and pair values. MongoDB stores documents in collection. Collections are analogous to tables in relational databases.

## Key Features of MongoDB {.wp-block-heading}

  * MongoDB provides high performance to data persistence 
  * MongoDB has rich query language 
  * MongoDB&#8217;s replication facility called replica set provides automatic failover and data redundancy increasing data high availability.
  * MongoDB supports horizontal scalability.
  * MongoDB supports multiple storage engines.

## Related Articles  {.wp-block-heading}

  1. [How to Deploy MongoDB with Docker][1]
  2. [How to install MongoDB 5 on Ubuntu 21.04][2]

## Prerequisites  {.wp-block-heading}

  1. Fedora server
  2. Access to internet 
  3. User with sudo privileges

## Table of Contents {.wp-block-heading}

  1. Update your Fedora repositories 
  2. Configure the package management system
  3. Install MongoDB packages 
  4. Conclusion

## 1. Update Fedora Repositories  {.wp-block-heading}

The first thing we need to do is to update our Fedora repository to reflect on recent changes done to the system operating system.

<pre class="wp-block-code"><code>$ sudo dnf update -y</code></pre>

After updates are complete you can proceed to configure Fedora package management.

Fedora users need to approach [upstream][3] in order to install MongoDB because of the licence changes which was introduced. You can not install directly because MongoDB is not found on the Fedora repositories. The best choice we can install Fedora server is to use RHEL-8 RPMs 

## 2. Create a Repository with Upstream RHEL-8 RPM {.wp-block-heading}

I am following this route because I need to install specific version of MongoDB. I will be installing MongoDB version 5.0 because it is the current version. To do that we need to do the following

Let me show you how it is done with the following steps.

<pre class="wp-block-code"><code>$ cd /etc/yum.repos.d
$ sudo touch mongodb-org-4.4.repo</code></pre>

When that is done, insert the following into mongodb-org.repo. 

<pre class="wp-block-code"><code>$ sudo vi /etc/yum.repos.d/mongodb-org-4.4.repo</code></pre>

<pre class="wp-block-code"><code>
&#91;mongodb-upstream]
name=MongoDB Repository
baseurl=https://repo.mongodb.org/yum/redhat/$releasever/mongodb-org/4.4/x86_64/
gpgcheck=1
enabled=1
gpgkey=https://www.mongodb.org/static/pgp/server-4.4.asc</code></pre>

## 3. Install MongoDB Packages {.wp-block-heading}

You are now good to install Mongodb into your system with the following command.

<pre class="wp-block-code"><code>$ sudo dnf install mongodb-org -y</code></pre>

Sample output

<pre class="wp-block-code"><code> Installed:
  cyrus-sasl-2.1.27-13.fc35.x86_64                 cyrus-sasl-gssapi-2.1.27-13.fc35.x86_64                      
  cyrus-sasl-plain-2.1.27-13.fc35.x86_64           fontconfig-2.13.94-3.fc35.x86_64                             
  libX11-1.7.2-3.fc35.x86_64                       libX11-common-1.7.2-3.fc35.noarch                            
  libXau-1.0.9-7.fc35.x86_64                       libXft-2.3.3-7.fc35.x86_64                                   
  libXrender-0.9.10-15.fc35.x86_64                 libpkgconf-1.8.0-1.fc35.x86_64                               
  libxcb-1.13.1-8.fc35.x86_64                      mongodb-database-tools-100.5.1-1.x86_64                      
  mongodb-org-4.4.4-1.el8.x86_64                   mongodb-org-database-tools-extra-4.4.4-1.el8.x86_64          
  mongodb-org-mongos-4.4.4-1.el8.x86_64            mongodb-org-server-4.4.4-1.el8.x86_64                        
  mongodb-org-shell-4.4.4-1.el8.x86_64             mongodb-org-tools-4.4.4-1.el8.x86_64                         
  openssl-1:1.1.1l-2.fc35.x86_64                   pkgconf-1.8.0-1.fc35.x86_64                                  
  pkgconf-m4-1.8.0-1.fc35.noarch                   pkgconf-pkg-config-1.8.0-1.fc35.x86_64                       
  python2.7-2.7.18-15.fc35.x86_64                  tcl-1:8.6.10-5.fc34.x86_64                                   
  tix-1:8.4.3-32.fc35.x86_64                       tk-1:8.6.10-7.fc35.x86_64                                    
  xml-common-0.6.3-57.fc35.noarch                 
Skipped:
  mongodb-org-4.4.10-1.el8.x86_64                 mongodb-org-database-tools-extra-4.4.10-1.el8.x86_64          
  mongodb-org-tools-4.4.10-1.el8.x86_64          

Complete!</code></pre>

## 4. Start MongoDB  {.wp-block-heading}

After the installation is complete we can start MongoDB with the following command;

<pre class="wp-block-code"><code>$ sudo service mongod start</code></pre>

Then you can check the status of MongoDB with the following command

<pre class="wp-block-code"><code>$ sudo systemctl status mongod.service </code></pre>

You should be able to see the following as output

<pre class="wp-block-code"><code>● mongod.service - MongoDB Database Server
     Loaded: loaded (/usr/lib/systemd/system/mongod.service; enabled; vendor preset: disabled)
     Active: active (running) since Wed 2021-11-17 09:08:49 UTC; 15s ago
       Docs: https://docs.mongodb.org/manual
    Process: 18507 ExecStartPre=/usr/bin/mkdir -p /var/run/mongodb (code=exited, status=0/SUCCESS)
    Process: 18508 ExecStartPre=/usr/bin/chown mongod:mongod /var/run/mongodb (code=exited, status=0/SUCCESS)
    Process: 18509 ExecStartPre=/usr/bin/chmod 0755 /var/run/mongodb (code=exited, status=0/SUCCESS)
    Process: 18510 ExecStart=/usr/bin/mongod $OPTIONS (code=exited, status=0/SUCCESS)
   Main PID: 18512 (mongod)
     Memory: 59.1M
        CPU: 648ms
     CGroup: /system.slice/mongod.service
             └─18512 /usr/bin/mongod -f /etc/mongod.conf

Nov 17 09:08:49 fedora-35 systemd&#91;1]: Starting MongoDB Database Server...
Nov 17 09:08:49 fedora-35 mongod&#91;18510]: about to fork child process, waiting until server is ready for connecti>
Nov 17 09:08:49 fedora-35 mongod&#91;18512]: forked process: 18512
Nov 17 09:08:49 fedora-35 mongod&#91;18510]: child process started successfully, parent exiting
Nov 17 09:08:49 fedora-35 systemd&#91;1]: Started MongoDB Database Server.</code></pre>

To verify the version of MongoDB you have installed, use the following command to check available version.

<pre class="wp-block-code"><code>$ mongod --version
db version v4.4.4
Build Info: {
    "version": "4.4.4",
    "gitVersion": "8db30a63db1a9d84bdcad0c83369623f708e0397",
    "openSSLVersion": "OpenSSL 1.1.1l  FIPS 24 Aug 2021",
    "modules": &#91;],
    "allocator": "tcmalloc",
    "environment": {
        "distmod": "rhel80",
        "distarch": "x86_64",
        "target_arch": "x86_64"
    }
}</code></pre>

Lastly we can start mongod shell with the following 

<pre class="wp-block-code"><code>$ mongo
MongoDB shell version v4.4.4
connecting to: mongodb://127.0.0.1:27017/?compressors=disabled&gssapiServiceName=mongodb
Implicit session: session { "id" : UUID("d7b4c9a0-9243-463b-bb7e-920737acfa7c") }
MongoDB server version: 4.4.4
Welcome to the MongoDB shell.
For interactive help, type "help".
For more comprehensive documentation, see
        https:&#47;&#47;docs.mongodb.com/
Questions? Try the MongoDB Developer Community Forums
        https://community.mongodb.com
---
The server generated these startup warnings when booting: 
        2021-11-17T09:08:49.637+00:00: Access control is not enabled for the database. Read and write access to data and configuration is unrestricted
---
---
        Enable MongoDB's free cloud-based monitoring service, which will then receive and display
        metrics about your deployment (disk utilization, CPU, operation statistics, etc).

        The monitoring data will be available on a MongoDB website with a unique URL accessible to you
        and anyone you share the URL with. MongoDB may use this information to make product
        improvements and to suggest MongoDB products and deployment options to you.

        To enable free monitoring, run the following command: db.enableFreeMonitoring()
        To permanently disable this reminder, run the following command: db.disableFreeMonitoring()
---
> </code></pre>

## 5. Conclusion {.wp-block-heading}

We have installed MongoDB 4.4 on our Fedora 35 successfully. Do consult [MongoDB documentation][4] for further reading.

 [1]: https://nextgentips.com/2021/10/26/how-to-deploy-mongodb-with-docker/
 [2]: https://nextgentips.com/2021/10/26/how-to-install-mongodb-5-on-ubuntu-21-04/
 [3]: https://www.redhat.com/en/blog/what-open-source-upstream
 [4]: https://developer.fedoraproject.org/tech/database/mongodb/about.html