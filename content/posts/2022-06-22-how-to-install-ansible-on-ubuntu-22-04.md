---
title: How to install Ansible on Ubuntu 22.04
author: Kipkoech Sang
type: post
date: 2022-06-22T15:11:47+00:00
url: /2022/06/22/how-to-install-ansible-on-ubuntu-22-04/
rank_math_internal_links_processed:
  - 1
rank_math_seo_score:
  - 15
rank_math_primary_category:
  - 37
rank_math_analytic_object_id:
  - 189
categories:
  - Automation

---
<a href="https://www.redhat.com/en/technologies/management/ansible" target="_blank" rel="noreferrer noopener">Ansible</a> is an open-source software provisioning, configuration management, and application deployment tool enabling infrastructure as code. It runs both on Unix-like systems and on Windows platforms.

Ansible is a radically simple IT automation engine that automates cloud provisioning, configuration management, application deployment, intra-service orchestration, etc.

Ansible uses no agents and no additional custom security infrastructure. It is easy to deploy because it uses YAML file systems that allow you to describe your deployment environment in plain English.

It is an agentless automation tool installed on a single host called a control node. It is from the control node that Ansible starts managing the entire machine called managed nodes via remote SSH controls.

### Requirements to run Ansible {.wp-block-heading}

  * Unix machine with Python 3.8 or newer.
  * User with sudo privileges

## Installing Ansible on Ubuntu 22.04 {.wp-block-heading}

### 1. Run system updates {.wp-block-heading}

To begin with we need to run system updates in order to make our repositories up to date. Open your terminal and type in the following code.

<pre class="wp-block-code"><code>$ sudo apt update && apt upgrade -y</code></pre>

When updates are complete, move forward to install pip.

### 2. Install Pip {.wp-block-heading}

If pip is not already installed, we can do install it with the following command;

<pre class="wp-block-code"><code>$ curl https://bootstrap.pypa.io/get-pip.py -o get-pip.py</code></pre>

After you have finished bootstrapping pip then run the following;

<pre class="wp-block-code"><code>$ python3 get-pip.py --user</code></pre>

You will get the following output.

<pre class="wp-block-code"><code>Output
Collecting pip
  Downloading pip-22.1.2-py3-none-any.whl (2.1 MB)
     ━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━ 2.1/2.1 MB 36.0 MB/s eta 0:00:00
Collecting wheel
  Downloading wheel-0.37.1-py2.py3-none-any.whl (35 kB)
Installing collected packages: wheel, pip
 
Successfully installed pip-22.1.2 wheel-0.37.1
</code></pre>

Once pip is installed then it’s time for us to install Ansible.

### 3. Installing Ansible with pip {.wp-block-heading}

Install Ansible with the following command;

<pre class="wp-block-code"><code>$ sudo python3 -m pip install --user ansible</code></pre>

Sample output you will get.

<pre class="wp-block-code"><code>Output
Collecting ansible
  Downloading ansible-6.0.0-py3-none-any.whl (40.3 MB)
     ━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━ 40.3/40.3 MB 29.7 MB/s eta 0:00:00
Collecting ansible-core~=2.13.0
  Downloading ansible_core-2.13.1-py3-none-any.whl (2.1 MB)
     ━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━ 2.1/2.1 MB 55.3 MB/s eta 0:00:00
Requirement already satisfied: PyYAML>=5.1 in /usr/lib/python3/dist-packages (from ansible-core~=2.13.0->ansible) (5.4.1)
Collecting packaging
  Downloading packaging-21.3-py3-none-any.whl (40 kB)
     ━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━ 40.8/40.8 kB 79.3 kB/s eta 0:00:00
Requirement already satisfied: jinja2>=3.0.0 in /usr/lib/python3/dist-packages (from ansible-core~=2.13.0->ansible) (3.0.3)
Requirement already satisfied: cryptography in /usr/lib/python3/dist-packages (from ansible-core~=2.13.0->ansible) (3.4.8)
Collecting resolvelib&lt;0.9.0,>=0.5.3
  Downloading resolvelib-0.8.1-py2.py3-none-any.whl (16 kB)
Requirement already satisfied: pyparsing!=3.0.5,>=2.0.2 in /usr/lib/python3/dist-packages (from packaging->ansible-core~=2.13.0->ansible) (2.4.7)
Installing collected packages: resolvelib, packaging, ansible-core, ansible
Successfully installed ansible-6.0.0 ansible-core-2.13.1 packaging-21.3 resolvelib-0.8.1</code></pre>

To upgrade Ansible to a newer version, We upgrade the command on our terminal.

<pre class="wp-block-code"><code>$ python3 -m pip install --upgrade --user ansible</code></pre>

Check the version of the installed version.

<pre class="wp-block-code"><code>&lt;strong>$ python3 -m pip show ansible&lt;/strong>
&lt;strong># output&lt;/strong>
Name: ansible
Version: 6.0.0
Summary: Radically simple IT automation
Home-page: https://ansible.com/
Author: Ansible, Inc.
Author-email: info@ansible.com
License: GPLv3+
Location: /root/.local/lib/python3.10/site-packages
Requires: ansible-core
Required-by: </code></pre>

## Installing Ansible via PPA packages {.wp-block-heading}

Ansible Ubuntu builds are available in PPA repositories.

First Run system updates on your system. 

<pre class="wp-block-code"><code>$ sudo apt update </code></pre>

## 1. Install prerequisites {.wp-block-heading}

We need to install Ansible prerequisites to aid in the installation of Ansible. Run the following command;

<pre class="wp-block-code"><code>$ sudo apt install software-properties-common</code></pre>

### 2. Add PPA Repository to Ubuntu 22.04 {.wp-block-heading}

To add the Ubuntu PPA repository to our system, run the following command on your terminal.

<pre class="wp-block-code"><code>$ sudo add-apt-repository --yes --update ppa:ansible/ansible</code></pre>

Then run updates again

### 3. Install Ansible on Ubuntu 22.04 {.wp-block-heading}

Run the following command on your terminal to install Ansible.

<pre class="wp-block-code"><code>$ sudo apt install ansible</code></pre>

Press Y to allow installation to continue, dependencies will be installed in the process.

We can check the version with the following command;

<pre class="wp-block-code"><code>$ python3 -m pip show ansible</code></pre>

<pre class="wp-block-code"><code>output
Name: ansible
Version: 6.0.0
Summary: Radically simple IT automation
Home-page: https://ansible.com/
Author: Ansible, Inc.
Author-email: info@ansible.com
License: GPLv3+
Location: /root/.local/lib/python3.10/site-packages
Requires: ansible-core
Required-by: </code></pre>

## Uninstall Ansible {.wp-block-heading}

If you will like to remove ansible use the following command.

<pre class="wp-block-code"><code>$ sudo apt remove ansible</code></pre>

It will remove Ansible with its all dependencies.

## Conclusion {.wp-block-heading}

We have successfully installed Ansible in our Ubuntu 22.04. Continue learning more with <a href="https://docs.ansible.com/ansible/latest/installation_guide/intro_installation.html#prerequisites" target="_blank" rel="noreferrer noopener">Ansible documentation.</a>