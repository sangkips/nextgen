---
title: How to install Ruby on Manjaro Linux
author: Kipkoech Sang
type: post
date: 2022-11-16T08:17:34+00:00
url: /2022/11/16/how-to-install-ruby-on-manjaro-linux/
categories:
  - Programming

---
Installing Ruby is a bit of a hustle simply because Ruby on Rails has many dependencies. It depends on Sqlite3, node, and yarn which sometimes becomes tiresome to start getting all the dependencies together. 

## Ways of installing Ruby {.wp-block-heading}

  * You can use package managers on Unix-like systems. This method always has outdated versions of Ruby which is something we don&#8217;t want.
  * Building Ruby from the source also sometimes gets a bit challenging 
  * Using installers, this method is the best because installers are always up to date. I advise one to use this method. Even we have installers for windows operating systems.
  * Also, there are Managers which help you to switch between Ruby versions on your system.

### Install Ruby on Manjaro using the package manager {.wp-block-heading}

Installing Ruby on Manjaro using the package manager gives you the latest release version. Let&#8217;s try out this method.

First, update your system repositories, use the following to accomplish that.

<pre class="wp-block-code"><code>sudo pacman -Syu</code></pre>

Secondly, let&#8217;s install Ruby on our Manjaro with the following command.

<pre class="wp-block-code"><code>sudo pacman -S ruby</code></pre>

The installation will take some time to finish, you will be in a position to see a lot of warnings like this:

<pre class="wp-block-code"><code>looking for conflicting packages...
warning: dependency cycle detected:
warning: rubygems will be installed before its ruby dependency
warning: dependency cycle detected:
warning: ruby-abbrev will be installed before its ruby dependency
warning: dependency cycle detected:
warning: ruby-base64 will be installed before its ruby dependency
warning: dependency cycle detected:
warning: ruby-benchmark will be installed before its ruby dependency
warning: dependency cycle detected:
warning: ruby-bigdecimal will be installed before its ruby dependency
warning: dependency cycle detected:
warning: ruby-bundler will be installed before its ruby dependency
warning: dependency cycle detected:
warning: ruby-cgi will be installed before its ruby dependency
warning: dependency cycle detected:
warning: ruby-csv will be installed before its ruby dependency
warning: dependency cycle detected:
warning: ruby-date will be installed before its ruby dependency
warning: dependency cycle detected:
warning: ruby-delegate will be installed before its ruby dependency
warning: dependency cycle detected:
warning: ruby-did_you_mean will be installed before its ruby dependency
warning: dependency cycle detected:
warning: ruby-digest will be installed before its ruby dependency
warning: dependency cycle detected:
warning: ruby-drb will be installed before its ruby dependency
warning: dependency cycle detected:
warning: ruby-ruby2_keywords will be installed before its ruby dependency
warning: dependency cycle detected:
warning: ruby-english will be installed before its ruby dependency
warning: dependency cycle detected:
warning: ruby-erb will be installed before its ruby dependency
warning: dependency cycle detected:
warning: ruby-etc will be installed before its ruby dependency
warning: dependency cycle detected:
warning: ruby-fcntl will be installed before its ruby dependency
warning: dependency cycle detected:
warning: ruby-fiddle will be installed before its ruby dependency
warning: dependency cycle detected:
warning: ruby-fileutils will be installed before its ruby dependency
warning: dependency cycle detected:
warning: ruby-find will be installed before its ruby dependency
warning: dependency cycle detected:
warning: ruby-forwardable will be installed before its ruby dependency
warning: dependency cycle detected:
warning: ruby-getoptlong will be installed before its ruby dependency
warning: dependency cycle detected:
warning: ruby-io-console will be installed before its ruby dependency
warning: dependency cycle detected:
warning: ruby-io-nonblock will be installed before its ruby dependency
warning: dependency cycle detected:
warning: ruby-io-wait will be installed before its ruby dependency
warning: dependency cycle detected:
warning: ruby-ipaddr will be installed before its ruby dependency
warning: dependency cycle detected:
warning: ruby-irb will be installed before its ruby dependency
warning: dependency cycle detected:
warning: ruby-reline will be installed before its ruby dependency
warning: dependency cycle detected:
warning: ruby-json will be installed before its ruby dependency
warning: dependency cycle detected:
warning: ruby-logger will be installed before its ruby dependency
warning: dependency cycle detected:
warning: ruby-mutex_m will be installed before its ruby dependency
warning: dependency cycle detected:
warning: ruby-net-http will be installed before its ruby dependency
warning: dependency cycle detected:
warning: ruby-uri will be installed before its ruby dependency
warning: dependency cycle detected:
warning: ruby-open-uri will be installed before its ruby dependency
warning: dependency cycle detected:
warning: ruby-stringio will be installed before its ruby dependency
warning: dependency cycle detected:
warning: ruby-time will be installed before its ruby dependency
warning: dependency cycle detected:
warning: ruby-psych will be installed before its ruby dependency
warning: dependency cycle detected:
warning: ruby-racc will be installed before its ruby dependency
warning: dependency cycle detected:
warning: ruby-rdoc will be installed before its ruby dependency
warning: dependency cycle detected:
warning: ruby-tmpdir will be installed before its ruby dependency
warning: dependency cycle detected:
warning: ruby-minitest will be installed before its ruby dependency
warning: dependency cycle detected:
warning: ruby-power_assert will be installed before its ruby dependency
warning: dependency cycle detected:
warning: ruby-rake will be installed before its ruby dependency
warning: dependency cycle detected:
warning: ruby-rexml will be installed before its ruby dependency
warning: dependency cycle detected:
warning: ruby-test-unit will be installed before its ruby dependency

Packages (49) ruby-abbrev-0.1.0-3  ruby-base64-0.1.1-3  ruby-benchmark-0.2.0-3
              ruby-bigdecimal-3.1.2-3  ruby-bundledgems-3.0.4-20
              ruby-bundler-2.3.23-1  ruby-cgi-0.3.3-1  ruby-csv-3.2.5-3
              ruby-date-3.2.2-3  ruby-delegate-0.2.0-3
              ruby-did_you_mean-1.6.1-3  ruby-digest-3.1.0-5  ruby-drb-2.1.0-4
              ruby-english-0.7.1-3  ruby-erb-2.2.3-4  ruby-etc-1.3.0-5
              ruby-fcntl-1.0.1-3  ruby-fiddle-1.1.0-3  ruby-fileutils-1.6.0-3
              ruby-find-0.1.1-3  ruby-forwardable-1.3.2-5
              ruby-getoptlong-0.1.1-2  ruby-io-console-0.5.11-2
              ruby-io-nonblock-0.1.0-2  ruby-io-wait-0.2.3-3
              ruby-ipaddr-1.2.4-2  ruby-irb-1.4.2-1  ruby-json-2.6.2-2
              ruby-logger-1.5.1-2  ruby-minitest-5.16.3-1  ruby-mutex_m-0.1.1-2
              ruby-net-http-0.2.2-2  ruby-open-uri-0.2.0-3
              ruby-power_assert-2.0.2-1  ruby-psych-4.0.6-1  ruby-racc-1.6.0-3
              ruby-rake-13.0.6-1  ruby-rdoc-6.4.0-4  ruby-reline-0.3.1-2
              ruby-rexml-3.2.5-1  ruby-ruby2_keywords-0.0.5-1
              ruby-stdlib-3.0.4-20  ruby-stringio-3.0.2-4
              ruby-test-unit-3.5.5-1  ruby-time-0.2.0-4  ruby-tmpdir-0.1.2-3
              ruby-uri-0.11.0-5  rubygems-3.3.25-1  ruby-3.0.4-20

Total Download Size:    6.37 MiB
Total Installed Size:  25.35 MiB
</code></pre>

After the installation process is complete, we can check the version to ascertain its version.

<pre class="wp-block-code"><code>ruby -v
ruby 3.0.4p208 (2022-04-12 revision 3fa771dded) &#91;x86_64-linux]</code></pre>

We have successfully installed Ruby. Next article, we will see <a href="https://nextgentips.com/2022/11/17/how-to-install-rails-7-on-manjaro-linux/" target="_blank" rel="noopener" title="">how to install Rails 7.0 on Manjaro Linux</a>