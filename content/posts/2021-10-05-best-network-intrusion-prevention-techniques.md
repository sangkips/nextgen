---
title: Best network intrusion prevention techniques
author: Kipkoech Sang
type: post
date: 2021-10-05T18:41:13+00:00
url: /2021/10/05/best-network-intrusion-prevention-techniques/
site-sidebar-layout:
  - default
site-content-layout:
  - default
theme-transparent-header-meta:
  - default
entry_views:
  - 1
view_ip:
  - 'a:1:{i:0;s:15:"174.128.180.180";}'
rank_math_primary_category:
  - 8
rank_math_news_sitemap_robots:
  - index
rank_math_robots:
  - 'a:1:{i:0;s:5:"index";}'
rank_math_analytic_object_id:
  - 146
rank_math_internal_links_processed:
  - 1
categories:
  - Security

---
## What is network intrusion? {.wp-block-heading}

A network intrusion is an unauthorized penetration of a computer in an enterprise or an address in your assigned domain. An intrusion can be passive or active. Intrusion can come from outside your network structure or inside. 

As a first window of defense, here&#8217;s a brief summary of attack methods.

## Traffic flooding {.wp-block-heading}

Traffic food is a type of Denial of Service (DOS) attack targeting web servers. The attacker creates too many traffic loads which makes the system unable to screen. In the process attackers can launch undetected attacks.

This attack explores a characteristic of HTTP protocol, opening too many connections at the same time to attend a single request. This results in two different kind of exploitation, the connect attack done during the establishment of a connection and a closing attack done during closing stage of a connection.

## Worms {.wp-block-heading}

Worms are any computer code intended to replicate itself without altering authorized program files. They often spread through email attachments or the Internet Relay Chat (IRC) protocol. 

## Trojans  {.wp-block-heading}

Trojans create network backdoors that give attackers easy access to systems and any available data. Unlike other viruses and worms, Trojans don’t reproduce by infecting other files, and they don’t self-replicate. Trojans can be introduced from online archives and file repositories, and often originate from peer-to-peer file exchanges.

## Buffer overwriting  {.wp-block-heading}

This approach attempts to overwrite specific sections of computer memory within a network, replacing normal data in those memory locations with a set of commands that will later be executed as part of the attack.

## Intrusion prevention techniques  {.wp-block-heading}

  * **Ending the current TCP session**. A passive sensor can attempt to end an existing TCP session by sending TCP reset packets to both endpoints; this is sometimes called session sniping. The sensor does this to make it appear to each endpoint that the other endpoint is trying to end the connection. The goal is for one of the endpoints to terminate the connection before an attack can succeed.
  * **Performing inline firewalling**. Most inline IDPS sensors offer firewall capabilities that can be used to drop or reject suspicious network activity.
  * **Throttling bandwidth usage.** If a particular protocol is being used inappropriately, such as for a DoS attack, malware distribution, or peer-to-peer file sharing, some inline IDPS sensors can limit the percentage of network bandwidth that the protocol can use. This prevents the activity from negatively impacting bandwidth usage for other resources.
  * **Reconfiguring Other Network Security Devices**. Many IDPS sensors can instruct network security devices such as firewalls, routers, and switches to reconfigure themselves to block certain types of activity or route it elsewhere. This can be helpful in several situations, such as keeping an external attacker out of a network and quarantining an internal host that has been compromised (e.g., moving it to a quarantine VLAN). This prevention technique is useful only for network traffic that can be differentiated by packet header characteristics typically recognized by network security devices, such as IP addresses and port numbers.
  * **Running a Third-Party Program or Script.** Some IDPS sensors can run an administrator specified script or program when certain malicious activity is detected. This could trigger any prevention action desired by the administrator, such as reconfiguring other security devices to block the malicious activity. Third-party programs or scripts are most commonly used when the IDPS does not support the prevention actions that administrators want to have performed.

## Conclusion {.wp-block-heading}

Organizations should consider using management networks for their network-based IDPS deployments whenever feasible. If an IDPS is deployed without a separate management network, organizations should consider whether or not a VLAN is needed to protect the IDPS communications.