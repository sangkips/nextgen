---
title: How to install and configure CyberPanel on CentOS 8
author: Kipkoech Sang
type: post
date: 2021-11-13T09:04:49+00:00
url: /2021/11/13/how-to-install-and-configure-cyberpanel-on-centos-8/
ocean_gallery_link_images:
  - off
ocean_disable_margins:
  - enable
ocean_display_top_bar:
  - default
ocean_display_header:
  - default
ocean_disable_title:
  - default
ocean_disable_heading:
  - default
ocean_disable_breadcrumbs:
  - default
ocean_display_footer_widgets:
  - default
ocean_display_footer_bottom:
  - default
ocean_link_format_target:
  - self
ocean_quote_format_link:
  - post
entry_views:
  - 1
view_ip:
  - 'a:1:{i:0;s:14:"114.119.132.18";}'
rank_math_news_sitemap_robots:
  - index
rank_math_robots:
  - 'a:1:{i:0;s:5:"index";}'
rank_math_analytic_object_id:
  - 103
rank_math_internal_links_processed:
  - 1
categories:
  - Linux

---
Today we are going to learn how to install and configure CyberPanel on CentOS 8.

CyberPanel is a free open source control panel that uses OpenLiteSpeed web server. CyberPanel is a web hosting control panel powered by OpenLiteSpeed and LiteSpeed fro enterprises, it helps you set up sites on your preferred cloud provider Example setting up on Amazon cloud, Digital Ocean etc. CyberPanel is simple, fast and secure to use.

## CybePanel key features  {.wp-block-heading}

Cyberpanel is gaining momentum because of the following key features it posses:

  * It has modsecurity tool which provide built-in support for real-time defense against web application attacks, with an option to enable OWASP rules.
  * It supports auto install for WordPress which make it easy to install WordPress with one-click.
  * It has one-click SSL which is now mandatory for all online properties. You can easily issue Let&#8217;s Encrypt SSL for web sites.
  * It has LiteSpeed cache for WordPress which is auto configured with all WordPress sites. 
  * It has Git Manager support which attach git repos to your CyberPanel websites which make it easy to deploy code updates directly from your repos via webhooks.

## Related Articles  {.wp-block-heading}

  1. [How to Install OpenLiteSpeed on Rocky Linux/AlmaLinux][1]
  2. [How to install aapanel 6 on Fedora 34][2]

## Requirements  {.wp-block-heading}

  1. Fresh CentOS 8 server
  2. Have users with sudo privileges 
  3. Python 3.x
  4. Internet connections 

## Table of Contents  {.wp-block-heading}

  1. Update the system repos 
  2. Run the installation script
  3. Choose OpenLiteSpeed version to use 
  4. Choose options and add-ons to include 
  5. Configuring CyberPanel
  6. Testing 

## 1. Update Rocky Linux Repos {.wp-block-heading}

For any fresh server install, updating the system is the first thing you can do so that you can make all the repositories up to date. We can begin then by issuing the following command on our terminal.

<pre class="wp-block-code"><code>$ sudo dnf update -y</code></pre>

After the update is complete, then we can proceed to the next step.

## 2. Run CyberPanel installation script {.wp-block-heading}

In order for us to install CyberPanel we need to add the following script on our terminal. Scripts provide an easier an automated way of running the installers. Type the following scritpt into our terminal.

<pre class="wp-block-code"><code>$ wget https://cyberpanel.net/install.sh</code></pre>

Sample Output

<pre class="wp-block-code"><code># wget https://cyberpanel.net/install.sh
--2021-11-13 07:22:45--  https://cyberpanel.net/install.sh
Resolving cyberpanel.net (cyberpanel.net)... 172.67.203.238, 104.21.44.216, 2606:4700:3030::6815:2cd8, ...
Connecting to cyberpanel.net (cyberpanel.net)|172.67.203.238|:443... connected.
HTTP request sent, awaiting response... 200 OK
Length: 1305 (1.3K) &#91;application/x-sh]
Saving to: ‘install.sh’

install.sh                   100%&#91;===========================================>]   1.27K  --.-KB/s    in 0s      

2021-11-13 07:22:45 (28.3 MB/s) - ‘install.sh’ saved &#91;1305/1305]</code></pre>

Once download is complete we now need to install the script with the following command;

<pre class="wp-block-code"><code>$ bash install .sh</code></pre>

This bash script will start the installation of CyberPanel into our system. 

<pre class="wp-block-code"><code>$ Detecting Centos 8...


Initializing...

Fetching latest data from CyberPanel server...

This may take few seconds...

Branch name fetched...v2.1.2

Checking root privileges...

You are runing as root...

Valid IP detected...

Checking server location...

Desktop OS not detected. Proceeding

System: CentOS 8 detected...

Checking virtualization type...

Initialized...

                CyberPanel Installer v2.1.2

1. Install CyberPanel.

2. Exit.


  Please enter the number&#91;1-2]: </code></pre>

Choose option 1 to install CyberPanel

## Install CyberPanel with OpenLiteSpeed {.wp-block-heading}

<pre class="wp-block-code"><code>....
                CyberPanel Installer v2.1.2

RAM check : 216/1816MB (11.89%)

Disk check : 2/60GB (5%) (Minimal 10GB free space)

1. Install CyberPanel with OpenLiteSpeed.

2. Install Cyberpanel with LiteSpeed Enterprise.

3. Exit.


  Please enter the number&#91;1-3]: </code></pre>

You can then follow all the prompts to complete the installation

Allow the installation to continue. 

After it is complete you will see the following;

<pre class="wp-block-code"><code> CyberPanel Successfully Installed                  
                                                                   
                Current Disk usage : 8/60GB (14%)                        
                                                                   
                Current RAM  usage : 479/1816MB (26.38%)                         
                                                                   
                Installation time  : 0 hrs 9 min 0 sec                 
                                                                   
                Visit: https://188.166.125.193:8090                     
                Panel username: admin                              
                Panel password: 1234567                        
                Visit: https://188.166.125.193:7080                     
                WebAdmin console username: admin                   
                WebAdmin console password: qoUtjUgbMOMgIGo7          
                                                                   
                Visit: https://188.166.125.193:8090/rainloop/?admin     
                Rainloop Admin username: admin                     
                Rainloop Admin password: NGUso49WFbn04W        
                                                                   
             Run cyberpanel help to get FAQ info
             Run cyberpanel upgrade to upgrade it to latest version.
             Run cyberpanel utility to access some handy tools .
                                                                   
              Website : https://www.cyberpanel.net                 
              Forums  : https://forums.cyberpanel.net              
              Wikipage: https://docs.cyberpanel.net                
              Docs    : https://cyberpanel.net/docs/               
                                                                   
            Enjoy your accelerated Internet by                  
                CyberPanel & OpenLiteSpeed                                                   
###################################################################
Digital Ocean detected...
This provider has a network-level firewall
Please make sure you have opened following port for both in/out:
TCP: 8090 for CyberPanel
TCP: 80, TCP: 443 and UDP: 443 for webserver
TCP: 21 and TCP: 40110-40210 for FTP
TCP: 25, TCP: 587, TCP: 465, TCP: 110, TCP: 143 and TCP: 993 for mail service
TCP: 53 and UDP: 53 for DNS service
Your provider seems blocked port 25 , E-mail sending may not work properly.
Would you like to restart your server now? &#91;y/N]: </code></pre>

## Testing CyberPanel {.wp-block-heading}

CyberPanel runs on port 8090, so to access it use the following on your browser

<pre class="wp-block-code"><code>http://&lt;your_IP_address>:8090</code></pre>

Now you access the browser <figure class="wp-block-image size-large">

<img decoding="async" loading="lazy" width="810" height="464" src="https://i0.wp.com/nextgentips.com/wp-content/uploads/2021/11/Screenshot-from-2021-11-13-11-25-51.png?resize=810%2C464&#038;ssl=1" alt="CyberPanel interface " class="wp-image-594" srcset="https://i0.wp.com/nextgentips.com/wp-content/uploads/2021/11/Screenshot-from-2021-11-13-11-25-51.png?resize=1024%2C586&ssl=1 1024w, https://i0.wp.com/nextgentips.com/wp-content/uploads/2021/11/Screenshot-from-2021-11-13-11-25-51.png?resize=300%2C172&ssl=1 300w, https://i0.wp.com/nextgentips.com/wp-content/uploads/2021/11/Screenshot-from-2021-11-13-11-25-51.png?resize=768%2C440&ssl=1 768w, https://i0.wp.com/nextgentips.com/wp-content/uploads/2021/11/Screenshot-from-2021-11-13-11-25-51.png?w=1294&ssl=1 1294w" sizes="(max-width: 810px) 100vw, 810px" data-recalc-dims="1" /> <figcaption>CyberPanel interface </figcaption></figure> <figure class="wp-block-image size-large"><img decoding="async" loading="lazy" width="810" height="464" src="https://i0.wp.com/nextgentips.com/wp-content/uploads/2021/11/Screenshot-from-2021-11-13-11-26-49.png?resize=810%2C464&#038;ssl=1" alt="CyberPanel dashboard" class="wp-image-595" srcset="https://i0.wp.com/nextgentips.com/wp-content/uploads/2021/11/Screenshot-from-2021-11-13-11-26-49.png?resize=1024%2C586&ssl=1 1024w, https://i0.wp.com/nextgentips.com/wp-content/uploads/2021/11/Screenshot-from-2021-11-13-11-26-49.png?resize=300%2C172&ssl=1 300w, https://i0.wp.com/nextgentips.com/wp-content/uploads/2021/11/Screenshot-from-2021-11-13-11-26-49.png?resize=768%2C440&ssl=1 768w, https://i0.wp.com/nextgentips.com/wp-content/uploads/2021/11/Screenshot-from-2021-11-13-11-26-49.png?w=1294&ssl=1 1294w" sizes="(max-width: 810px) 100vw, 810px" data-recalc-dims="1" /><figcaption>CyberPanel dashboard</figcaption></figure> 

## Conclusion {.wp-block-heading}

We have learn how to install CyberPanel on CentOS 8. Now you can experiment more with it to learn more. Consult [documentation][3] for further instructions.

 [1]: https://nextgentips.com/2021/11/12/how-to-install-openlitespeed-on-rocky-linux-almalinux/
 [2]: https://nextgentips.com/2021/11/05/how-to-install-aapanel-6-on-fedora-34/
 [3]: https://cyberpanel.net/docs/installing-cyberpanel/