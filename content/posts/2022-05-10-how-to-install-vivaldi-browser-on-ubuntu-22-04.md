---
title: How to install Vivaldi Browser on Ubuntu 22.04
author: Kipkoech Sang
type: post
date: 2022-05-10T06:40:05+00:00
url: /2022/05/10/how-to-install-vivaldi-browser-on-ubuntu-22-04/
rank_math_seo_score:
  - 8
rank_math_primary_category:
  - 6
rank_math_internal_links_processed:
  - 1
rank_math_analytic_object_id:
  - 176
categories:
  - Linux

---
Vivaldi browser is a freeware, cross-platform web browser developed by Vivaldi Technologies. It has a minimalistic user interface with basic icons and fonts and, an optionally color scheme that changes based on the background and design of the web page being visited.

## Features of Vivaldi browser {.wp-block-heading}

  * It has a built-in ad blocker, pop-up blocker, and tracker blocker. It helps block intrusive ads and makes the browser much faster which many users want.
  * It comes with a built-in email client with IMAP and POP3 support.
  * The browser can be used as a feed reader to save RSS and Atom feeds.
  * It has the ability to stack and tile tabs, annotate web pages, and add notes to bookmarks.
  * It supports numerous mouse gestures for actions like tab switching and keyboard activation
  * It supports hibernation for both individual tabs and for tab stacks, hence freeing resources while the user does not actively use those tabs.
  * It has translate features that can translate any web page with a single click with a built-in, private translation tool.

## Prerequisites  {.wp-block-heading}

  * Have a user with sudo privileges.
  * Have Ubuntu 22.04 up and running.
  * Have basic knowledge of terminal 

## Install Vivaldi browser on Ubuntu 22.04 {.wp-block-heading}

### 1. Update system repositories  {.wp-block-heading}

To start installing Vivaldi, we can first make our repositories up to date by doing an update and upgrade where necessary.

<pre class="wp-block-code"><code>sudo apt update && apt upgrade -y</code></pre>

### 2. Import public GPG key {.wp-block-heading}

We need to import a public GPG key in order to allow for the verification of apt packages in our system. To import it we are going to use the **wget** command on our terminal 

<pre class="wp-block-code"><code>wget -qO- https://repo.vivaldi.com/archive/linux_signing_key.pub | gpg --dearmor | sudo dd of=/usr/share/keyrings/vivaldi-browser.gpg</code></pre>

### 3. Add Vivaldi repository  {.wp-block-heading}

After the GPG key has been added we need to add the Vivaldi repository to our own repository on Ubuntu.

<pre class="wp-block-code"><code>echo "deb &#91;signed-by=/usr/share/keyrings/vivaldi-browser.gpg arch=$(dpkg --print-architecture)] https://repo.vivaldi.com/archive/deb/ stable main" | sudo dd of=/etc/apt/sources.list.d/vivaldi-archive.list</code></pre>

### 4. Install Vivaldi on Ubuntu 22.04 {.wp-block-heading}

To install Vivaldi first run the system update and then run install.

<pre class="wp-block-code"><code>$ sudo apt update
Ign:6 https://repo.vivaldi.com/archive/deb stable InRelease
Get:7 https://repo.vivaldi.com/archive/deb stable Release &#91;3,840 B]
Get:8 https://repo.vivaldi.com/archive/deb stable Release.gpg &#91;833 B]
Get:9 https://repo.vivaldi.com/archive/deb stable/main amd64 Packages &#91;1,773 B]
Fetched 277 kB in 3s (91.2 kB/s)      
Reading package lists... Done</code></pre>

After the update is complete, run the installation command.

<pre class="wp-block-code"><code>$ sudo apt install vivaldi-stable</code></pre>

You will see the output like this.

<pre class="wp-block-code"><code>Output
The following NEW packages will be installed:
  &lt;strong>vivaldi-stable&lt;/strong>
0 upgraded, 1 newly installed, 0 to remove and 0 not upgraded.
Need to get 87.8 MB of archives.
After this operation, 309 MB of additional disk space will be used.
Get:1 https://repo.vivaldi.com/archive/deb stable/main amd64 vivaldi-stable amd64 5.2.2623.41-1 &#91;87.8 MB]
Fetched 87.8 MB in 22s (3,970 kB/s)                                            
Selecting previously unselected package vivaldi-stable.
(Reading database ... 210907 files and directories currently installed.)
Preparing to unpack .../vivaldi-stable_5.2.2623.41-1_amd64.deb ...
Unpacking vivaldi-stable (5.2.2623.41-1) ...
Setting up vivaldi-stable (5.2.2623.41-1) ...
update-alternatives: using /usr/bin/vivaldi-stable to provide /usr/bin/x-www-bro
wser (x-www-browser) in auto mode
update-alternatives: using /usr/bin/vivaldi-stable to provide /usr/bin/gnome-www
-browser (gnome-www-browser) in auto mode
update-alternatives: using /usr/bin/vivaldi-stable to provide /usr/bin/vivaldi (
vivaldi) in auto mode
Processing triggers for desktop-file-utils (0.26-1ubuntu3) ...
Processing triggers for gnome-menus (3.36.0-1ubuntu3) ...
Processing triggers for mailcap (3.70+nmu1ubuntu1) ...</code></pre>

To start using the browser go to the dock and click show applications and search for Vivaldi in the search bar.

## Conclusion {.wp-block-heading}

We have successfully installed Vivaldi browser, you can now enjoy your privacy while browsing.