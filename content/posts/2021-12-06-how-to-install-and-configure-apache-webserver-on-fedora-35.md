---
title: How to install and configure Apache webserver on Fedora 35.
author: Kipkoech Sang
type: post
date: 2021-12-06T18:54:47+00:00
url: /2021/12/06/how-to-install-and-configure-apache-webserver-on-fedora-35/
rank_math_news_sitemap_robots:
  - index
rank_math_robots:
  - 'a:1:{i:0;s:5:"index";}'
rank_math_analytic_object_id:
  - 74
rank_math_internal_links_processed:
  - 1
categories:
  - Linux

---
[Apache][1] HTTP Server is a free and open-source cross-platform web server software. The goal of Apache is to provide a secure, efficient, and extensible server that provides HTTP services in sync with the current HTTP standards.

The main job of the Apache web server is to establish connections between the server and a browser. This aids in the transfer of files between the server and the client. 

Apache provides many modules that allow server administrators to turn on and off some functionalities. It has modules such as those for security, caching, password authentication, URL rewriting, etc. 

In this tutorial guide, I will take you through the installation steps of the Apache webserver on the Fedora 35 server.

## Why Apache web server? {.wp-block-heading}

  * Apache is reliable and stable software.
  * It is open-source and free therefore attracts a larger community.
  * It is easy to configure, therefore more friendly to all tech entusiasts.
  * It work out of the box with many sites such as WordPress.
  * It supports all platforms such as Windows, Linux servers.
  * Its always upto date because of its frequently updated patches.

## Related Article {.wp-block-heading}

  * [How to Install Apache, MYSQL, PHP on Ubuntu 20.04][2]

## Prerequisites  {.wp-block-heading}

  * Have basic knowlege of shell 
  * Have a Fedora 35 server up and running 
  * Have a root user or if you dont have one create one with sudo privileges.

## Table of Contents. {.wp-block-heading}

  1. Run system updates 
  2. Install Apache on Fedora 35 server
  3. Enable Apache to start on boot
  4. Start Apache.
  5. Run basic Apache configuration settings
  6. Conclusion

## 1. Run system updates. {.wp-block-heading}

First, run the update command to update package repositories in order to get the latest package information. Use the following command to update the system.

<pre class="wp-block-code"><code>$ sudo dnf update -y</code></pre>

When updates and possible upgrades are complete, we can now run Apache install command.

## 2. Install Apache Webserver on Fedora 35. {.wp-block-heading}

Fedora 35 repositories have Apache in their base repository. So it is easy to install Apache on Fedora 35. Run the following command on your terminal.

<pre class="wp-block-code"><code>$ sudo dnf install httpd -y</code></pre>

You will get the following sample output.

<pre class="wp-block-code"><code>Output
Dependencies resolved.
=================================================================================================================
 Package                                 Architecture       Version                    Repository           Size
=================================================================================================================
Installing:
 &lt;strong>httpd                                   x86_64             2.4.51-2.fc35              fedora              1.4 M&lt;/strong>
Installing dependencies:
 apr                                     x86_64             1.7.0-14.fc35              fedora              123 k
 apr-util                                x86_64             1.6.1-17.fc35              fedora               94 k
 fedora-logos-httpd                      noarch             35.0.0-2.fc35              updates              16 k
 httpd-filesystem                        noarch             2.4.51-2.fc35              fedora               12 k
 httpd-tools                             x86_64             2.4.51-2.fc35              fedora               80 k
 mailcap                                 noarch             2.1.53-2.fc35              fedora               33 k
Installing weak dependencies:
 apr-util-bdb                            x86_64             1.6.1-17.fc35              fedora               12 k
 apr-util-openssl                        x86_64             1.6.1-17.fc35              fedora               15 k
 julietaula-montserrat-fonts             noarch             1:7.222-1.fc35             updates             1.6 M
 mod_http2                               x86_64             1.15.24-1.fc35             updates             150 k
 mod_lua                                 x86_64             2.4.51-2.fc35              fedora               60 k

Transaction Summary
=================================================================================================================
Install  12 Packages

Total download size: 3.6 M
Installed size: 10 M
Is this ok &#91;y/N]: y</code></pre>

Press Y to allow installation to continue.

## 3. Start Apache webserver {.wp-block-heading}

To enable Apache to function, ensure you start Apache services. To start Apache run the following command on your terminal.

<pre class="wp-block-code"><code>$ sudo systemctl start httpd</code></pre>

## 4. Enable Apache webserver to start on boot. {.wp-block-heading}

To avoid starting Apache every time you boot your system, you can enable it to run automatically whenever you boot your system. To enable Apache to run on boot, run the following command on your terminal.

<pre class="wp-block-code"><code>$ sudo systemctl enable httpd</code></pre>

When this is complete, you can check the status of Apache with the following command.

<pre class="wp-block-code"><code>$ sudo systemctl status httpd</code></pre>

<pre class="wp-block-code"><code>Output
● httpd.service - The Apache HTTP Server
     Loaded: loaded (/usr/lib/systemd/system/httpd.service; enabled; vendor preset: disabled)
     Active: active (running) since Mon 2021-12-06 18:29:57 UTC; 1min 31s ago
       Docs: man:httpd.service(8)
   Main PID: 17856 (httpd)
     Status: "Total requests: 1; Idle/Busy workers 100/0;Requests/sec: 0.0112; Bytes served/sec:  11 B/sec"
      Tasks: 177 (limit: 1112)
     Memory: 14.1M
        CPU: 104ms
     CGroup: /system.slice/httpd.service
             ├─17856 /usr/sbin/httpd -DFOREGROUND
             ├─17857 /usr/sbin/httpd -DFOREGROUND
             ├─17858 /usr/sbin/httpd -DFOREGROUND
             ├─17859 /usr/sbin/httpd -DFOREGROUND
             └─17860 /usr/sbin/httpd -DFOREGROUND

Dec 06 18:29:57 fedora-35 systemd&#91;1]: Starting The Apache HTTP Server...
Dec 06 18:29:57 fedora-35 httpd&#91;17856]: AH00558: httpd: Could not reliably determine the server's fully qualifie>
Dec 06 18:29:57 fedora-35 systemd&#91;1]: Started The Apache HTTP Server.
Dec 06 18:29:57 fedora-35 httpd&#91;17856]: Server configured, listening on: port 80</code></pre>

If it is active then we can continue.

## 5. Configure firewall {.wp-block-heading}

We do firewall configuration to allow traffic to pass through on port 80. Let&#8217;s begin by allowing traffic on port 80. Use the following command.

make sure you have firewalld up and running before using the following command.

<pre class="wp-block-code"><code>$ sudo firewall-cmd --add-service={http,https} --permanent</code></pre>

To install firewalld, run the following command.

<pre class="wp-block-code"><code>$ sudo dnf install firewalld</code></pre>

Then, after the install is complete, start with the following;

<pre class="wp-block-code"><code>$ sudo systemctl start firewalld </code></pre>

Reload the service for the changes to take effect.

<pre class="wp-block-code"><code>$ sudo firewall-cmd --reload</code></pre>

Now that we have completed, the basics, we can test to see if the Apache webserver is up and running by doing the following:

<pre class="wp-block-code"><code>http://&lt;your_server_IP_Address></code></pre>

You will find the Apache test page. If you got that, then you know Apache webserver is up and running as expected.

## 6. Conclusion {.wp-block-heading}

Thank you for reading along, I hope you have learned how to install and configure Apache webserver accordingly.

 [1]: https://httpd.apache.org/
 [2]: https://nextgentips.com/2021/10/11/how-to-install-apache-mysql-php-on-ubuntu-20-04/