---
title: How to install OpenStack on Ubuntu 20.04 with MicroStack
author: Kipkoech Sang
type: post
date: 2022-01-02T17:13:36+00:00
url: /2022/01/02/how-to-install-openstack-on-ubuntu-20-04-with-microstack/
entry_views:
  - 1
view_ip:
  - 'a:1:{i:0;s:15:"178.147.187.219";}'
rank_math_news_sitemap_robots:
  - index
rank_math_robots:
  - 'a:1:{i:0;s:5:"index";}'
rank_math_analytic_object_id:
  - 43
rank_math_internal_links_processed:
  - 1
categories:
  - Linux

---
Openstack is a free, open standard cloud computing platform. It is deployed as an infrastructure-as-service in both public and private clouds where virtual servers and other resources are made available to users. 

I love how Ubuntu puts it &#8220;Imagine a data centre with hundreds of physical machines, racked, powered and connected to each other. One tool turns this data centre into a cloud and enables on-demand resource provisioning through a self-service portal. That&#8217;s OpenStack &#8220;.

MicroStack is a micro cloud platform based on OpenStack. It is designed for small scale data centre deployments. It eliminates the complexity behind OpenStack. MicroStack answers the need for edge computing, providing reliable and secure cloud platforms. 

## Prerequisites  {.wp-block-heading}

  * Ubuntu 20.04 workstation
  * Multicore CPU
  * 8 GB RAM
  * 100 GB of storage

## Install OpenStack on Ubuntu 20.04 {.wp-block-heading}

## 1. Run System updates  {.wp-block-heading}

As the first step, we are going to run system updates. We need to update our repositories to make them up to date.

<pre class="wp-block-code"><code>$ sudo apt update
$ sudo apt upgrade -y</code></pre>

## 2. Install MicroStack {.wp-block-heading}

Next we will install MicroStack in our Ubuntu system with Snap. Open your terminal and run the following command to install MicroStack.

<pre class="wp-block-code"><code>$ sudo snap install microstack --beta --devmode</code></pre>

You will get the following as the output

<pre class="wp-block-code"><code>microstack (beta) ussuri from Canonical✓ installed</code></pre>

## 3. Initialized MicroStack {.wp-block-heading}

MicroStack can be initialized in either single or multinode mode. To initialize in a single node run the following command. 

<pre class="wp-block-code"><code>$ sudo microstack init --auto --control</code></pre>

This process will initialize many components including, RabitMQ, MYSQL, initializing TL certificate, configuring keystone, nova, etc.

This process will take some time to complete. so grab a cup of coffee as you wait to finish.

<pre class="wp-block-code"><code>Output
2022-01-02 19:27:47,014 - microstack_init - INFO - Configuring clustering ...
2022-01-02 19:27:47,237 - microstack_init - INFO - Setting up as a control node.
2022-01-02 19:27:51,143 - microstack_init - INFO - Generating TLS Certificate and Key
2022-01-02 19:27:52,599 - microstack_init - INFO - Configuring networking ...
2022-01-02 19:28:01,792 - microstack_init - INFO - Opening horizon dashboard up to *
2022-01-02 19:28:03,018 - microstack_init - INFO - Waiting for RabbitMQ to start ...
Waiting for 192.168.43.237:5672
2022-01-02 19:28:11,787 - microstack_init - INFO - RabbitMQ started!
2022-01-02 19:28:11,788 - microstack_init - INFO - Configuring RabbitMQ ...
2022-01-02 19:28:12,952 - microstack_init - INFO - RabbitMQ Configured!
2022-01-02 19:28:12,983 - microstack_init - INFO - Waiting for MySQL server to start ...
Waiting for 192.168.43.237:3306
2022-01-02 19:28:26,721 - microstack_init - INFO - Mysql server started! Creating databases ...
2022-01-02 19:28:29,135 - microstack_init - INFO - Configuring Keystone Fernet Keys ...
2022-01-02 19:28:59,019 - microstack_init - INFO - Bootstrapping Keystone ...
2022-01-02 19:29:16,054 - microstack_init - INFO - Creating service project ...
2022-01-02 19:29:24,724 - microstack_init - INFO - Keystone configured!
2022-01-02 19:29:24,763 - microstack_init - INFO - Configuring the Placement service...
2022-01-02 19:29:55,749 - microstack_init - INFO - Running Placement DB migrations...
2022-01-02 19:30:02,482 - microstack_init - INFO - Configuring nova control plane services ...
2022-01-02 19:30:22,798 - microstack_init - INFO - Running Nova API DB migrations (this may take a lot of time)...
2022-01-02 19:30:59,583 - microstack_init - INFO - Running Nova DB migrations (this may take a lot of time)...
......</code></pre>

## 4. Launch an OpenStack instance  {.wp-block-heading}

An instance is a virtual machine that runs inside the cloud. We are going to launch a VM with an image called cirrOS.

<pre class="wp-block-code"><code>$ microstack launch cirros --name test</code></pre>

You will get the following output

<pre class="wp-block-code"><code>Output
Creating local "microstack" ssh key at /home/nextgentips/snap/microstack/common/.ssh/id_microstack
Launching server ...
Allocating floating ip ...
Server test launched! (status is BUILD)

Access it with `ssh -i /home/nextgentips/snap/microstack/common/.ssh/id_microstack cirros@10.20.20.163`
You can also visit the OpenStack dashboard at &lt;strong>https://10.20.20.1:443&lt;/strong></code></pre>

Proceed to your favorite browser to launch the OpenStack dashboard 

## Conclusion {.wp-block-heading}

We have successfully learn how to install Openstack on Ubuntu 20.04. If faced with any challenge please comment and we will be happy to help, also check the <a href="https://docs.openstack.org/xena/install/" target="_blank" rel="noreferrer noopener">documentation</a> for further reading.