---
title: How to Install Envoy Proxy on Ubuntu 22.04
author: Kipkoech Sang
type: post
date: 2022-04-28T06:21:59+00:00
url: /2022/04/28/how-to-install-envoy-proxy-on-ubuntu-22-04/
rank_math_seo_score:
  - 16
rank_math_internal_links_processed:
  - 1
rank_math_primary_category:
  - 6
rank_math_analytic_object_id:
  - 170
categories:
  - Linux

---
In this tutorial guide, I will show you how to install Envoy Proxy Server on Ubuntu 22.04

<a href="https://www.envoyproxy.io/docs/envoy/latest/intro/what_is_envoy" target="_blank" rel="noreferrer noopener">Envoy</a>&nbsp;is an L7 proxy and communication bus designed for large modern service-oriented architecture. The project was born out of the belief that the network should be transparent to applications. When network and application problems occur, it should be easy to determine the source of the problem.

Envoy is an open-source edge and service proxy, designed for cloud-native applications. Let’s dive in and learn how to install Envoy on Ubuntu 22.04.

## High Level Envoy Proxy features {#high-level-envoy-features.wp-block-heading}

  * Its known for its advanced load balancing technique. It implements load balancing in a single place and have them accessible to any application.
  * It has front/edge proxy support.
  * It has best in class observability
  * It has gRPC support.
  * Has support for HTTP L7 routing
  * It supports HTTP/2
  * L3/L4 filter architecture. Envoy acts as a L3/L4 proxy
  * It support use of API for configuration management

## Prerequisites {.wp-block-heading}

  * At least 2GB of RAM
  * Good internet connection
  * Ubuntu 22.04 server

## Related Articles {#related-articles.wp-block-heading}

  * [How to install Envoy Proxy server on Ubuntu 20.04][1]

## Installing Envoy proxy server on Ubuntu 22.04 {#installing-envoy-proxy-server-on-ubuntu-20-04.wp-block-heading}

Installing Envoy Proxy, we are going to use the Tetrate packages until official packages are available.

### 1. Update system repositories. {#1-update-system-repositories.wp-block-heading}

The first thing is to update system repositories in order to make them up to date. This will avoid running into errors in the process of installation.

<pre class="wp-block-code"><code>$ sudo apt update && apt upgrade -y</code></pre>

When both updates and upgrades are complete, we can now install envoy dependencies.

### 2. Install Envoy proxy dependencies {#2-install-envoy-proxy-dependencies.wp-block-heading}

Envoy requires some set of programs to run effectively such as curl for running downloads, lsb-release, etc. Let’s install the following;

<pre class="wp-block-code"><code>sudo apt install apt-transport-https gnupg2 curl lsb-release</code></pre>

You will see the following output.

<pre class="wp-block-code"><code># output
Reading package lists... Done
Building dependency tree... Done
Reading state information... Done
curl is already the newest version (7.81.0-1).
curl set to manually installed.
lsb-release is already the newest version (11.1.0ubuntu4).
lsb-release set to manually installed.
apt-transport-https is already the newest version (2.4.5).
gnupg2 is already the newest version (2.2.27-3ubuntu2).
0 upgraded, 0 newly installed, 0 to remove and 0 not upgraded.</code></pre>

### 3. Add GPG key to envoy repository {#3-add-gpg-key-to-envoy-repository.wp-block-heading}

In order to sign the envoy program, we need to download the GPG key to self-sign it. Keys help to prove the authenticity of the program’s downloads.

<pre class="wp-block-code"><code>curl -sL 'https://deb.dl.getenvoy.io/public/gpg.8115BA8E629CC074.key' | sudo gpg --dearmor -o /usr/share/keyrings/getenvoy-keyring.gpg</code></pre>

If you want, you can verify the key with the echo command, if you get an ok as the output then it&#8217;s signed therefore move ahead and install the program.

<pre class="wp-block-code"><code>echo a077cb587a1b622e03aa4bf2f3689de14658a9497a9af2c427bba5f4cc3c4723 /usr/share/keyrings/getenvoy-keyring.gpg | sha256sum --check

# output
/usr/share/keyrings/getenvoy-keyring.gpg: OK</code></pre>

Now we need to add the key to sources.list.d repository. To do so use the following command.

<pre class="wp-block-code"><code>echo "deb &#91;arch=amd64 signed-by=/usr/share/keyrings/getenvoy-keyring.gpg] https://deb.dl.getenvoy.io/public/deb/ubuntu $(lsb_release -cs) main" | sudo tee /etc/apt/sources.list.d/getenvoy.list</code></pre>

We need to update our repositories again by doing an update again.

<pre class="wp-block-code"><code>$ sudo apt update </code></pre>

Whenever you update, you must see what you have added. Have look at the output below

<pre class="wp-block-code"><code># sample output
Get:6 https://deb.dl.getenvoy.io/public/deb/ubuntu jammy InRelease &#91;5079 B]
Get:7 https://deb.dl.getenvoy.io/public/deb/ubuntu jammy/main amd64 Packages &#91;4323 B]
Fetched 279 kB in 1s (362 kB/s)
Reading package lists... Done
Building dependency tree... Done
Reading state information... Done
All packages are up to date.</code></pre>

### 4. Install Envoy Proxy on Ubuntu 22.04 {#4-install-envoy-proxy-on-debian-11.wp-block-heading}

Now that we have completed all the prerequisites, we can now install Envoy proxy with the following command.

<pre class="wp-block-code"><code>$ sudo apt install getenvoy-envoy</code></pre>

See the output below

<pre class="wp-block-code"><code>The following NEW packages will be installed:
  &lt;strong>getenvoy-envoy&lt;/strong>
0 upgraded, 1 newly installed, 0 to remove and 0 not upgraded.
Need to get 16.5 MB of archives.
After this operation, 0 B of additional disk space will be used.
Get:1 https://deb.dl.getenvoy.io/public/deb/ubuntu jammy/main amd64 getenvoy-envoy amd64 1.18.2.p0.gd362e79-1p75.g76c310e &#91;16.5 MB]
Fetched 16.5 MB in 1s (14.2 MB/s)   
Selecting previously unselected package getenvoy-envoy.
(Reading database ... 93697 files and directories currently installed.)
Preparing to unpack .../getenvoy-envoy_1.18.2.p0.gd362e79-1p75.g76c310e_amd64.deb ...
Unpacking getenvoy-envoy (1.18.2.p0.gd362e79-1p75.g76c310e) ...
Setting up getenvoy-envoy (1.18.2.p0.gd362e79-1p75.g76c310e) ...</code></pre>

We can check the Envoy version installed with the following command;

<pre class="wp-block-code"><code>$ envoy --version
envoy  version: d362e791eb9e4efa8d87f6d878740e72dc8330ac/&lt;strong>1.18.2&lt;/strong>/clean-getenvoy-76c310e-envoy/RELEASE/BoringSSL</code></pre>

### 5. Run Envoy {#5-run-envoy.wp-block-heading}

To check Envoy commands use the help command

<pre class="wp-block-code"><code>$ envoy --help</code></pre>

Let’s **run Envoy** with a demo configuration file

Download a envoy-demo.yaml file

<pre class="wp-block-code"><code>wget https://www.envoyproxy.io/docs/envoy/latest/_downloads/92dcb9714fb6bc288d042029b34c0de4/envoy-demo.yaml</code></pre>

Run with this command:

<pre class="wp-block-code"><code>$ envoy -c envoy-demo.yaml</code></pre>

**-c&nbsp;**tells envoy the path to its initial configuration

To know if Envoy is proxying, use the following

<pre class="wp-block-code"><code>$ curl -v localhost:10000</code></pre>

## Conclusion {#conclusion.wp-block-heading}

We have learned how to install Envoy proxy on Ubuntu 22.04. To learn more of these check out the <a href="http://Run Envoy To check Envoy commands use the help command  $ envoy --help Let’s run Envoy with a demo configuration file  Create a demo.yaml file and run with the following command  $ envoy -c envoy-demo.yaml -c tells envoy the path to its initial configuration  To know if Envoy is proxying, use the following  $ curl -v localhost:10000 Conclusion We have learned how to install Envoy proxy on Ubuntu 20.04. To enjoy more of these tutorials keep checking this blog for more." target="_blank" rel="noreferrer noopener">Envoy documentation.</a>

 [1]: https://nextgentips.com/2021/12/14/how-to-install-envoy-proxy-server-on-ubuntu-20-04/