---
title: How to install MongoDB 5 on Rocky Linux
author: Kipkoech Sang
type: post
date: 2022-01-30T18:28:33+00:00
url: /2022/01/30/how-to-install-mongodb-5-on-rocky-linux/
rank_math_news_sitemap_robots:
  - index
rank_math_robots:
  - 'a:1:{i:0;s:5:"index";}'
rank_math_analytic_object_id:
  - 17
rank_math_internal_links_processed:
  - 1
categories:
  - Database

---
In this tutorial guide, we are going to learn how to install MongoDB 5.0 on Rocky Linux.

MongoDB is an open-source NoSQL database that provides high throughput for data-driven applications. Unlike relational databases such as MySQL, Oracle, and SQL servers which store data in tables according to a rigid schema, MongoDB stores data in documents with flexible schema.

## Why do we need MongoDB? {#why-do-we-need-mongodb.wp-block-heading}

  * MongoDB makes it easy for developers to store structured or unstructured data, it uses JSON-like format to store documents. This format directly maps to native objects in most programming languages, making natural database for most developers.
  * MongoDB is built on a scale-out architecture making popular for developers developing scalable applications with evolving data schemas.
  * MongoDB is available in every major public cloud provider such as Azure, AWS, GCP making it easy for developers to deploy to any cloud provider of choice.
  * MongoDB supports rapid iterative development where developers collaborate with larger teams.
  * In MongoDB records are stored as documents in compressed&nbsp;[BSON][1]&nbsp;files.

**MongoDB documents can be retrieved in JSON formats which has many benefits such as:**

  * It is human readable, makes it easy to be read
  * It is a natural form to store data
  * You can nest JSON to store complex data objects.
  * Documents maps to objects in most popular programming languages.
  * Structured and unstructured information can be stored in the same document
  * JSON has a flexible and dynamic schema, so adding fields or leaving a field out is not a problem.

## Prerequisites {#prerequisites.wp-block-heading}

  * Have Rocky Linux server running
  * Have a user with sudo priviliges
  * MongoDB only supports 64 bits version of Linux, so make sure you are running x64.

## Related Articles {#related-articles.wp-block-heading}

  * [How to Install MongoDB with Podman on Rocky Linux 8][2]
  * [How to install MongoDB 4.4 on Fedora 35][3]
  * [How to Deploy MongoDB with Docker][4]
  * [How to install MongoDB 5 on Ubuntu 21.04][5]
  * [How to Install MongoDB 5 on Ubuntu 20.04][6]

## Install MongoDB 5 on Rocky Linux {#install-mongodb-5-on-rocky-linux.wp-block-heading}

### 1. Run system updates  {#1-run-system-updates.wp-block-heading}

To begin with we need to make our system repositories up to date. This will help avoid running into errors while doing the installations.

<pre class="wp-block-code"><code>$ sudo dnf update -y </code></pre>

### 2. Configure package management (yum) {#2-configure-package-management-yum.wp-block-heading}

The easy way to install Mongo is to create **/etc/yum.repos.d/mongodb-org-5.0.repo** file so that we can easily install using the yum repository.

<pre class="wp-block-code"><code>$ sudo vi /etc/yum.repos.d/mongodb-org-5.0.repo</code></pre>

Add the following content ;

<pre class="wp-block-code"><code>&#91;mongodb-org-5.0]
name=MongoDB Repository
baseurl=https://repo.mongodb.org/yum/redhat/$releasever/mongodb-org/5.0/x86_64/
gpgcheck=1
enabled=1
gpgkey=https://www.mongodb.org/static/pgp/server-5.0.asc</code></pre>

### 3. Install MongoDB 5 on Rocky Linux {#3-install-mongodb-5-on-rocky-linux.wp-block-heading}

Now we can install the latest version of MongoDB with the following

<pre class="wp-block-code"><code>$ sudo dnf install mongodb-org -y</code></pre>

We will see the following output

<pre class="wp-block-code"><code>MongoDB Repository                                                                              42 kB/s |  16 kB     00:00    
Dependencies resolved.
===============================================================================================================================
 Package                                Architecture Version                                       Repository             Size
===============================================================================================================================
Installing:
 mongodb-org                            x86_64       5.0.5-1.el8                                   mongodb-org-5.0        11 k
Installing dependencies:
 cyrus-sasl                             x86_64       2.1.27-5.el8                                  baseos                 95 k
 cyrus-sasl-gssapi                      x86_64       2.1.27-5.el8                                  baseos                 49 k
 cyrus-sasl-plain                       x86_64       2.1.27-5.el8                                  baseos                 46 k
 mongodb-database-tools                 x86_64       100.5.1-1                                     mongodb-org-5.0        47 M
 mongodb-mongosh                        x86_64       1.1.9-1.el7                                   mongodb-org-5.0        44 M
 mongodb-org-database                   x86_64       5.0.5-1.el8                                   mongodb-org-5.0        11 k
 mongodb-org-database-tools-extra       x86_64       5.0.5-1.el8                                   mongodb-org-5.0        16 k
 mongodb-org-mongos                     x86_64       5.0.5-1.el8                                   mongodb-org-5.0        19 M
 mongodb-org-server                     x86_64       5.0.5-1.el8                                   mongodb-org-5.0        28 M
 mongodb-org-shell                      x86_64       5.0.5-1.el8                                   mongodb-org-5.0        15 M
 mongodb-org-tools                      x86_64       5.0.5-1.el8                                   mongodb-org-5.0        11 k
 python3-pip                            noarch       9.0.3-20.el8.rocky.0                          appstream              19 k
 python3-setuptools                     noarch       39.2.0-6.el8                                  baseos                162 k
 python36                               x86_64       3.6.8-38.module+el8.5.0+671+195e4563          appstream              18 k
Enabling module streams:
 python36                                            3.6                                                                      

Transaction Summary
===============================================================================================================================
Install  15 Packages

Total download size: 153 M
Installed size: 565 M
Is this ok &#91;y/N]: 
.....
Installed:
  cyrus-sasl-2.1.27-5.el8.x86_64                                
  cyrus-sasl-gssapi-2.1.27-5.el8.x86_64                        
  cyrus-sasl-plain-2.1.27-5.el8.x86_64                           
  mongodb-database-tools-100.5.1-1.x86_64                      
  mongodb-mongosh-1.1.9-1.el7.x86_64                             
  mongodb-org-5.0.5-1.el8.x86_64                               
  mongodb-org-database-5.0.5-1.el8.x86_64                        
  mongodb-org-database-tools-extra-5.0.5-1.el8.x86_64          
  mongodb-org-mongos-5.0.5-1.el8.x86_64                          
  mongodb-org-server-5.0.5-1.el8.x86_64                        
  mongodb-org-shell-5.0.5-1.el8.x86_64                           
  mongodb-org-tools-5.0.5-1.el8.x86_64                         
  python3-pip-9.0.3-20.el8.rocky.0.noarch                        
  python3-setuptools-39.2.0-6.el8.noarch                       
  python36-3.6.8-38.module+el8.5.0+671+195e4563.x86_64   </code></pre>

Lets check the version of MongoDB we have just installed.

<pre class="wp-block-code"><code>$ mongod --version
db version v5.0.5
Build Info: {
    "version": "5.0.5",
    "gitVersion": "d65fd89df3fc039b5c55933c0f71d647a54510ae",
    "openSSLVersion": "OpenSSL 1.1.1k  FIPS 25 Mar 2021",
    "modules": &#91;],
    "allocator": "tcmalloc",
    "environment": {
        "distmod": "rhel80",
        "distarch": "x86_64",
        "target_arch": "x86_64"
    }
}</code></pre>

### 5. Configure MongoDB 5 {#5-configure-mongodb-5.wp-block-heading}

To start using MongoDB we need to **start** the MongoDB.service, to do that run the following command;

<pre class="wp-block-code"><code>$ sudo systemctl start mongod</code></pre>

Then after starting you need to **enable** it to start every time you boot up your device.

<pre class="wp-block-code"><code>$ sudo systemctl enable mongod</code></pre>

Lastly, you can check the **status** of our MongoDB service if it is running as expected.

<pre class="wp-block-code"><code>$ sudo systemctl status mongod</code></pre>

<pre class="wp-block-code"><code>Output
● mongod.service - MongoDB Database Server
   Loaded: loaded (/usr/lib/systemd/system/mongod.service; enabled; vendor preset: disabled)
   Active: active (running) since Sun 2022-01-30 18:15:31 UTC; 38s ago
     Docs: https://docs.mongodb.org/manual
 Main PID: 38404 (mongod)
   Memory: 66.2M
   CGroup: /system.slice/mongod.service
           └─38404 /usr/bin/mongod -f /etc/mongod.conf

Jan 30 18:15:29 rockylinux systemd&#91;1]: Starting MongoDB Database Server...
Jan 30 18:15:29 rockylinux mongod&#91;38401]: about to fork child process, waiting until server is ready for connections.
Jan 30 18:15:29 rockylinux mongod&#91;38401]: forked process: 38404
Jan 30 18:15:31 rockylinux mongod&#91;38401]: child process started successfully, parent exiting
Jan 30 18:15:31 rockylinux systemd&#91;1]: Started MongoDB Database Server.</code></pre>

Our MongoDB service is now running as expected.

To start using MongoDB type on your terminal **mongosh**, it will show something like this output;

<pre class="wp-block-code"><code>$ mongosh 
Current Mongosh Log ID: 61f6d61a8cda0cc1b432fec6
Connecting to:          mongodb://127.0.0.1:27017/?directConnection=true&serverSelectionTimeoutMS=2000&appName=mongosh+1.1.9
Using MongoDB:          5.0.5
Using Mongosh:          1.1.9

For mongosh info see: https://docs.mongodb.com/mongodb-shell/


To help improve our products, anonymous usage data is collected and sent to MongoDB periodically (https://www.mongodb.com/legal/privacy-policy).
You can opt-out by running the disableTelemetry() command.

------
   The server generated these startup warnings when booting:
   2022-01-30T18:15:30.984+00:00: Access control is not enabled for the database. Read and write access to data and configuration is unrestricted
   2022-01-30T18:15:30.984+00:00: /sys/kernel/mm/transparent_hugepage/enabled is 'always'. We suggest setting it to 'never'
------

test> </code></pre>

### 6. MongoDB crud operations {#6-mongodb-crud-operations.wp-block-heading}

Crud operations include creating, reading, updating, and deleting.

To check the current database you are operating on type&nbsp;**db**

<pre class="wp-block-code"><code>$ db 
test</code></pre>

Here am working on the test database.

To switch database you just say&nbsp;**use db_name**&nbsp;and it will create and switch to the database automatically.

<pre class="wp-block-code"><code>$ use nextgentips</code></pre>

### Insert into MongoDB database {#insert-into-mongodb-database.wp-block-heading}

MongoDB provides the following methods to insert documents into a collection.

<pre class="wp-block-code"><code># insert one 
$ db.collection.insertOne()
# insert many
$ db.collection.insertMany()</code></pre>

Example

<pre class="wp-block-code"><code>nextgen> db.nextgen.insertOne(&#91; { Name: 'Nextgentips', Niche: 'Linux Tech Blog' }])
{
  acknowledged: true,
  insertedId: ObjectId("61f6d75e80b3d76dd79c1018")
}</code></pre>

In order to&nbsp;**select documents**&nbsp;you can use the find statement like this

<pre class="wp-block-code"><code>db.collection.find()</code></pre>

Example

<pre class="wp-block-code"><code>db.nexgen.find()</code></pre>

### 7. Uninstall MongoDB 5 {#7-uninstall-mongodb-5.wp-block-heading}

If you want to uninstall MongoDB, first you will have to stop the MongoDB running instance and then remove the packages

**Stop MongoDB instance**

<pre class="wp-block-code"><code>$ sudo systemctl stop mongod</code></pre>

**Remove MongoDB packages**

To completely remove the packages run the following command;

<pre class="wp-block-code"><code>$ sudo dnf erase $(rpm -qa | grep mongodb-org)</code></pre>

Lastly, **remove data** **directories**

<pre class="wp-block-code"><code># log
$ sudo rm -r /var/log/mongodb
# lib
$ sudo rm -r /var/lib/mongodb</code></pre>

## Conclusion. {#conclusion.wp-block-heading}

We have learned how to install MongoDB on Rocky Linux. I am glad you have enjoyed the tutorial. Feel free to comment in case you are faced with a challenge.

 [1]: https://www.mongodb.com/json-and-bson
 [2]: https://nextgentips.com/2021/11/17/how-to-install-mongodb-with-podman-on-rocky-linux-8/
 [3]: https://nextgentips.com/2021/11/17/how-to-install-mongodb-4-4-on-fedora-35/
 [4]: https://nextgentips.com/2021/10/26/how-to-deploy-mongodb-with-docker/
 [5]: https://nextgentips.com/2021/10/26/how-to-install-mongodb-5-on-ubuntu-21-04/
 [6]: https://nextgentips.com/2021/12/26/how-to-install-mongodb-5-on-ubuntu-20-04/