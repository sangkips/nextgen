---
title: How to install Prometheus 2.30.3 Server in Ubuntu 20.04
author: Kipkoech Sang
type: post
date: 2021-10-07T12:45:11+00:00
url: /2021/10/07/how-to-install-prometheus-server-in-ubuntu-20-04/
site-sidebar-layout:
  - default
site-content-layout:
  - default
theme-transparent-header-meta:
  - default
rank_math_primary_category:
  - 9
rank_math_news_sitemap_robots:
  - index
rank_math_robots:
  - 'a:1:{i:0;s:5:"index";}'
rank_math_analytic_object_id:
  - 144
rank_math_internal_links_processed:
  - 1
categories:
  - Monitoring

---
In this guide, we will learn how to install the Prometheus server on Ubuntu 20.04. Prometheus is an open-source system monitoring and alerting toolkit. Prometheus collects and stores its metrics as time-series data. Metrics information is stored with the timestamp at which it was recorded, alongside optional key-value pairs called labels.

And now you are wondering what are metrics? Metrics are numeric measurements and time series mean that the changes are recorded over time.

## Components {.wp-block-heading}

  * Has an [alertmanager][1] to handle alerts
  * Has a [push gateway][2] for supporting short-lived jobs 
  * Has main [Prometheus server][3] which scrapes and stores time series data
  * [Client libraries][4] for instrumenting application code 
  * Special purpose [exporters][5] for services like HAProxy, statD, Graphite etc<meta http-equiv="content-type" content="text/html; charset=utf-8" />
    

## Related Article {.wp-block-heading}

  * [How to run Prometheus with Podman][6]

## Install Prometheus 2.30 {.wp-block-heading}

Let us update the system first:

<pre class="wp-block-code"><code>$ sudo apt update</code></pre>

## Download and install Prometheus  {.wp-block-heading}

Download the latest release of 2.30.3 into our system.

<pre class="wp-block-code"><code>$ wget https://github.com/prometheus/prometheus/releases/download/v2.30.3/prometheus-2.30.3.linux-amd64.tar.gz
</code></pre>

After the download is complete, the next step is to extract the archive:

<pre class="wp-block-code"><code>$ tar xvf prometheus-2.30.3.linux-amd64.tar.gz</code></pre>

For convenience, we can change the directory for the extracted archive 

<pre class="wp-block-code"><code>$ cd prometheus-2.30.3.linux-amd64</code></pre>

From there we can create a configuration file directory

<pre class="wp-block-code"><code>$ sudo mkdir -p /etc/prometheus</code></pre>

Then we can create the data directory

<pre class="wp-block-code"><code>$sudo mkdir -p /var/lib/prometheus</code></pre>

Let&#8217;s now move the binary files Prometheus and promtool to /usr/local/bin/

Use the following code snippet

<pre class="wp-block-code"><code>$ sudo mv prometheus promtool /usr/local/bin/</code></pre>

Move the template configuration file&nbsp;`prometheus.yml`&nbsp;to&nbsp;`/etc/prometheus/`&nbsp;directory

<pre class="wp-block-code"><code>$ sudo mv prometheus.yml /etc/prometheus/prometheus.yml</code></pre>

We have installed Prometheus. Lets verify the version 

<pre class="wp-block-code"><code>$ prometheus --version</code></pre>

## Create Prometheus System group {.wp-block-heading}

The following code can help create the Prometheus system group.

<pre class="wp-block-code"><code>$ sudo groupadd --system prometheus</code></pre>

We can then create Prometheus system user and assign the primary group created 

<pre class="wp-block-code"><code>$ sudo useradd -s /sbin/nologin --system -g prometheus prometheus</code></pre>

Set the ownership of Prometheus files and data directories to the Prometheus group and user.

<pre class="wp-block-code"><code>$ sudo chown -R prometheus:prometheus /etc/prometheus/  /var/lib/prometheus/

$ sudo chmod -R 775 /etc/prometheus/ /var/lib/prometheus/</code></pre>

Lastly, we can create a systemd service to run at boot time

<pre class="wp-block-code"><code>$ sudo nano /etc/systemd/system/prometheus.service</code></pre>

Add the following content to prometheus.service 

<pre class="wp-block-code"><code>&#91;Unit]
Description=Prometheus
Wants=network-online.target
After=network-online.target

&#91;Service]
User=prometheus
Group=prometheus
Restart=always
Type=simple
ExecStart=/usr/local/bin/prometheus \
    --config.file=/etc/prometheus/prometheus.yml \
    --storage.tsdb.path=/var/lib/prometheus/ \
    --web.console.templates=/etc/prometheus/consoles \
    --web.console.libraries=/etc/prometheus/console_libraries \
    --web.listen-address=0.0.0.0:9090

&#91;Install]
WantedBy=multi-user.target</code></pre>

Start the Prometheus service 

<pre class="wp-block-code"><code>$ sudo systemctl start prometheus</code></pre>

Now we can check if our Prometheus is up and running 

<pre class="wp-block-code"><code>$ sudo systemctl status prometheus</code></pre>

Add firewall rules to allow Prometheus to run on TCP port 9090

<pre class="wp-block-code"><code>$ ufw allow 9090/tcp</code></pre>

Access your server from the browser.

<pre class="wp-block-code"><code>http://&lt;ip address&gt;or&lt;hostname&gt;:9090

for example

http:&#47;&#47;192.0.1.1:9090</code></pre>

## Conclusion  {.wp-block-heading}

Congratulations! you have successfully installed Prometheus on Ubuntu 20.04. To read more consult [Prometheus documentation][7].

 [1]: https://github.com/prometheus/alertmanager
 [2]: https://github.com/prometheus/pushgateway
 [3]: https://github.com/prometheus/prometheus
 [4]: https://prometheus.io/docs/instrumenting/clientlibs/
 [5]: https://prometheus.io/docs/instrumenting/exporters/
 [6]: https://nextgentips.com/2021/11/25/how-to-run-prometheus-with-podman/
 [7]: https://prometheus.io/docs/prometheus/latest/getting_started/