---
title: How to install Streamlit on Ubuntu 22.04
author: Kipkoech Sang
type: post
date: 2022-09-06T06:54:23+00:00
url: /2022/09/06/how-to-install-streamlit-on-ubuntu-22-04/
categories:
  - Uncategorized

---
In this tutorial, we are going to learn how to install Streamlit on Ubuntu 22.04

Streamlit is an open-source Python library that makes it easy to create and share beautiful, custom web apps for machine learning and data science. This is a faster way to build and share data apps. Streamlit turns data scripts into shareable web apps in minutes.

You don&#8217;t need to write the backend or frontend to define Streamlit, you only need to add widgets which is the same as declaring variables. This makes it easy to use and also deploy. Deploying your app is just a click of a button and your application is in the cloud.

## Prerequisites  {.wp-block-heading}

  * Have PIP installed
  * Python 3.7 and above 
  * IDE

Something to note here is, have a virtual environment installed so that Streamlit doesn&#8217;t impact any other projects you might be working on.

## Install Streamlit on Ubuntu 22.04 {.wp-block-heading}

### 1. Create a virtual environment  {.wp-block-heading}

First, we need to create a virtual environment so that we can separate our Streamlit work from others. To create a virtual environment using the following command. First create a project directory.

<pre class="wp-block-code"><code>$ sudo mkdir streamlit 
$ cd streamlit
$ python3 -m venv env # env is the name of the virtual environment.</code></pre>

After creating a virtual environment, you can then open this project from your favorite IDE or you can continue from the terminal. 

To activate the virtual environment, use the following command.

<pre class="wp-block-code"><code>$ source env/bin/activate</code></pre>

If it happens you don&#8217;t have pip installed, you can do install using the following command.

<pre class="wp-block-code"><code>sudo apt-get install python3-pip</code></pre>

After this we need to install <mark style="background-color:#abb8c3" class="has-inline-color">`pipenv` </mark> on our virtual environment.

<pre class="wp-block-code"><code>$ pip3 install pipenv</code></pre>

This will create `<mark style="background-color:#abb8c3" class="has-inline-color">pipfile</mark>` in your workspace.

### 2. Install streamlit  {.wp-block-heading}

To install Streamlit, use the following command on your terminal.

<pre class="wp-block-code"><code>pip install streamlit</code></pre>

You will be in a position to see an output such as this one.

<pre class="wp-block-code"><code>Collecting streamlit
  Downloading streamlit-1.12.2-py2.py3-none-any.whl (9.1 MB)
     ━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━ 9.1/9.1 MB 118.6 kB/s eta 0:00:00
Collecting protobuf&lt;4,>=3.12
  Downloading protobuf-3.20.1-cp310-cp310-manylinux_2_12_x86_64.manylinux2010_x86_64.whl (1.1 MB)
     ━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━ 1.1/1.1 MB 88.4 kB/s eta 0:00:00
Collecting blinker>=1.0.0
  Downloading blinker-1.5-py2.py3-none-any.whl (12 kB)
Collecting python-dateutil
  Using cached python_dateutil-2.8.2-py2.py3-none-any.whl (247 kB)
Collecting importlib-metadata>=1.4
  Downloading importlib_metadata-4.12.0-py3-none-any.whl (21 kB)
Collecting packaging>=14.1
  Using cached packaging-21.3-py3-none-any.whl (40 kB)
Collecting watchdog
  Downloading watchdog-2.1.9-py3-none-manylinux2014_x86_64.whl (78 kB)
     ━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━ 78.4/78.4 KB 202.5 kB/s eta 0:00:00
</code></pre>

We can test whether the installation worked, let&#8217;s create an hello file from the terminal

<pre class="wp-block-code"><code>streamlit hello</code></pre>

This will automatically open a web browser on `<mark style="background-color:#abb8c3" class="has-inline-color">localhost:8501</mark>`.<figure class="wp-block-image size-large">

<img decoding="async" loading="lazy" width="810" height="330" src="https://i0.wp.com/nextgentips.com/wp-content/uploads/2022/09/Screenshot-from-2022-09-06-09-09-00.png?resize=810%2C330&#038;ssl=1" alt="Nextgentips: Streamlit hello interface" class="wp-image-1547" srcset="https://i0.wp.com/nextgentips.com/wp-content/uploads/2022/09/Screenshot-from-2022-09-06-09-09-00.png?resize=1024%2C417&ssl=1 1024w, https://i0.wp.com/nextgentips.com/wp-content/uploads/2022/09/Screenshot-from-2022-09-06-09-09-00.png?resize=300%2C122&ssl=1 300w, https://i0.wp.com/nextgentips.com/wp-content/uploads/2022/09/Screenshot-from-2022-09-06-09-09-00.png?resize=768%2C313&ssl=1 768w, https://i0.wp.com/nextgentips.com/wp-content/uploads/2022/09/Screenshot-from-2022-09-06-09-09-00.png?w=1295&ssl=1 1295w" sizes="(max-width: 810px) 100vw, 810px" data-recalc-dims="1" /> <figcaption>Nextgentips: Streamlit hello interface</figcaption></figure> 

### Create Streamlit App {.wp-block-heading}

Let&#8217;s create a simple example. Paste the following code on your browser and name it app.py

<pre class="wp-block-code"><code>import streamlit as st

st.write('Hello world')</code></pre>

To run this code go to your terminal and type `<mark style="background-color:#abb8c3" class="has-inline-color">streamlit run app.py</mark>` 

<pre class="wp-block-code"><code>streamlit run app.py</code></pre>

The output will be an hello world.

Let&#8217;s find the square of different numbers.

<pre class="wp-block-code"><code>x = st.slider('x')
st.write(x, 'squared is', x * x)</code></pre>

You will get a slider as an output.<figure class="wp-block-image size-full">

<img decoding="async" loading="lazy" width="810" height="233" src="https://i0.wp.com/nextgentips.com/wp-content/uploads/2022/09/Screenshot-from-2022-09-06-09-49-28.png?resize=810%2C233&#038;ssl=1" alt="Nextgentips: slider squared" class="wp-image-1548" srcset="https://i0.wp.com/nextgentips.com/wp-content/uploads/2022/09/Screenshot-from-2022-09-06-09-49-28.png?w=817&ssl=1 817w, https://i0.wp.com/nextgentips.com/wp-content/uploads/2022/09/Screenshot-from-2022-09-06-09-49-28.png?resize=300%2C86&ssl=1 300w, https://i0.wp.com/nextgentips.com/wp-content/uploads/2022/09/Screenshot-from-2022-09-06-09-49-28.png?resize=768%2C221&ssl=1 768w" sizes="(max-width: 810px) 100vw, 810px" data-recalc-dims="1" /> <figcaption>Nextgentips: slider squared</figcaption></figure> 

## Conclusion {.wp-block-heading}

We have successfully installed Streamlit on ubuntu 22.04 and we have learned how to create sliders. This is a very important tool when it comes to data science. Feel free to consult <a href="https://docs.streamlit.io/" target="_blank" rel="noreferrer noopener">Streamlit documentation</a>.