---
title: How to install Tailwind CSS and Flowbite inside a Django Project
author: Kipkoech Sang
type: post
date: 2022-11-21T09:16:25+00:00
url: /2022/11/21/how-to-install-tailwind-css-and-flowbite-inside-a-django-project/
categories:
  - Programming

---
Tailwind CSS is an open-source CSS framework. **The difference between Bootstrap is that Tailwind does not provide predefined classes for elements such as tables or buttons**. It makes it easier to write and maintain the code of your application. You don&#8217;t have to switch back and forth between CSS and your main HTML template.

Flowbite enables us to build websites faster with components on top of Tailwind CSS. **It&#8217;s open-source and it&#8217;s built with utility classes from Tailwind CSS that can be used as a starting point when creating user interfaces.**

In this tutorial, I will show you how you can create a beautiful Django UI using Tailwind CSS and Flowbite. 

## Prerequisites {.wp-block-heading}

  * Node js. <a href="https://nextgentips.com/2021/12/26/how-to-install-node-js-17-on-ubuntu-20-04/" target="_blank" rel="noopener" title="">How to install Node.js 17 on Ubuntu 20.04</a>
  * Python. Here is a peek view of Installing python <a href="https://nextgentips.com/2021/10/04/install-python-3-on-ubuntu-20-04/" target="_blank" rel="noopener" title="">How to install Python 3.9 interpreter on Ubuntu 20.04</a>
  * Know how to create a Django project. Check this out <a href="https://nextgentips.com/2022/05/03/how-to-set-up-a-python-django-application-using-django-4-0/" target="_blank" rel="noopener" title="">How to set up a Python Django Application using Django 4.0</a>

### 1. Create a Django project {.wp-block-heading}

To create a Django project, start by creating a virtual environment and make sure to activate the environment so that whatever you are installing is isolated from your system processes.

<pre class="wp-block-code"><code>python3 -m venv env</code></pre>

To activate the virtual environment created, use the following command.

<pre class="wp-block-code"><code>source env/bin/activate</code></pre>

### 2. Install Django  {.wp-block-heading}

After the virtual environment have been activated, proceed to install Django.

<pre class="wp-block-code"><code>pip install django </code></pre>

It will proceed to pull Django and install it on your virtual environment. 

After Django has been installed, go ahead to create a project to hold all the Django functionality.

<pre class="wp-block-code"><code>django-admin startproject flowbite.</code></pre>

The above command will create a project called flowbite and inside flowbite folder, you will see `<mark style="background-color:#abb8c3" class="has-inline-color">settings.py, asgi.py, urls.py, wsgi.py, __init__.py</mark>`

We are only going to deal with the `<mark style="background-color:#abb8c3" class="has-inline-color">settings.py</mark>`. We need to tell the application where to find the templates and that is done inside the settings file, but first create a templates folder under the root of your project.

<pre class="wp-block-code"><code>#settings.py
TEMPLATES = &#91;
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': &#91;
            BASE_DIR / 'templates' #connects templates folder created
        ],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': &#91;
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
            ],
        },
    },
]</code></pre>

### 3. Install the Django compressor {.wp-block-heading}

Django compressor compresses linked and inline JavaScript or CSS into a single cached file. Django compressor doesn&#8217;t care if different pages use a different combination of statics. It doesn&#8217;t care if you use inline scripts or style, it doesn&#8217;t get in the way. Install the Django compressor with the following command.

<pre class="wp-block-code"><code>pip install django-compressor</code></pre>

Once installed, go to the settings.py file and add compressor to the installed apps 

<pre class="wp-block-code"><code>#settings.py
INSTALLED_APPS = &#91;
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',

# third party apps
    'compressor',#new app
]</code></pre>

We can now modify the Django compressor&#8217;s default behavior and make adjustments to the website. In settings.py add the following 

<pre class="wp-block-code"><code>#settings.py
COMPRESS_ROOT = BASE_DIR / 'static'

COMPRESS_ENABLED = True

STATICFILES_FINDERS = ('compressor.finders.CompressorFinder',)</code></pre>

### 4. Create static folder {.wp-block-heading}

Now is the time to create a static folder that can hold images, javaScript. In the root of the project create static folder and inside it create another folder called src and lastly create a `<mark style="background-color:#abb8c3" class="has-inline-color">input.css</mark>`. Input.css will come once we install Tailwind CSS.

### 5. Create a Django application {.wp-block-heading}

To create a django app, on the root of your django project type `<mark style="background-color:#abb8c3" class="has-inline-color">python manage.py startapp <app_name></mark>`

<pre class="wp-block-code"><code>python manage.py startapp css</code></pre>

Then we need to modify our `<mark style="background-color:#abb8c3" class="has-inline-color">views.py</mark>` inside app folder to handle what will be displayed on the UI. Add the following to the views.py file 

<pre class="wp-block-code"><code>from django.shortcuts import render

# Create your views here.

def home(request):
    context={}
    return render(request, 'index.html', context)</code></pre>

Inside CSS app create a urls.py file for routes creation, then add the following 

<pre class="wp-block-code"><code>#css/urls.py
from django.urls import path
from . import views

urlpatterns = &#91;
    path('', views.home, name='home')
]</code></pre>

We now need to tell the root project where to find our home directory. Inside flowbite/urls.py add the following 

<pre class="wp-block-code"><code>from django.contrib import admin
from django.urls import path, include

urlpatterns = &#91;
    path('admin/', admin.site.urls),
    path('', include('css.urls'))
]</code></pre>

Next, go to the templates folder we created earlier and create a `<mark style="background-color:#abb8c3" class="has-inline-color">_base.html</mark>` template that can be inherited by all templates in the browser. Add the following simple HTML file.

Remember the views.py file we created earlier, it has an index.html. Create index.html inside the templates folder to serve that view.

<pre class="wp-block-code"><code>{% extends "_base.html" %} #inheriting the base template

{% block content %}
  &lt;h1 class="text-3xl text-green-800">Welcome to Django Project that uses Flowbite and Tailwind CSS&lt;/h1>
{% endblock content %}</code></pre>

### 6. Install Tailwind CSS {.wp-block-heading}

To install Tailwind CSS we require NPM package. If you didn&#8217;t install Node at the beginning this is the time to do so before you continue. Run npm to install Tailwind css 

<pre class="wp-block-code"><code>npm install -D tailwindcss</code></pre>

After that is finished, create `<mark style="background-color:#abb8c3" class="has-inline-color">tailwind.config.js</mark>` file with the help of TailwindCLI using the following command 

<pre class="wp-block-code"><code>$ npx tailwindcss init
Created Tailwind CSS config file: tailwind.config.js</code></pre>

Then, configure template paths inside the tailwind.config.js file

<pre class="wp-block-code"><code>/** @type {import('tailwindcss').Config} */
module.exports = {
  content: &#91;
     './templates/**/*.html'
],
  theme: {
    extend: {},
  },
  plugins: &#91;],
}</code></pre>

We need to add the Tailwind directives to our CSS. Inside input.css add the following 

<pre class="wp-block-code"><code>@tailwind base;
@tailwind components;
@tailwind utilities;</code></pre>

The next thing is to start the Tailwind CLI build process.

<pre class="wp-block-code"><code>npx tailwindcss -i ./static/src/input.css -o ./static/src/output.css --watch</code></pre>

### 7. Install Flowbite {.wp-block-heading}

Install Flowbite as a dependency using npm

<pre class="wp-block-code"><code>npm i flowbite</code></pre>

Then we need to include flowbite inside tailwind.config.js file 

<pre class="wp-block-code"><code>/** @type {import('tailwindcss').Config} */
module.exports = {
  content: &#91;
     './templates/**/*.html'
     './node_modules/flowbite/**/*.js' #flowbite
],
  theme: {
    extend: {},
  },
  plugins: &#91;
     require('flowbite/plugin')# flowbite
],
}</code></pre>

Lastly, add the following script to the _base.html template to serve Flowbite correctly.

<pre class="wp-block-code"><code>&lt;script src="https://unpkg.com/flowbite@1.5.4/dist/flowbite.js">&lt;/script></code></pre>

Now you can run your server with `<mark style="background-color:#abb8c3" class="has-inline-color">python manage.py runserver</mark>`. You will see a beautiful message you created earlier in your views.py

## Conclusion {.wp-block-heading}

We have successfully added Tailwind CSS and Flowbite to our Django project. You can implement Nvabrar easily now. For the full project check it at <a href="https://github.com/sangkips/Tailwing-django-flowbite" target="_blank" rel="noopener" title="">GitHub</a>.