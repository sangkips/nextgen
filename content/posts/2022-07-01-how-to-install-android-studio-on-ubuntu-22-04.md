---
title: How to install Android Studio on Ubuntu 22.04
author: Kipkoech Sang
type: post
date: 2022-07-01T12:00:28+00:00
url: /2022/07/01/how-to-install-android-studio-on-ubuntu-22-04/
categories:
  - Programming

---
In this tutorial, we will explore how to install Android Studio on Ubuntu 22.04.

Android Studio is the Integrated Development Environment for Android app development. Whenever you want to start developing android applications, the easiest and the most effective IDE is always the Android studio. It is well tested and offers all the functionalities other code editors provide.

## Features of Android Studio {.wp-block-heading}

  * It is fast and has many features
  * It offers a unified environment for the development of all android based applications 
  * It offers code templates and Github integration for easy code deployment
  * It has extensive testing tools and frameworks.
  * It has linting tools to catch performance, usability, version compatibility
  * It has built-in support for the Google Cloud environment making it easy to integrate the Google App Engine and messaging services 

## Install Android Studio on Ubuntu 22.04 {.wp-block-heading}

### 1. Update system repositories  {.wp-block-heading}

The first thing is to run system updates so that all the repositories are up to date. 

<pre class="wp-block-code"><code>sudo apt update && apt upgrade -y</code></pre>

### 2. Install required libraries  {.wp-block-heading}

To begin, we need to install the required libraries. Let&#8217;s install those libraries with the following command.

<pre class="wp-block-code"><code>sudo apt-get install libc6:i386 libncurses5:i386 libstdc++6:i386 lib32z1 libbz2-1.0:i386</code></pre>

You will get an output similar to this.

<pre class="wp-block-code"><code>#output
The following additional packages will be installed:
  gcc-12-base:i386 krb5-locales libc6-i386 libcom-err2 libcom-err2:i386
  libcrypt1:i386 libgcc-s1:i386 libgpm2:i386 libgssapi-krb5-2:i386
  libidn2-0:i386 libk5crypto3:i386 libkeyutils1:i386 libkrb5-3:i386
  libkrb5support0:i386 libnsl2:i386 libnss-nis:i386 libnss-nisplus:i386
  libssl3 libssl3:i386 libtinfo5:i386 libtirpc3:i386 libunistring2:i386
Suggested packages:
  glibc-doc:i386 locales:i386 gpm:i386 krb5-doc:i386 krb5-user:i386
The following NEW packages will be installed:
  gcc-12-base:i386 krb5-locales lib32z1 libbz2-1.0:i386 libc6:i386 libc6-i386
  libcom-err2:i386 libcrypt1:i386 libgcc-s1:i386 libgpm2:i386
  libgssapi-krb5-2:i386 libidn2-0:i386 libk5crypto3:i386 libkeyutils1:i386
  libkrb5-3:i386 libkrb5support0:i386 libncurses5:i386 libnsl2:i386
  libnss-nis:i386 libnss-nisplus:i386 libssl3:i386 libstdc++6:i386
  libtinfo5:i386 libtirpc3:i386 libunistring2:i386
The following packages will be upgraded:
  libcom-err2 libssl3
2 upgraded, 25 newly installed, 0 to remove and 182 not upgraded.
Need to get 12.5 MB of archives.
After this operation, 41.0 MB of additional disk space will be used.
Do you want to continue? &#91;Y/n] </code></pre>

### 3. Download Android Studio {.wp-block-heading}

Head over to the <a href="https://developer.android.com/studio" target="_blank" rel="noreferrer noopener">Android Studio download page</a> and hit on download options to download .gz version for Linux.

<pre class="wp-block-code"><code>sudo wget https://redirector.gvt1.com/edgedl/android/studio/ide-zips/2021.2.1.15/android-studio-2021.2.1.15-linux.tar.gz</code></pre>

After the download is complete, we need to extract the Android Studio

<pre class="wp-block-code"><code>sudo tar -xzvf android-studio-2021.2.1.15-linux.tar.gz</code></pre>

Next, we need to move Android Studio to an appropriate location, for us we are going to move to /opt to enable shared users to use it.

<pre class="wp-block-code"><code>sudo mv android-studio /opt/</code></pre>

### 3. Launch Android Studio {.wp-block-heading}

To start using Android Studio, head over to `<mark style="background-color:#abb8c3" class="has-inline-color">android-studio/bin/</mark>` directory and execute `<mark style="background-color:#abb8c3" class="has-inline-color">studio.sh</mark>`

<pre class="wp-block-code"><code>$ cd /opt/android-studio/bin
$ sudo ./studio.sh</code></pre>

When you launch your studio.sh bash command, the configuration wizard will appear.

Select whether you want to import the Android settings or not and then click ok.<figure class="wp-block-image size-full">

<img decoding="async" loading="lazy" width="800" height="601" src="https://i0.wp.com/nextgentips.com/wp-content/uploads/2022/07/Screenshot-from-2022-07-01-14-41-32.png?resize=800%2C601&#038;ssl=1" alt="nextgentips Android studio welcome page " class="wp-image-1431" srcset="https://i0.wp.com/nextgentips.com/wp-content/uploads/2022/07/Screenshot-from-2022-07-01-14-41-32.png?w=800&ssl=1 800w, https://i0.wp.com/nextgentips.com/wp-content/uploads/2022/07/Screenshot-from-2022-07-01-14-41-32.png?resize=300%2C225&ssl=1 300w, https://i0.wp.com/nextgentips.com/wp-content/uploads/2022/07/Screenshot-from-2022-07-01-14-41-32.png?resize=768%2C577&ssl=1 768w, https://i0.wp.com/nextgentips.com/wp-content/uploads/2022/07/Screenshot-from-2022-07-01-14-41-32.png?resize=105%2C80&ssl=1 105w" sizes="(max-width: 800px) 100vw, 800px" data-recalc-dims="1" /> <figcaption>nextgentips Android studio welcome page </figcaption></figure> 

  * Choose the type of settings you want Android Studio to have. Select standard for now 
  * Select the UI theme you want to use.
  * Verify settings that it is ok
  * Accept the license agreement and hit next and finish.

From here the Android Studio will start its installation.

You can now start your project by clicking new project.

### 4. Uninstall Android Studio {.wp-block-heading}

To remove Android studio from your system do the following from the command line.

<pre class="wp-block-code"><code>sudo rm -rf /opt/android-studio/</code></pre>

## Conclusion {.wp-block-heading}

Congratulations, you have installed Android Studio on Ubuntu 22.04. Happy coding.