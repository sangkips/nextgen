---
title: Basic Linux commands
author: Kipkoech Sang
type: post
date: 2022-09-18T08:43:16+00:00
url: /2022/09/18/basic-linux-commands/
categories:
  - Linux

---
### How to check if a file exists on the filesystem. {.wp-block-heading}

To check whether a file exists in a Linux filesystem, we can deploy the use of the following commonly used file operators, that is `<mark style="background-color:#abb8c3" class="has-inline-color">-e</mark>` and `<mark style="background-color:#abb8c3" class="has-inline-color">-f</mark>`

`<mark style="background-color:#abb8c3" class="has-inline-color">-e</mark>` will check whether a file exists regardless of the type, and will return true if any type of file exists.

`<mark style="background-color:#abb8c3" class="has-inline-color">-f</mark>` will only return true only if the file is a regular file I.e, not a directory or a device.

We use the following syntax

<pre class="wp-block-code"><code>test -e filename
&#91; e filename ]
test -f filename
&#91; -f filename ]</code></pre>

An example. Let&#8217;s check if we have a filename called shell in the /_etc/ directory._

<pre class="wp-block-code"><code>&#91; -e /etc/shell ] && echo “Found” || echo “Not Found”
&#91; -f &lt;em>etc&lt;/em>/shell ] && echo “Found” || echo “Not Found”</code></pre>

If you run the following you will get an answer, if there is a file matching what you have specified then it will `<mark style="background-color:#abb8c3" class="has-inline-color">echo Found</mark>` and if the file is not available it will `<mark style="background-color:#abb8c3" class="has-inline-color">echo not found</mark>`.

### **Find the 50th line of a file using tail and head commands.** {.wp-block-heading}

The `<mark style="background-color:#abb8c3" class="has-inline-color">Head command</mark>` will always output the first 10 items on the file while the `<mark style="background-color:#abb8c3" class="has-inline-color">Tail command</mark>` will print the last 10 items in the file.

If you want to find the 50<sup>th</sup> line of a file, we need to combine the two, tail and head commands.

Let’s say I have a spreadsheet called students.csv and this list contains 800 students, to display the 50<sup>th</sup> student we use the following command.

<pre class="wp-block-code"><code>Tail -n +50 students.csv | head -n 1

head -n 50 students.csv | tail -n 1</code></pre>

`<mark style="background-color:#abb8c3" class="has-inline-color">-n</mark>` shows the nth line

`<mark style="background-color:#abb8c3" class="has-inline-color">+50</mark>` tells us to print from line 50. This is an option to be used with the tail command.

`<mark style="background-color:#abb8c3" class="has-inline-color">Head -n 1</mark>` will tell us to print the first line only and that is the 50<sup>th</sup> line. If you want to print the first 5 lines you use head -n 5 or tail -n 5

### How to copy a file from one Linux machine to another {.wp-block-heading}

To copy a file from one Linux machine to another, use <mark style="background-color:#abb8c3" class="has-inline-color">`scp command`</mark> which is the secure copy command. The command will look like this:

<pre class="wp-block-code"><code>scp &lt;filename> user@host: &lt;file destination></code></pre>

The filename is the file you want to copy to the remote machine

The user is the destination username

The host is the destination host, and the best way to determine the host is to use the IP address of that host.

The file destination is the directory you want your file to be sent to.

Let&#8217;s see an example, We have a file called students.csv on the local machine, to transfer this file we use the following command.

<pre class="wp-block-code"><code>scp students.csv root@97.107.132.178:/remote/directory</code></pre>

This will prompt you to authenticate, fill in your password and you are good to go

If you check on your remote server you will see the students.csv file

To copy a directory to a remote machine we use the following command.

<pre class="wp-block-code"><code>scp -r test root@97.107.132.178:/remote/directory</code></pre>

**The test** is the directory I want to copy.

### How to monitor a continuously updating log file {.wp-block-heading}

The most effective tool to manage continuously changing log files is the `<mark style="background-color:#abb8c3" class="has-inline-color">tail command</mark>`.

The tail command shows the last 10 commands when used, in this case, if logs are continuously changing we can use the tail with -f option will print to the terminal any new lines added to the log file in real-time.

Let&#8217;s take an example, we need to monitor apache2, this is how we can see the most recent logs.

Tail -f <location of the fail>

<pre class="wp-block-code"><code>sudo tail -f /var/log/apache2/access.log</code></pre>

With this command, we will be in a position to get the latest logs as it occurs.