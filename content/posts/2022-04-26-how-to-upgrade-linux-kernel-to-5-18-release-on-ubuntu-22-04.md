---
title: How to Upgrade Linux Kernel to 5.18 Release on Ubuntu 22.04
author: Kipkoech Sang
type: post
date: 2022-04-26T07:41:37+00:00
url: /2022/04/26/how-to-upgrade-linux-kernel-to-5-18-release-on-ubuntu-22-04/
rank_math_seo_score:
  - 17
rank_math_focus_keyword:
  - upgrade to linux 5.18 kernel
rank_math_internal_links_processed:
  - 1
rank_math_primary_category:
  - 6
rank_math_analytic_object_id:
  - 169
categories:
  - Linux

---
In this tutorial, we are going to learn how to upgrade Linux Kernel to 5.18 mainline release on Ubuntu 22.04.

<a href="https://www.kernel.org/" target="_blank" rel="noreferrer noopener">Linux Kernel</a>&nbsp;is a **free and open-source, monolithic, modular, multitasking Unix-like operating system**. **It is the main component of a Linux operating system and is the core interface between the computer’s hardware and its processes. It makes communication possible between computer hardware and processes running on it and it manages resources effectively.**

Linux 5.18 mainline was released recently by **Linux Torvalds** with better new features to try out. The mainline tree is maintained by Linus Torvalds and It is where all new features are added and releases always come from.

## Notable features on Linux Kernel 5.18 {.wp-block-heading}

  * Introduction of schedular updates around NUMA balancing that can further enhance the performance of AMD EPYC servers.
  * Support for Intel Hardware feedback interface whih is now merged with intel&#8217;s new HFI driver for Hybrid processor.
  * Intel software Defined Silicone has been merged to that controversial Intel CPU feature about allowing the activation of extra silicone features using the cryptographic-signed keys.
  * Introduction of Intel Indirect Branch Tracking IBT which is part of Intel control flow enforcement technology with tiger lake CPUs.

## Table of Contents {#table-of-contents.wp-block-heading}

  1. Run updates for your system
  2. Check the current version of Linux kernel you are running
  3. Download Linux kernel headers from Ubuntu Mainline
  4. Download Linux Kernel image
  5. Download modules required to build the kernel
  6. Install new kernel
  7. Reboot the system
  8. Conclusion

## Upgrade Linux Kernel to 5.18 release {#upgrade-linux-kernel-to-5-16-release.wp-block-heading}

### 1. Run system update {#1-run-system-update.wp-block-heading}

The first thing to do is to run system updates on our Ubuntu 20.04 server. Use the following command on your terminal.

<pre class="wp-block-code"><code>$ sudo apt update && apt upgrade -y</code></pre>

When upgrades and updates are complete, we can now begin to download headers, modules, and images.

Before we can proceed let&#8217;s check the Linux kernel we are having with the following command.

<pre class="wp-block-code"><code>uname -r
5.15.0-25-generic</code></pre>

Ubuntu 22.04 comes with a default Linux 5.15

### 2. Download Linux kernel Headers. {#2-download-linux-kernel-headers.wp-block-heading}

Linux kernel headers is a package providing the Linux kernel headers. These are part of the Kernel even though shipped separately. The headers act as an interface between internal kernel components and also between userspace and the kernel.

To download this package header, head over to the <a href="https://kernel.ubuntu.com/~kernel-ppa/mainline" target="_blank" rel="noreferrer noopener">Ubuntu PPA mainline repository</a> and make downloads for your amd64 system. We are going to download the following header files.

<pre class="wp-block-code"><code>wget -c https://kernel.ubuntu.com/~kernel-ppa/mainline/v5.18-rc1/amd64/linux-headers-5.18.0-051800rc1_5.18.0-051800rc1.202204032230_all.deb</code></pre>

Another header file to download is this one. Download the generic one here.

<pre class="wp-block-code"><code>wget -c https://kernel.ubuntu.com/~kernel-ppa/mainline/v5.18-rc1/amd64/linux-headers-5.18.0-051800rc1-generic_5.18.0-051800rc1.202204032230_amd64.deb</code></pre>

The sample output will look like this:

<pre class="wp-block-code"><code>#output
--2022-04-26 07:18:57--  https://kernel.ubuntu.com/~kernel-ppa/mainline/v5.18-rc1/amd64/linux-headers-5.18.0-051800rc1-generic_5.18.0-051800rc1.202204032230_amd64.deb
Resolving kernel.ubuntu.com (kernel.ubuntu.com)... 91.189.94.216
Connecting to kernel.ubuntu.com (kernel.ubuntu.com)|91.189.94.216|:443... connected.
HTTP request sent, awaiting response... 200 OK
Length: 3063028 (2.9M) &#91;application/x-debian-package]
Saving to: ‘linux-headers-5.18.0-051800rc1-generic_5.18.0-051800rc1.202204032230_amd64.deb’

linux-headers-5.18.0-051800rc1-gene 100%&#91;=================================================================>]   2.92M  --.-KB/s    in 0.1s    

2022-04-26 07:18:57 (29.3 MB/s) - ‘linux-headers-5.18.0-051800rc1-generic_5.18.0-051800rc1.202204032230_amd64.deb’ saved &#91;3063028/3063028]</code></pre>

Note the difference between those headers

### 3. Download Linux kernel Modules {#2-download-linux-kernel-modules.wp-block-heading}

Linux kernels are pieces of code that can be loaded and unloaded into the kernel upon demand. They extend the functionality of the kernel without the need to reboot the system. A module can be configured as built-in or loadable. To dynamically load or remove a module, it has to be configured as a loadable module in the kernel configuration.

To download the Linux Kernel module run the following command on your terminal.

<pre class="wp-block-code"><code>wget -c https://kernel.ubuntu.com/~kernel-ppa/mainline/v5.18-rc1/amd64/linux-modules-5.18.0-051800rc1-generic_5.18.0-051800rc1.202204032230_amd64.deb</code></pre>

Next is to download the image.

## 3. Download Linux Kernel Image {#3-download-linux-kernel-image.wp-block-heading}

Linux kernel image is a snapshot of the Linux kernel that is able to run by itself after giving the control to it. For example, whenever you need to boot the system up, it will bootload an image from the hard disk.

To download the Linux Kernel image 5.17 run the following command on your terminal.

<pre class="wp-block-code"><code>wget -c https://kernel.ubuntu.com/~kernel-ppa/mainline/v5.18-rc1/amd64/linux-image-unsigned-5.18.0-051800rc1-generic_5.18.0-051800rc1.202204032230_amd64.deb</code></pre>

Make sure you are seeing the image from the downloads if you do an ls.

Now that we have finished downloading **images, modules,** and **headers** it is now time to install them.

### 4. Install Linux Kernel on Ubuntu 22.04 {#4-install-linux-kernel-on-ubuntu-20-04.wp-block-heading}

To install Linux Kernel 5.18, let’s run the following command;

<pre class="wp-block-code"><code>$ sudo dpkg -i *.deb</code></pre>

Wait for the process to complete before restarting the system.

<pre class="wp-block-code"><code>reboot -n</code></pre>

After you have restarted the system, check the Linux Kernel release installed.

<pre class="wp-block-code"><code>$ uname -r</code></pre>

The output you will now get is like this;

<pre class="wp-block-code"><code>5.18.0-051800rc1-generic</code></pre>

## Conclusion {#conclusion.wp-block-heading}

As you can see we have successfully upgraded from 5.15.0-25-generic to 5.18.0-051800rc1-generic, the latter being the latest release.

check out this also: <a href="https://nextgentips.com/2022/03/21/how-to-upgrade-linux-kernel-to-5-17-release-on-ubuntu-20-04/" target="_blank" rel="noreferrer noopener">How to Upgrade Linux Kernel to 5.17 Release on Ubuntu 20.04</a>