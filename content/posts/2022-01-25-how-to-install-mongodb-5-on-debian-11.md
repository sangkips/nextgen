---
title: How to install MongoDB 5 on Debian 11
author: Kipkoech Sang
type: post
date: 2022-01-25T13:56:19+00:00
url: /2022/01/25/how-to-install-mongodb-5-on-debian-11/
entry_views:
  - 4
view_ip:
  - 'a:4:{i:0;s:13:"105.167.31.21";i:1;s:39:"2a00:23c4:c383:c901:e680:7c16:cbc6:b594";i:2;s:13:"94.59.138.168";i:3;s:13:"18.234.30.224";}'
rank_math_news_sitemap_robots:
  - index
rank_math_robots:
  - 'a:1:{i:0;s:5:"index";}'
rank_math_analytic_object_id:
  - 22
rank_math_internal_links_processed:
  - 1
categories:
  - Database

---
In this tutorial guide, we are going to learn how to install MongoDB 5 on Debian 11.

MongoDB is an open-source NoSQL database that provides high throughput for data-driven applications. Unlike relational databases such as MySQL, Oracle, and SQL servers which store data in tables according to a rigid schema, MongoDB stores data in documents with flexible schema.

## Why do we need MongoDB? {.wp-block-heading}

  * MongoDB makes it easy for developers to store structured or unstructured data, it uses JSON-like format to store documents. This format directly maps to native objects in most programming languages, making natural database for most developers.
  * MongoDB is built on a scale-out architecture making popular for developers developing scalable applications with evolving data schemas.
  * MongoDB is available in every major public cloud provider such as Azure, AWS, GCP making it easy for developers to deploy to any cloud provider of choice.
  * MongoDB supports rapid iterative development where developers collaborate with larger teams.
  * In MongoDB records are stored as documents in compressed&nbsp;[BSON][1]&nbsp;files.
  * MongoDB scales further giving developers easier time whenever they want to build applications which can scale higher.
  * MongoDB ensures their is data integrity, high availability, and can meet security compliance standards. 

MongoDB documents can be retrieved in JSON formats which has many benefits such as:

  * it is human readable, makes it easy to be read
  * It is a natural form to store data
  * You can nest JSON to to store complex data objects.
  * Documents maps to objects in most popular programming languages.
  * Structured and unstructured information can be stored in the same document
  * JSON has a flexible and dynamic schema, so adding fields or leaving a field out is not a problem.

## Prerequisites {.wp-block-heading}

  * Have Debian 11 server up and running
  * Have a user with sudo priviliges
  * MongoDB only supports 64 bits version of Linux, so make sure you are running x64.

## Related Articles {.wp-block-heading}

  * [How to Install MongoDB with Podman on Rocky Linux 8][2]
  * [How to install MongoDB 4.4 on Fedora 35][3]
  * [How to Deploy MongoDB with Docker][4]
  * [How to install MongoDB 5 on Ubuntu 21.04][5]

## Installing MongoDB 5 on Debian 11 {.wp-block-heading}

### 1. Run system updates {.wp-block-heading}

To begin our installation, we need to update our repositories to make them up to date. open your terminal and type in the following command;

<pre class="wp-block-code"><code>$ sudo apt update && apt upgrade -y</code></pre>

You might need to install **gnupg** and its dependencies 

<pre class="wp-block-code"><code>$ sudo apt-get install gnupg</code></pre>

### 2. Add MongoDB 5 GPG key {.wp-block-heading}

To begin with, let’s import the GPG key used to sign MongoDB. To import use the following command on your terminal.

<pre class="wp-block-code"><code>$ wget -qO - https://www.mongodb.org/static/pgp/server-5.0.asc | sudo apt-key add -</code></pre>

You will get ok as the output on the above command. If so you can move ahead.

### 3. Create a list file for MongoDB {.wp-block-heading}

We need to create a sources.list.d file in our MongoDB repository. This repository helps us to easily add new repositories without the need to edit the central file.

To add this file use the following command on your terminal;

<pre class="wp-block-code"><code>$ echo "deb http://repo.mongodb.org/apt/debian buster/mongodb-org/5.0 main" | sudo tee /etc/apt/sources.list.d/mongodb-org-5.0.list</code></pre>

You need to run the system update again to update our repositories. Make sure when you update, you can see the MongoDB-org added otherwise it won’t work.

<pre class="wp-block-code"><code>$ sudo apt update</code></pre>

### 4. Install MongoDB on Debian 11 {.wp-block-heading}

Having satisfied all the requirements we can now run the MongoDB installer in our system.

<pre class="wp-block-code"><code>$ sudo apt install mongodb-org -y</code></pre>

Sample output

<pre class="wp-block-code"><code>Output
...
Setting up mongodb-org-shell (5.0.5) ...
Setting up mongodb-database-tools (100.5.1) ...
Setting up mongodb-org-mongos (5.0.5) ...
Setting up mongodb-org-database-tools-extra (5.0.5) ...
Setting up mongodb-org-database (5.0.5) ...
Setting up mongodb-org-tools (5.0.5) ...
Setting up mongodb-org (5.0.5) ...
Processing triggers for man-db (2.9.4-2) ...</code></pre>

To check the version of installed Mongod, run the following command;

<pre class="wp-block-code"><code>$ mongod --version
db version v5.0.5
Build Info: {
    "version": "5.0.5",
    "gitVersion": "d65fd89df3fc039b5c55933c0f71d647a54510ae",
    "openSSLVersion": "OpenSSL 1.1.1k  25 Mar 2021",
    "modules": &#91;],
    "allocator": "tcmalloc",
    "environment": {
        "distmod": "debian10",
        "distarch": "x86_64",
        "target_arch": "x86_64"
    }
}</code></pre>

### 5. Configuring MongoDB 5 {.wp-block-heading}

To start using MongoDB we need to start the MongoDB.service, to do that run the following command;

<pre class="wp-block-code"><code>$ sudo systemctl start mongod</code></pre>

Then after starting you need to enable it to start every time you boot up your device.

<pre class="wp-block-code"><code>$ sudo systemctl enable mongod</code></pre>

Lastly, you can check the status of our MongoDB service if it is running as expected.

<pre class="wp-block-code"><code>$ sudo systemctl status mongod</code></pre>

<pre class="wp-block-code"><code>Output
● mongod.service - MongoDB Database Server
     Loaded: loaded (/lib/systemd/system/mongod.service; enabled; vendor preset: enabled)
     Active: active (running) since Tue 2022-01-25 13:17:45 UTC; 15s ago
       Docs: https://docs.mongodb.org/manual
   Main PID: 22796 (mongod)
     Memory: 66.1M
        CPU: 649ms
     CGroup: /system.slice/mongod.service
             └─22796 /usr/bin/mongod --config /etc/mongod.conf

Jan 25 13:17:45 debian systemd&#91;1]: Started MongoDB Database Server.</code></pre>

Our MongoDB service is now running as expected.

To start using MongoDB type on your terminal **mongosh**, it will show something like this output;

<pre class="wp-block-code"><code>Output
Current Mongosh Log ID: 61eff8c1021408bb6fdecb08
Connecting to:          mongodb://127.0.0.1:27017/?directConnection=true&serverSelectionTimeoutMS=2000&appName=mongosh+1.1.9
Using MongoDB:          5.0.5
Using Mongosh:          1.1.9

For mongosh info see: https://docs.mongodb.com/mongodb-shell/


To help improve our products, anonymous usage data is collected and sent to MongoDB periodically (https://www.mongodb.com/legal/privacy-policy).
You can opt-out by running the disableTelemetry() command.

------
   The server generated these startup warnings when booting:
   2022-01-25T13:17:45.140+00:00: Using the XFS filesystem is strongly recommended with the WiredTiger storage engine. See http://dochub.mongodb.org/core/prodnotes-filesystem
   2022-01-25T13:17:45.684+00:00: Access control is not enabled for the database. Read and write access to data and configuration is unrestricted
   2022-01-25T13:17:45.685+00:00: /sys/kernel/mm/transparent_hugepage/enabled is 'always'. We suggest setting it to 'never'
------

test> db.version()
5.0.5
test> </code></pre>

Now you are good to run your first database instance.

### Uninstall MongoDB 5 {.wp-block-heading}

If you want to uninstall MongoDB, first you will have to stop the MongoDB running instance and then remove the packages

**Stop MongoDB instance**

<pre class="wp-block-code"><code>$ sudo systemctl stop mongod</code></pre>

**Remove MongoDB packages**

To completely remove the packages run the following command;

<pre class="wp-block-code"><code>$ sudo apt purge mongodb-org*</code></pre>

Lastly, **remove data** directories 

<pre class="wp-block-code"><code># log
$ sudo rm -r /var/log/mongodb
# lib
$ sudo rm -r /var/lib/mongodb</code></pre>

## MongoDB crud operations  {.wp-block-heading}

Crud operations include creating, reading, updating, and deleting.

To check the current database you are operating on type **db** 

<pre class="wp-block-code"><code>$ db 
test</code></pre>

Here am working on the test database.

To switch database you just say **use db_name** and it will create and switch to the database automatically.

<pre class="wp-block-code"><code>$ use nextgen</code></pre>

### Insert into MongoDB database {.wp-block-heading}

MongoDB provides the following methods to insert documents into a collection.

<pre class="wp-block-code"><code># insert one 
$ db.collection.insertOne()
# insert many
$ db.collection.insertMany()</code></pre>

Example 

<pre class="wp-block-code"><code>nextgen> db.nextgen.insertOne(&#91; { Name: 'Nextgentips', Niche: 'Linux Tech Blog' }])
{
  acknowledged: true,
  insertedId: ObjectId("61efff5d0a75a0e6b1815e0b")
}</code></pre>

In order to **select documents** you can use the find statement like this

<pre class="wp-block-code"><code>db.collection.find()</code></pre>

Example

<pre class="wp-block-code"><code>db.nexgen.find()</code></pre>

## Conclusion. {.wp-block-heading}

We have learned how to install MongoDB on Debian 11 bullseye. I am glad you have enjoyed the tutorial. Feel free to comment in case you are faced with a challenge.

 [1]: https://www.mongodb.com/json-and-bson
 [2]: https://nextgentips.com/2021/11/17/how-to-install-mongodb-with-podman-on-rocky-linux-8/
 [3]: https://nextgentips.com/2021/11/17/how-to-install-mongodb-4-4-on-fedora-35/
 [4]: https://nextgentips.com/2021/10/26/how-to-deploy-mongodb-with-docker/
 [5]: https://nextgentips.com/2021/10/26/how-to-install-mongodb-5-on-ubuntu-21-04/