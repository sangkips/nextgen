---
title: How to upgrade Linux Kernel on Rocky Linux/AlmaLinux/CentOS 8
author: Kipkoech Sang
type: post
date: 2021-11-14T19:08:05+00:00
url: /2021/11/14/how-to-upgrade-linux-kernel-on-rocky-linux-almalinux-centos-8/
ocean_gallery_link_images:
  - off
ocean_disable_margins:
  - enable
ocean_display_top_bar:
  - default
ocean_display_header:
  - default
ocean_disable_title:
  - default
ocean_disable_heading:
  - default
ocean_disable_breadcrumbs:
  - default
ocean_display_footer_widgets:
  - default
ocean_display_footer_bottom:
  - default
ocean_link_format_target:
  - self
ocean_quote_format_link:
  - post
entry_views:
  - 1
view_ip:
  - 'a:1:{i:0;s:13:"61.245.128.96";}'
rank_math_news_sitemap_robots:
  - index
rank_math_robots:
  - 'a:1:{i:0;s:5:"index";}'
rank_math_analytic_object_id:
  - 102
rank_math_internal_links_processed:
  - 1
categories:
  - Linux

---
In this guide I will show you how to upgrade our Linux Kernel to the latest version on our Rocky Linux/ AlmaLinux and CentOS.

Linux Kernel is a free and open source, monolithic, modular, multitasking unix-like operating system. It is the main component of a Linux operating system and is the core interface between the computer&#8217;s hardware and its processes. It makes communication possible between computer hardware and processes running on it and it manages resources effectively.

## Related Articles  {.wp-block-heading}

[How to upgrade Linux Kernel 5.13 to 5.15 on Ubuntu 21.10][1]

## Prerequisites  {.wp-block-heading}

  1. Rocky Linux server/AlmaLinux
  2. User with sudo privileges 
  3. Strong internet connection

## Table of Contents  {.wp-block-heading}

  1. Update the operating system repositories 
  2. Check current installed Linux Kernel
  3. Import ElRepo GPG key
  4. Install ElRepo repository for Rocky Linux
  5. Install latest Linux Kernel

## 1. Pass Updates on Rocky Linux/AlmaLinux {.wp-block-heading}

To begin our installation process, we need to update our repositories to reflect the new state of our operating system. We can do the update by issuing the following command on our terminal.

<pre class="wp-block-code"><code>$ sudo dnf update -y</code></pre>

When the update is complete, we can then check our current Linux Kernel to know where we are starting from.

## 2. Check current installed Linux Kernel  {.wp-block-heading}

To check the current running Linux Kernel in our system we can pass the following command in our terminal. It will show us the current installed Linux Kernel.

<pre class="wp-block-code"><code>$ uname -r</code></pre>

You out should look like this 

<pre class="wp-block-code"><code># uname -r
4.18.0-305.3.1.el8_4.x86_64</code></pre>

Now that we have seen the current installed Linux Kernel, let&#8217;s now begin the installation process but first we need to add the [GPG key to our repository.][2] 

## 3. Import ELRepo GPG Key {.wp-block-heading}

The ELRepo project uses a GPG key to sign all RPM packages that is released. Each ELRepo project is sign with a GPG signature. To ensure autenticity of the signature you must verify it before installation. It must match with the following fingerprint.

<pre class="wp-block-code"><code># Fingerprint: 96C0 104F 6315 4731 1E0B B1AE 309B C305 BAAD AE52</code></pre>

To verify your fingerprint you can use the following command on our terminal.

<pre class="wp-block-code"><code>$ gpg --quiet --with-fingerprint RPM-GPG-KEY-elrepo.org</code></pre>

If you have a matching key with the fingerprint then you can now import it. Use the below command to do the import.

<pre class="wp-block-code"><code>$ sudo rpm --import https://www.elrepo.org/RPM-GPG-KEY-elrepo.org</code></pre>

Next we can install ELRepo in our repository.

## 4. Install ELRepo repository on Rocky Linux {.wp-block-heading}

If your import was successful, you can now move ahead and install it in your system with the following command.

<pre class="wp-block-code"><code>$ sudo dnf install https://www.elrepo.org/elrepo-release-8.el8.elrepo.noarch.rpm</code></pre>

Press Y to allow installation to continue. The output you will get will be as follows 

Output

<pre class="wp-block-code has-normal-font-size"><code># sudo dnf install https://www.elrepo.org/elrepo-release-8.el8.elrepo.noarch.rpm
DigitalOcean Droplet Agent                                                        43 kB/s | 3.3 kB     00:00    
elrepo-release-8.el8.elrepo.noarch.rpm                                            52 kB/s |  13 kB     00:00    
Dependencies resolved.
=================================================================================================================
 Package                     Architecture        Version                         Repository                 Size
=================================================================================================================
Installing:
 elrepo-release              noarch              8.2-1.el8.elrepo                @commandline               13 k

Transaction Summary
=================================================================================================================
Install  1 Package

Total size: 13 k
Installed size: 5.0 k
Is this ok &#91;y/N]: y
Downloading Packages:
Running transaction check
Transaction check succeeded.
Running transaction test
Transaction test succeeded.
Running transaction
  Preparing        :                                                                                         1/1 
  Installing       : elrepo-release-8.2-1.el8.elrepo.noarch                                                  1/1 
  Verifying        : elrepo-release-8.2-1.el8.elrepo.noarch                                                  1/1 

Installed:
  elrepo-release-8.2-1.el8.elrepo.noarch                                                                         

Complete!</code></pre>

When all the prerequisites had been meet we can now install the latest kernel which is Linux Kernel 5.15 as of this writing.

## 5. Install latest Linux Kernel  {.wp-block-heading}

We are going to install the mainline version because that is always the latest release by the Kernel maintainers. You can use the following command.

<pre class="wp-block-code"><code>$ sudo dnf --enablerepo=elrepo-kernel install kernel-ml kernel-ml-devel kernel-ml-headers</code></pre>

After the installation is complete you must reboot the system for the new changes to take effect.

<pre class="wp-block-code"><code>$ sudo systemctl reboot </code></pre>

Check again to see if the changes have been effected.

<pre class="wp-block-code"><code>$ uname -r</code></pre>

You will see the following output 

<pre class="wp-block-code"><code># uname -r
5.15.2-1.el8.elrepo.x86_64</code></pre>

Hurray! we have successfully upgrade to Linux Kernel 5.15.2.

## Conclusion {.wp-block-heading}

Congratulations! You have learn how to upgrade Linux Kernel to the latest release on Rocky Linux. To learn more please consult the documentation.

 [1]: https://nextgentips.com/2021/11/01/how-to-upgrade-linux-kernel-5-13-to-5-15-on-ubuntu-21-10/
 [2]: https://help.ubuntu.com/community/GnuPrivacyGuardHowto#:~:text=%22GnuPG%20uses%20public%2Dkey%20cryptography,the%20user%20wants%20to%20communicate.%22