---
title: How to install PrestoDB with Podman
author: Kipkoech Sang
type: post
date: 2022-01-07T11:57:46+00:00
url: /2022/01/07/how-to-install-prestodb-with-podman/
entry_views:
  - 2
view_ip:
  - 'a:2:{i:0;s:15:"114.119.132.183";i:1;s:14:"54.242.182.186";}'
rank_math_news_sitemap_robots:
  - index
rank_math_robots:
  - 'a:1:{i:0;s:5:"index";}'
rank_math_analytic_object_id:
  - 40
rank_math_internal_links_processed:
  - 1
categories:
  - Database

---
In this article, we are going to learn how to install PrestoDB with Podman.

[Presto][1] is a high-performance, distributed SQL query engine for big data, its architecture allows users to query a variety of data sources such as Hadoop, AWS S3, Teradata, Cassandra, Kafka, etc. You can even query data from multiple sources within a single query. 

## Understanding Prestodb Concepts {.wp-block-heading}

## Server Types  {.wp-block-heading}

We have two types of servers, Workers and Coordinators.

**Coordinator** is a server responsible for parsing statements, planning, and managing Presto worker nodes. It&#8217;s a node to which a client connects in order to submit statements for execution.

**Worker** is responsible for executing tasks and processing data. Worker nodes fetch data from connectors and exchange intermediate data with each other. The coordinator is responsible for fetching results from the workers and returning the final results to the client.

**Connector** allows Presto to interact with a resource using a standard API.

**Catalog** contains schemas and references a data source via a connector. When addressing a table in Presto, the fully qualified table name is always rooted on the catalog.

**Schema** is a way to organize a table. Both catalog and schemas define a set of tables that can be queried.

**Table** is a set of unordered rows which are organized into named columns and types. 

## Prerequisites  {.wp-block-heading}

  * Make sure you have Podman up and running
  * Have any operating system running of your own
  * Have basic understanding of terminal 

So to start with I am going to be using Rocky Linux as my chosen operating system. You can choose any which you are conversant with.

## 1. Run system updates  {.wp-block-heading}

We need to ensure that our system repositories are up to date. On Rocky Linux terminal run system updates with the following command.

<pre class="wp-block-code"><code>$ sudo dnf update -y</code></pre>

## 2. Install Podman on Rocky Linux  {.wp-block-heading}

When updates are complete, proceed to install Podman. If you are running a Debian based system check this article on how to install Podman 

[How to install Podman 3 on Debian 11][2]

Let me take you through the installation of Podman on Rocky Linux.

To install Podman use the following command;

<pre class="wp-block-code"><code>$ sudo dnf install -y podman</code></pre>

This command will install Podman with all its dependencies.

Sample output will look like this 

<pre class="wp-block-code"><code>Output
....
Installed:
  conmon-2:2.0.29-1.module+el8.5.0+710+4c471e88.x86_64                                                           
  container-selinux-2:2.167.0-1.module+el8.5.0+710+4c471e88.noarch                                               
  containernetworking-plugins-1.0.0-1.module+el8.5.0+710+4c471e88.x86_64                                         
  containers-common-2:1-2.module+el8.5.0+710+4c471e88.noarch                                                     
  criu-3.15-3.module+el8.5.0+710+4c471e88.x86_64                                                                 
  fuse-common-3.2.1-12.el8.x86_64                                                                                
  fuse-overlayfs-1.7.1-1.module+el8.5.0+710+4c471e88.x86_64                                                      
  fuse3-3.2.1-12.el8.x86_64                                                                                      
  fuse3-libs-3.2.1-12.el8.x86_64                                                                                 
  iptables-1.8.4-20.el8.x86_64                                                                                   
  iptables-libs-1.8.4-20.el8.x86_64                                                                              
  libibverbs-35.0-1.el8.x86_64                                                                                   
  libnet-1.1.6-15.el8.x86_64                                                                                     
  libnetfilter_conntrack-1.0.6-5.el8.x86_64                                                                      
  libnfnetlink-1.0.1-13.el8.x86_64                                                                               
  libnftnl-1.1.5-4.el8.x86_64                                                                                    
  libpcap-14:1.9.1-5.el8.x86_64                                                                                  
  libslirp-4.4.0-1.module+el8.5.0+710+4c471e88.x86_64                                                            
  nftables-1:0.9.3-21.el8.x86_64                                                                                 
  pciutils-3.7.0-1.el8.x86_64                                                                                    
  &lt;strong>podman-3.3.1-9.module+el8.5.0+710+4c471e88.x86_64 &lt;/strong>                                                             
  podman-catatonit-3.3.1-9.module+el8.5.0+710+4c471e88.x86_64                                                    
  protobuf-c-1.3.0-6.el8.x86_64                                                                                  
  rdma-core-35.0-1.el8.x86_64                                                                                    
  runc-1.0.2-1.module+el8.5.0+710+4c471e88.x86_64                                                                
  slirp4netns-1.1.8-1.module+el8.5.0+710+4c471e88.x86_64 </code></pre>

You can check your Podman version with this command 

<pre class="wp-block-code"><code>$ podman --version
podman version 3.3.1</code></pre>

## 3. Start Podman Container  {.wp-block-heading}

To start Podman services, we need to start and enable podman service in our system which runs the container services. Use the below commands to do so.

<pre class="wp-block-code"><code>$ sudo systemctl start podman
$ sudo systemctl enable podman</code></pre>

## 4. Pull Prestodb image  {.wp-block-heading}

To run Podman images we are going to use **podman run** command to accomplish this, but first, we need to create a directory where we will store our Prestodb data

<pre class="wp-block-code"><code>$ mkdir data
$ cd data</code></pre>

Now use podman run to get Presto image 

<pre class="wp-block-code"><code>$ podman run -d \
     --name nextgenprestodb \
     -p 8080:8080 \
     -v $(pwd):/data/db:Z \
     ahanaio/prestodb-sandbox</code></pre>

Select the image to pull from

<pre class="wp-block-code"><code>Please select an image: 
  ▸ registry.fedoraproject.org/ahanaio/prestodb-sandbox:latest
    registry.access.redhat.com/ahanaio/prestodb-sandbox:latest
    registry.centos.org/ahanaio/prestodb-sandbox:latest
    docker.io/ahanaio/prestodb-sandbox:latest
✔ docker.io/ahanaio/prestodb-sandbox:latest
Trying to pull docker.io/ahanaio/prestodb-sandbox:latest...
Getting image source signatures
Copying blob 8b8a142162d2 done  
Copying blob 2b19b9881a49 done  
Copying blob b6411b120751 done  
Copying blob 0aa95afc0a74 done  
Copying blob 568624b43211 done  
Copying blob 930f129b845e done  
Copying blob e17b1de81e59 done  
Copying config c9b1b55401 done  
Writing manifest to image destination
Storing signatures
b1e045c0417a0ad1eb592d67dbf3193a3662a6ce683065242a7e434095d59ce2</code></pre>

Let&#8217;s check our image with **podman ps** command 

<pre class="wp-block-code"><code>$ podman ps </code></pre>

To access Prestodb from the browser use the following:

<pre class="wp-block-code"><code>$ http://&lt;your_IP_Address>:8080</code></pre>

## Conclusion {.wp-block-heading}

We have successfully deploy Prestodb using Podman. If incase of any difficulty feel free to get in touch for assistance.

 [1]: https://prestodb.io/docs/current/
 [2]: https://nextgentips.com/2021/10/22/how-to-install-podman-3-on-debian-11/