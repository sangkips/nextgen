---
title: Getting started with Amazon Elastic Compute Cloud(EC2)
author: Kipkoech Sang
type: post
date: 2022-10-07T10:13:20+00:00
url: /2022/10/07/getting-started-with-amazon-elastic-compute-cloudec2/
categories:
  - AWS
  - EC2

---
In this tutorial, we are going to learn about Amazon Elastic Compute Cloud(EC2).

Amazon Elastic Compute Cloud(Ec2) allows users to rent virtual machines on which to run their own computing functions. Ec2 provides a way to host your applications on the cloud at an affordable rate because data centers are much more expensive when you are not fully utilizing your services. 

What makes Ec2 suitable for production?

  * Ec2 provides resizeable compute capacity in the cloud. 
  * It reduces the time required to obtain and boot new server instances to minutes and allows you to scale faster.
  * You pay for what you require
  * The owner controls his own servers.
  * Ec2 is very reliable, secure, and easy to start.
  * It&#8217;s very inexpensive 
  * Secure login to your instance using key pairs 
  * You are provided with instance store volumes for temporary storage.
  * It provides you with elastic IP addresses for dynamic cloud computing.
  * It offers persistent storage volumes with the help of the Amazon Elastic Block store which is highly available and independent.

### Prerequisites {.wp-block-heading}

  * Have AWS account 
  * Select which region to spin your instance 
  * Create key pairs for a secure connection to our instance via ssh.

### Steps to Spin up EC2 {.wp-block-heading}

### 1. Login to your AWS account  {.wp-block-heading}

When you first login into your AWS account, you will be greeted with an interface like the one shown below.<figure class="wp-block-image size-large">

<img decoding="async" loading="lazy" width="810" height="419" src="https://i0.wp.com/nextgentips.com/wp-content/uploads/2022/10/Screenshot-from-2022-10-07-09-38-25.png?resize=810%2C419&#038;ssl=1" alt="nextgentips: AWS dashboard" class="wp-image-1563" srcset="https://i0.wp.com/nextgentips.com/wp-content/uploads/2022/10/Screenshot-from-2022-10-07-09-38-25.png?resize=1024%2C530&ssl=1 1024w, https://i0.wp.com/nextgentips.com/wp-content/uploads/2022/10/Screenshot-from-2022-10-07-09-38-25.png?resize=300%2C155&ssl=1 300w, https://i0.wp.com/nextgentips.com/wp-content/uploads/2022/10/Screenshot-from-2022-10-07-09-38-25.png?resize=768%2C397&ssl=1 768w, https://i0.wp.com/nextgentips.com/wp-content/uploads/2022/10/Screenshot-from-2022-10-07-09-38-25.png?w=1102&ssl=1 1102w" sizes="(max-width: 810px) 100vw, 810px" data-recalc-dims="1" /> <figcaption>nextgentips: AWS dashboard</figcaption></figure> 

2. Choose the region where you want to host your server. For me, I am using N.Virginia, Choose a region next to you.

3. From the search bar, search for EC2 and you will be taken to the Ec2 dashboard.<figure class="wp-block-image size-full">

<img decoding="async" loading="lazy" width="810" height="488" src="https://i0.wp.com/nextgentips.com/wp-content/uploads/2022/10/Screenshot-from-2022-10-07-09-31-30-1.png?resize=810%2C488&#038;ssl=1" alt="nextgentips: Ec2 dashboard" class="wp-image-1564" srcset="https://i0.wp.com/nextgentips.com/wp-content/uploads/2022/10/Screenshot-from-2022-10-07-09-31-30-1.png?w=932&ssl=1 932w, https://i0.wp.com/nextgentips.com/wp-content/uploads/2022/10/Screenshot-from-2022-10-07-09-31-30-1.png?resize=300%2C181&ssl=1 300w, https://i0.wp.com/nextgentips.com/wp-content/uploads/2022/10/Screenshot-from-2022-10-07-09-31-30-1.png?resize=768%2C463&ssl=1 768w" sizes="(max-width: 810px) 100vw, 810px" data-recalc-dims="1" /> <figcaption>nextgentips: Ec2 dashboard</figcaption></figure> 

4. Create key pair for ssh login. This is critical whenever you want to log in via ssh. Go to key pair and click create key pair. Choose a name, key pair type, private key file format, .pem for Linux environment and .ppk for windows and lastly click create<figure class="wp-block-image size-large">

<img decoding="async" loading="lazy" width="810" height="405" src="https://i0.wp.com/nextgentips.com/wp-content/uploads/2022/10/Screenshot-from-2022-10-07-09-47-56.png?resize=810%2C405&#038;ssl=1" alt="nextgentips: key pair" class="wp-image-1565" srcset="https://i0.wp.com/nextgentips.com/wp-content/uploads/2022/10/Screenshot-from-2022-10-07-09-47-56.png?resize=1024%2C512&ssl=1 1024w, https://i0.wp.com/nextgentips.com/wp-content/uploads/2022/10/Screenshot-from-2022-10-07-09-47-56.png?resize=300%2C150&ssl=1 300w, https://i0.wp.com/nextgentips.com/wp-content/uploads/2022/10/Screenshot-from-2022-10-07-09-47-56.png?resize=768%2C384&ssl=1 768w, https://i0.wp.com/nextgentips.com/wp-content/uploads/2022/10/Screenshot-from-2022-10-07-09-47-56.png?w=1074&ssl=1 1074w" sizes="(max-width: 810px) 100vw, 810px" data-recalc-dims="1" /> <figcaption>nextgentips: key pair</figcaption></figure> 

5. Next is to create a security group that acts as a firewall for your instance to control inbound and outbound traffic. It&#8217;s easy to create, just click create input name, and description, choose VPC to use, and create different rules you want to allow.

6. Now it&#8217;s time to launch the instance. On the EC2 dashboard click on launch instance then input the name of the instance, then choose OS for that instance. Always choose free tier OS for experimentation to avoid incurring any expenses in the long run. The next thing is to choose your instance type. Select the t2.micro which is free tier eligible. Then you need to choose the key pair we created earlier. Move to network settings and choose the security group we created earlier also or you can create one if you didn&#8217;t create one at the beginning. For now, leave advance settings and click launch instance.

7. After you have successfully initiated the ec2 instance, click view all instances and you will see something like the below image.<figure class="wp-block-image size-large">

<img decoding="async" loading="lazy" width="810" height="123" src="https://i0.wp.com/nextgentips.com/wp-content/uploads/2022/10/Screenshot-from-2022-10-07-10-33-32.png?resize=810%2C123&#038;ssl=1" alt="" class="wp-image-1568" srcset="https://i0.wp.com/nextgentips.com/wp-content/uploads/2022/10/Screenshot-from-2022-10-07-10-33-32.png?resize=1024%2C155&ssl=1 1024w, https://i0.wp.com/nextgentips.com/wp-content/uploads/2022/10/Screenshot-from-2022-10-07-10-33-32.png?resize=300%2C45&ssl=1 300w, https://i0.wp.com/nextgentips.com/wp-content/uploads/2022/10/Screenshot-from-2022-10-07-10-33-32.png?resize=768%2C116&ssl=1 768w, https://i0.wp.com/nextgentips.com/wp-content/uploads/2022/10/Screenshot-from-2022-10-07-10-33-32.png?w=1059&ssl=1 1059w" sizes="(max-width: 810px) 100vw, 810px" data-recalc-dims="1" /> <figcaption>nextgentips: Ec2 instance</figcaption></figure> 

8. The next step is to connect the instance so that we can do whatever we want with it. Click on the checkbox and click on connect. You will see the following being shown.<figure class="wp-block-image size-full">

<img decoding="async" loading="lazy" width="800" height="464" src="https://i0.wp.com/nextgentips.com/wp-content/uploads/2022/10/Screenshot-from-2022-10-07-10-46-25.png?resize=800%2C464&#038;ssl=1" alt="nextgentips: connect instance" class="wp-image-1569" srcset="https://i0.wp.com/nextgentips.com/wp-content/uploads/2022/10/Screenshot-from-2022-10-07-10-46-25.png?w=800&ssl=1 800w, https://i0.wp.com/nextgentips.com/wp-content/uploads/2022/10/Screenshot-from-2022-10-07-10-46-25.png?resize=300%2C174&ssl=1 300w, https://i0.wp.com/nextgentips.com/wp-content/uploads/2022/10/Screenshot-from-2022-10-07-10-46-25.png?resize=768%2C445&ssl=1 768w" sizes="(max-width: 800px) 100vw, 800px" data-recalc-dims="1" /> <figcaption>nextgentips: connect instance</figcaption></figure> 

I always love connecting via ssh client, this gives me an isolated environment away from the Ec2 interface. So to connect via ssh follow the procedure shown in the image above.

Always use the best way that suits you to connect to your instance. From you can start running your intended functions of the instance.

9. Terminate your instance when you are done with your instance to avoid incurring extra costs. This is done by clicking instance state and clicking terminate. 

Congrats on spinning up your first instance on AWS. Happy coding!