---
title: How to install Travis CI on Ubuntu 20.04
author: Kipkoech Sang
type: post
date: 2021-11-29T11:02:16+00:00
url: /2021/11/29/how-to-install-travis-ci-on-ubuntu-20-04/
entry_views:
  - 1
view_ip:
  - 'a:1:{i:0;s:15:"136.243.148.249";}'
rank_math_news_sitemap_robots:
  - index
rank_math_robots:
  - 'a:1:{i:0;s:5:"index";}'
rank_math_analytic_object_id:
  - 82
rank_math_internal_links_processed:
  - 1
categories:
  - Automation

---
In this tutorial, I am going to take you through the installation of Travis CI on UIbuntu 20.04.

Travis CI is a hosted continuous integration service used to build and test software projects hosted on Github and Bitbucket. Travis CI provided services to Open-source projects for free.

Travis CI enables teams to test and ship apps with confidence. You can easily sync your projects with Travis CI. Travis CI automatically detects when a commit is made and pushed to the GitHub repository that is using Travis CI, and each time this happens, it will try to build the project and run tests.

## Continuos Integration (CI) {.wp-block-heading}

Continuous Integration is the practice of merging in small code changes frequently rather than merging in a large change at the end of a software development cycle.

The goal of CI is to build healthier software by developing and testing in smaller increments. When you run a build Travis CI clones your GitHub repository into a brand new virtual environment and carries out a series of tasks to build and test your code. If one or more builds fail, it is considered **broken**. If non of the tasks fail the process is considered **passed**.

## Features of Travis CI {.wp-block-heading}

  * Travis CI is available for Continuous Delivery and Continuos Integration.
  * Travis CI is available for open source projects 
  * It supports integration with other external tools eg slack, email
  * It supports a large set of programming languages 
  * It is available for Linux, MacOS and Windows environment
  * Travis CI supports parallel testing 
  * It has community support

## Prerequisites  {.wp-block-heading}

  * You need to have GitHub, Bitbucket, Assemla or Gitlab account. Either one of the accounts.
  * Have owner permission for a project hosted on either, Gitlab, GitHub, Bitbucket or Assembla.

## Install Travis CI on Ubuntu 20.04 using Snap. {.wp-block-heading}

Snaps are applications packaged with all their dependencies to run on all popular Linux distributions from a single build. Applications installed using snap are always up to date because updates are done automatically.

## 1. Update Ubuntu 20.04 system repositories. {.wp-block-heading}

To run updates in the system, run the following command on your terminal.

<pre class="wp-block-code"><code>$ sudo apt update && apt upgrade -y</code></pre>

When updates are complete, enable **[snapd][1]** 

## 2. Enable snapd {.wp-block-heading}

Let&#8217;s enable snapd on your system with the following command. snapd is a rest API daemon for managing snap packages. Users can interact with it by using the snap client.

<pre class="wp-block-code"><code>$ sudo apt install snapd</code></pre>

Snapd come preinstalled with Ubuntu 20.04.

## Install Travis CI on Ubuntu 20.04. {.wp-block-heading}

You can now install Travis using the following command.

<pre class="wp-block-code"><code>$ sudo snap install travis
travis 1.8.9 from Travis CI✓ installed</code></pre>

You can check on the version you have installed with the following command.

<pre class="wp-block-code"><code>$ travis version
1.8.9</code></pre>

## Using Travis CI with GitHub {.wp-block-heading}

Follow the following procedure to start using Travis CI with GitHub.

  1. Go to  [Travis-ci.com][2] and [sign up with GitHub][3]
  2. Accept Authorization of Travis CI. You will get redirected to GitHub
  3. Click on your profile picture, click settings and then the green activation button and select repositories you want to use with Travis CI
  4. Add travis.yaml file to your repository to tell Travis CI what to do.

## Conclusion {.wp-block-heading}

There you go, we have managed to install Travis CI on our Ubuntu 20.04 system.

 [1]: https://wiki.archlinux.org/title/Snap
 [2]: https://travis-ci.com/
 [3]: https://travis-ci.com/signin