---
title: How to install Python 3.9 interpreter on Ubuntu 20.04
author: Kipkoech Sang
type: post
date: 2021-10-04T13:33:27+00:00
url: /2021/10/04/install-python-3-on-ubuntu-20-04/
site-sidebar-layout:
  - default
site-content-layout:
  - default
theme-transparent-header-meta:
  - default
rank_math_primary_category:
  - 6
rank_math_news_sitemap_robots:
  - index
rank_math_robots:
  - 'a:1:{i:0;s:5:"index";}'
rank_math_analytic_object_id:
  - 147
rank_math_internal_links_processed:
  - 1
categories:
  - Linux

---
In this tutorial guide, we are going to learn how to install Python 3.9 on Ubuntu 20.04.

Python is an interpreted high-level general-purpose programming language. Its designed philosophy emphasizes code readability with its use of significant indentation.

We can begin by installing a python 3 interpreter on our system. This is a program that reads Python programs and carries out their instructions, you need it before you can begin python programming.

Linux distribution may include an outdated version of Python 2. We need to install Python 3 which is an updated version. 

There are two ways we can install the official Python distribution in Linux:

  * Using operating system package manager 
  * Building Python from source code

Let&#8217;s first check the version of python in our systems.

<pre class="wp-block-syntaxhighlighter-code">python --version
Command 'python' not found, did you mean:

  command 'python3' from deb python3
  command 'python' from deb python-is-python3
</pre>

Check python 2 version

<pre class="wp-block-syntaxhighlighter-code">python2 --version
Command 'python2' not found, but can be installed with:

sudo apt install python2
</pre>

check python3 version

<pre class="wp-block-syntaxhighlighter-code">python3 --version
Python 3.8.10</pre>

From the above, you can see that Python2 isn&#8217;t available from the latest version of ubuntu 20.04. Python3 comes as a default.

Let&#8217;s begin the installation process: Type the following code into your terminal. 

<pre class="wp-block-syntaxhighlighter-code">sudo apt-get update</pre>

This code updates the Ubuntu repository:

## Install Python 3.9  {.wp-block-heading}

Next, we can install Python 3.9 plus pip package which is the package installer for Python. 

<pre class="wp-block-syntaxhighlighter-code">sudo apt-get install python3.9 python3-pip</pre>

And that&#8217;s all with python installation.

Now let&#8217;s check once again

<pre class="wp-block-syntaxhighlighter-code">python3.9 --version
Python 3.9.5</pre>

That is all. Happy coding.