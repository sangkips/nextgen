---
title: How to install Envoy Proxy server on Ubuntu 20.04
author: Kipkoech Sang
type: post
date: 2021-12-14T15:32:32+00:00
url: /2021/12/14/how-to-install-envoy-proxy-server-on-ubuntu-20-04/
rank_math_news_sitemap_robots:
  - index
rank_math_robots:
  - 'a:1:{i:0;s:5:"index";}'
rank_math_analytic_object_id:
  - 63
rank_math_internal_links_processed:
  - 1
categories:
  - Linux

---
In this tutorial, I will show you how to install the Envoy proxy server on Ubuntu 20.04.

[Envoy][1] is an L7 proxy and communication bus designed for large modern service-oriented architecture. The project was born out of the belief that the network should be transparent to applications. When network and applications problems occur, it should be easy to determine the source of the problem.

Envoy is an open-source edge and service proxy, designed for cloud-native applications. 

## High Level Envoy features  {.wp-block-heading}

  * Its known for its advanced load balancing technique. It implements load balancing in a single place and have them accessible to any application.
  * It has front/edge proxy support. 
  * It has best in class observability
  * It has gRPC support.
  * Has support for HTTP L7 routing 
  * It supports HTTP/2 
  * L3/L4 filter architecture. Envoy acts as a L3/L4 proxy
  * It support use of API for configuration management 

## Installing Envoy proxy server on Ubuntu 20.04 {.wp-block-heading}

## 1. Run system update {.wp-block-heading}

Running system updates will make your system repositories up to date. Open your terminal and run the following command;

<pre class="wp-block-code"><code>$ sudo apt update && apt upgrade -y</code></pre>

Next, you need to envoy dependencies 

## 2. Install Envoy proxy server dependencies  {.wp-block-heading}

Envoy requires some dependencies to run effectively. Install the following dependencies 

<pre class="wp-block-code"><code>$ sudo apt install apt-transport-https gnupg2 curl lsb-release</code></pre>

Sample output

<pre class="wp-block-code"><code>output
Reading package lists... Done
Building dependency tree       
Reading state information... Done
lsb-release is already the newest version (11.1.0ubuntu2).
lsb-release set to manually installed.
curl is already the newest version (7.68.0-1ubuntu2.7).
curl set to manually installed.
apt-transport-https is already the newest version (2.0.6).
gnupg2 is already the newest version (2.2.19-3ubuntu2.1).
0 upgraded, 0 newly installed, 0 to remove and 0 not upgraded.</code></pre>

From the following output, you can see nothing was installed because Ubuntu 20.04 comes as a default.

## 3. Import GPG key {.wp-block-heading}

In order to sign Envoy, we need to import the GPG key with the following command. 

<pre class="wp-block-code"><code>$ curl -sL 'https://deb.dl.getenvoy.io/public/gpg.8115BA8E629CC074.key' | sudo gpg --dearmor -o /usr/share/keyrings/getenvoy-keyring.gpg</code></pre>

Now you can verify if the key is signed with the **echo command**. If it returns ok you know it is signed.

<pre class="wp-block-code"><code>$ echo a077cb587a1b622e03aa4bf2f3689de14658a9497a9af2c427bba5f4cc3c4723 /usr/share/keyrings/getenvoy-keyring.gpg | sha256sum --check</code></pre>

This command should return ok as the output.

Run system updates again for changes to take effect.

Now we need to add the key to the system with the following command.

<pre class="wp-block-code"><code>$ echo "deb &#91;arch=amd64 signed-by=/usr/share/keyrings/getenvoy-keyring.gpg] https://deb.dl.getenvoy.io/public/deb/ubuntu $(lsb_release -cs) main" | sudo tee /etc/apt/sources.list.d/getenvoy.list</code></pre>

## 4. Install Envoy proxy server on Ubuntu 20.04 {.wp-block-heading}

Lastly, let&#8217;s install Envoy with the following command;

<pre class="wp-block-code"><code>$ sudo apt install getenvoy-envoy</code></pre>

Now you can check the Envoy version with the following command;

<pre class="wp-block-code"><code>$ envoy --version
envoy  version: d362e791eb9e4efa8d87f6d878740e72dc8330ac/1.18.2/clean-getenvoy-76c310e-envoy/RELEASE/BoringSSL</code></pre>

## Run Envoy {.wp-block-heading}

To check Envoy commands use the help command

<pre class="wp-block-code"><code>$ envoy --help</code></pre>

Let&#8217;s run Envoy with a demo configuration file

Create a demo.yaml file and run with the following command 

<pre class="wp-block-code"><code>$ envoy -c envoy-demo.yaml</code></pre>

**-c** tells envoy the path to its initial configuration

To know if Envoy is proxying, use the following 

<pre class="wp-block-code"><code>$ curl -v localhost:10000</code></pre>

## Conclusion {.wp-block-heading}

We have learned how to install Envoy proxy on Ubuntu 20.04. To enjoy more of these tutorials keep checking this blog for more.

 [1]: https://www.envoyproxy.io/docs/envoy/latest/intro/what_is_envoy