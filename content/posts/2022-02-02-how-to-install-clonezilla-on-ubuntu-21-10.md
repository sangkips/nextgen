---
title: How to install Clonezilla on Ubuntu 21.10
author: Kipkoech Sang
type: post
date: 2022-02-02T07:33:52+00:00
url: /2022/02/02/how-to-install-clonezilla-on-ubuntu-21-10/
rank_math_primary_category:
  - 6
rank_math_description:
  - Clonezilla is a suite of open-source, disk cloning program used for bare metal backup and recovery and used during system deployment.
rank_math_focus_keyword:
  - Install Clonezilla
rank_math_news_sitemap_robots:
  - index
rank_math_robots:
  - 'a:1:{i:0;s:5:"index";}'
rank_math_analytic_object_id:
  - 10
rank_math_internal_links_processed:
  - 1
categories:
  - Linux

---
[Clonezilla][1] is a suite of open-source, disk cloning programs used for bare metal backup Fand recovery and also used during system deployment. Clonezilla server edition uses multicast technologies to deploy a single image file to a group of computers in a local area network.

In this tutorial guide, we are going to explore how to install and use Clonezilla on Ubuntu 21.10.

## Types of Clonezilla {#types-of-clonezilla.wp-block-heading}

  * Clonezilla live. This is suitable for single machine backup and restore. It allows you to use the CD/DVD or USB flash drive to boot and run clonezilla
  * Clonezilla lite server. Suitable for massive deployments. Ity allows you to use clonezilla live to do massive clonnning.
  * Clonezilla SE (server edition). Suitable for massive deployments

Clonezilla only saves and restores only used blocks in the hard disk, therefore, increasing the clone efficiency.

## Features of Clonezilla {#features-of-clonezilla.wp-block-heading}

  * It supports many File sytems such as ext2, ext3, eiserfs, reiser4, xfs, jfs, btrfs, f2fs etc
  * It support both MBR and GPT partion formats of hard drive are supported.
  * One image restoring to multiple local devices are supported
  * Images are encrypted in the process
  * AES-256 encryption is used to secure data access, storage and transfer
  * Multicast is supported on Clonzilla SE which is suitable for massive clone.

## Prerequisites {#prerequisites.wp-block-heading}

  * X86 or X86-X64 processor
  * 256 MB of RAM
  * Boot devices
  * Ubuntu 21.10 server

## Install Clonezilla on Ubuntu 21.10 {#install-clonezilla-on-ubuntu-21-10.wp-block-heading}

### 1. Update system repositories  {#1-update-system-repositories.wp-block-heading}

The first thing to do on any new system is to update its repositories in order to make them up to date. 

<pre class="wp-block-code"><code>$ sudo apt update && apt upgrade -y</code></pre>

### 2. Install Clonezilla on Ubuntu 21.10 {#2-install-clonezilla-on-ubuntu-21-10.wp-block-heading}

After updates and upgrades are complete, we can now run the installation program with the following command on your terminal. The program will install all the dependencies as needed by Clonezilla.

<pre class="wp-block-code"><code>$ sudo apt install clonezilla -y</code></pre>

You will see the following as the output.

<pre class="wp-block-code"><code>The following additional packages will be installed:
  dialog drbl ipcalc mtools nilfs-tools partclone partimage pigz syslinux syslinux-common wakeonlan
Suggested packages:
  cifs-utils udpcast sshfs floppyd
The following NEW packages will be installed:
  clonezilla dialog drbl ipcalc mtools nilfs-tools partclone partimage pigz syslinux syslinux-common wakeonlan
0 upgraded, 12 newly installed, 0 to remove and 0 not upgraded.
Need to get 4777 kB of archives.
After this operation, 18.3 MB of additional disk space will be used.
Do you want to continue? &#91;Y/n] </code></pre>

Press y to allow installation to continue.

To start using Clonezilla, type **Clonezilla** on your command line

<pre class="wp-block-code"><code>$ clonezilla</code></pre>

You will get the following image;<figure class="wp-block-image size-full is-resized">

<img decoding="async" loading="lazy" src="https://i0.wp.com/nextgentips.com/wp-content/uploads/2022/02/Screenshot-from-2022-02-02-10-10-05.png?resize=810%2C585&#038;ssl=1" alt="Clonezilla interface " class="wp-image-1123" width="810" height="585" srcset="https://i0.wp.com/nextgentips.com/wp-content/uploads/2022/02/Screenshot-from-2022-02-02-10-10-05.png?w=886&ssl=1 886w, https://i0.wp.com/nextgentips.com/wp-content/uploads/2022/02/Screenshot-from-2022-02-02-10-10-05.png?resize=300%2C217&ssl=1 300w, https://i0.wp.com/nextgentips.com/wp-content/uploads/2022/02/Screenshot-from-2022-02-02-10-10-05.png?resize=768%2C556&ssl=1 768w" sizes="(max-width: 810px) 100vw, 810px" data-recalc-dims="1" /> <figcaption>Clonezilla interface </figcaption></figure> 

### 3. Using Clonezilla {#3-using-clonezilla.wp-block-heading}

Once you have a Clonezilla bootable device, you can then boot the media to the machine you want. Use the Clonezilla media such as CD, USB flash drive etc.

## Conclusion {#conclusion.wp-block-heading}

We have successfully installed Clonezilla on Ubuntu 21.10. If you experience any difficulty, consult <a href="https://clonezilla.org/clonezilla-usage/general-live-use.php" target="_blank" rel="noreferrer noopener" title="Clonzilla documentation">Clonzilla documentation</a>. 

Check this article if you want to use Ubuntu 20.04

<a href="https://nextgentips.com/2021/12/06/how-to-install-and-use-clonezilla-on-ubuntu-20-04/" target="_blank" rel="noreferrer noopener">How to install and use Clonezilla on Ubuntu 20.04</a>

 [1]: https://clonezilla.org/