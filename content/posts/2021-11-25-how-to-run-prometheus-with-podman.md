---
title: How to run Prometheus with Podman
author: Kipkoech Sang
type: post
date: 2021-11-25T17:50:45+00:00
url: /2021/11/25/how-to-run-prometheus-with-podman/
rank_math_news_sitemap_robots:
  - index
rank_math_robots:
  - 'a:1:{i:0;s:5:"index";}'
rank_math_analytic_object_id:
  - 85
rank_math_internal_links_processed:
  - 1
categories:
  - Monitoring

---
Prometheus is an open-source system monitoring and alerting toolkit. Prometheus collects and stores its metrics as time-series data. Metrics information is stored with the timestamp at which it was recorded, alongside optional key-value pairs called labels.

In this tutorial we are going to learn how to run Prometheus with Podman.

## Components of Prometheus {.wp-block-heading}

  * Has an alertmanager to handle alerts
  * Has a push gateway for supporting short-lived jobs
  * Has main Prometheus server which scrapes and stores time series data
  * Client libraries for instrumenting application code
  * Special purpose exporters for services like HAProxy, statD, Graphite etc

## Related Articles  {.wp-block-heading}

  * [How to install Prometheus 2.30.3 Server in Ubuntu 20.04][1]

## Prerequisites  {.wp-block-heading}

  * Have a basic understanding of Linux terminal
  * Make sure you have access to internet

## Table of Contents {.wp-block-heading}

  * Run system updates 
  * Install podman
  * Pull Prometheus image
  * Start Prometheus

## Run system updates  {.wp-block-heading}

Running system updates are mandatory if you expect your system packages to be up to date every time. Let&#8217;s run updates with the following command; I am using Fedora 35, you can use any distro you like.

<pre class="wp-block-code"><code>$ sudo dnf update -y</code></pre>

When the updates are complete, we can proceed to install podman.

## 2. Install Podman {.wp-block-heading}

Podman is a daemon-less, open-source, Linux native tool designed to make it easy to find, run, build, share and deploy applications using Open Container Initiative (OCI) containers and container images. Containers can be run as root or as a regular users. Podman = Docker

Let&#8217;s install Podman with the following command;

<pre class="wp-block-code"><code>$ sudo dnf install podman -y</code></pre>

You have successfully installed podman on Fedora 35. 

You will see the following sample output.

<pre class="wp-block-code"><code>Sample output
Installing:
&lt;strong> podman                                 x86_64            3:3.4.2-1.fc35                updates             12 M&lt;/strong>
Installed:
  catatonit-0.1.7-1.fc35.x86_64                        conmon-2:2.0.30-2.fc35.x86_64                             
  container-selinux-2:2.170.0-2.fc35.noarch            containernetworking-plugins-1.0.1-1.fc35.x86_64           
  containers-common-4:1-32.fc35.noarch                 criu-3.16.1-2.fc35.x86_64                                 
  criu-libs-3.16.1-2.fc35.x86_64                       crun-1.3-1.fc35.x86_64                                    
  dnsmasq-2.86-3.fc35.x86_64                           fuse-common-3.10.5-1.fc35.x86_64                          
  fuse-overlayfs-1.7.1-2.fc35.x86_64                   fuse3-3.10.5-1.fc35.x86_64                                
  fuse3-libs-3.10.5-1.fc35.x86_64                      iptables-legacy-1.8.7-13.fc35.x86_64                      
  libbsd-0.10.0-8.fc35.x86_64                          libnet-1.2-4.fc35.x86_64                                  
  libnftnl-1.2.0-2.fc35.x86_64                         libslirp-4.6.1-2.fc35.x86_64                              
  nftables-1:1.0.0-1.fc35.x86_64                       podman-3:3.4.2-1.fc35.x86_64                              
  podman-gvproxy-3:3.4.2-1.fc35.x86_64                 podman-plugins-3:3.4.2-1.fc35.x86_64                      
  shadow-utils-subid-2:4.9-7.fc35.x86_64               slirp4netns-1.1.12-2.fc35.x86_64                          
  yajl-2.1.0-17.fc35.x86_64                           

Complete!</code></pre>

You can check the version of Podman installed.

<pre class="wp-block-code"><code>$ podman --version
podman version 3.4.2</code></pre>

If you get the version of Podman, then you have successfully installed Podman.

Now we can proceed to pull Prometheus image.

## 3. Pull Prometheus image. {.wp-block-heading}

To pull Prometheus, use the podman pull command. Let&#8217;s see how we can do that;

<pre class="wp-block-code"><code>$ podman pull prom/prometheus</code></pre>

You will get the following sample output. Choose where you could pull your image from.

<pre class="wp-block-code"><code>Sample output
? Please select an image: 
    registry.fedoraproject.org/prom/prometheus:latest
    registry.access.redhat.com/prom/prometheus:latest
    docker.io/prom/prometheus:latest
  ▸ quay.io/prom/prometheus:latest
✔ docker.io/prom/prometheus:latest
Trying to pull docker.io/prom/prometheus:latest...
Getting image source signatures
Copying blob 620e8dfd5447 done  
Copying blob 21b2df964046 done  
Copying blob ddd09594f788 done  
Copying blob aa2a8d90b84c done  
Copying blob b45d31ee2d7f done  
Copying blob 7d3b94bc0edd done  
Copying blob ddbba1a8341e done  
Copying blob ceb78b2ad8d8 done  
Copying blob cf4a6b37f5be done  
Copying blob ae4192f5013d done  
Copying blob f87c90634d35 done  
Copying blob 1860fdfbca21 done  
Copying config c10e9cbf22 done  
Writing manifest to image destination
Storing signatures
c10e9cbf22cd70dc656320cffe4f42347f114702a061395d586b492386720be0</code></pre>

From the above choices, you can only pull from docker.io 

We can verify our image like this;

<pre class="wp-block-code"><code>$ podman images 
REPOSITORY                 TAG         IMAGE ID      CREATED      SIZE
docker.io/prom/prometheus  latest      c10e9cbf22cd  2 weeks ago  196 MB</code></pre>

We can see that our imge is there. To confirm use the image ID.

## 4. Start Prometheus Container  {.wp-block-heading}

To start Prometheus, we would first need to create a directory where we will store our data.

<pre class="wp-block-code"><code>$ sudo mkdir data
cd data</code></pre>

Once inside data directory run the following command power up the Prometheus.

<pre class="wp-block-code"><code>$ podman run --name myprometheus -d -p 9090:9090 -v $(pwd):/data/db:Z prometheus </code></pre>

You can use **podman ps** command to know if it running;

<pre class="wp-block-code"><code>$ podman ps
CONTAINER ID  IMAGE                             COMMAND               CREATED         STATUS             PORTS                   NAMES
64779f0e23f6  docker.io/prom/prometheus:latest  --config.file=/et...  22 seconds ago  Up 22 seconds ago  0.0.0.0:9090->9090/tcp  myprometheus</code></pre>

From the above, it shows it is up

Now let&#8217;s open prometheus in our preferred browser. 

<pre class="wp-block-code"><code>http://&lt;Your_IP_Address>:9090</code></pre>

You will get the following from the browser<figure class="wp-block-image size-large">

<img decoding="async" loading="lazy" width="810" height="270" src="https://i0.wp.com/nextgentips.com/wp-content/uploads/2021/11/Screenshot-from-2021-11-25-20-23-57-1.png?resize=810%2C270&#038;ssl=1" alt="Prometheus Dashboard" class="wp-image-719" srcset="https://i0.wp.com/nextgentips.com/wp-content/uploads/2021/11/Screenshot-from-2021-11-25-20-23-57-1.png?resize=1024%2C341&ssl=1 1024w, https://i0.wp.com/nextgentips.com/wp-content/uploads/2021/11/Screenshot-from-2021-11-25-20-23-57-1.png?resize=300%2C100&ssl=1 300w, https://i0.wp.com/nextgentips.com/wp-content/uploads/2021/11/Screenshot-from-2021-11-25-20-23-57-1.png?resize=768%2C256&ssl=1 768w, https://i0.wp.com/nextgentips.com/wp-content/uploads/2021/11/Screenshot-from-2021-11-25-20-23-57-1.png?w=1294&ssl=1 1294w" sizes="(max-width: 810px) 100vw, 810px" data-recalc-dims="1" /> <figcaption>Prometheus Dashboard</figcaption></figure> 

Move to status and click configuration to see the Prometheus.yml file.

The following is a sample output.

<pre class="wp-block-code"><code>Sample output
global:
  scrape_interval: 15s
  scrape_timeout: 10s
  evaluation_interval: 15s
alerting:
  alertmanagers:
  - follow_redirects: true
    scheme: http
    timeout: 10s
    api_version: v2
    static_configs:
    - targets: &#91;]
scrape_configs:
- job_name: prometheus
  honor_timestamps: true
  scrape_interval: 15s
  scrape_timeout: 10s
  metrics_path: /metrics
  scheme: http
  follow_redirects: true
  static_configs:
  - targets:
    - localhost:9090</code></pre>

## 5. Conclusion {.wp-block-heading}

You have learned how to run Prometheus with Podman. Don&#8217;t hesitate to contact us in case of any difficulty. Also, you can read more on [Podman Documentation][2].

 [1]: https://nextgentips.com/2021/10/07/how-to-install-prometheus-server-in-ubuntu-20-04/
 [2]: https://github.com/containers/podman/blob/main/docs/tutorials/image_signing.md