---
title: How to install InfluxDB on Ubuntu 20.04
author: Kipkoech Sang
type: post
date: 2022-02-08T13:46:58+00:00
url: /2022/02/08/how-to-install-influxdb-on-ubuntu-20-04/
rank_math_news_sitemap_robots:
  - index
rank_math_robots:
  - 'a:1:{i:0;s:5:"index";}'
rank_math_analytic_object_id:
  - 8
rank_math_internal_links_processed:
  - 1
categories:
  - Database

---
InfluxDB is an open-source time-series database. It is used for storage and retrieval of time series data in fields such as operation monitoring, operations metrics, internet of things sensor data, and real-time analytics. In this tutorial, we are going to learn how to install InfluxDB on Ubuntu 20.04.

By default, InfluxDB uses the following network ports for communication. If you need to change any port configuration head over to **/etc/influxdb/influxdb.conf**.

  * **TCP port 8086** for client-server communication.
  * **TCP port 8088** for RPC service to perform backup and restore operations.

## Why InfluxDB? {#why-influxdb.wp-block-heading}

  * It provides deep insight and analytics 
  * InfluxDB is optimized for developer productivity
  * It has best UI which features dashboard, explorer and script editor.
  * It has faster time to market. That is production is faster while usin influxDB.
  * It is easy to build and easy to share templates.

## Prerequisites  {#prerequisites.wp-block-heading}

  * You need to be root to perform tasks
  * Have basic knowledge of terminal.
  * Have Ubuntu 20.04 server up and running.

## Installing InfluxDB on Ubuntu 20.04 {#installing-influxdb-on-ubuntu-20-04.wp-block-heading}

### 1. Update system repositories {#1-update-system-repositories.wp-block-heading}

In order to make our system repositories up to date, use the following command in your terminal.

<pre class="wp-block-code"><code>$ sudo apt update && apt upgrade </code></pre>

### 2. Add InfluxDB repository to Ubuntu Repository {#2-add-influxdb-repository-to-ubuntu-repository.wp-block-heading}

To install the stable latest version using apt, run the following command using curl.

**Add key**

<pre class="wp-block-code"><code>$ curl -s https://repos.influxdata.com/influxdb.key | gpg --dearmor > /etc/apt/trusted.gpg.d/influxdb.gpg</code></pre>

<pre class="wp-block-code"><code>$ export DISTRIB_ID=$(lsb_release -si); export DISTRIB_CODENAME=$(lsb_release -sc)</code></pre>

### 3. Add Repository {#3-add-repository.wp-block-heading}

<pre class="wp-block-code"><code>$ echo "deb &#91;signed-by=/etc/apt/trusted.gpg.d/influxdb.gpg] https://repos.influxdata.com/${DISTRIB_ID,,} ${DISTRIB_CODENAME} stable" > /etc/apt/sources.list.d/influxdb.list</code></pre>

Then update the system repositories again

<pre class="wp-block-code"><code>$ sudo apt update
&lt;strong>Get:1 https://repos.influxdata.com/ubuntu focal InRelease &#91;4736 B]&lt;/strong>
Hit:2 https://repos-droplet.digitalocean.com/apt/droplet-agent main InRelease                                                 
Hit:3 http://mirrors.digitalocean.com/ubuntu focal InRelease                                                                  
Get:4 http://security.ubuntu.com/ubuntu focal-security InRelease &#91;114 kB]
Hit:5 http://mirrors.digitalocean.com/ubuntu focal-updates InRelease
&lt;strong>Get:6 https://repos.influxdata.com/ubuntu focal/stable amd64 Packages &#91;1263 B]&lt;/strong>
Hit:7 http://mirrors.digitalocean.com/ubuntu focal-backports InRelease   </code></pre>

### 4. Install influxDB on Ubuntu 20.04 {#4-install-influxdb-on-ubuntu-20-04.wp-block-heading}

To install the latest stable version of InfluxDB we use the following command:

<pre class="wp-block-code"><code>$ sudo apt install influxdb</code></pre>

The following is the output you will get from the terminal.

<pre class="wp-block-code"><code>Output.
The following NEW packages will be installed:
  influxdb
0 upgraded, 1 newly installed, 0 to remove and 0 not upgraded.
Need to get 54.4 MB of archives.
After this operation, 153 MB of additional disk space will be used.
Get:1 https://repos.influxdata.com/ubuntu focal/stable amd64 influxdb amd64 1.8.10-1 &#91;54.4 MB]
Fetched 54.4 MB in 1s (41.5 MB/s)   
Selecting previously unselected package influxdb.
(Reading database ... 94672 files and directories currently installed.)
Preparing to unpack .../influxdb_1.8.10-1_amd64.deb ...
Unpacking influxdb (1.8.10-1) ...
Setting up influxdb (1.8.10-1) ...
Created symlink /etc/systemd/system/influxd.service → /lib/systemd/system/influxdb.service.
Created symlink /etc/systemd/system/multi-user.target.wants/influxdb.service → /lib/systemd/system/influxdb.service.
Processing triggers for man-db (2.9.1-1) ...</code></pre>

### 5. Configuring InfluxDB {#5-configuring-influxdb.wp-block-heading}

In order to start using InfluxDB, we need to start and enable it.

<pre class="wp-block-code"><code># start InfluxDB
$ sudo systemctl start influxdb
# enable
$ sudo systemctl enable --now influxdb
# status
$ sudo systemctl status influxdb</code></pre>

<pre class="wp-block-code"><code>Status 
● influxdb.service - InfluxDB is an open-source, distributed, time series database
     Loaded: loaded (/lib/systemd/system/influxdb.service; enabled; vendor preset: enabled)
     Active: active (running) since Tue 2022-02-08 13:04:23 UTC; 1min 21s ago
       Docs: https://docs.influxdata.com/influxdb/
   Main PID: 1616 (influxd)
      Tasks: 8 (limit: 1131)
     Memory: 14.7M
     CGroup: /system.slice/influxdb.service
             └─1616 /usr/bin/influxd -config /etc/influxdb/influxdb.conf

Feb 08 13:04:23 ubuntu influxd-systemd-start.sh&#91;1616]: ts=2022-02-08T13:04:23.805189Z lvl=info msg="Starting HTTP service" log>
Feb 08 13:04:23 ubuntu influxd-systemd-start.sh&#91;1616]: ts=2022-02-08T13:04:23.805868Z lvl=info msg="opened HTTP access log" lo>
Feb 08 13:04:23 ubuntu influxd-systemd-start.sh&#91;1616]: ts=2022-02-08T13:04:23.810007Z lvl=info msg="Storing statistics" log_id>
Feb 08 13:04:23 ubuntu influxd-systemd-start.sh&#91;1616]: ts=2022-02-08T13:04:23.812489Z lvl=info msg="Listening on HTTP" log_id=>
Feb 08 13:04:23 ubuntu influxd-systemd-start.sh&#91;1616]: ts=2022-02-08T13:04:23.812998Z lvl=info msg="Starting retention policy >
Feb 08 13:04:23 ubuntu influxd-systemd-start.sh&#91;1616]: ts=2022-02-08T13:04:23.813507Z lvl=info msg="Listening for signals" log>
Feb 08 13:04:23 ubuntu influxd-systemd-start.sh&#91;1616]: ts=2022-02-08T13:04:23.814335Z lvl=info msg="Sending usage statistics t>
Feb 08 13:04:23 ubuntu influxd-systemd-start.sh&#91;1616]: &#91;httpd] ::1 - - &#91;08/Feb/2022:13:04:23 +0000] "GET /health HTTP/1.1" 200>
Feb 08 13:04:23 ubuntu influxd-systemd-start.sh&#91;1612]: InfluxDB started
Feb 08 13:04:23 ubuntu systemd&#91;1]: Started InfluxDB is an open-source, distributed, time series database.</code></pre>

Enable &#8211;now InfluxDB will ensure that InfluxDB starts every time the server is reloaded. We don&#8217;t need to reenable every time.

Whenever you need to enable a certain configuration, head over to **/etc/influxdb/influxdb.conf**. Lets see an example.

<pre class="wp-block-code"><code>$ sudo vi /etc/influxdb/influxdb.conf</code></pre>

Let&#8217;s enabled HTTP on our server, just comment out **enabled = true** save changes and exit.

To apply the changes stop the server and restart back again.

<pre class="wp-block-code"><code># stop
$ sudo systemctl stop influxdb
# restart
$ sudo systemctl start influxdb</code></pre>

### 6. Enable Firewall settings  {#6-enable-firewall-settings.wp-block-heading}

To ensure that we can access InfluxDB from outside, we need to enable TCP 8086 on the firewall.

To know more about how to enable firewall read this article [How to setup Ubuntu 20.04 Server for the first time][1]

<pre class="wp-block-code"><code>$ sudo ufw allow 8086/tcp</code></pre>

To check the status we use **ufw enable and status** command

<pre class="wp-block-code"><code>$ sudo ufw enable
$ sudo ufw status
Status: active

To                         Action      From
--                         ------      ----
8086/tcp                   ALLOW       Anywhere                  
8086/tcp (v6)              ALLOW       Anywhere (v6)  </code></pre>

### 7. Launching InfluxDB  {#7-launching-influxdb.wp-block-heading}

To launch InfluxDB we need to point the process to the correct configuration file by using **-config** flag. Lets see how we can do this.

<pre class="wp-block-code"><code>$ influxd -config /etc/influxdb/influxdb.conf</code></pre>

This is what we get when we invoke the above command.

<pre class="wp-block-code"><code>8888888           .d888 888                   8888888b.  888888b.
   888            d88P"  888                   888  "Y88b 888  "88b
   888            888    888                   888    888 888  .88P
   888   88888b.  888888 888 888  888 888  888 888    888 8888888K.
   888   888 "88b 888    888 888  888  Y8bd8P' 888    888 888  "Y88b
   888   888  888 888    888 888  888   X88K   888    888 888    888
   888   888  888 888    888 Y88b 888 .d8""8b. 888  .d88P 888   d88P
 8888888 888  888 888    888  "Y88888 888  888 8888888P"  8888888P"

2022-02-08T13:34:41.401402Z     info    InfluxDB starting       {"log_id": "0ZYOY6yW000", "version": "1.8.10", "branch": "1.8", "commit": "688e697c51fd"}
2022-02-08T13:34:41.402018Z     info    Go runtime      {"log_id": "0ZYOY6yW000", "version": "go1.13.8", "maxprocs": 1}
run: open server: listen: listen tcp 127.0.0.1:8088: bind: address already in use</code></pre>

## Conclusion {#conclusion.wp-block-heading}

We have successfully installed InfluxDB on Ubuntu 20.04. In case of any issue head over to <a href="https://docs.influxdata.com/influxdb/v1.8/introduction/install/?t=curl" target="_blank" rel="noreferrer noopener" title="InfluxDB documentation">InfluxDB documentation</a> for clarification.

 [1]: https://nextgentips.com/2021/10/10/initial-server-setup-for-ubuntu-20-04/