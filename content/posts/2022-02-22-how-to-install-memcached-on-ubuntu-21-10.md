---
title: How to Install memcached on Ubuntu 21.10
author: Kipkoech Sang
type: post
date: 2022-02-22T11:18:39+00:00
url: /2022/02/22/how-to-install-memcached-on-ubuntu-21-10/
rank_math_primary_category:
  - 6
rank_math_description:
  - Memcached is a free and open-source, high-performance distributed memory object caching system, Learn how to install memcached on Ubuntu.
rank_math_focus_keyword:
  - install memcached
rank_math_news_sitemap_robots:
  - index
rank_math_robots:
  - 'a:1:{i:0;s:5:"index";}'
rank_math_analytic_object_id:
  - 4
rank_math_internal_links_processed:
  - 1
categories:
  - Linux

---
In this tutorial we are going to install memcached on Ubuntu 21.10. 

**Memcached** is a free and open-source, high-performance distributed memory object caching system. It is use in speeding up dynamic web applications but enabling faster database loading time. It enables you to make better use of your memory by doing load distribution.

**The advantage of using Memcached is that, Its powerful, it promote quick deployment of needed applications, makes easy for development because of its cache and its API can easily integrate with other languages.** 

From a easy point of view, Memcached allows you to take memory from parts of your system where you have more than you need and make it accessible, to areas where its needed. 

The best place to run Memcached is on **dedicated hosts** because you will not have to worry about other programs on the machine from interfering with the Memcached. 

Where can we run Memcached?

We run Memcached anywhere from the following:

  * Databases
  * Web servers 

## Related Articles {#related-articles.wp-block-heading}

  * [How to install and configure Memcached on Fedora 35][1]

## Installing memcached on Ubuntu 21.10 {#installing-memcached-on-ubuntu-21-10.wp-block-heading}

Installing Memcached on Ubuntu system is easy by using the following command **apt install memcached** but keep in mind that Ubuntu LTS may have old packages, so you may run into bugs easily. The best option to get and install memcached is to install from the source, here you will be assured you will get the latest version of memcached.

### Installing memcached from Ubuntu packages  {#installing-memcached-from-ubuntu-packages.wp-block-heading}

Lets see how we can install memcached from Ubuntu packages. First you will need to do system updates.

<pre class="wp-block-code"><code>$ sudo apt update && apt upgrade </code></pre>

When updates and upgrades are complete, move ahead and install memcached.

### Installing memcached on Ubuntu 21.10 {#installing-memcached-on-ubuntu-21-10-1.wp-block-heading}

To install memcached simply run the following command on your terminal.

<pre class="wp-block-code"><code>$ sudo apt install memcached -y</code></pre>

See the following output

<pre class="wp-block-code"><code># output
The following NEW packages will be installed:
  memcached
0 upgraded, 1 newly installed, 0 to remove and 0 not upgraded.
Need to get 158 kB of archives.
After this operation, 384 kB of additional disk space will be used.
Get:1 http://mirrors.digitalocean.com/ubuntu impish/main amd64 memcached amd64 1.6.9+dfsg-1build1 &#91;158 kB]
Fetched 158 kB in 6s (25.5 kB/s)
Selecting previously unselected package memcached.
(Reading database ... 91489 files and directories currently installed.)
Preparing to unpack .../memcached_1.6.9+dfsg-1build1_amd64.deb ...
Unpacking memcached (1.6.9+dfsg-1build1) ...
Setting up memcached (1.6.9+dfsg-1build1) ...
Created symlink /etc/systemd/system/multi-user.target.wants/memcached.service → /lib/systemd/system/memcached.service.
....</code></pre>

We can check the version of memcached installed.

<pre class="wp-block-code"><code>$ memcached --version
memcached 1.6.9 </code></pre>

### Installing memcached from the source  {#installing-memcached-from-the-source.wp-block-heading}

Installing memcached from the source help avoid running into bugs, it installed the latest version free of bugs.

It requires that we install some dependencies first.

<pre class="wp-block-code"><code>$ sudo apt install libevent-dev -y</code></pre>

You will get the following output.

<pre class="wp-block-code"><code>The following additional packages will be installed:
  libevent-core-2.1-7 libevent-extra-2.1-7 libevent-openssl-2.1-7 libevent-pthreads-2.1-7
The following NEW packages will be installed:
  libevent-core-2.1-7 libevent-dev libevent-extra-2.1-7 libevent-openssl-2.1-7 libevent-pthreads-2.1-7
0 upgraded, 5 newly installed, 0 to remove and 0 not upgraded.
Need to get 433 kB of archives.
After this operation, 2345 kB of additional disk space will be used.
Do you want to continue? &#91;Y/n] y</code></pre>

### Download memcached  {#download-memcached.wp-block-heading}

Now that we have installed its dependencies, we can now download memcached using wget command.

<pre class="wp-block-code"><code>$ sudo wget http://www.memcached.org/files/memcached-1.6.14.tar.gz</code></pre>

Look at the sample output below.

<pre class="wp-block-code"><code>Saving to: ‘memcached-1.6.14.tar.gz’

memcached-1.6.14.tar.gz     100%&#91;===================================>]   1010K  1.25MB/s    in 0.8s    

2022-02-22 10:32:57 (1.25 MB/s) - ‘memcached-1.6.14.tar.gz’ saved &#91;1034514/1034514]</code></pre>

So we are going to extract latest to our system directory.

When download is complete we need to extract the contents from the archives. 

<pre class="wp-block-code"><code>$ sudo tar -zxf memcached-1.6.14.tar.gz</code></pre>

If you ls to see the contents you will see that memcached have been extracted.

<pre class="wp-block-code"><code>$ ls 
memcached-1.6.14  memcached-1.6.14.tar.gz  snap</code></pre>

The next thing is to **cd into memcached directory and make install** to install memcached.

<pre class="wp-block-code"><code>$ cd memcached-1.6.14</code></pre>

Lastly we need to configure, test and make install. We will use the following command.

<pre class="wp-block-code"><code>$ ./configure && make && make test && sudo make install</code></pre>

I am assuming you too are getting the following error when you run the **./configure** command

<pre class="wp-block-code"><code>checking build system type... x86_64-pc-linux-gnu
checking host system type... x86_64-pc-linux-gnu
checking for a BSD-compatible install... /usr/bin/install -c
checking whether build environment is sane... yes
checking for a thread-safe mkdir -p... /usr/bin/mkdir -p
checking for gawk... gawk
checking whether make sets $(MAKE)... no
checking whether make supports nested variables... no
checking for gcc... no
checking for cc... no
checking for cl.exe... no
configure: error: in `/root/memcached-1.6.14':
&lt;strong>configure: error: no acceptable C compiler found in $PATH&lt;/strong>
See `config.log' for more details</code></pre>

To get rid of the error install **build essential**

<pre class="wp-block-code"><code>$ sudo apt install build-essential</code></pre>

Run ./configure again, this time round no error will be encountered.

When the long process is over, you can check the version of the installed memcached.

<pre class="wp-block-code"><code>$ memcached --version
memcached 1.6.14</code></pre>

### Start memcached service  {#start-memcached-service.wp-block-heading}

In order to start using memcached, lets start the memcached service

<pre class="wp-block-code"><code>$ sudo systemctl start memcached</code></pre>

Check the service if its running on your system.

<pre class="wp-block-code"><code>sudo systemctl status memcached</code></pre>

When you see the below as the output then memcached is running effectively.

<pre class="wp-block-code"><code>$ ● memcached.service - memcached daemon
     Loaded: loaded (/lib/systemd/system/memcached.service; enabled; vendor preset: enabled)
     Active: active (running) since Tue 2022-02-22 10:58:51 UTC; 26s ago
       Docs: man:memcached(1)
   Main PID: 24977 (memcached)
      Tasks: 10 (limit: 1131)
     Memory: 1.3M
        CPU: 25ms
     CGroup: /system.slice/memcached.service
             └─24977 /usr/bin/memcached -m 64 -p 11211 -u memcache -l 127.0.0.1 -P /var/run/memcached/memcach>

Feb 22 10:58:51 ubuntu systemd&#91;1]: Started memcached daemon.</code></pre>

## Conclusion {#conclusion.wp-block-heading}

You have learned how to install memcached on Ubuntu 21.10. Check the <a href="https://github.com/memcached/memcached/wiki/Install" target="_blank" rel="noreferrer noopener">memcached documentation</a> for more insight.

 [1]: https://nextgentips.com/2021/11/16/how-to-install-and-configure-memcached-on-fedora-35/