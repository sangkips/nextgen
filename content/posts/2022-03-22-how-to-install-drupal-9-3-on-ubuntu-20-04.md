---
title: How to install Drupal 9.3 on Ubuntu 20.04
author: Kipkoech Sang
type: post
date: 2022-03-22T10:58:23+00:00
url: /2022/03/22/how-to-install-drupal-9-3-on-ubuntu-20-04/
rank_math_ca_keyword:
  - 'a:2:{s:7:"keyword";s:16:"install drupal 9";s:7:"country";s:3:"all";}'
rank_math_seo_score:
  - 81
rank_math_focus_keyword:
  - install drupal 9
rank_math_contentai_score:
  - 'a:5:{s:9:"wordCount";s:5:"33.72";s:9:"linkCount";s:1:"0";s:12:"headingCount";s:5:"22.86";s:10:"mediaCount";s:3:"100";s:8:"keywords";s:4:"7.14";}'
rank_math_internal_links_processed:
  - 1
rank_math_primary_category:
  - 6
rank_math_analytic_object_id:
  - 157
categories:
  - Linux

---
Drupal 9.3 is one of the best CMS out there. In this tutorial, I am going to show you how to install Drupal 9 on your Ubuntu 20.04|22.04. Let&#8217;s get started.

**Drupal** is a free and open-source web content management system powered by PHP. It is a highly flexible platform for digital innovation. The reason why Drupal is so important is because of its availability and is free and open-source. No license is needed to operate it. It has a larger community meaning any time you experience an issue with the platform you can always turn to a ready-to-help community.

## Features of Drupal 9.3 {.wp-block-heading}

  * It provides easy content authoring for all the content creators.
  * It provides easy and reliable performance, you will hardly get issues whwnever you are using Drupal CMS.
  * Content Management system needs to be impenetrable therefore security is key thats why drupal 9 is the epitome of sucurity success.
  * It provides flexibility. Its tools helps you build the versatile, structured content needed for dynamic websites.
  * Extending modules in drupal is easy thanks to digital integrated frameworks. 
  * Its powerful and can scale much faster.

## What you need to run Drupal 9.3 {.wp-block-heading}

These are system requirements needed to operate Drupal CMS.

  * Have a database such as MySQL 5.7 or higher or Postresql 10 or higher
  * Drupal works well on Apache, Nginx
  * It needs PHP 7.3 and higher

## Installling Drupal 9.3 on Ubuntu 20.04|22.04 {.wp-block-heading}

The first thing before we can install Drupal on our system is to run system updates so that we can avoid running into errors at a later stage in our installation.

<pre class="wp-block-code"><code>sudo apt update && apt upgrade -y</code></pre>

### 1. Install MariaDB database {.wp-block-heading}

The second thing to do after updating our system is to install MariaDB or PostgreSQL. I will be using MariaDB for this process. So let&#8217;s install MariaDB with the following command.

<pre class="wp-block-code"><code>sudo apt install mariadb-server mariadb-client -y</code></pre>

For any database, we are required to secure it before we can use it. So to secure our installed MariaDB database run the following command.

<pre class="wp-block-code"><code>sudo mysql_secure_installation

# output
NOTE: RUNNING ALL PARTS OF THIS SCRIPT IS RECOMMENDED FOR ALL MariaDB
      SERVERS IN PRODUCTION USE!  PLEASE READ EACH STEP CAREFULLY!

In order to log into MariaDB to secure it, we'll need the current
password for the root user.  If you've just installed MariaDB, and
you haven't set the root password yet, the password will be blank,
so you should just press enter here.

Enter current password for root (enter for none): 
OK, successfully used password, moving on...

Setting the root password ensures that nobody can log into the MariaDB
root user without the proper authorisation.

Set root password? &#91;Y/n] n 
 ... skipping.

By default, a MariaDB installation has an anonymous user, allowing anyone
to log into MariaDB without having to have a user account created for
them.  This is intended only for testing, and to make the installation
go a bit smoother.  You should remove them before moving into a
production environment.

Remove anonymous users? &#91;Y/n] y
 ... Success!

Normally, root should only be allowed to connect from 'localhost'.  This
ensures that someone cannot guess at the root password from the network.

Disallow root login remotely? &#91;Y/n] y
 ... Success!

By default, MariaDB comes with a database named 'test' that anyone can
access.  This is also intended only for testing, and should be removed
before moving into a production environment.

Remove test database and access to it? &#91;Y/n] y
 - Dropping test database...
 ... Success!
 - Removing privileges on test database...
 ... Success!

Reloading the privilege tables will ensure that all changes made so far
will take effect immediately.

Reload privilege tables now? &#91;Y/n] y
 ... Success!

Cleaning up...

All done!  If you've completed all of the above steps, your MariaDB
installation should now be secure.

Thanks for using MariaDB!</code></pre>

When this is complete, then we need to allow regular users to log in as root with the following command.

<pre class="wp-block-code"><code>sudo mysql -u root
# UPDATE mysql.user SET plugin = 'mysql_native_password' WHERE User = 'root';
# FLUSH PRIVILEGES;
# QUIT</code></pre>

Now that we are able to log in as regular users, we can now create a Drupal database that Drupal can use once we installed it into our system. To create one use the following command:

<pre class="wp-block-code"><code># login command
mysql -u root -p</code></pre>

Once logged in create the database with the following command:

<pre class="wp-block-code"><code>MariaDB &#91;(none)]&gt; &lt;strong>Create database nextdrupal;&lt;/strong>
Query OK, 1 row affected (0.000 sec)

MariaDB &#91;(none)]&gt; &lt;strong>Grant all privileges on nextdrupal.* to 'nextdrupal'@'localhost' identified by 'passwd';&lt;/strong>
Query OK, 0 rows affected (0.000 sec)

MariaDB &#91;(none)]&gt; &lt;strong>Flush privileges;&lt;/strong>
Query OK, 0 rows affected (0.000 sec)

MariaDB &#91;(none)]&gt; &lt;strong>quit&lt;/strong>
Bye</code></pre>

### 2. Install PHP {.wp-block-heading}

Now we can install PHP 7.3 or higher. If you love installing PHP using PPA repositories follow this link to read more about How to install PHP 8.1 on Ubuntu 20.04. Otherwise use the below command to continue the installation.

<pre class="wp-block-code"><code>sudo apt install php php-{cli,fpm,json,common,mysql,zip,gd,intl,mbstring,curl,xml,pear,tidy,soap,bcmath,xmlrpc}</code></pre>

The following command will install all PHP and its dependencies at a go.

### 3. Install Apache web server {.wp-block-heading}

Install apache web server with the following command. Note that apache is recommended over Nginx. Read more here on <a href="https://nextgentips.com/2021/10/11/how-to-install-apache-mysql-php-on-ubuntu-20-04/" target="_blank" rel="noreferrer noopener">how to install Apache 2 on Ubuntu 20.04</a>.

<pre class="wp-block-code"><code>sudo apt install apache2</code></pre>

Make sure the time zone is reading the place you are. For my case, I am setting the time zone to be Africa/Nairobi. You can do so like this **sudo nano /etc/php/*/apache2/php.ini**. I will be using nano as my preferred text editor

<pre class="wp-block-code"><code>sudo nano /etc/php/*/apache2/php.ini</code></pre>

Set out memory limit and time zone.

### 4. Install Drupal 9.3 on Ubuntu 20.04|22.04 {.wp-block-heading}

Now that all the prerequisites have been met, it&#8217;s now time we download and install Drupal. We are going to use the wget command to do so.

<pre class="wp-block-code"><code>wget https://ftp.drupal.org/files/projects/drupal-9.3.7.tar.gz</code></pre>

When the download is complete, we need to extract the archive.

<pre class="wp-block-code"><code>sudo tar -xvf drupal.tar.gz</code></pre>

Then we need to move it to var/www/html directory using the following command.

<pre class="wp-block-code"><code>rm -f drupal*.tar.gz
sudo mv drupal drupal-9.3.7/ drupal</code></pre>

After this, we need to set Apache2 ownership by giving permission to the drupal files.

<pre class="wp-block-code"><code>sudo chown -R www-data:www-data /var/www/html/drupal
sudo chmod -R 755 /var/www/html/drupal</code></pre>

### 4. Create Apache 2 virtual host file. {.wp-block-heading}

We can create Apache virtual host file for Drupal with the following command.

<pre class="wp-block-code"><code>sudo nano /etc/apache2/sites-available/drupal.conf</code></pre>

This creates **drupal.conf** file. Add the following:

<pre class="wp-block-code"><code>&lt;VirtualHost *:80&gt;
  Server Admin admin@example.com
 DocumentRoot /var/www/html/drupal/ 
Server Name example.com ServerAlias www.example.com 
ErrorLog ${APACHE_LOG_DIR}/error.log 
CustomLog ${APACHE_LOG_DIR}/access. log combined 
Options FollowSymlinks 
AllowOverride All 
Require all granted 
RewriteEngine on 
RewriteBase / 
RewriteCond %{REQUEST_FILENAME} !-f 
RewriteCond %{REQUEST_FILENAME} !-d 
RewriteRule ^(.*)$ index.php?q=$1 &#91;L,QSA)
&lt;/VirtualHost&gt;</code></pre>

Save and restart the apache 2

<pre class="wp-block-code"><code>sudo systemctl restart apache2</code></pre>

Enable Drupal site with the following:

<pre class="wp-block-code"><code>sudo a2dismod mpm_event
sudo a2enmod mpm_prefork
sudo sudo a2enmod php7.4
sudo a2enmod rewrite
sudo a2ensite drupal.conf</code></pre>

Lastly restart apache again.

### 5. Configure Drupal 9.3 via Wizzard. {.wp-block-heading}

Now we can start our Drupal by going into our favorite browser and opening **http://<your-ip-address/drupal>**

Installing wizard will appear like this:<figure class="wp-block-image size-full">

<img decoding="async" loading="lazy" width="755" height="546" src="https://i0.wp.com/nextgentips.com/wp-content/uploads/2022/03/Screenshot-from-2022-03-22-13-16-54.png?resize=755%2C546&#038;ssl=1" alt="install Drupal 9.3" class="wp-image-1211" srcset="https://i0.wp.com/nextgentips.com/wp-content/uploads/2022/03/Screenshot-from-2022-03-22-13-16-54.png?w=755&ssl=1 755w, https://i0.wp.com/nextgentips.com/wp-content/uploads/2022/03/Screenshot-from-2022-03-22-13-16-54.png?resize=300%2C217&ssl=1 300w" sizes="(max-width: 755px) 100vw, 755px" data-recalc-dims="1" /> <figcaption>install Drupal 9.3</figcaption></figure> 

## Conclusion {.wp-block-heading}

We have successfully installed drupal 9.3 on our Ubuntu 20.04. Play along with the CMS to learn more. For more info check out <a href="https://www.drupal.org/docs/installing-drupal" target="_blank" rel="noreferrer noopener">Drupal documentation</a>.