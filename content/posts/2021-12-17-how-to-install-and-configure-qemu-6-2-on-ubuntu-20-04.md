---
title: How to install and configure QEMU 5 on Ubuntu 20.04
author: Kipkoech Sang
type: post
date: 2021-12-17T06:51:33+00:00
url: /2021/12/17/how-to-install-and-configure-qemu-6-2-on-ubuntu-20-04/
rank_math_news_sitemap_robots:
  - index
rank_math_robots:
  - 'a:1:{i:0;s:5:"index";}'
rank_math_analytic_object_id:
  - 60
rank_math_internal_links_processed:
  - 1
categories:
  - Linux

---
In this tutorial, we are going to learn how to install and configure QEMU on Ubuntu 20.04.

QEMU is a free and open-source hypervisor, it emulates the machine&#8217;s processor through dynamic binary translation and provides a set of different hardware and device models for the machine, enabling it to run a variety of guest operating systems.

QEMU is capable of emulating a complete machine in software without the need for hardware virtualization support. It is also capable of providing userspace API virtualization for Linux and BSD kernel services. It is commonly invoked indirectly via libvirt library when using open source applications such as oVirt, OpenStack, and virt-manager.

## Install QEMU on Ubuntu 20.04 {.wp-block-heading}

### 1. Run system updates {.wp-block-heading}

To begin with, we need to update our repositories in order to make them up to date, Use the following command on your terminal.

<pre class="wp-block-code"><code>$ sudo apt update && apt upgrade -y</code></pre>

When Updates are complete, now we can install QEMU.

### 2. Install QEMU 5.0 on Ubuntu 20.04 {.wp-block-heading}

QEMU is available from Ubuntu repositories so we only need to install using virt-manager with the following command.

<pre class="wp-block-code"><code>$ sudo apt-get install qemu-kvm \
	qemu virt-manager \
	virt-viewer libvirt-clients \
	libvirt-daemon-system \
	bridge-utils \
	virtinst \
	libvirt-daemon</code></pre>

Sample output you will get

<pre class="wp-block-code"><code>Output
Building dependency tree       
Reading state information... Done
bridge-utils is already the newest version (1.6-2ubuntu1).
bridge-utils set to manually installed.
qemu-kvm is already the newest version (1:4.2-3ubuntu6.18).
qemu-kvm set to manually installed.
The following packages were automatically installed and are no longer required:
  libfprint-2-tod1 libgtkglext1 libllvm9 libminizip1 libpangox-1.0-0 shim
Use 'sudo apt autoremove' to remove them.
The following additional packages will be installed:
  gir1.2-appindicator3-0.1 gir1.2-gtk-vnc-2.0 gir1.2-libosinfo-1.0
  gir1.2-libvirt-glib-1.0 gir1.2-spiceclientglib-2.0 gir1.2-spiceclientgtk-3.0
  libgovirt-common libgovirt2 libosinfo-1.0-0 libvirt-daemon-driver-qemu
  libvirt-daemon-driver-storage-rbd libvirt-glib-1.0-0 libvirt0 osinfo-db
  python3-libvirt python3-libxml2
Suggested packages:
  libosinfo-l10n libvirt-daemon-driver-lxc libvirt-daemon-driver-vbox
  libvirt-daemon-driver-xen libvirt-daemon-driver-storage-gluster
  libvirt-daemon-driver-storage-zfs numad auditd nfs-common open-iscsi
  pm-utils radvd systemtap zfsutils python3-guestfs ssh-askpass
The following NEW packages will be installed:
  gir1.2-appindicator3-0.1 gir1.2-gtk-vnc-2.0 gir1.2-libosinfo-1.0
  gir1.2-libvirt-glib-1.0 gir1.2-spiceclientglib-2.0 gir1.2-spiceclientgtk-3.0
  libgovirt-common libgovirt2 libosinfo-1.0-0 libvirt-glib-1.0-0 osinfo-db
  python3-libvirt python3-libxml2 qemu virt-manager virt-viewer virtinst
The following packages will be upgraded:
  libvirt-clients libvirt-daemon libvirt-daemon-driver-qemu
  libvirt-daemon-driver-storage-rbd libvirt-daemon-system libvirt0
6 upgraded, 17 newly installed, 0 to remove and 91 not upgraded.
Need to get 4,979 kB of archives.
After this operation, 14.6 MB of additional disk space will be used.
Do you want to continue? &#91;Y/n] </code></pre>

Press Y to allow the installation to continue.

When the installation is complete, we can now check if the virtualization daemon is running. That virtualization daemon is libvirtd. check using the following command;

<pre class="wp-block-code"><code>$ sudo systemctl status libvirtd</code></pre>

<pre class="wp-block-code"><code>Output
● libvirtd.service - Virtualization daemon
     Loaded: loaded (/lib/systemd/system/libvirtd.service; enabled; vendor pres&gt;
     Active: active (running) since Fri 2021-12-17 09:06:54 EAT; 7min ago
TriggeredBy: ● libvirtd-admin.socket
             ● libvirtd.socket
             ● libvirtd-ro.socket
       Docs: man:libvirtd(8)
             https:&#47;&#47;libvirt.org
   Main PID: 10022 (libvirtd)
      Tasks: 19 (limit: 32768)
     Memory: 24.4M
     CGroup: /system.slice/libvirtd.service
             ├─ 1337 /usr/sbin/dnsmasq --conf-file=/var/lib/libvirt/dnsmasq/def&gt;
             ├─ 1338 /usr/sbin/dnsmasq --conf-file=/var/lib/libvirt/dnsmasq/def&gt;
             └─10022 /usr/sbin/libvirtd

Dec 17 09:06:54 nextgentips-pc systemd&#91;1]: Starting Virtualization daemon...
Dec 17 09:06:54 nextgentips-pc systemd&#91;1]: Started Virtualization daemon.
Dec 17 09:06:54 nextgentips-pc dnsmasq&#91;1337]: read /etc/hosts - 7 addresses
Dec 17 09:06:54 nextgentips-pc dnsmasq&#91;1337]: read /var/lib/libvirt/dnsmasq/def&gt;
Dec 17 09:06:54 nextgentips-pc dnsmasq-dhcp&#91;1337]: read /var/lib/libvirt/dnsmas&gt;
lines 1-21/21 (END)</code></pre>

To open QEMU using GUI, just type **_virt-manager_** on terminal

<pre class="wp-block-code"><code>$ virt-manager</code></pre>

It will show the following <figure class="wp-block-image size-full">

<img decoding="async" loading="lazy" width="570" height="605" src="https://i0.wp.com/nextgentips.com/wp-content/uploads/2021/12/Screenshot-from-2021-12-17-09-40-24.png?resize=570%2C605&#038;ssl=1" alt="QEMU interface" class="wp-image-863" srcset="https://i0.wp.com/nextgentips.com/wp-content/uploads/2021/12/Screenshot-from-2021-12-17-09-40-24.png?w=570&ssl=1 570w, https://i0.wp.com/nextgentips.com/wp-content/uploads/2021/12/Screenshot-from-2021-12-17-09-40-24.png?resize=283%2C300&ssl=1 283w" sizes="(max-width: 570px) 100vw, 570px" data-recalc-dims="1" /> <figcaption>QEMU interface</figcaption></figure> 

Follow the prompts to add iso images you need.

### 3. Upgrade Qemu to version 5.0  {.wp-block-heading}

You will realize that Qemu version 4.2 is the one found on Ubuntu 20.04 repositories, So to upgrade to 5.0 we need to use PPA to achieve this. I will be using Jacob Zimmermann PPA.

Follow these steps:

**Add Jacob Zimmermann to your Ubuntu repositories** 

<pre class="wp-block-code"><code>$ sudo add-apt-repository ppa:jacob/virtualisation</code></pre>

Update repositories again for the changes to take effect and run upgrade command;

<pre class="wp-block-code"><code>$ sudo apt update
$ sudo apt upgrade -y</code></pre>

When the upgrade is complete, you can check the version of Qemu installed.

Check version using the following command;

<pre class="wp-block-code"><code>$ sudo apt show qemu-system-x86</code></pre>

The following output will be displayed.

<pre class="wp-block-code"><code># output
Package: qemu-system-x86
Version: 1:5.0-5ubuntu6~ppa0
Priority: optional
Section: otherosfs
Source: qemu
Maintainer: Ubuntu Developers &lt;ubuntu-devel-discuss@lists.ubuntu.com>
Original-Maintainer: Debian QEMU Team &lt;pkg-qemu-devel@lists.alioth.debian.org>
Installed-Size: 37.8 MB
Provides: qemu-system-i386, qemu-system-x86-64, qemu-system-x86-microvm
Depends: libaio1 (>= 0.3.93), libasound2 (>= 1.0.16), libbrlapi0.7, libc6 (>= 2.29), libcacard0 (>= 2.2), libepoxy0 (>= 1.3), libfdt1 (>= 1.5.1), libgbm1 (>= 7.11~1), libgcc-s1 (>= 4.7), libglib2.0-0 (>= 2.39.4), libgnutls30 (>= 3.6.12), libibverbs1 (>= 28), libjpeg8 (>= 8c), libncursesw6 (>= 6), libnettle7, libnuma1 (>= 2.0.11), libpixman-1-0 (>= 0.19.6), libpmem1 (>= 1.4), libpng16-16 (>= 1.6.2-1), librdmacm1 (>= 1.0.15), libsasl2-2 (>= 2.1.27+dfsg), libseccomp2 (>= 2.1.0), libslirp0 (>= 4.0.0), libspice-server1 (>= 0.14.2), libtinfo6 (>= 6), liburing1 (>= 0.2), libusb-1.0-0 (>= 2:1.0.22), libusbredirparser1 (>= 0.6), libvirglrenderer1 (>= 0.8.2), zlib1g (>= 1:1.2.0), qemu-system-common (= 1:5.0-5ubuntu6~ppa0), qemu-system-data (>> 1:5.0-5ubuntu6~ppa0~), ipxe-qemu-256k-compat-efi-roms, seabios (>= 1.10.2-1~), ipxe-qemu (>= 1.0.0+git-20131111.c3d1e78-1~)
Recommends: qemu-system-gui (= 1:5.0-5ubuntu6~ppa0), qemu-utils, ovmf, cpu-checker
Suggests: samba, vde2, qemu-block-extra (= 1:5.0-5ubuntu6~ppa0), sgabios
Breaks: qemu-system-x86-microvm (&lt;&lt; 1:5.0-5ubuntu1~)
Replaces: qemu-system-x86-microvm (&lt;&lt; 1:5.0-5ubuntu1~)
Download-Size: 8,289 kB
APT-Sources: http://ppa.launchpad.net/jacob/virtualisation/ubuntu focal/main amd64 Packages
Description: QEMU full system emulation binaries (x86)
 QEMU is a fast processor emulator: currently the package supports
 i386 and x86-64 emulation. By using dynamic translation it achieves
 reasonable speed while being easy to port on new host CPUs.
 .
 This package provides the full system emulation binaries to emulate
 the following x86 hardware:  i386 x86_64.
 .
 In system emulation mode QEMU emulates a full system, including a processor
 and various peripherals.  It enables easier testing and debugging of system
 code.  It can also be used to provide virtual hosting of several virtual
 machines on a single server.
 .
 On x86 host hardware this package also enables KVM kernel virtual machine
 usage on systems which supports it.

N: There are 3 additional records. Please use the '-a' switch to see them.</code></pre>

To uninstall Qemu do the following 

<pre class="wp-block-code"><code>$ sudo apt purge "qemu*"
$ sudo apt autoremove</code></pre>

## Conclusion {.wp-block-heading}

We have successfully installed QEMU on our Ubuntu 20.04. To learn more you can consult [QEMU documentation][1].

 [1]: https://planet.virt-tools.org/