---
title: How to install and use Docker-ce on Fedora 35
author: Kipkoech Sang
type: post
date: 2021-11-22T19:27:37+00:00
url: /2021/11/22/how-to-install-and-use-docker-ce-on-fedora-35/
mtm_data:
  - 'a:1:{i:0;a:3:{s:9:"reference";s:0:"";s:4:"type";s:0:"";s:5:"value";s:0:"";}}'
rank_math_news_sitemap_robots:
  - index
rank_math_robots:
  - 'a:1:{i:0;s:5:"index";}'
rank_math_analytic_object_id:
  - 90
rank_math_internal_links_processed:
  - 1
categories:
  - Linux

---
In this tutorial guide, we are going to learn how to install and use Docker-ce on Fedora 35.

Docker is a set of platforms as a service product that uses OS-level virtualization to deliver software in packages called containers. Containers are usually isolated from one another and bundled their own software libraries and configuration files, they can communicate with each other through well-defined channels. 

Docker makes it possible to get more apps running on the same old servers and also makes it easy to package and ship programs.

## Related Articles {.wp-block-heading}

[How to install Docker on Arch Linux][1]

## Prerequisites  {.wp-block-heading}

  * Make sure you have user with sudo privileges 
  * Have Fedora 35 server up and running 
  * Have basic kwoledge about running commands on a terminal

## Table of Contents  {.wp-block-heading}

  1. Run system updates 
  2. Uninstall old docker versions 
  3. Set up Docker repositories 
  4. Install Docker Engine 
  5. Pulling Images 

## Run system updates {.wp-block-heading}

The first thing we need to do is to install recent updates into our system. This makes our packages up to date. We can do so with the following command;

<pre class="wp-block-code"><code>$ sudo dnf update -y</code></pre>

After the updates are complete then we need to remove old docker versions from the system. 

## 2. Uninstall old Docker versions  {.wp-block-heading}

We need to uninstall old Docker versions in our system. The new Docker is called Docker-ce. You can do that with the following command;

<pre class="wp-block-code"><code>$ sudo dnf remove docker \
                  docker-client \
                  docker-client-latest \
                  docker-common \
                  docker-latest \
                  docker-latest-logrotate \
                  docker-logrotate \
                  docker-selinux \
                  docker-engine-selinux \
                  docker-engine</code></pre>

If you get a message like this on your terminal, don&#8217;t worry, it means you do not have any docker on your system. Move ahead and install a newer version of Docker into your system

<pre class="wp-block-code"><code>No match for argument: docker
No match for argument: docker-client
No match for argument: docker-client-latest
No match for argument: docker-common
No match for argument: docker-latest
No match for argument: docker-latest-logrotate
No match for argument: docker-logrotate
No match for argument: docker-selinux
No match for argument: docker-engine-selinux
No match for argument: docker-engine
No packages marked for removal.
Dependencies resolved.
Nothing to do.
Complete!</code></pre>

## 3. Set up Docker-ce repository {.wp-block-heading}

For newer systems, you need to add the Docker repository into your system. For this learning, I am running a fresh system install of Fedora. So we need to add a Docker repository with the following command;

First, add the following plugins, it provides the commands to manage dnf repositories.

<pre class="wp-block-code"><code>$ sudo dnf install dnf-plugins-core -y</code></pre>

It seems nothing to do because dnf-plugin-core comes as a default with the Fedora 35.

<pre class="wp-block-code"><code>Sample output
DigitalOcean Droplet Agent                                                        32 kB/s | 3.3 kB     00:00    
Package dnf-plugins-core-4.0.24-1.fc35.noarch is already installed.
Dependencies resolved.
Nothing to do.
Complete!</code></pre>

Then proceed to add the repository with the following command;

<pre class="wp-block-code"><code>$ sudo dnf config-manager --add-repo https://download.docker.com/linux/fedora/docker-ce.repo</code></pre>

If your addition is successful then you can proceed to install Docker Engine.

## Install Docker-ce  {.wp-block-heading}

Use the following command to install the latest Docker Engine and [containerd][2].

<pre class="wp-block-code"><code>$ sudo dnf install docker-ce docker-ce-cli containerd.io</code></pre>

<pre class="wp-block-code"><code>Sample output
Docker CE Stable - x86_64                                                        106 kB/s | 5.7 kB     00:00    
DigitalOcean Droplet Agent                                                        47 kB/s | 3.3 kB     00:00    
Dependencies resolved.
=================================================================================================================
 Package                            Architecture    Version                      Repository                 Size
=================================================================================================================
Installing:
 containerd.io                      x86_64          1.4.12-3.1.fc35              docker-ce-stable           27 M
 docker-ce                          x86_64          3:20.10.11-3.fc35            docker-ce-stable           22 M
 docker-ce-cli                      x86_64          1:20.10.11-3.fc35            docker-ce-stable           29 M
Installing dependencies:
 container-selinux                  noarch          2:2.170.0-2.fc35             updates                    50 k
 docker-ce-rootless-extras          x86_64          20.10.11-3.fc35              docker-ce-stable          3.9 M
 docker-scan-plugin                 x86_64          0.9.0-3.fc35                 docker-ce-stable          3.7 M
 fuse-common                        x86_64          3.10.5-1.fc35                fedora                    8.3 k
 fuse-overlayfs                     x86_64          1.7.1-2.fc35                 fedora                     72 k
 fuse3                              x86_64          3.10.5-1.fc35                fedora                     54 k
 fuse3-libs                         x86_64          3.10.5-1.fc35                fedora                     92 k
 iptables-legacy                    x86_64          1.8.7-13.fc35                fedora                     53 k
 libcgroup                          x86_64          2.0-3.fc35                   fedora                     73 k
 libslirp                           x86_64          4.6.1-2.fc35                 fedora                     72 k
 slirp4netns                        x86_64          1.1.12-2.fc35                fedora                     55 k

Transaction Summary
=================================================================================================================
Install  14 Packages

Total download size: 86 M
Installed size: 373 M
Is this ok &#91;y/N]: y</code></pre>

From here we have installed Docker-CE, lets check the version of the installed docker with the following command.

<pre class="wp-block-code"><code>$ docker version
Client: Docker Engine - Community
 Version:           20.10.11
 API version:       1.41
 Go version:        go1.16.9
 Git commit:        dea9396
 Built:             Thu Nov 18 00:38:10 2021
 OS/Arch:           linux/amd64
 Context:           default
 Experimental:      true
Cannot connect to the Docker daemon at unix:///var/run/docker.sock. Is the docker daemon running?</code></pre>

## Start Docker-ce  {.wp-block-heading}

For you to start using Docker, you need to start docker engine with the following,

<pre class="wp-block-code"><code>$ systemctl start docker </code></pre>

## Enble Docker-ce {.wp-block-heading}

Enable the Docker engine to start on boot with the following command; you don&#8217;t have to be starting docker every time you boot your system.

<pre class="wp-block-code"><code>$ systemctl enable docker </code></pre>

## Check the Docker-ce status {.wp-block-heading}

To check if our Docker engine daemon is running, invoke **systemctl status docker** on your command line.

<pre class="wp-block-code"><code>$ systemctl status docker</code></pre>

If you get the status active, then your docker daemon is running.

<pre class="wp-block-code"><code>Sample output
● docker.service - Docker Application Container Engine
     Loaded: loaded (/usr/lib/systemd/system/docker.service; enabled; vendor preset: disabled)
     Active: active (running) since Mon 2021-11-22 19:01:53 UTC; 4min 43s ago
TriggeredBy: ● docker.socket
       Docs: https://docs.docker.com
   Main PID: 19171 (dockerd)
      Tasks: 7
     Memory: 27.2M
        CPU: 271ms
     CGroup: /system.slice/docker.service
             └─19171 /usr/bin/dockerd -H fd:// --containerd=/run/containerd/containerd.sock

Nov 22 19:01:53 fedora-35 dockerd&#91;19171]: time="2021-11-22T19:01:53.220298055Z" level=info msg="scheme \"unix\" &gt;
Nov 22 19:01:53 fedora-35 dockerd&#91;19171]: time="2021-11-22T19:01:53.220408376Z" level=info msg="ccResolverWrappe&gt;
Nov 22 19:01:53 fedora-35 dockerd&#91;19171]: time="2021-11-22T19:01:53.220456897Z" level=info msg="ClientConn switc&gt;
Nov 22 19:01:53 fedora-35 dockerd&#91;19171]: time="2021-11-22T19:01:53.251216816Z" level=info msg="Loading containe&gt;
Nov 22 19:01:53 fedora-35 dockerd&#91;19171]: time="2021-11-22T19:01:53.405807849Z" level=info msg="Default bridge (&gt;
Nov 22 19:01:53 fedora-35 dockerd&#91;19171]: time="2021-11-22T19:01:53.517435166Z" level=info msg="Loading containe&gt;
Nov 22 19:01:53 fedora-35 dockerd&#91;19171]: time="2021-11-22T19:01:53.539112957Z" level=info msg="Docker daemon" c&gt;
Nov 22 19:01:53 fedora-35 dockerd&#91;19171]: time="2021-11-22T19:01:53.540644313Z" level=info msg="Daemon has compl&gt;
Nov 22 19:01:53 fedora-35 systemd&#91;1]: Started Docker Application Container Engine.
Nov 22 19:01:53 fedora-35 dockerd&#91;19171]: time="2021-11-22T19:01:53.578310053Z" level=info msg="API listen on</code></pre>

## Pull Docker-ce image {.wp-block-heading}

We can pull the Docker image with the following command. As an example let me pull Redis image from the Docker hub.

<pre class="wp-block-code"><code>$ docker pull redis</code></pre>

<pre class="wp-block-code"><code>Sample output
Using default tag: latest
latest: Pulling from library/redis
eff15d958d66: Pull complete 
1aca8391092b: Pull complete 
06e460b3ba1b: Pull complete 
def49df025c0: Pull complete 
646c72a19e83: Pull complete 
db2c789841df: Pull complete 
Digest: sha256:619af14d3a95c30759a1978da1b2ce375504f1af70ff9eea2a8e35febc45d747
Status: Downloaded newer image for redis:latest
docker.io/library/redis:latest</code></pre>

## Conclusion {.wp-block-heading}

From here you can see our Docker-CE is running as expected, Learn other docker commands to be well conversant with docker. [Docker Documentation][3] has a tone of information

 [1]: https://nextgentips.com/2021/09/30/how-to-install-docker-on-arch-linux/
 [2]: https://containerd.io/
 [3]: https://docs.docker.com/engine/install/