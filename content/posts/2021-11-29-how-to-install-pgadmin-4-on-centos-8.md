---
title: How to Install pgAdmin 4 on CentOS 8
author: Kipkoech Sang
type: post
date: 2021-11-29T16:36:35+00:00
url: /2021/11/29/how-to-install-pgadmin-4-on-centos-8/
rank_math_news_sitemap_robots:
  - index
rank_math_robots:
  - 'a:1:{i:0;s:5:"index";}'
rank_math_analytic_object_id:
  - 81
rank_math_internal_links_processed:
  - 1
categories:
  - Linux

---
In this tutorial guide, I will be taking through the installation of pgAdmin 4 version 6.2 on CentOS 8

pgAdmin 4 is a free and open-source management tool for Postgres. Its desktop runtime written in NWjs allows it to run standalone for individual users, or the web applications code may be directly deployed on a web server for use by the web browser.

pgAdmin 4 is a complete rewrite of pgAdmin, built using Python and Java.

## New features of pgAdmin 4 version 6.2 {.wp-block-heading}

The following new features were introduced for the latest release of 6.2. these features are mostly for bug fixes.

  * It added support of&nbsp;[Aggregate and Operator node][1]&nbsp;in view-only mode
  * It ensures that users will be able to modify the REMOTE\_USER environment variable as per their environment by introducing the new config parameter WEBSERVER\_REMOTE_USER.

You can refer to&nbsp;[pgAdmin 4 documentation][2]&nbsp;for more concerning the bug fixes introduced with the current version 6.2.

We need to install PostgreSQL before we can install pgAdmin4. Check this article on how to install PostgreSQL.

  * [How to Install PostgreSQL 14 on Rocky Linux 8][3]
  * [How to install PostgreSQL 14 on Ubuntu 20.04/21.04/21.10][4]
  * [How to install pgAdmin 4 version 6.2 on Ubuntu 21.10][5]

## Prerequisites {.wp-block-heading}

  * Have a user with sudo priviliges 
  * CentOS 8 server
  * Basic knowlege of terminal.

## Table of Contents {.wp-block-heading}

  1. Run system updates
  2. Uninstall any pgAdmin4 repo currently installed
  3. Set pgAdmin 4 repository
  4. Install pgAdmin 4
  5. Configure pgAdmin 4 on CentOS 8
  6. Conclusion

## 1. Update CentOS 8 repositories {.wp-block-heading}

System updates need to be run every time on a new system, this is in order to make packages up to date. Run the updates using the following command.

<pre class="wp-block-code"><code>$ sudo yum update -y</code></pre>

## 2. Uninstall pgAdmin 4 repo currently installed {.wp-block-heading}

uninstall any pgAdmin 4 currently installed. We can do that like this;

<pre class="wp-block-code"><code>$ sudo rpm -e pgadmin4-redhat-repo</code></pre>

## 3. Set pgAdmin 4 repository {.wp-block-heading}

To add pgAdmin 4 repository, use the following command;

<pre class="wp-block-code"><code>$ sudo yum install https://ftp.postgresql.org/pub/pgadmin/pgadmin4/yum/pgadmin4-redhat-repo-2-1.noarch.rpm</code></pre>

## 4. Install pgAdmin 4 on CentOS 8 {.wp-block-heading}

To install pgAdmin 4 both desktop and web modes, run the following command;

<pre class="wp-block-code"><code>$ sudo yum install pgadmin4</code></pre>

Sample output

<pre class="wp-block-code"><code>Installing:
 pgadmin4                   noarch        6.2-1.el8                                       pgAdmin4         6.2 k
Upgrading:
 chkconfig                  x86_64        1.19.1-1.el8                                    baseos           198 k
 platform-python-pip        noarch        9.0.3-20.el8                                    baseos           1.7 M
Installing dependencies:
 apr                        x86_64        1.6.3-12.el8                                    appstream        129 k
 apr-util                   x86_64        1.6.1-6.el8                                     appstream        105 k
 centos-logos-httpd         noarch        85.8-2.el8                                      baseos            75 k
 desktop-file-utils         x86_64        0.23-8.el8                                      appstream         80 k
 emacs-filesystem           noarch        1:26.1-7.el8                                    baseos            70 k
 httpd                      x86_64        2.4.37-43.module_el8.5.0+1022+b541f3b1          appstream        1.4 M
 httpd-filesystem           noarch        2.4.37-43.module_el8.5.0+1022+b541f3b1          appstream         39 k
 httpd-tools                x86_64        2.4.37-43.module_el8.5.0+1022+b541f3b1          appstream        107 k
 libatomic                  x86_64        8.5.0-4.el8_5                                   baseos            24 k
 mailcap                    noarch        2.1.48-3.el8                                    baseos            39 k
 mod_http2                  x86_64        1.15.7-3.module_el8.4.0+778+c970deab            appstream        154 k
 pgadmin4-desktop           x86_64        6.2-1.el8                                       pgAdmin4          80 M
 pgadmin4-server            x86_64        6.2-1.el8                                       pgAdmin4          87 M
 pgadmin4-web               noarch        6.2-1.el8                                       pgAdmin4         8.6 k
 postgresql14-libs          x86_64        14.1-1PGDG.rhel8                                pgAdmin4         274 k
 python3-mod_wsgi           x86_64        4.6.4-4.el8                                     appstream        2.5 M
 python3-pip                noarch        9.0.3-20.el8                                    appstream         20 k
 python3-setuptools         noarch        39.2.0-6.el8                                    baseos           163 k
 python36                   x86_64        3.6.8-38.module_el8.5.0+895+a459eca8            appstream         19 k
 xdg-utils                  noarch        1.1.2-5.el8                                     appstream         84 k
Installing weak dependencies:
 apr-util-bdb               x86_64        1.6.1-6.el8                                     appstream         25 k
 apr-util-openssl           x86_64        1.6.1-6.el8                                     appstream         27 k
Enabling module streams:
 httpd                                    2.4                                                                   
 python36                                 3.6    

</code></pre>

## 5. Configure pgAdmin 4 on CentOS 8 {.wp-block-heading}

We need to serve our pgadmin 4 using an Apache webserver. First, enable it to start on boot

<pre class="wp-block-code"><code>$ sudo systemctl start httpd </code></pre>

If you are getting the error message &#8220;Failed to start httpd.service: Unit httpd.service not found.&#8221; please install httpd into your system.

<pre class="wp-block-code"><code>$ sudo yum install httpd -y</code></pre>

Then go ahead and start the http service.

Also make sure you enable the service to run every time you boot your machine.

<pre class="wp-block-code"><code>$ sudo systemctl enable httpd</code></pre>

You can check the status with the following command;

<pre class="wp-block-code"><code>$ sudo systemctl status httpd</code></pre>

If the service status is active then it&#8217;s good to continue.

<pre class="wp-block-code"><code>● httpd.service - The Apache HTTP Server
   Loaded: loaded (/usr/lib/systemd/system/httpd.service; enabled; vendor preset: disabled)
   Active: active (running) since Mon 2021-11-29 15:56:36 UTC; 2min 56s ago
     Docs: man:httpd.service(8)
 Main PID: 72868 (httpd)
   Status: "Running, listening on: port 80"
    Tasks: 213 (limit: 4763)
   Memory: 25.1M
   CGroup: /system.slice/httpd.service
           ├─72868 /usr/sbin/httpd -DFOREGROUND
           ├─72869 /usr/sbin/httpd -DFOREGROUND
           ├─72870 /usr/sbin/httpd -DFOREGROUND
           ├─72871 /usr/sbin/httpd -DFOREGROUND
           └─72872 /usr/sbin/httpd -DFOREGROUND

Nov 29 15:56:36 centos-8 systemd&#91;1]: Starting The Apache HTTP Server...
Nov 29 15:56:36 centos-8 httpd&#91;72868]: AH00558: httpd: Could not reliably determine the server's fully qualified&gt;
Nov 29 15:56:36 centos-8 systemd&#91;1]: Started The Apache HTTP Server.
Nov 29 15:56:36 centos-8 httpd&#91;72868]: Server configured, listening on: port 80</code></pre>

Let&#8217;s now run the web setup script so that we can configure the system to run in web mode.

<pre class="wp-block-code"><code>$ sudo /usr/pgadmin4/bin/setup-web.sh</code></pre>

Sample output

<pre class="wp-block-code"><code>Setting up pgAdmin 4 in web mode on a Redhat based platform...
Creating configuration database...
NOTE: Configuring authentication for SERVER mode.

Enter the email address and password to use for the initial pgAdmin user account:

Email address: nextgentips01@gmail.com
Password: 
Retype password:
pgAdmin 4 - Application Initialisation
======================================

Creating storage and log directories...
Configuring SELinux...
The Apache web server is running and must be restarted for the pgAdmin 4 installation to complete. Continue (y/n)? y
Apache successfully restarted. You can now start using pgAdmin 4 in web mode at &lt;strong>http://127.0.0.1/pgadmin4&lt;/strong></code></pre>

Open your favorite browser and paste **http://<Your\_IP\_Address>/pgadmin4** to open pgadmin 4<figure class="wp-block-image size-large">

<img decoding="async" loading="lazy" width="810" height="401" src="https://i0.wp.com/nextgentips.com/wp-content/uploads/2021/11/Screenshot-from-2021-11-29-19-28-28.png?resize=810%2C401&#038;ssl=1" alt="pgAdmin interface" class="wp-image-759" srcset="https://i0.wp.com/nextgentips.com/wp-content/uploads/2021/11/Screenshot-from-2021-11-29-19-28-28.png?resize=1024%2C507&ssl=1 1024w, https://i0.wp.com/nextgentips.com/wp-content/uploads/2021/11/Screenshot-from-2021-11-29-19-28-28.png?resize=300%2C148&ssl=1 300w, https://i0.wp.com/nextgentips.com/wp-content/uploads/2021/11/Screenshot-from-2021-11-29-19-28-28.png?resize=768%2C380&ssl=1 768w, https://i0.wp.com/nextgentips.com/wp-content/uploads/2021/11/Screenshot-from-2021-11-29-19-28-28.png?w=1051&ssl=1 1051w" sizes="(max-width: 810px) 100vw, 810px" data-recalc-dims="1" /> <figcaption>pgAdmin interface</figcaption></figure> 

## 6. Conclusion {.wp-block-heading}

We have installed pgAdmin 4 on our CentOS 8 successfully. Continue exploring by adding new servers then connecting with the databases in order to know more.

 [1]: https://www.postgresql.org/docs/9.1/functions-aggregate.html
 [2]: https://www.pgadmin.org/docs/pgadmin4/latest/release_notes_6_2.html
 [3]: https://nextgentips.com/2021/10/28/how-to-install-postgresql-14-on-rocky-linux-8/
 [4]: https://nextgentips.com/2021/10/09/how-to-install-postgresql-on-ubuntu-20-04/
 [5]: https://nextgentips.com/2021/11/19/how-to-install-pgadmin-4-version-6-2-on-ubuntu-20-04/