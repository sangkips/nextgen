---
title: How to use PostgreSQL to perform CRUD operations in FastAPI
author: Kipkoech Sang
type: post
date: 2022-12-02T11:45:05+00:00
url: /2022/12/02/how-to-use-postgresql-to-perform-crud-operations-in-fastapi/
categories:
  - Programming

---
In the <a href="https://nextgentips.com/2022/11/30/how-to-perform-crud-functionalities-in-fastapi/" target="_blank" rel="noopener" title="">previous tutorial</a>, we saw how to perform CRUD operations using dummy data inside your project, but that can&#8217;t help us in real-world issues. Today I will take you through using the PostgreSQL database to perform CRUD operations in FastAPI RESTful services.

FastAPI really works well with both SQL and NoSQL databases, you can use SQL databases with the help of SQLAlchemy which is the Python SQL toolkit and Object Relational Mapper. It gives developers the full power and flexibility of SQL.

To start our project create a folder where you will install the dependencies and create a virtual environment. I will still be using the song API I created in the previous tutorial.

### 1. Create a virtual environment {.wp-block-heading}

It&#8217;s good to create a virtual environment so that you avoid installing programs globally. Create a virtual environment and activate

<pre class="wp-block-code"><code>python3 -m venv env
source env/bin/activate</code></pre>

### 2. Install FastAPI {.wp-block-heading}

To start using FastAPI, we need to install it on our system with the following command.

<pre class="wp-block-code"><code>pip install fastapi</code></pre>

You can `<mark style="background-color:#abb8c3" class="has-inline-color">pip freeze > requirements.txt</mark>` to see all the dependencies

<pre class="wp-block-code"><code>#requirements.txt
anyio==3.6.2
fastapi==0.88.0
idna==3.4
pydantic==1.10.2
sniffio==1.3.0
starlette==0.22.0
typing_extensions==4.4.0</code></pre>

**Starlette** is a lightweight ASGI framework/toolkit which is ideal for building async web services in Python. Startlette gives you the following and among many others:

  * Websocket support
  * In-process background tasks
  * Test client built on **httpx**
  * Session and cookie support 
  * CORS, GZip, static files, and Streaming responses

**Pydantic** is used for data validations and it uses Python-type annotations. It enforces type hints at runtime and provides user-friendly errors when data is invalid.

### 3. Install Uvicorn  {.wp-block-heading}

Uvicorn is an ASGI web server for Python code implementation. You need ASGI for production. Install Uvicorn with the following command.

<pre class="wp-block-code"><code>pip install "uvicorn&#91;standard]"</code></pre>

The above command will install Uvicorn with `<mark style="background-color:#abb8c3" class="has-inline-color">Cython-based</mark>` dependencies. Cython is used in managing event loops with `<mark style="background-color:#abb8c3" class="has-inline-color">uvloop</mark>` which is a fast, drop-in replacement of the built-in <mark style="background-color:#abb8c3" class="has-inline-color">`asyncio` </mark>event loop.

### 4. Testing the routes {.wp-block-heading}

The first thing we need to do is to import pydantic into our project `<mark style="background-color:#abb8c3" class="has-inline-color">schema.py</mark>` file. Pydantic enables us to specify our models&#8217; hints.

<pre class="wp-block-code"><code>#schema.py 
from pydantic import BaseModel # import pydantic 

app = FastAPI

class song(BaseModel):
     title: str
     artist: str
     year: int
     description: str | None = None # None shows that the attribute is not necessary supplied. It can be blank.</code></pre>

As an example let&#8217;s create a request to see how pydantic simplifies our view.

<pre class="wp-block-code"><code>#main.py
from fastapi import FastAPI
from songs import Song

app = FastAPI()


# Project root endpoint
@app.get('/')
async def root():
    return {'Message': 'Welcome to my Songs Library'}

# Create a song
@app.post('/songs') # endpoint
def create_Song(song:Song): # creating a schema
    return song</code></pre>

To run this code use `<mark style="background-color:#abb8c3" class="has-inline-color">uvicorn main:app--releoad</mark>`. 

**Uvicorn** is the ASGI server**, Main** is the root of a project `<mark style="background-color:#abb8c3" class="has-inline-color">main.py</mark>` and **reload** is to make sure the server reloads every time we introduce changes.

### 5. Install Python psycopg2 {.wp-block-heading}

**psycopg2** is a PostgreSQL database adapter. So because we are using PostgreSQL here, we need to install psycopg2 into our environment. If you don&#8217;t have PostgreSQL installed already make sure you check it here. <a href="https://nextgentips.com/2022/10/14/how-to-install-and-configure-postgresql-15-on-debian-11/" target="_blank" rel="noopener" title="">How to install PostgreSQL 15 on Debian 11</a>.

<pre class="wp-block-code"><code>pip install psycopg2 # needs a c compiler 
or 
pip install psycopg2-binary</code></pre>

### 6. Install SQLAlchemy {.wp-block-heading}

SQLAlchemy will handle ourdatabase connections with our PostgreSQL. It provides Object Relational Mapper (ORM) to enable us write our SQL queries.

<pre class="wp-block-code"><code>pip install sqlalchemy</code></pre>

When installation is complete, go ahead and create a `<mark style="background-color:#abb8c3" class="has-inline-color">database.py</mark>` file. i like separating everything so that I will be in a position to scale the application and also for simplicity purposes.

Inside the database.py paste the following code.

<pre class="wp-block-code"><code>#database.py
from sqlalchemy import create_engine
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker

# connection database url for SQLAlchemy
SQLALCHEMY_DATABASE_URL = "postgresql://fastapiuser:strpassword@localhost/db"

# create SQLAlchemy engine 
engine = create_engine(
    SQLALCHEMY_DATABASE_URL
)
# Create a sessionLocal class which acts as a database session
SessionLocal = sessionmaker(autocommit=False, autoflush=False, bind=engine)

# create a decrative base that returns a class
Base = declarative_base()</code></pre>

`<mark style="background-color:#abb8c3" class="has-inline-color">Create_engine</mark>` creates a new Engine instance. It lets SQLAlchemy deals with most of the database issues.

`<mark style="background-color:#abb8c3" class="has-inline-color">Sessionmaker</mark>` is a function that creates an instance of a session 

`<mark style="background-color:#abb8c3" class="has-inline-color">Declarative_base</mark>` maps models directly to the tables that are on the database.

### 7. Create SQLAlchemy models from base model {.wp-block-heading}

In the project folder create a <mark style="background-color:#abb8c3" class="has-inline-color">`models.py` </mark>file. This is where we will place all the SQLAlchemy models class inheriting from the `<mark style="background-color:#abb8c3" class="has-inline-color">Base class</mark>` we declared above. Add the following code to the models.py file.

<pre class="wp-block-code"><code>from database import Base
from sqlalchemy import Column, ForeignKey, Integer, String
from sqlalchemy.orm import relationship


class Artist(Base):
    __tablename__ = 'artists'

    id = Column(Integer, primary_key=True, index=True)
    name = Column(String, index=True)
    songs = relationship('Song', back_populates='artist')


class Song(Base):
    __tablename__ = 'songs'

    id = Column(Integer, primary_key=True, index=True)
    title = Column(String, index=True)
    year = Column(Integer, index=True)
    description = Column(String, index=True)
    artist_id = Column(Integer, ForeignKey('artists.id'))

    artist = relationship('Artist', back_populates='songs')

    # represent an object as a string

    def __repr__(self):
        return f'&lt;Song title = {self.title} year={self.year}&gt;'</code></pre>

The&nbsp;`<mark style="background-color:#abb8c3" class="has-inline-color">__tablename__</mark>`&nbsp;attribute tells SQLAlchemy the name of the table to use in the database for each of these models.

We use `<mark style="background-color:#abb8c3" class="has-inline-color">=</mark>` when representing SQLAlchemy models. 

### 8. Create Pydantic Models {.wp-block-heading}

Pydantic models or schemas create attributes of the entire database structure, it will show how data will be created and read. In the schema.py we created earlier add add the following code.

<pre class="wp-block-code"><code># entire schema.py file
from pydantic import BaseModel

class Song(BaseModel):
    id: int
    title: str
    artist: str
    year: int
    description: str | None = None
    artist_id: int

    class Config: # provides configuration to pydantic
        orm_mode = True

class Artist(BaseModel):
    name: str

    class Config:
        orm_mode = True</code></pre>

Class Config provides configuration to pydantic. Not the difference between pydantic models and SQLAlchemy models. For pydantic we use `<mark style="background-color:#f78da7" class="has-inline-color">:</mark>` while SQLAlchemy we use `<mark style="background-color:#f78da7" class="has-inline-color">=</mark>` 

Pydantic&#8217;s `<mark style="background-color:#abb8c3" class="has-inline-color">orm_mode</mark>` will tell the Pydantic model to read the data even if it is not a `<mark style="background-color:#abb8c3" class="has-inline-color">dict</mark>`, but an ORM model (or any other arbitrary object with attributes). 

### Test Database Connection {.wp-block-heading}

In order to test whether we can connect to our PostgreSQL database, we can `<mark style="background-color:#abb8c3" class="has-inline-color">create_db.py</mark>` file. I am creating this file because I don&#8217;t want to use the python shell to test my connection. This is a better and easier way to do the test. Inside paste the following code.

<pre class="wp-block-code"><code>#create_db.py
from database import Base, engine
from models import Song, Artist

Base.metadata.create_all(engine)</code></pre>

This is what you are going to see if you don&#8217;t run into errors if you run `<mark style="background-color:#abb8c3" class="has-inline-color">python create_db.py</mark>` in your terminal

<pre class="wp-block-code"><code>2022-12-01 20:48:36,735 INFO sqlalchemy.engine.Engine select pg_catalog.version()
2022-12-01 20:48:36,735 INFO sqlalchemy.engine.Engine &#91;raw sql] {}
2022-12-01 20:48:36,736 INFO sqlalchemy.engine.Engine select current_schema()
2022-12-01 20:48:36,736 INFO sqlalchemy.engine.Engine &#91;raw sql] {}
2022-12-01 20:48:36,737 INFO sqlalchemy.engine.Engine show standard_conforming_strings
2022-12-01 20:48:36,737 INFO sqlalchemy.engine.Engine &#91;raw sql] {}
2022-12-01 20:48:36,737 INFO sqlalchemy.engine.Engine BEGIN (implicit)
2022-12-01 20:48:36,738 INFO sqlalchemy.engine.Engine select relname from pg_class c join pg_namespace n on n.oid=c.relnamespace where pg_catalog.pg_table_is_visible(c.oid) and relname=%(name)s
2022-12-01 20:48:36,738 INFO sqlalchemy.engine.Engine &#91;generated in 0.00021s] {'name': 'artists'}
2022-12-01 20:48:36,740 INFO sqlalchemy.engine.Engine select relname from pg_class c join pg_namespace n on n.oid=c.relnamespace where pg_catalog.pg_table_is_visible(c.oid) and relname=%(name)s
2022-12-01 20:48:36,740 INFO sqlalchemy.engine.Engine &#91;cached since 0.001525s ago] {'name': 'songs'}
2022-12-01 20:48:36,741 INFO sqlalchemy.engine.Engine 
&lt;strong>CREATE TABLE artists (
        id SERIAL NOT NULL, 
        name VARCHAR, 
        PRIMARY KEY (id)
)&lt;/strong>

2022-12-01 20:48:36,741 INFO sqlalchemy.engine.Engine &#91;no key 0.00018s] {}
2022-12-01 20:48:36,758 INFO sqlalchemy.engine.Engine CREATE INDEX ix_artists_name ON artists (name)
2022-12-01 20:48:36,758 INFO sqlalchemy.engine.Engine &#91;no key 0.00024s] {}
2022-12-01 20:48:36,763 INFO sqlalchemy.engine.Engine CREATE INDEX ix_artists_id ON artists (id)
2022-12-01 20:48:36,764 INFO sqlalchemy.engine.Engine &#91;no key 0.00028s] {}
2022-12-01 20:48:36,769 INFO sqlalchemy.engine.Engine 
&lt;strong>CREATE TABLE songs (
        id SERIAL NOT NULL, 
        title VARCHAR, 
        year INTEGER, 
        description VARCHAR, 
        artist_id INTEGER, 
        PRIMARY KEY (id), 
        FOREIGN KEY(artist_id) REFERENCES artists (id)
)&lt;/strong>

2022-12-01 20:48:36,769 INFO sqlalchemy.engine.Engine &#91;no key 0.00018s] {}
2022-12-01 20:48:36,785 INFO sqlalchemy.engine.Engine CREATE INDEX ix_songs_year ON songs (year)
2022-12-01 20:48:36,786 INFO sqlalchemy.engine.Engine &#91;no key 0.00020s] {}
2022-12-01 20:48:36,798 INFO sqlalchemy.engine.Engine CREATE INDEX ix_songs_id ON songs (id)
2022-12-01 20:48:36,798 INFO sqlalchemy.engine.Engine &#91;no key 0.00016s] {}
2022-12-01 20:48:36,804 INFO sqlalchemy.engine.Engine CREATE INDEX ix_songs_title ON songs (title)
2022-12-01 20:48:36,804 INFO sqlalchemy.engine.Engine &#91;no key 0.00020s] {}
2022-12-01 20:48:36,810 INFO sqlalchemy.engine.Engine CREATE INDEX ix_songs_description ON songs (description)
2022-12-01 20:48:36,810 INFO sqlalchemy.engine.Engine &#91;no key 0.00035s] {}
2022-12-01 20:48:36,821 INFO sqlalchemy.engine.Engine COMMIT</code></pre>

### 9. Perform CRUD operations {.wp-block-heading}

After we have successfully connected our database, its now time to perform CRUD operations on our API. The operations we will perform are:

  * Create songs/artist
  * Read songs/artist from the database
  * Update given song/artist
  * Delete given song/artist

#### List all songs and Artists(GET) {.wp-block-heading}

To be able to list all the songs from the database use the following code

<pre class="wp-block-code"><code># Retrieve all songs
@app.get('/songs', response_model=List&#91;Song], status_code=200)
def get_songs():
    songs = db.query(models.Song).all()
    return songs</code></pre>

<pre class="wp-block-code"><code># Retrieve all Artists
@app.get('/artists', response_model=List&#91;Artist], status_code=200)
def get_all_artists():
    artists = db.query(models.Artist).all()
    return artists</code></pre>

`<mark style="background-color:#abb8c3" class="has-inline-color">Response_model</mark>` is a parameter of the decorator method such as put,get,post,delete etc. response model is used to convert the output data to its type declaration and to validate data.

The above code when you run on swagger UI will give an empty list because the database is still empty.

#### List a single song or artist(GET) {.wp-block-heading}

If you want to retrieve a single song from the database, use the following code:

<pre class="wp-block-code"><code># Retrieve a single song
@app.get('/song/{song_id}', response_model=Song, status_code=status.HTTP_200_OK)
def get_one_song(song_id:int):
    song = db.query(models.Song).filter(models.Song.id==song_id).first()
    return song</code></pre>

And for retrieving a single artist, it the same with that of the song. Just change models song to artist.

#### Create a new song and artist(POST) {.wp-block-heading}

Lets start by creating new artist first so that we can not violate the foreign key constrain. Because songs belong to different artists or a single artist. The most important thing with create is the `<mark style="background-color:#abb8c3" class="has-inline-color">add</mark>` and `<mark style="background-color:#abb8c3" class="has-inline-color">commit</mark>`. Add will add the newly created data to the database but it doesn&#8217;t save until you commit the changes.

<pre class="wp-block-code"><code># Add an Artist
@app.post('/Add_artist', response_model=Artist, status_code=status.HTTP_201_CREATED)
def create_an_artist(artist:Artist):
   new_artist = models.Artist(name=artist.name)

   db.add(new_artist)
   db.commit()
   return new_artist</code></pre>

For adding a new song use the following code, both codes are the same its only the attributes which are different.

<pre class="wp-block-code"><code># Create a song
@app.post('/create_song', response_model=Song, status_code=status.HTTP_201_CREATED)
def create_a_song(song:Song):
   new_song = models.Song(title=song.title, year=song.year, description=song.description, artist_id=song.artist_id)
   
   # add new song to the db using SessionLocal class
   db.add(new_song)

   # save the new song to the database
   db.commit()
   return new_song</code></pre>

#### Updating a song or artist(PUT) {.wp-block-heading}

To update data, we need to get the id of the song to update and then invoke the db update method then use the commit function to save the changes to the database. Check the code below.

<pre class="wp-block-code"><code># Update a song
@app.put('/update_song/song_id', response_model=Song, status_code=status.HTTP_200_OK)
def update_a_song(song_id: int, song: Song):
    song_to_be_updated = db.query(models.Song).filter(
        models.Song.id == song_id).first()
    song_to_be_updated.title = song.title  # for updating the title
    song_to_be_updated.year = song.year
    song_to_be_updated.description = song.description
    song_to_be_updated.artist_id = song.artist_id

    db.commit()  # save changes

    return song_to_be_updated
</code></pre>

For updating the artist, just change the song&#8217;s attributes 

#### Deleting songs or artist(DELETE) {.wp-block-heading}

Invoking delete will remove an item from the database. Look at the following code

<pre class="wp-block-code"><code># delete a song from database
@app.delete('/song/{song_id}')
def delete_a_song(song_id: int):
    song_to_be_deleted = db.query(models.Song).filter(
        models.Song.id == song_id).first()
# raise an exception if the song is not found
    if song_to_be_deleted is None:
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND, detail='That song is not found in the databse')
    db.delete(song_to_be_deleted)  # delete a song instance
    db.commit()  # save to the databse
    return {'message': 'The song has been deleted successfully'}</code></pre>

For delete artist just change the query model to artist.

## Conclusion {.wp-block-heading}

We have successfully implemented CRUD operations using PostgreSQL, SQLAlchemy on a fastAPI application. I hope you have learned something new today. Get the code from <a href="https://github.com/sangkips/my-songs-api" target="_blank" rel="noopener" title="">github repository</a>.