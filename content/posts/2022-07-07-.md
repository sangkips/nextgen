---
title: How to install Apache Cassandra 4 on Fedora 36
author: Kipkoech Sang
type: post
date: -001-11-30T00:00:00+00:00
draft: true
url: /?p=1475
categories:
  - Uncategorized

---
<a href="https://cassandra.apache.org/_/index.html" target="_blank" rel="noreferrer noopener">Apache Cassandra</a>&nbsp;is an open-source, free, NoSQL database used to store large data. Its linear scalability and proven fault tolerance on commodity hardware or cloud infrastructure make it the perfect platform for mission-critical data.

NoSQL databases are lightweight, open-source, non-relational, and largely distributed. It has the following strengths:

  * It has horizontal scalability. Cassandra allows for the seamless addition of nodes. A node represents an instance of Cassandra. These nodes do communicate with each other through a protocol called gossip. Gossip is a peer-to-peer communication protocol in which nodes periodically exchange communication about themselves and other nodes they know about.
  * It contains distributed architecture. Distributed means that Cassandra can run on multiple machines while appearing to users as a unified whole. Cassandra databases easily scale when an application is under stress, this prevents data loss from any given data center’s hardware failure.
  * It has a flexible approach to the schema definition. A schema once defined its columns for a table while inserting data in every row, all columns must at least be filled with null value but for Cassandra column families are defined the columns are not. You can add any column to any column family at any time.

## Install Apache Cassandra on Fedora 36 {.wp-block-heading}

1. Update the system.

We begin by updating our system repositories in order to make them up to date.

<pre class="wp-block-code"><code>$ sudo dnf update -y</code></pre>

### 2. Install Java {.wp-block-heading}

Ensure java 8 or above is installed because Cassandra depends on Java to run. So let us check if we have java in our system with the following command on our terminal.

<pre class="wp-block-code"><code>$ java version</code></pre>

You should be in a position to see the following output.

<pre class="wp-block-code"><code>-bash: java: command not found</code></pre>

To install Java on your system, use the following command:

<pre class="wp-block-code"><code>$ sudo dnf install java-latest-openjdk.x86_64</code></pre>

### 3. Add Apache Cassandra repository {.wp-block-heading}

We need to add Apache Cassandra to the file `<mark style="background-color:#abb8c3" class="has-inline-color">/etc/yum.repos.d/cassandra.repo</mark>`. Use vim or nano to execute this task.

<pre class="wp-block-code"><code>sudo vi &lt;span style="background-color: rgb(171, 184, 195); font-family: inherit; font-size: inherit; color: initial;">&lt;mark style="background-color:#ffffff" class="has-inline-color">/etc/yum.repos.d/cassandra.repo&lt;/mark>&lt;/span></code></pre>

Add this contents.

<pre class="wp-block-code"><code>&#91;cassandra]
name=Apache Cassandra
baseurl=https://downloads.apache.org/cassandra/redhat/40x/
gpgcheck=1
repo_gpgcheck=1
gpgkey=https://downloads.apache.org/cassandra/KEYS</code></pre>

Save and quit and update system repositories again.

<pre class="wp-block-code"><code>$ sudo apt update
Apache Cassandra                                846  B/s | 833  B     00:00    
Apache Cassandra                                267 kB/s | 268 kB     00:01    
Importing GPG key 0xF2833C93:
 Userid     : "Eric Evans &lt;eevans@sym-link.com>"
 Fingerprint: CEC8 6BB4 A0BA 9D0F 9039 7CAE F835 8FA2 F283 3C93
 From       : https://downloads.apache.org/cassandra/KEYS
Importing GPG key 0x8D77295D:
 Userid     : "Eric Evans &lt;eevans@sym-link.com>"
 Fingerprint: C496 5EE9 E301 5D19 2CCC F2B6 F758 CE31 8D77 295D
 From       : https://downloads.apache.org/cassandra/KEYS
Importing GPG key 0x2B5C1B00:
 Userid     : "Sylvain Lebresne (pcmanus) &lt;sylvain@datastax.com>"
 Fingerprint: 5AED 1BF3 78E9 A19D ADE1 BCB3 4BD7 36A8 2B5C 1B00
 From       : https://downloads.apache.org/cassandra/KEYS
Importing GPG key 0x0353B12C:
 Userid     : "T Jake Luciani &lt;jake@apache.org>"
 Fingerprint: 514A 2AD6 31A5 7A16 DD00 47EC 749D 6EEC 0353 B12C
 From       : https://downloads.apache.org/cassandra/KEYS
Importing GPG key 0xFE4B2BDA:
 Userid     : "Michael Shuler &lt;michael@pbandjelly.org>"
 Fingerprint: A26E 528B 271F 19B9 E5D8 E19E A278 B781 FE4B 2BDA
 From       : https://downloads.apache.org/cassandra/KEYS
Importing GPG key 0x7E3E87CB:
 Userid     : "Michael Semb Wever &lt;mick@thelastpickle.com>"
 Fingerprint: A4C4 65FE A0C5 5256 1A39 2A61 E913 35D7 7E3E 87CB
 From       : https://downloads.apache.org/cassandra/KEYS
Importing GPG key 0xB7F6840C:
 Userid     : "Alex Petrov &lt;oleksandr.petrov@gmail.com>"
 Fingerprint: 9E66 CEC6 106D 578D 0B1E B9BF F100 0962 B7F6 840C
 From       : https://downloads.apache.org/cassandra/KEYS
Importing GPG key 0xAF30F054:
 Userid     : "Jordan West &lt;jwest@apache.org>"
 Fingerprint: C400 9872 C59B 4956 1310 D966 D006 2876 AF30 F054
 From       : https://downloads.apache.org/cassandra/KEYS
Importing GPG key 0x0B84C041:
 Userid     : "Brandon Williams &lt;brandonwilliams@apache.org>"
 Fingerprint: B784 2CDA F36E 6A32 14FA E35D 5E85 B9AE 0B84 C041
 From       : https://downloads.apache.org/cassandra/KEYS
Importing GPG key 0x15BBF5F0:
 Userid     : "Ekaterina Buryanova Dimitrova (CODE SIGNING KEY) &lt;e.dimitrova@gmail.com>"
 Fingerprint: 3E9C 8769 07A5 60AC A009 64F3 63E9 BAD2 15BB F5F0
 From       : https://downloads.apache.org/cassandra/KEYS
Importing GPG key 0x162C5A55:
 Userid     : "Sam Tunnicliffe (CODE SIGNING KEY) &lt;samt@apache.org>"
 Fingerprint: F8B7 FD00 E05C 9329 91A2 CD61 50EE 103D 162C 5A55
 From       : https://downloads.apache.org/cassandra/KEYS
Apache Cassandra                                2.8 kB/s | 3.6 kB     00:01    
Dependencies resolved.
Nothing to do.</code></pre>

Install Apache Cassandra with the following command.

<pre class="wp-block-code"><code>$ sudo dnf install cassandra -y</code></pre>

You should see the following output 

<pre class="wp-block-code"><code>Last metadata expiration check: 0:04:39 ago on Thu Jul  7 11:57:53 2022.
Dependencies resolved.
================================================================================
 Package                    Arch     Version                  Repository   Size
================================================================================
Installing:
 cassandra                  noarch   4.0.4-1                  cassandra    45 M
Installing dependencies:
 java-17-openjdk            x86_64   1:17.0.3.0.7-2.fc36      updates     223 k
 java-17-openjdk-headless   x86_64   1:17.0.3.0.7-2.fc36      updates      40 M

Transaction Summary
================================================================================
Install  3 Packages

Total download size: 85 M
Installed size: 244 M
Is this ok &#91;y/N]: y
Downloading Packages:
(1/3): java-17-openjdk-17.0.3.0.7-2.fc36.x86_64 810 kB/s | 223 kB     00:00    
(2/3): java-17-openjdk-headless-17.0.3.0.7-2.fc  20 MB/s |  40 MB     00:02    
(3/3): cassandra-4.0.4-1.noarch.rpm              14 MB/s |  45 MB     00:03    
--------------------------------------------------------------------------------
Total                                            24 MB/s |  85 MB     00:03     
Apache Cassandra                                291 kB/s | 268 kB     00:00    
Importing GPG key 0xF2833C93:
 Userid     : "Eric Evans &lt;eevans@sym-link.com>"
 Fingerprint: CEC8 6BB4 A0BA 9D0F 9039 7CAE F835 8FA2 F283 3C93
 From       : https://downloads.apache.org/cassandra/KEYS
Is this ok &#91;y/N]: y
Key imported successfully
Importing GPG key 0x8D77295D:
 Userid     : "Eric Evans &lt;eevans@sym-link.com>"
 Fingerprint: C496 5EE9 E301 5D19 2CCC F2B6 F758 CE31 8D77 295D
 From       : https://downloads.apache.org/cassandra/KEYS
Is this ok &#91;y/N]: y
Key imported successfully
Importing GPG key 0x2B5C1B00:
 Userid     : "Sylvain Lebresne (pcmanus) &lt;sylvain@datastax.com>"
 Fingerprint: 5AED 1BF3 78E9 A19D ADE1 BCB3 4BD7 36A8 2B5C 1B00
 From       : https://downloads.apache.org/cassandra/KEYS
Is this ok &#91;y/N]: y
Key imported successfully
Importing GPG key 0x0353B12C:
 Userid     : "T Jake Luciani &lt;jake@apache.org>"
 Fingerprint: 514A 2AD6 31A5 7A16 DD00 47EC 749D 6EEC 0353 B12C
 From       : https://downloads.apache.org/cassandra/KEYS
Is this ok &#91;y/N]: y
Key imported successfully
Importing GPG key 0xFE4B2BDA:
 Userid     : "Michael Shuler &lt;michael@pbandjelly.org>"
 Fingerprint: A26E 528B 271F 19B9 E5D8 E19E A278 B781 FE4B 2BDA
 From       : https://downloads.apache.org/cassandra/KEYS
Is this ok &#91;y/N]: y
Key imported successfully
Importing GPG key 0x7E3E87CB:
 Userid     : "Michael Semb Wever &lt;mick@thelastpickle.com>"
 Fingerprint: A4C4 65FE A0C5 5256 1A39 2A61 E913 35D7 7E3E 87CB
 From       : https://downloads.apache.org/cassandra/KEYS
Is this ok &#91;y/N]: y
Key imported successfully
Importing GPG key 0xB7F6840C:
 Userid     : "Alex Petrov &lt;oleksandr.petrov@gmail.com>"
 Fingerprint: 9E66 CEC6 106D 578D 0B1E B9BF F100 0962 B7F6 840C
 From       : https://downloads.apache.org/cassandra/KEYS
Is this ok &#91;y/N]: y
Key imported successfully
Importing GPG key 0xAF30F054:
 Userid     : "Jordan West &lt;jwest@apache.org>"
 Fingerprint: C400 9872 C59B 4956 1310 D966 D006 2876 AF30 F054
 From       : https://downloads.apache.org/cassandra/KEYS
Is this ok &#91;y/N]: y
Key imported successfully
Importing GPG key 0x0B84C041:
 Userid     : "Brandon Williams &lt;brandonwilliams@apache.org>"
 Fingerprint: B784 2CDA F36E 6A32 14FA E35D 5E85 B9AE 0B84 C041
 From       : https://downloads.apache.org/cassandra/KEYS
Is this ok &#91;y/N]: y
Key imported successfully
Importing GPG key 0x15BBF5F0:
 Userid     : "Ekaterina Buryanova Dimitrova (CODE SIGNING KEY) &lt;e.dimitrova@gmail.com>"
 Fingerprint: 3E9C 8769 07A5 60AC A009 64F3 63E9 BAD2 15BB F5F0
 From       : https://downloads.apache.org/cassandra/KEYS
Is this ok &#91;y/N]: y
Key imported successfully
Importing GPG key 0x162C5A55:
 Userid     : "Sam Tunnicliffe (CODE SIGNING KEY) &lt;samt@apache.org>"
 Fingerprint: F8B7 FD00 E05C 9329 91A2 CD61 50EE 103D 162C 5A55
 From       : https://downloads.apache.org/cassandra/KEYS
Is this ok &#91;y/N]: y
Key imported successfully
Running transaction check
Transaction check succeeded.
Running transaction test
Transaction test succeeded.
Running transaction
  Running scriptlet: java-17-openjdk-headless-1:17.0.3.0.7-2.fc36.x86_64    1/1 
  Preparing        :                                                        1/1 
  Installing       : java-17-openjdk-headless-1:17.0.3.0.7-2.fc36.x86_64    1/3 
  Running scriptlet: java-17-openjdk-headless-1:17.0.3.0.7-2.fc36.x86_64    1/3 
  Installing       : java-17-openjdk-1:17.0.3.0.7-2.fc36.x86_64             2/3 
  Running scriptlet: java-17-openjdk-1:17.0.3.0.7-2.fc36.x86_64             2/3 
  Running scriptlet: cassandra-4.0.4-1.noarch                               3/3 
  Installing       : cassandra-4.0.4-1.noarch                               3/3 
  Running scriptlet: cassandra-4.0.4-1.noarch                               3/3 
  Running scriptlet: java-17-openjdk-headless-1:17.0.3.0.7-2.fc36.x86_64    3/3 
  Running scriptlet: java-17-openjdk-1:17.0.3.0.7-2.fc36.x86_64             3/3 
  Running scriptlet: cassandra-4.0.4-1.noarch                               3/3 
  Verifying        : cassandra-4.0.4-1.noarch                               1/3 
  Verifying        : java-17-openjdk-1:17.0.3.0.7-2.fc36.x86_64             2/3 
  Verifying        : java-17-openjdk-headless-1:17.0.3.0.7-2.fc36.x86_64    3/3 

Installed:
  cassandra-4.0.4-1.noarch                                                      
  java-17-openjdk-1:17.0.3.0.7-2.fc36.x86_64                                    
  java-17-openjdk-headless-1:17.0.3.0.7-2.fc36.x86_64                           

Complete!
</code></pre>

The next thing is to start the Cassandra service 

.

<pre class="wp-block-code"><code>$ sudo systemctl start cassandra</code></pre>