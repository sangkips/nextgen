---
title: How to install Visual Studio Code on Opensuse 15.4
author: Kipkoech Sang
type: post
date: 2022-07-04T09:21:05+00:00
url: /2022/07/04/how-to-install-visual-studio-code-on-opensuse-15-4/
categories:
  - Linux

---
In this article, we are going to learn how to install <a href="https://code.visualstudio.com/docs" target="_blank" rel="noreferrer noopener">Visual Studio code</a> also known as (VScode) on Opensuse 15.4. Visual Studio Code is a lightweight source code editor that runs on desktops and is available to all operating systems. It comes with built-in Javascript, Node.js, and Typescript. One can do programming for almost languages with ease with Visual Studio Code. The languages supported are, Go, PHP, C++, C#, Java, Python, and also .NET.

## Why do we like Visual Studio Code? {.wp-block-heading}

The reason VS code is such an excellent tool is because of the following reasons:

  * It is available on all operating systems so it does not limit one on where to run the VS code, you can easily hit the ground running.
  * Vscode has a rich built-in developer tooling for example IntelliSense code completion and debugging which become handy when you do not know the complete code snippet. It acts as a guide and also takes less time to code.
  * VScode can be customized to suit your needs. You can customize every feature the way you wish and also you can add third-party extensions easily.
  * VScode is an open-source project. So it means you can contribute to the project development, also it means that you have a bigger community where you can ask questions whenever you are faced with a problem.
  * VScode is built for the web. It includes great tools for web development such React JSx, JSON, SCSS,CSS, HTML and Less.

## Install Visual Studio code on Opensuse 15.4 {.wp-block-heading}

### 1. Update system repositories  {.wp-block-heading}

Begin by updating system repositories in order to make them up to date.

<pre class="wp-block-code"><code>$ sudo zypper update</code></pre>

### 2. Add key to the repository {.wp-block-heading}

Currently, the Visual studio code ships a 64-bit version. So let&#8217;s import the key to sign our repository using the following command.

<pre class="wp-block-code"><code>$ sudo rpm --import https://packages.microsoft.com/keys/microsoft.asc</code></pre>

After you have imported the key, add Visual studio code to the yum repositories.

<pre class="wp-block-code"><code>$ sudo sh -c 'echo -e "&#91;code]\nname=Visual Studio Code\nbaseurl=https://packages.microsoft.com/yumrepos/vscode\nenabled=1\ntype=rpm-md\ngpgcheck=1\ngpgkey=https://packages.microsoft.com/keys/microsoft.asc" &gt; /etc/zypp/repos.d/vscode.repo'</code></pre>

The next thing is to update your system repositories for the changes to be effected.

<pre class="wp-block-code"><code>$ sudo zypper refresh</code></pre>

### 3. Install Visual Studio code on Opensuse 15.4 {.wp-block-heading}

To install Visual Studio code run the following command on your terminal.

<pre class="wp-block-code"><code>$ sudo zypper install code</code></pre>

You should get something like this.

<pre class="wp-block-code"><code># output
The following 27 recommended packages were automatically selected:
  adobe-sourcecodepro-fonts adwaita-icon-theme cantarell-fonts gcr-ssh-agent
  gcr-viewer gnome-online-accounts gtk3-branding-openSUSE gtk3-immodule-amharic
  gtk3-immodule-inuktitut gtk3-immodule-thai gtk3-immodule-tigrigna
  gtk3-immodule-vietnamese gvfs gvfs-backends gvfs-fuse libfreebl3-hmac
  libsoftokn3-hmac libudisks2-0_btrfs notification-daemon perl-HTTP-Message
  perl-libwww-perl perl-LWP-Protocol-https perl-TimeDate postfix
  sound-theme-freedesktop udisks2 xkeyboard-config-lang

The following 292 NEW packages are going to be installed:
  adobe-sourcecodepro-fonts adwaita-icon-theme atk-lang at-spi2-atk-common
  at-spi2-atk-gtk2 at-spi2-core at-spi2-core-lang bubblewrap cantarell-fonts
  code cryptsetup cryptsetup-lang cups-config dconf desktop-translations
  dosfstools enchant-2-backend-hunspell enchant-data expat fontconfig
  fontconfig-lang fuse3 gcr-data gcr-lang gcr-prompter gcr-ssh-agent gcr-viewer
  gdk-pixbuf-lang gdk-pixbuf-loader-rsvg gdk-pixbuf-query-loaders
  gdk-pixbuf-thumbnailer gio-branding-openSUSE glib2-tools glib-networking
  glib-networking-lang gnome-online-accounts gnome-online-accounts-lang
  gsettings-backend-dconf gsettings-desktop-schemas
  gsettings-desktop-schemas-lang gstreamer gstreamer-lang gstreamer-plugins-base
  gstreamer-plugins-base-lang gtk2-branding-openSUSE gtk2-data
  gtk2-immodule-amharic gtk2-immodule-inuktitut gtk2-immodule-thai
  gtk2-immodule-tigrigna gtk2-immodule-vietnamese gtk2-lang
  gtk2-metatheme-adwaita gtk2-theming-engine-adwaita gtk2-tools
  gtk3-branding-openSUSE gtk3-data gtk3-immodule-amharic gtk3-immodule-inuktitut
  gtk3-immodule-thai gtk3-immodule-tigrigna gtk3-immodule-vietnamese gtk3-lang
  gtk3-metatheme-adwaita gtk3-schema gtk3-tools gvfs gvfs-backend-afc
  gvfs-backends gvfs-fuse gvfs-lang json-glib-lang libaom3 libarchive13
  libasound2 libatasmart4 libatasmart-utils libatk-1_0-0 libatk-bridge-2_0-0
  libatspi0 libavahi-client3 libavahi-common3 libavahi-glib1 libavif13
  libbd_btrfs2 libbd_crypto2 libbd_fs2 libbd_loop2 libbd_lvm2 libbd_mdraid2
  libbd_part2 libbd_swap2 libbd_utils2 libblockdev libblockdev2 libbluray2
  libbytesize1 libbytesize-lang libcairo2 libcairo-gobject2 libcdda_interface0
  libcdda_paranoia0 libcdio16 libcdio19 libcdio_cdda2 libcdio_paranoia2
  libcolord2 libcups2 libdatrie1 libdav1d5 libdconf1 libdrm2 libenchant-2-2
  libepoxy0 libevdev2 libexif12 libfontconfig1 libfreebl3 libfreebl3-hmac
  libfribidi0 libfuse3-3 libgbm1 libgck-1-0 libgcr-3-1 libgd3 libgdata22
  libgdata-lang libgdk_pixbuf-2_0-0 libgio-2_0-0 libglvnd libgoa-1_0-0
  libgoa-backend-1_0-1 libgobject-2_0-0 libgphoto2-6 libgphoto2-6-lang
  libgraphene-1_0-0 libgraphite2-3 libgstallocators-1_0-0 libgstapp-1_0-0
  libgstaudio-1_0-0 libgstfft-1_0-0 libgstgl-1_0-0 libgstpbutils-1_0-0
  libgstreamer-1_0-0 libgstriff-1_0-0 libgsttag-1_0-0 libgstvideo-1_0-0
  libgtk-2_0-0 libgtk-3-0 libgudev-1_0-0 libharfbuzz0 libharfbuzz-icu0
  libhunspell-1_6-0 libhyphen0 libicu65_1-ledata libicu-suse65_1
  libimobiledevice-1_0-6 libimobiledevice-glue-1_0-0 libinih0
  libjavascriptcoregtk-4_0-18 libjbig2 libjpeg8 libjson-glib-1_0-0 liblcms2-2
  liblmdb-0_9_17 liblockdev1 libltdl7 liblvm2cmd2_03 libmanette-0_2-0
  libmozjs-60 libmtp9 libmtp-udev libnfs8 libnotify4 liboauth0 libogg0
  libopenjp2-7 libopus0 liborc-0_4-0 libpango-1_0-0 libpixman-1-0 libplist-2_0-3
  libpolkit0 libpwquality1 libpwquality-lang librav1e0 librest-0_7-0 librsvg-2-2
  libsecret-1-0 libsecret-lang libsoftokn3 libsoftokn3-hmac libsoup-2_4-1
  libsoup2-lang libthai0 libthai-data libtheoradec1 libtheoraenc1 libtiff5
  libudisks2-0 libudisks2-0_btrfs libunwind libusbmuxd-2_0-6 libvisual
  libvorbis0 libvorbisenc2 libwayland-client0 libwayland-cursor0 libwayland-egl1
  libwayland-server0 libwebkit2gtk3-lang libwebkit2gtk-4_0-37 libwebp7
  libwebpdemux2 libwoff2common1_0_2 libwoff2dec1_0_2 libwpe-1_0-1
  libWPEBackend-fdo-1_0-1 libX11-xcb1 libxcb-render0 libxcb-shm0 libXcomposite1
  libXcursor1 libXdamage1 libXext6 libXfixes3 libXft2 libXi6 libXinerama1
  libxkbcommon0 libxkbfile1 libXpm4 libXrandr2 libXrender1 libXtst6 libXv1
  lockdev lvm2 mdadm metatheme-adwaita-common mozilla-nspr mozilla-nss
  mozilla-nss-certs notification-daemon notification-daemon-lang
  openssh-askpass-gnome perl-CPAN-Changes perl-Devel-Symdump perl-Encode-Locale
  perl-File-Listing perl-HTML-Parser perl-HTML-Tagset perl-HTTP-Cookies
  perl-HTTP-Daemon perl-HTTP-Date perl-HTTP-Message perl-HTTP-Negotiate
  perl-IO-HTML perl-IO-Socket-SSL perl-libwww-perl perl-LWP-MediaTypes
  perl-LWP-Protocol-https perl-Net-DBus perl-Net-HTTP perl-Net-SSLeay
  perl-Pod-Coverage perl-Test-Pod perl-Test-Pod-Coverage perl-TimeDate
  perl-Try-Tiny perl-URI perl-WWW-RobotRules perl-X11-Protocol perl-XML-Parser
  perl-XML-Twig polkit polkit-default-privs postfix shared-mime-info
  shared-mime-info-lang sound-theme-freedesktop system-user-lp system-user-mail
  udisks2 udisks2-lang usbmuxd webkit2gtk-4_0-injected-bundles xdg-dbus-proxy
  xdg-utils xfsprogs xkeyboard-config xkeyboard-config-lang

292 new packages to install.
Overall download size: 210.0 MiB. Already cached: 0 B. After the operation,
additional 695.1 MiB will be used.
Continue? &#91;y/n/v/...? shows all options] (y):</code></pre>

### <span style="color: revert; font-size: revert; font-weight: revert;">4. Uninstall Visual Studio code</span>. {.wp-block-heading}

<span style="font-size: revert; color: initial;">To remove visual studio code from your system, use the following command.</span>

The following package is going to be REMOVED:

<pre class="wp-block-code"><code>1 package to remove.
After the operation, 332.6 MiB will be freed.
Continue? y/n/v/…? shows all options: y
(1/1) Removing code-1.68.1-1655263151.el7.x86_64 …………………….&#91;done]</code></pre>