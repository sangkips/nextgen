---
title: How to install PHP 8.1 on Ubuntu 20.04
author: Kipkoech Sang
type: post
date: 2022-01-31T09:25:15+00:00
url: /2022/01/31/how-to-install-php-8-1-on-ubuntu-20-04/
rank_math_news_sitemap_robots:
  - index
rank_math_robots:
  - 'a:1:{i:0;s:5:"index";}'
rank_math_analytic_object_id:
  - 16
rank_math_internal_links_processed:
  - 1
post_view:
  - 1
is_post_view:
  - 'a:1:{i:0;s:26:"74.125.150.7, 99.82.171.66";}'
categories:
  - Programming

---
In this tutorial, I am going to show you how to install the current stable release version of PHP 8.1 on Ubuntu 20.04.

[PHP][1]&nbsp;is a general-purpose scripting language suitable for web development. Most websites out there are programmed using PHP language because it is:

  * **Fast**&nbsp;– PHP is fast because it runs on its own memory space. Also PHP has Zend engine that parses PHP code and turns it into opcodes which are then interpreted.
  * **Flexible&nbsp;**– PHP is flexible because it allows to almost all databases. MySQL being the de-facto database to connect to because it is open source.
  * Free and open source
  * **PHP is forgiving&nbsp;**– Why do we say PHP is forgiving? It is forgiving meaning its learning it is not so hard therefore suitable to almost all programmers starting out.
  * PHP supports major communication protocols i.e LDAP, IMAP, POP3.

## Prerequisites for Installing PHP 8.1 {#prerequisites-for-installing-php-8-1.wp-block-heading}

  * Before you can use PHP you must have&nbsp;**web server installed**. Apache web server is the most widely used with PHP.
  * **Database**&nbsp;must be there for PHP to run. It works well on all databases such Oracle, MySQL, PostgreSQL.
  * **PHP parser**&nbsp;must be there. What is PHP parser if you can ask? PHP Parser is a library that takes a source code written in PHP, passes it through a lexical analyzer and creates its respective syntax tree.

PHP 8.1 is a major update release version. It contains many new features and optimizations e.g

  * It introduced Enumerations feature. Enumerations or Enum feature allow a developer to define a custom type that is limited to one of the discrete number of possible values. This is helpful while defining domain model.
  * Improvement of RFC readonly property version 2. This prevents modification of the property after initialization.
  * Introduction of RFC fibers call action. This seeks to eliminate synchronous and asychronous function call by allowing functions to become interruptible without affecting the entire fuction.
  * Introduction of a non return type. This indicates that a function either exits, throws an exception or doesn’t terminate.
  * Introduction of first class collable actions.
  * Introduction of&nbsp;[fsync][2]&nbsp;and&nbsp;[fdatasync][3]&nbsp;functions
  * Introduction of&nbsp;[array\_is\_list][4]&nbsp;functions
  * Introduction of explicit octal numeral notation

## Related Articles {#related-articles.wp-block-heading}

  * [How to install PHP 8.1 on CentOS 8/RHEL 8][5]
  * [How to install PHP 8.1 on Ubuntu 21.04][6]
  * [How to Install PHP 8 on Ubuntu 21.04][7]
  * [How to install PHP 8.1 on Ubuntu 21.10][8]

## Installing PHP 8.1 on Ubuntu 20.04 {#installing-php-8-1-on-ubuntu-20-04.wp-block-heading}

### 1. Run system updates  {#1-run-system-updates.wp-block-heading}

The first thing to do in a new system is to update our repositories in order to make them up to date. Run upgrade command also.

<pre class="wp-block-code"><code>$ sudo apt update && apt upgrade -y</code></pre>

### 2. Add Ondrej sury PPA repository {#2-add-ondrej-sury-ppa-repository.wp-block-heading}

To run PHP 8.1 on Ubuntu 20.04, we need to add Ondrej sury PPA into our system. This is the maintainer of PHP repository at the moment. This PPA is not currently checked so installing from it will not guaranteed 100% results.

To add this PPA use the following command on our terminal.

<pre class="wp-block-code"><code>$ sudo add-apt-repository ppa:ondrej/php</code></pre>

You will get the following output

<pre class="wp-block-code"><code>Co-installable PHP versions: PHP 5.6, PHP 7.x and most requested extensions are included. Only Supported Versions of PHP (http://php.net/supported-versions.php) for Supported Ubuntu Releases (https://wiki.ubuntu.com/Releases) are provided. Don't ask for end-of-life PHP versions or Ubuntu release, they won't be provided.

Debian oldstable and stable packages are provided as well: https://deb.sury.org/#debian-dpa

You can get more information about the packages at https://deb.sury.org

IMPORTANT: The &lt;foo>-backports is now required on older Ubuntu releases.

BUGS&FEATURES: This PPA now has a issue tracker:
https:&#47;&#47;deb.sury.org/#bug-reporting

CAVEATS:
1. If you are using php-gearman, you need to add ppa:ondrej/pkg-gearman
2. If you are using apache2, you are advised to add ppa:ondrej/apache2
3. If you are using nginx, you are advised to add ppa:ondrej/nginx-mainline
   or ppa:ondrej/nginx
....</code></pre>

After installation is complete we need to update the repositories again for the changes to take effect.

<pre class="wp-block-code"><code>$ sudo apt update </code></pre>

### 3. Install PHP 8.1 on Ubuntu 20.04 {#3-install-php-8-1-on-ubuntu-20-04.wp-block-heading}

Having added the PPA into our system, we now need to install PHP 8.1. The installation will be easy because we have added what is needed to run the installer effectively.

<pre class="wp-block-code"><code>$ sudo apt install php8.1</code></pre>

The following sample output will be shown

<pre class="wp-block-code"><code># output
The following additional packages will be installed:
  apache2 apache2-bin apache2-data apache2-utils libapache2-mod-php8.1 libapr1 libaprutil1 libaprutil1-dbd-sqlite3
  libaprutil1-ldap libjansson4 liblua5.2-0 libpcre2-8-0 php-common php8.1-cli php8.1-common php8.1-opcache php8.1-readline
  ssl-cert
Suggested packages:
  apache2-doc apache2-suexec-pristine | apache2-suexec-custom www-browser php-pear openssl-blacklist
The following NEW packages will be installed:
  apache2 apache2-bin apache2-data apache2-utils libapache2-mod-php8.1 libapr1 libaprutil1 libaprutil1-dbd-sqlite3
  libaprutil1-ldap libjansson4 liblua5.2-0 php-common php8.1 php8.1-cli php8.1-common php8.1-opcache php8.1-readline ssl-cert
The following packages will be upgraded:
  libpcre2-8-0
1 upgraded, 18 newly installed, 0 to remove and 5 not upgraded.
Need to get 6797 kB of archives.
After this operation, 29.4 MB of additional disk space will be used.
Do you want to continue? &#91;Y/n] y
...</code></pre>

Having successfully installed PHP 8.1, we can confirm our installed version with the following command.

<pre class="wp-block-code"><code>$ php --version
PHP 8.1.2 (cli) (built: Jan 24 2022 10:42:33) (NTS)
Copyright (c) The PHP Group
Zend Engine v4.1.2, Copyright (c) Zend Technologies
    with Zend OPcache v8.1.2, Copyright (c), by Zend Technologies</code></pre>

## Conclusion {#conclusion.wp-block-heading}

We have learned how to install PHP 8.1 on Ubuntu 20.04, now you can proceed to write your programs using this awesome language. Consult [PHP documentation][9] in case you experience any difficulty. I hope you have learned something new today.

 [1]: https://www.php.net/
 [2]: https://www.php.net/manual/en/function.fsync.php
 [3]: https://www.php.net/manual/en/function.fdatasync.php
 [4]: https://www.php.net/manual/en/function.array-is-list.php
 [5]: https://nextgentips.com/2021/12/04/how-to-install-php-8-1-on-centos-8-rhel-8/
 [6]: https://nextgentips.com/2021/12/03/how-to-install-php-8-1-on-ubuntu-21-04/
 [7]: https://nextgentips.com/2021/10/19/how-to-install-php-8-on-ubuntu-21-04/
 [8]: https://nextgentips.com/2021/12/26/how-to-install-php-8-1-on-ubuntu-21-10/
 [9]: https://www.php.net/docs.php