---
title: How to install QTCreator 6 on Ubuntu 20.04
author: Kipkoech Sang
type: post
date: -001-11-30T00:00:00+00:00
draft: true
url: /?p=781
rank_math_news_sitemap_robots:
  - index
rank_math_robots:
  - 'a:1:{i:0;s:5:"index";}'
categories:
  - Uncategorized

---
In this tutorial, you will learn how to install QTCreator 6 on Ubuntu 20.04.

QTCreator is a cross-platform C++, javascript, and QML integrated development environment which simplifies GUI applications. It is part of the SDK for Qt GUI application development framework and uses the Qt API.

## Why QTCreator? {.wp-block-heading}

  * QTCreator has code completion, syntax highligthing, refactoring and has built in documentation.
  * It has integrated UI design, making it easy for use for non programmers.
  * It integrates easily with most version control such as git, Mercurial, subversion and Perforce.
  * QtCreator generates projects in template form becoming easy to create projects easily.
  * IT has ability to compile code into machine code and accelerate start-up time and performance.

## Prerequisites  {.wp-block-heading}

  * Have Ubuntu 20.04 workstation
  * Have basic understanding for Linux terminal
  * Have user with sudo privileges.

## Table of Contents {.wp-block-heading}

  1. Run system updates
  2. Install required Libraries
  3. Install QTCreator
  4. Conclusion

## 1. Run system updates {.wp-block-heading}

Running system updates is the first thing to do for any new system. This will help make repositories up to date. Run the following command from your terminal which helps update the system.

<pre class="wp-block-code"><code>$ Sudo apt update </code></pre>

After the updates are complete, you can proceed to install build-essential packages.

## 2. Install QTCreator Required Build Essential {.wp-block-heading}

Build essential are tools necessary required to compile software programs. These commands install many programs e.g GCC, g++, make, etc.

You can install build-essential with the following command;

<pre class="wp-block-code"><code>$ sudo apt install build-essential</code></pre>

You will get the following output.

<pre class="wp-block-code"><code>Reading package lists... Done
Building dependency tree       
Reading state information... Done
The following additional packages will be installed:
  binutils binutils-common binutils-x86-64-linux-gnu cpp cpp-9 dpkg-dev fakeroot g++ g++-9 gcc gcc-9 gcc-9-base
  libalgorithm-diff-perl libalgorithm-diff-xs-perl libalgorithm-merge-perl libasan5 libatomic1 libbinutils
  libc-dev-bin libc6-dev libcc1-0 libcrypt-dev libctf-nobfd0 libctf0 libdpkg-perl libfakeroot
  libfile-fcntllock-perl libgcc-9-dev libgomp1 libisl22 libitm1 liblsan0 libmpc3 libquadmath0 libstdc++-9-dev
  libtsan0 libubsan1 linux-libc-dev make manpages-dev
Suggested packages:
  binutils-doc cpp-doc gcc-9-locales debian-keyring g++-multilib g++-9-multilib gcc-9-doc gcc-multilib autoconf
  automake libtool flex bison gdb gcc-doc gcc-9-multilib glibc-doc bzr libstdc++-9-doc make-doc
The following NEW packages will be installed:
  binutils binutils-common binutils-x86-64-linux-gnu build-essential cpp cpp-9 dpkg-dev fakeroot g++ g++-9 gcc
  gcc-9 gcc-9-base libalgorithm-diff-perl libalgorithm-diff-xs-perl libalgorithm-merge-perl libasan5 libatomic1
  libbinutils libc-dev-bin libc6-dev libcc1-0 libcrypt-dev libctf-nobfd0 libctf0 libdpkg-perl libfakeroot
  libfile-fcntllock-perl libgcc-9-dev libgomp1 libisl22 libitm1 liblsan0 libmpc3 libquadmath0 libstdc++-9-dev
  libtsan0 libubsan1 linux-libc-dev make manpages-dev
0 upgraded, 41 newly installed, 0 to remove and 0 not upgraded.
Need to get 43.0 MB of archives.
After this operation, 189 MB of additional disk space will be used.
Do you want to continue? &#91;Y/n] </code></pre>

Press Y to allow installation to continue.

When necessary packages have been installed, now it&#8217;s time to install qtcreator.

## 3. Install QTCreator. {.wp-block-heading}

To install QTCreator run the following command;

<pre class="wp-block-code"><code>$ sudo apt install qtcreator</code></pre>

You can also install QT documentation to assist you while creating projects.

<pre class="wp-block-code"><code>$ sudo apt install qt5-doc qt5-doc-html qtbase5-doc-html qtbase5-examples</code></pre>

This process will take a few minutes, wait for the installation to complete before launching the QTCreator.