---
title: How to upgrade Ubuntu 20.04 Linux Kernel from 5.4 to 5.15
author: Kipkoech Sang
type: post
date: 2021-11-21T07:17:59+00:00
url: /2021/11/21/how-to-upgrade-ubuntu-20-04-linux-kernel-from-5-4-to-5-15/
rank_math_news_sitemap_robots:
  - index
rank_math_robots:
  - 'a:1:{i:0;s:5:"index";}'
rank_math_analytic_object_id:
  - 92
rank_math_internal_links_processed:
  - 1
categories:
  - Linux

---
In this tutorial, I will show you how to upgrade your Linux kernel to the latest version.

But first, what is Linux Kernel? Linux Kernel is a free and open-source, monolithic, modular, multitasking Unix-like operating system. It is the main component of a Linux operating system and is the core interface between the computer’s hardware and its processes. It makes communication possible between computer hardware and processes running on it and it manages resources effectively.

## Related Articles  {.wp-block-heading}

  * [How to upgrade Linux Kernel on Rocky Linux/AlmaLinux/CentOS 8][1]
  * [How to upgrade Linux Kernel 5.13 to 5.15 on Ubuntu 21.10][2]

## Prerequisites  {.wp-block-heading}

  * Have Ubuntu 20.04 server
  * Have commandline knowledge
  * Have access to internet.

## Table of Contents {.wp-block-heading}

  1. Run updates for your system
  2. Check the current version of Linux kernel you are running 
  3. Download Linux kernel headers from Ubuntu Mainline
  4. Download Linux Kernel image 
  5. Download modules required to build the kernel
  6. Install new kernel
  7. Reboot the system
  8. Conclusion

## 1. Run system update {.wp-block-heading}

The first thing we need to do in any system is to run updates in order to make system repositories up to date. If there is any upgrade needed you can run the upgrade command too. 

<pre class="wp-block-code"><code>$ sudo apt update
$ sudo apt upgarde -y</code></pre>

When that is complete, move to the next step.

## 2. Check the current Kernel version {.wp-block-heading}

You need to check the current kernel you are running on your system with the following command. 

<pre class="wp-block-code"><code>$ uname -r
5.4.0-88-generic</code></pre>

My Ubuntu 20.04 is running Kernel version 5.4.0-88-generic, so we need to upgrade this to the latest version. Head over to Ubuntu mainline to download the necessary files.

## 3. Get Linux kernel headers  {.wp-block-heading}

We need to download the Linux kernel headers from the Ubuntu mainline. Head there and check for the latest release. If you do not want to download the tarball you can use the following command to download.

<pre class="wp-block-code"><code>$ wget -c https://kernel.ubuntu.com/~kernel-ppa/mainline/v5.15-rc7/amd64/linux-headers-5.15.0-051500rc7_5.15.0-051500rc7.202110251930_all.deb</code></pre>

You will get the following sample output

<pre class="wp-block-code"><code>linux-headers-5.15.0-051500r 100%&#91;===========================================>]  11.62M  16.2MB/s    in 0.7s    

2021-11-21 06:39:15 (16.2 MB/s) - ‘linux-headers-5.15.0-051500rc7_5.15.0-051500rc7.202110251930_all.deb’ saved &#91;12189104/12189104]</code></pre>

Also, download generic headers with the following command 

<pre class="wp-block-code"><code>$ wget -c https://kernel.ubuntu.com/~kernel-ppa/mainline/v5.15-rc7/amd64/linux-headers-5.15.0-051500rc7-generic_5.15.0-051500rc7.202110251930_amd64.deb</code></pre>

## 4. Download Linux kernel image  {.wp-block-heading}

Next, we will need the Linux kernel image. Download with the following command;

<pre class="wp-block-code"><code>$ wget -c https://kernel.ubuntu.com/~kernel-ppa/mainline/v5.15-rc7/amd64/linux-image-unsigned-5.15.0-051500rc7-generic_5.15.0-051500rc7.202110251930_amd64.deb</code></pre>

<pre class="wp-block-code"><code>Sample output
Saving to: ‘linux-image-unsigned-5.15.0-051500rc7-generic_5.15.0-051500rc7.202110251930_amd64.deb’

linux-image-unsigned-5.15.0- 100%&#91;===========================================>]   9.77M  14.1MB/s    in 0.7s    

2021-11-21 06:51:50 (14.1 MB/s) - ‘linux-image-unsigned-5.15.0-051500rc7-generic_5.15.0-051500rc7.202110251930_amd64.deb’ saved &#91;10241908/10241908]
</code></pre>

## 5. Download modules necessary to build Linux kernel {.wp-block-heading}

What we need now are the modules that will aid in building the Linux kernel. Get the modules by downloading them from the following;

<pre class="wp-block-code"><code>$ wget -c https://kernel.ubuntu.com/~kernel-ppa/mainline/v5.15-rc7/amd64/linux-modules-5.15.0-051500rc7-generic_5.15.0-051500rc7.202110251930_amd64.deb</code></pre>

<pre class="wp-block-code"><code>sample output
Saving to: ‘linux-modules-5.15.0-051500rc7-generic_5.15.0-051500rc7.202110251930_amd64.deb’

linux-modules-5.15.0-051500r 100%&#91;===========================================>]  72.18M  32.9MB/s    in 2.2s    

2021-11-21 07:00:18 (32.9 MB/s) - ‘linux-modules-5.15.0-051500rc7-generic_5.15.0-051500rc7.202110251930_amd64.deb’ saved &#91;75682652/75682652]</code></pre>

After the download is complete it is now time to run the installation

Let&#8217;s ls to see if everything has been downloaded into the system.

<pre class="wp-block-code"><code>$ ls
linux-headers-5.15.0-051500rc7-generic_5.15.0-051500rc7.202110251930_amd64.deb
linux-headers-5.15.0-051500rc7_5.15.0-051500rc7.202110251930_all.deb
linux-image-unsigned-5.15.0-051500rc7-generic_5.15.0-051500rc7.202110251930_amd64.deb
linux-modules-5.15.0-051500rc7-generic_5.15.0-051500rc7.202110251930_amd64.deb
snap</code></pre>

So everything is there so we are good to run the installation.

## 6. Install Linux kernel {.wp-block-heading}

To run the installation, you can use the following command;

<pre class="wp-block-code"><code>$ sudo dpkg -i *.deb</code></pre>

<pre class="wp-block-code"><code>sample output..
Generating grub configuration file ...
Found linux image: /boot/vmlinuz-5.15.0-051500rc7-generic
Found initrd image: /boot/initrd.img-5.15.0-051500rc7-generic
Found linux image: /boot/vmlinuz-5.4.0-90-generic
Found initrd image: /boot/initrd.img-5.4.0-90-generic
Found linux image: /boot/vmlinuz-5.4.0-88-generic
Found initrd image: /boot/initrd.img-5.4.0-88-generic
done</code></pre>

Give it time for the installation to complete. Now we need to reboot the system for the changes to take effect.

<pre class="wp-block-code"><code>$ sudo shutdown -r now </code></pre>

Check your version again to confirm if changes took effect

<pre class="wp-block-code"><code>$ uname -r
5.15.0-051500rc7-generic</code></pre>

## 7. Conclusion {.wp-block-heading}

As you can see we have successfully upgraded to the latest stable Kernel release. In case of any difficulty, you can check this [documentation][3].

 [1]: https://nextgentips.com/2021/11/14/how-to-upgrade-linux-kernel-on-rocky-linux-almalinux-centos-8/
 [2]: https://nextgentips.com/2021/11/01/how-to-upgrade-linux-kernel-5-13-to-5-15-on-ubuntu-21-10/
 [3]: https://gist.github.com/wongsyrone/aca57ed19fa0f7afef172dbfea3c9cd2