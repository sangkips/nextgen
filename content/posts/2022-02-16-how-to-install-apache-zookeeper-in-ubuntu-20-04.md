---
title: How to install Apache ZooKeeper in Ubuntu 20.04
author: Kipkoech Sang
type: post
date: 2022-02-16T17:43:30+00:00
url: /2022/02/16/how-to-install-apache-zookeeper-in-ubuntu-20-04/
rank_math_primary_category:
  - 6
rank_math_description:
  - Apache ZooKeeper is a centralized service for maintaining configuration information, naming, providing synchronization, and synchronization.
rank_math_focus_keyword:
  - Apache zookeeper
rank_math_news_sitemap_robots:
  - index
rank_math_robots:
  - 'a:1:{i:0;s:5:"index";}'
rank_math_analytic_object_id:
  - 5
rank_math_internal_links_processed:
  - 1
categories:
  - Linux

---
**Apache ZooKeeper** is a centralized service for maintaining configuration information, naming, providing synchronization, and providing group services. It enables highly reliable distributed coordination. 

**In this tutorial, we are going to learn how to install Zookeeper in Ubuntu 20.04, see the alternatives to ZooKeeper.**

## ZooKeeper Alternatives  {#zookeeper-alternatives.wp-block-heading}

There are some other products you can use if ZooKeeper isn&#8217;t your favorite flavor. These are:

  * Docker
  * Hashicorp Consul
  * Eureka
  * GRPC
  * etcd
  * Traefik

## Errors to avoid if you configure ZooKeeper correctly. {#errors-to-avoid-if-you-configure-zookeeper-correctly.wp-block-heading}

If you want to avoid errors from occurring, you need to configure ZooKeeper properly. Here are the errors you can easily avoid:

  * Inconsistent list of servers. Zookeeper clients servers must match Zookeeper servers that each ZooKeeper server has. 
  * Inconsistent placement of transaction logs. ZooKeeper sync transactions to media before it returns a response. A dedicated transaction log device is key to consistent good performance.
  * Incorrect java heap size. To avoid this, set java heap size correctly. 
  * Publicly accessible deployment. Deploy a zooKeeper behind a firewall 

## Prerequisites {#prerequisites.wp-block-heading}

  * Have Java release 1.8 and above JDK 12 
  * Use 3 recommended ZooKeeper servers for ensemble 
  * Atleast 2GB RAM

## Installing Apache ZooKeeper on Ubuntu 20.04 {#installing-apache-zookeeper-on-ubuntu-20-04.wp-block-heading}

### 1. Updating system repositories  {#1-updating-system-repositories.wp-block-heading}

We need to make our system repositories up to date in order to avoid running into different errors later during installation. 

<pre class="wp-block-code"><code>$ sudo apt update && apt upgrade -y</code></pre>

### 2. Install Java runtime {#2-install-java-runtime.wp-block-heading}

Apache ZooKeeper requires java to run because it is written in java language. So to install java run the following command in your terminal. First, you can check the version if you already installed java.

<pre class="wp-block-code"><code>$ java -version
openjdk version "1.8.0_312"
OpenJDK Runtime Environment (build 1.8.0_312-8u312-b07-0ubuntu1~20.04-b07)
OpenJDK 64-Bit Server VM (build 25.312-b07, mixed mode)</code></pre>

For my case I already have java installed but for a fresh installation of java in your system, run the following command to make the installed. Check this article on <a href="http://How to install Oracle Java SE 17 on Ubuntu 20.04" target="_blank" rel="noreferrer noopener">how to install java on Ubuntu 20.04</a>.

<pre class="wp-block-code"><code>$ sudo apt install openjdk-8-jdk openjdk-8-jre</code></pre>

### 3. Create user for Apache ZooKeeper  {#3-create-user-for-apache-zookeeper.wp-block-heading}

We need to create a user that will run all the ZooKeeper services.

<pre class="wp-block-code"><code>$ sudo useradd nextgen -m</code></pre>

**-m** will create a dedicated home directory for the zookeeper user.

Next, we need to set bash as the default shell for the zookeeper user.

<pre class="wp-block-code"><code>$ sudo usermod --shell /bin/bash nextgen</code></pre>

set a password for this user

<pre class="wp-block-code"><code>$ sudo passwd nextgen</code></pre>

Next, add a user to sudoers group

<pre class="wp-block-code"><code>$ sudo usermod -aG sudo nextgen </code></pre>

You can check the created user if it exists.

<pre class="wp-block-code"><code>$ sudo getent group sudo 
sudo:x:27:nextgen</code></pre>

### 4. Create a data directory for Apache ZooKeeper  {#4-create-a-data-directory-for-apache-zookeeper.wp-block-heading}

ZooKeeper requires a place to read and write data to, this is the reason we need to create one for ZooKeeper. 

<pre class="wp-block-code"><code>$ sudo mkdir -p /data/zookeeper</code></pre>

To ensure that the created user can write into the directory, we need to give them permission.

<pre class="wp-block-code"><code>$ sudo chown nextgen:nextgen /data/zookeeper</code></pre>

### 5. Install Apache Zookeeper on Ubuntu 20.04 {#5-install-apache-zookeeper-on-ubuntu-20-04.wp-block-heading}

Now we can download Zookeeper from the <a href="https://www.apache.org/dyn/closer.lua/zookeeper/zookeeper-3.7.0/apache-zookeeper-3.7.0.tar.gz" target="_blank" rel="noreferrer noopener">Apache download page</a>. But first, move to /opt/ directory where you will extract the apache ZooKeeper to.

<pre class="wp-block-code"><code>$ cd /opt</code></pre>

Use wget to get the download.

<pre class="wp-block-code"><code>$ wget https://dlcdn.apache.org/zookeeper/zookeeper-3.7.0/apache-zookeeper-3.7.0.tar.gz</code></pre>

Next, we need to extract the binaries from the above download.

<pre class="wp-block-code"><code>$ sudo tar -xvf apache-zookeeper-3.7.0.tar.gz</code></pre>

Next, we need to give permission to extracted binaries. 

<pre class="wp-block-code"><code>$ sudo chown nextgen:nextgen -R apache-zookeeper-3.7.0</code></pre>

The next thing is to create a symbolic link so that ZooKeeper can become accessible from all directories in your system.

<pre class="wp-block-code"><code>$ sudo ln -s apache-zookeeper-3.7.0 zookeeper</code></pre>

we now need to change the ownership of that link to nextgen:nextgen

<pre class="wp-block-code"><code>$ sudo chown -h nextgen:nextgen zookeeper</code></pre>

**-h** specifies you have changed the ownership of the link.

### 6. Configuring Apache ZooKeeper {#6-configuring-apache-zookeeper.wp-block-heading}

Because we are not in a production environment, we are going to configure ZooKeeper in a standalone environment. When you are in a production environment configure replication mode. To configure ZooKeeper in standalone create a zoo.cfg in the Zookeeper directory.

<pre class="wp-block-code"><code>$ sudo vi /opt/zookeeper/conf/zoo.cfg</code></pre>

We need to add the following lines in the zoo.cfg file

<pre class="wp-block-code"><code>tickTime = 2000
dataDir = /data/zookeeper
clientPort = 2181
initLimit = 5
syncLimit = 2</code></pre>

Let&#8217;s look at the following in detail

  * **tickTime:** Sets the length of a tick in milliseconds.
  * **dataDir:** Specifies the directory used to store snapshots of the in-memory database and the transaction log for updates.
  * **clientPort:** The port used to listen for client connections.
  * **initLimit:** The number of ticks that the initial synchronization phase can take.
  * **syncLimit:** The number of ticks that can pass between sending a request and getting an acknowledgement.

### 7. Start Zookeeper service  {#7-start-zookeeper-service.wp-block-heading}

To start the ZooKeeper service run the following command. Run this command inside /opt/zookeeper directory

<pre class="wp-block-code"><code>$ sudo bin/zkServer.sh start</code></pre>

output

<pre class="wp-block-code"><code>/usr/bin/java
ZooKeeper JMX enabled by default
Using config: /opt/apache-zookeeper-3.7.0/bin/../conf/zoo.cfg
Starting zookeeper ... STARTED</code></pre>

### 8. Connecting to ZooKeeper server {#8-connecting-to-zookeeper-server.wp-block-heading}

To connect to the local ZooKeeper server run the following command 

<pre class="wp-block-code"><code>$ bin/zkCli.sh -server 127.0.0.1:2181</code></pre>

## Conclusion {#conclusion.wp-block-heading}

We have learned how to install Apache ZooKeeper on Ubuntu 20.04. I am happy that you have enjoyed the session.