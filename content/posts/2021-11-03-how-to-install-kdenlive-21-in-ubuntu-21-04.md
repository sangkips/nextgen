---
title: How to install Kdenlive 21 in Ubuntu 21.04
author: Kipkoech Sang
type: post
date: 2021-11-03T16:45:58+00:00
url: /2021/11/03/how-to-install-kdenlive-21-in-ubuntu-21-04/
rank_math_news_sitemap_robots:
  - index
rank_math_robots:
  - 'a:1:{i:0;s:5:"index";}'
rank_math_analytic_object_id:
  - 112
rank_math_internal_links_processed:
  - 1
categories:
  - Linux

---
In this tutorial we are going to install kdenlive on Debian 11. Kdenlive is an acronym for KDE Non-Linear Video Editor.

Kdenlive is open source non-linear video editing suite, which support DV, HDV and many more formats.

## Main Features of Kdenlive 21 {.wp-block-heading}

  * It has guides and marker for organizing timelines
  * It supports copy and paste support for clips, effects and transitions
  * Has got real time changes
  * It supports FireWire and Video4Linux capture
  * It supports screen grabbing
  * Files can be exported to any FFMPEG supported format

Kdenlive works well on GNU/Linux, BSD and windows based systems. Through the MLT frameworks, kdenlive integrates many plugin effects for video and sound integration effects for video and sound processing or creation. Video effects are provided by Freior while audio effects are provided by LADSPA.

## Installing Kdenlive  {.wp-block-heading}

### Ways to install Kdenlive {.wp-block-heading}

  * Building from the source
  * Using snapcraft

The best way to do install is to add PPA repository.

## Add PPA repository to the System. {.wp-block-heading}

First we need to add PPA repository to our system. Use the following command to accomplish that:

<pre class="wp-block-code"><code>$ sudo add-apt-repository ppa:kdenlive/kdenlive-master</code></pre>

Sample Output will be as follows:

<pre class="wp-block-code"><code># sudo add-apt-repository ppa:kdenlive/kdenlive-master
PPA publishes dbgsym, you may need to include 'main/debug' component
Repository: 'deb http://ppa.launchpad.net/kdenlive/kdenlive-master/ubuntu/ hirsute main'
Description:
This is a daily built development version of Kdenlive. 
This PPA contains untested software that might damage your data. 
Use at your own risk, but always backup your data if you install Kdenlive from this PPA
More info: https://launchpad.net/~kdenlive/+archive/ubuntu/kdenlive-master
Adding repository.
Press &#91;ENTER] to continue or Ctrl-c to cancel.
Adding deb entry to /etc/apt/sources.list.d/kdenlive-ubuntu-kdenlive-master-hirsute.list
Adding disabled deb-src entry to /etc/apt/sources.list.d/kdenlive-ubuntu-kdenlive-master-hirsute.list
Adding key to /etc/apt/trusted.gpg.d/kdenlive-ubuntu-kdenlive-master.gpg with fingerprint A59E5EBFCCC61564D6D4365B2763B0EE7709FE97
.....</code></pre>

We need to update the system for changes to take effect.

<pre class="wp-block-code"><code>$ sudo apt update
$ sudo apt upgrade -y</code></pre>

When the update is complete, go ahead and build required dependencies.

## Add required dependecies  {.wp-block-heading}

To add required dependencies use the following code to do so

<pre class="wp-block-code"><code>$ sudo apt install build-essential git cmake extra-cmake-modules libsm-dev</code></pre>

Press y to allow installation to continue 

Sample Output

<pre class="wp-block-code"><code># sudo apt install build-essential git cmake extra-cmake-modules libsm-dev
Reading package lists... Done
Building dependency tree... Done
Reading state information... Done
git is already the newest version (1:2.30.2-1ubuntu1).
git set to manually installed.
The following package was automatically installed and is no longer required:
  net-tools
Use 'sudo apt autoremove' to remove it.
The following additional packages will be installed:
  bzip2 cmake-data cpp cpp-10 dpkg-dev fakeroot fontconfig-config fonts-dejavu-core g++ g++-10 gcc gcc-10
  gcc-10-base libalgorithm-diff-perl libalgorithm-diff-xs-perl libalgorithm-merge-perl libasan6 libatomic1
  libc-dev-bin libc-devtools libc6-dev libcc1-0 libcrypt-dev libdeflate0 libdpkg-perl libfakeroot
  libfile-fcntllock-perl libfontconfig1 libgcc-10-dev libgd3 libgomp1 libice-dev libice6 libisl23 libitm1
  libjbig0 libjpeg-turbo8 libjpeg8 libjsoncpp24 liblsan0 libmpc3 libnsl-dev libquadmath0 librhash0 libsm6
  libstdc++-10-dev libtiff5 libtirpc-dev libtsan0 libubsan1 libwebp6 libxpm4 linux-libc-dev lto-disabled-list
  make manpages-dev rpcsvc-proto x11-common x11proto-core-dev x11proto-dev xorg-sgml-doctools
Suggested packages:
  bzip2-doc cmake-doc ninja-build cpp-doc gcc-10-locales debian-keyring qt5-qmake qtbase5-dev g++-multilib
  g++-10-multilib gcc-10-doc gcc-multilib autoconf automake libtool flex bison gdb gcc-doc gcc-10-multilib
  glibc-doc bzr libgd-tools libice-doc libsm-doc libstdc++-10-doc make-doc
The following NEW packages will be installed:
  build-essential bzip2 cmake cmake-data cpp cpp-10 dpkg-dev extra-cmake-modules fakeroot fontconfig-config
  fonts-dejavu-core g++ g++-10 gcc gcc-10 gcc-10-base libalgorithm-diff-perl libalgorithm-diff-xs-perl
  libalgorithm-merge-perl libasan6 libatomic1 libc-dev-bin libc-devtools libc6-dev libcc1-0 libcrypt-dev
  libdeflate0 libdpkg-perl libfakeroot libfile-fcntllock-perl libfontconfig1 libgcc-10-dev libgd3 libgomp1
  libice-dev libice6 libisl23 libitm1 libjbig0 libjpeg-turbo8 libjpeg8 libjsoncpp24 liblsan0 libmpc3 libnsl-dev
  libquadmath0 librhash0 libsm-dev libsm6 libstdc++-10-dev libtiff5 libtirpc-dev libtsan0 libubsan1 libwebp6
  libxpm4 linux-libc-dev lto-disabled-list make manpages-dev rpcsvc-proto x11-common x11proto-core-dev
  x11proto-dev xorg-sgml-doctools
0 upgraded, 65 newly installed, 0 to remove and 0 not upgraded.
Need to get 65.1 MB of archives.
After this operation, 247 MB of additional disk space will be used.
Do you want to continue? &#91;Y/n] y
....</code></pre>

Now we can install Kdenlive with the following command:

<pre class="wp-block-code"><code>$ sudo apt install kdenlive </code></pre>

After that install is complete you can install melt with the following command: Melt is used for video rendering so it is necessary to have 

<pre class="wp-block-code"><code>$ sudo apt install libmlt++-dev libmlt-dev melt</code></pre>

Lastly you can install localization dependencies, we can use the following command:

<pre class="wp-block-code"><code>sudo apt install ruby subversion gnupg2 gettext</code></pre>

When installation is complete we can now start kdenlive 

## Conclusion {.wp-block-heading}

Congratulations you have install Kdenlive video editor. Go ahead and start editing your videos. If you have any problem with installation don&#8217;t hesitate to consult the [Kdenlive documentation.][1]

 [1]: https://invent.kde.org/multimedia/kdenlive/-/blob/master/dev-docs/build.md