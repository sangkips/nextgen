---
title: How to install and setup SFTP server on Ubuntu 22.04
author: Kipkoech Sang
type: post
date: 2022-07-21T08:56:48+00:00
url: /2022/07/21/how-to-install-and-setup-sftp-server-on-ubuntu-22-04/
categories:
  - Linux

---
SFTP (Secure File Transfer Protocol) is a file transfer protocol that provides secure access to a remote computer. Think of it as a tunnel provider, whenever you want to connect remotely, you use SFTP protocol to ensure your connection is secure from eavesdropping, we use Secure Shell (SSH) for that.

FTP protocol is a client/server protocol that transfers files over the internet. FTP clients are used to sending and retrieving files to and from servers storing files and also responding to the clients&#8217; needs.

To get started with the installation, make sure ssh is installed, then set up sftp user group, configure ssh service, and lastly connect via sftp service.

### 1. Update system repositories.  {.wp-block-heading}

To start off, update your system repositories in order to make them up to date. 

<pre class="wp-block-code"><code>sudo apt update && apt upgrade -y</code></pre>

### 2. Install ssh server {.wp-block-heading}

SFTP depends on ssh protocol for communication, we have to install it if not already installed on your system. 

<pre class="wp-block-code"><code>sudo apt install ssh </code></pre>

The following will be the sample output.

<pre class="wp-block-code"><code>Reading package lists... Done
Building dependency tree... Done
Reading state information... Done
The following NEW packages will be installed:
 &lt;strong> ssh&lt;/strong>
0 upgraded, 1 newly installed, 0 to remove and 0 not upgraded.
Need to get 4834 B of archives.
After this operation, 133 kB of additional disk space will be used.
Get:1 http://mirrors.linode.com/ubuntu jammy/main amd64 ssh all 1:8.9p1-3 &#91;4834 B]
Fetched 4834 B in 0s (199 kB/s)
Selecting previously unselected package ssh.
(Reading database ... 108902 files and directories currently installed.)
Preparing to unpack .../ssh_1%3a8.9p1-3_all.deb ...
Unpacking ssh (1:8.9p1-3) ...
Setting up ssh (1:8.9p1-3) ...
Scanning processes...                                                           
Scanning candidates...                                                          
Scanning linux images...                                                        

Restarting services...
Service restarts being deferred:
 /etc/needrestart/restart.d/dbus.service
 systemctl restart networkd-dispatcher.service
 systemctl restart systemd-logind.service
 systemctl restart unattended-upgrades.service
 systemctl restart user@0.service</code></pre>

If you want to install FTP over OpenSSH, you need to edit the sshd configuration file.

<pre class="wp-block-code"><code>sudo vim /etc/ssh/sshd_config</code></pre>

You will need to add this below the file.

<pre class="wp-block-code"><code>Match group sftp
ChrootDirectory /home
X11Forwarding no
AllowTcpForwarding no
ForceCommand internal-sftp</code></pre>

Restart the ssh server for the changes to take effect.

<pre class="wp-block-code"><code>sudo systemctl restart ssh</code></pre>

### 3. Create SFTP user account. {.wp-block-heading}

Set up STTP user and group which will log in here.

To set the group use the following command.

<pre class="wp-block-code"><code>$ sudo addgroup sftpgroup
Adding group `sftpgroup' (GID 1000) ...
Done.</code></pre>

Then we need to create sftpuser and you need to assign it to sftp group

<pre class="wp-block-code"><code>sudo useradd -m sftpuser -g sftpgroup</code></pre>

Set the password for sftpuser.

<pre class="wp-block-code"><code>$ sudo passwd sftpuser
New password: 
Retype new password: 
passwd: password updated successfully</code></pre>

Lastly, grant the new user access home directory.

<pre class="wp-block-code"><code>sudo chmod 700 /home/sftpuser/</code></pre>

### 4. Connect to sftp  {.wp-block-heading}

To connect to sftp we use the command 

<pre class="wp-block-code"><code>$ sftp sftpuser@localhost</code></pre>

For instance, to login to the sftp I have created, I need to type `<mark style="background-color:#abb8c3" class="has-inline-color">sftp sftpuser@127.0.0.1</mark>`

<pre class="wp-block-code"><code>$ sftp sftpuser@127.0.0.1
The authenticity of host '127.0.0.1 (127.0.0.1)' can't be established.
ED25519 key fingerprint is 
This key is not known by any other names
Are you sure you want to continue connecting (yes/no/&#91;fingerprint])? yes
Warning: Permanently added '127.0.0.1' (ED25519) to the list of known hosts.
sftpuser@127.0.0.1's password: 
Connected to 127.0.0.1.
sftp> 
sftp> </code></pre>

### 5. Useful SFTP commands  {.wp-block-heading}

Whenever you need help with any command using the following command.

<pre class="wp-block-code"><code>sftp> help</code></pre>

If you want to navigate through your SFTP, use `<mark style="background-color:#abb8c3" class="has-inline-color">pwd</mark>`

<pre class="wp-block-code"><code>sftp> pwd
Remote working directory: /home/sftpuser</code></pre>

To download files with sftp command.

<pre class="wp-block-code"><code>sftp> get filename.zip</code></pre>

## Conclusion  {.wp-block-heading}

We have successfully installed sftp on Ubuntu 22.04, happy learning.