---
title: How to install Redmine on Ubuntu 20.04
author: Kipkoech Sang
type: post
date: 2022-01-27T19:27:11+00:00
url: /2022/01/27/how-to-install-redmine-on-ubuntu-20-04/
rank_math_news_sitemap_robots:
  - index
rank_math_robots:
  - 'a:1:{i:0;s:5:"index";}'
rank_math_analytic_object_id:
  - 20
rank_math_internal_links_processed:
  - 1
categories:
  - Linux

---
In this tutorial, we are going to learn how to install Redmine on Ubuntu 20.04.

Redmine is a free and open-source, web-based project management and issue tracking tool. It allows users to manage multiple projects and associated subprojects. It has project wikis and forums, time tracking, and role-based project controls. 

## Features of Redmine {#features-of-redmine.wp-block-heading}

  * It has flexible role-based controls 
  * Has multiple database support.
  * Support project wikis and forums 
  * It tracks project duration 
  * It supports multiple languages
  * It has email and feed notification 
  * Has multiple LDAP authentication support 
  * It has custom fields for issues, time-entries, projects, and users. 

## Prerequisites  {#prerequisites.wp-block-heading}

  * Have an Ubuntu 20.04 server up and running.
  * Have ruby running on your system. 
  * Have Apache2 server 
  * Have mariaDB/MYSQL database up and running. 

## Install Redmine on Ubuntu 20.04 {#install-redmine-on-ubuntu-20-04.wp-block-heading}

### 1. Run system updates  {#1-run-system-updates.wp-block-heading}

Run system updates in order to make the repositories up to date.

<pre class="wp-block-code"><code>$ sudo apt update && apt upgrade -y</code></pre>

### 2. Install Apache 2 webserver {#2-install-apache-2-webserver.wp-block-heading}

Redmine runs on a web server and also you will require a <a href="https://www.pair.com/support/kb/what-is-mod_passenger/" target="_blank" rel="noreferrer noopener" title="mod-passenger">mod-passenger</a> <a href="https://www.pair.com/support/kb/what-is-mod_passenger/" target="_blank" rel="noreferrer noopener" title="passenger "></a>module. To install this run the following command:

<pre class="wp-block-code"><code>$ sudo apt install apache2 libapache2-mod-passenger</code></pre>

The following is the output from the above command;

<pre class="wp-block-code"><code># output
The following additional packages will be installed:
  apache2-bin apache2-data apache2-utils fonts-lato javascript-common libapr1 libaprutil1 libaprutil1-dbd-sqlite3
  libaprutil1-ldap libjansson4 libjs-jquery liblua5.2-0 libruby2.7 passenger rake ruby ruby-minitest ruby-net-telnet
  ruby-power-assert ruby-rack ruby-test-unit ruby-xmlrpc ruby2.7 rubygems-integration ssl-cert unzip zip
Suggested packages:
  apache2-doc apache2-suexec-pristine | apache2-suexec-custom www-browser nodejs passenger-doc python rails ri ruby-dev
  bundler openssl-blacklist
The following NEW packages will be installed:
  apache2 apache2-bin apache2-data apache2-utils fonts-lato javascript-common libapache2-mod-passenger libapr1 libaprutil1
  libaprutil1-dbd-sqlite3 libaprutil1-ldap libjansson4 libjs-jquery liblua5.2-0 libruby2.7 passenger rake ruby ruby-minitest
  ruby-net-telnet ruby-power-assert ruby-rack ruby-test-unit ruby-xmlrpc ruby2.7 rubygems-integration ssl-cert unzip zip
0 upgraded, 29 newly installed, 0 to remove and 0 not upgraded.
Need to get 10.8 MB of archives.
After this operation, 47.5 MB of additional disk space will be used.</code></pre>

Press Y to allow the installation to continue 

We can check the status of the Apache webserver.

<pre class="wp-block-code"><code># &lt;strong>sudo systemctl status apache2&lt;/strong>
● apache2.service - The Apache HTTP Server
     Loaded: loaded (/lib/systemd/system/apache2.service; enabled; vendor preset: enabled)
     Active: active (running) since Thu 2022-01-27 18:26:52 UTC; 12min ago
       Docs: https://httpd.apache.org/docs/2.4/
    Process: 1510 ExecStart=/usr/sbin/apachectl start (code=exited, status=0/SUCCESS)
      Tasks: 74 (limit: 2339)
     Memory: 11.5M
     CGroup: /system.slice/apache2.service
             ├─1551 /usr/sbin/apache2 -k start
             ├─1552 Passenger watchdog
             ├─1555 Passenger core
             ├─1562 Passenger ust-router
             ├─1575 /usr/sbin/apache2 -k start
             └─1576 /usr/sbin/apache2 -k start

Jan 27 18:26:52 ubuntu systemd&#91;1]: Starting The Apache HTTP Server...
Jan 27 18:26:52 ubuntu apachectl&#91;1520]: AH00558: apache2: Could not reliably determine the server's fully qualified domain nam>
Jan 27 18:26:52 ubuntu systemd&#91;1]: Started The Apache HTTP Server.</code></pre>

### 3. Install MariaDB server {#3-install-mariadb-server.wp-block-heading}

Redmine uses Mysql/Mariadb as a database resource. So this is part of the requirement before installing Remine. So let&#8217;s run the installation then you must enable it on your system.

<pre class="wp-block-code"><code>$ sudo apt install mariadb-server</code></pre>

<pre class="wp-block-code"><code># output
.....
&lt;strong>Setting up mariadb-client-10.3&lt;/strong> (1:10.3.32-0ubuntu0.20.04.1) ...
Setting up libdbd-mysql-perl:amd64 (4.050-3) ...
Setting up libhtml-parser-perl (3.72-5) ...
Setting up mariadb-server-10.3 (1:10.3.32-0ubuntu0.20.04.1) ...
Created symlink /etc/systemd/system/mysql.service → /lib/systemd/system/mariadb.service.
Created symlink /etc/systemd/system/mysqld.service → /lib/systemd/system/mariadb.service.
Created symlink /etc/systemd/system/multi-user.target.wants/mariadb.service → /lib/systemd/system/mariadb.service.
Setting up libhttp-message-perl (6.22-1) ...
Setting up libcgi-pm-perl (4.46-1) ...
Setting up libhtml-template-perl (2.97-1) ...
Setting up mariadb-server (1:10.3.32-0ubuntu0.20.04.1) ...
Setting up libcgi-fast-perl (1:2.15-1) ...
Processing triggers for systemd (245.4-4ubuntu3.15) ...
Processing triggers for man-db (2.9.1-1) ...
Processing triggers for libc-bin (2.31-0ubuntu9.2) ...</code></pre>

**Enable MariaDB on your system**

<pre class="wp-block-code"><code>$ sudo systemctl enable mariadb</code></pre>

You can also check the status of Mysql with the status command 

<pre class="wp-block-code"><code>$ sudo systemctl status mariadb</code></pre>

This will be the output.

<pre class="wp-block-code"><code>● mariadb.service - MariaDB 10.3.32 database server
     Loaded: loaded (/lib/systemd/system/mariadb.service; enabled; vendor preset: enabled)
     Active: active (running) since Thu 2022-01-27 17:11:43 UTC; 1min 35s ago
       Docs: man:mysqld(8)
             https:&#47;&#47;mariadb.com/kb/en/library/systemd/
   Main PID: 10648 (mysqld)
     Status: "Taking your SQL requests now..."
      Tasks: 31 (limit: 2339)
     Memory: 66.4M
     CGroup: /system.slice/mariadb.service
             └─10648 /usr/sbin/mysqld</code></pre>

We can verify if MariaDB is working with the following 

<pre class="wp-block-code"><code>$ sudo mysql -u root </code></pre>

Let&#8217;s see the output generated 

<pre class="wp-block-code"><code># output
Welcome to the MariaDB monitor.  Commands end with ; or \g.
Your MariaDB connection id is 49
Server version: 10.3.32-MariaDB-0ubuntu0.20.04.1 Ubuntu 20.04

Copyright (c) 2000, 2018, Oracle, MariaDB Corporation Ab and others.

Type 'help;' or '\h' for help. Type '\c' to clear the current input statement.

MariaDB &#91;(none)]> </code></pre>

We need to make sure that the Mysql/Mariadb server is secure by discouraging remote logins 

<pre class="wp-block-code"><code>$ mysql_secure_installation</code></pre>

Follow the prompts to secure your database, for example, you need to remove anonymous users, disallow root login remotely and remove all test databases.

The output will look like this 

<pre class="wp-block-code"><code># output
NOTE: RUNNING ALL PARTS OF THIS SCRIPT IS RECOMMENDED FOR ALL MariaDB
      SERVERS IN PRODUCTION USE!  PLEASE READ EACH STEP CAREFULLY!

In order to log into MariaDB to secure it, we'll need the current
password for the root user.  If you've just installed MariaDB, and
you haven't set the root password yet, the password will be blank,
so you should just press enter here.

Enter current password for root (enter for none): 
OK, successfully used password, moving on...

Setting the root password ensures that nobody can log into the MariaDB
root user without the proper authorisation.

Set root password? &#91;Y/n] y
New password: 
Re-enter new password: 
Password updated successfully!
Reloading privilege tables..
 ... Success!


By default, a MariaDB installation has an anonymous user, allowing anyone
to log into MariaDB without having to have a user account created for
them.  This is intended only for testing, and to make the installation
go a bit smoother.  You should remove them before moving into a
production environment.

Remove anonymous users? &#91;Y/n] y
 ... Success!

Normally, root should only be allowed to connect from 'localhost'.  This
ensures that someone cannot guess at the root password from the network.

Disallow root login remotely? &#91;Y/n] y
 ... Success!

By default, MariaDB comes with a database named 'test' that anyone can
access.  This is also intended only for testing, and should be removed
before moving into a production environment.

Remove test database and access to it? &#91;Y/n] y
 - Dropping test database...
 ... Success!
 - Removing privileges on test database...
 ... Success!

Reloading the privilege tables will ensure that all changes made so far
will take effect immediately.

Reload privilege tables now? &#91;Y/n] y
 ... Success!

Cleaning up...

All done!  If you've completed all of the above steps, your MariaDB
installation should now be secure.

Thanks for using MariaDB!</code></pre>

### 4. Create a database for Redmine  {#4-create-a-database-for-redmine.wp-block-heading}

It is required to create a database user for Redmine. To do so use the following command;

<pre class="wp-block-code"><code>$ sudo mysql -u root -p</code></pre>

Insert the following into MariaDB console

<pre class="wp-block-code"><code>CREATE DATABASE redmine CHARACTER SET utf8mb4;
GRANT ALL PRIVILEGES ON redmine.* TO 'redmine'@'localhost' IDENTIFIED BY '$passwd';
FLUSH PRIVILEGES;
EXIT</code></pre>

This is the output you will get

<pre class="wp-block-code"><code>Enter password: 
Welcome to the MariaDB monitor.  Commands end with ; or \g.
Your MariaDB connection id is 58
Server version: 10.3.32-MariaDB-0ubuntu0.20.04.1 Ubuntu 20.04

Copyright (c) 2000, 2018, Oracle, MariaDB Corporation Ab and others.

Type 'help;' or '\h' for help. Type '\c' to clear the current input statement.

MariaDB &#91;(none)]> CREATE DATABASE redmine CHARACTER SET utf8mb4;
Query OK, 1 row affected (0.000 sec)

MariaDB &#91;(none)]> GRANT ALL PRIVILEGES ON redmine.* TO 'redmine'@'localhost' IDENTIFIED BY '$passwd';
Query OK, 0 rows affected (0.001 sec)

MariaDB &#91;(none)]> FLUSH PRIVILEGES;
Query OK, 0 rows affected (0.001 sec)

MariaDB &#91;(none)]> EXIT;
Bye</code></pre>

Let&#8217;s check if the Redmine user we have created is working correctly.

<pre class="wp-block-code"><code>$ mysql -u redmine -p
Enter password: 
Welcome to the MariaDB monitor.  Commands end with ; or \g.
Your MariaDB connection id is 36
Server version: 10.3.32-MariaDB-0ubuntu0.20.04.1 Ubuntu 20.04

Copyright (c) 2000, 2018, Oracle, MariaDB Corporation Ab and others.

Type 'help;' or '\h' for help. Type '\c' to clear the current input statement.

MariaDB &#91;(none)]> show databases;
+--------------------+
| Database           |
+--------------------+
| information_schema |
| &lt;strong>redmine &lt;/strong>           |
+--------------------+
2 rows in set (0.001 sec)

MariaDB &#91;(none)]> </code></pre>

### 5. Install and Configure Redmine  {#5-install-and-configure-redmine.wp-block-heading}

Now we are ready to install Redmine after completing all the prerequisites above. To install Redmine run the following command on your terminal.

<pre class="wp-block-code"><code>$ sudo apt install redmine redmine-mysql</code></pre>

When prompted to configure the database press yes to allow installation.

We must ensure that bundler gem is installed 

**Install bundler gem**

<pre class="wp-block-code"><code>$ sudo gem update
$ sudo gem install bundler </code></pre>

### 6. Configuring Redmine and Apache2  {#6-configuring-redmine-and-apache2.wp-block-heading}

We need to edit the Passenger.conf file located at **/etc/apache2/mods-available/passenger.conf.** and **/etc/apache2/sites-available/000-default.conf** and then add the following content to it.

<pre class="wp-block-code"><code>$ sudo vi /etc/apache2/mods-available/passenger.conf </code></pre>

Add this content 

<pre class="wp-block-code"><code>&lt;IfModule mod_passenger.c>
&lt;strong>PassengerDefaultUser www-data&lt;/strong>
PassengerRoot /usr/lib/ruby/vendor_ruby/phusion_passenger/locations.ini
PassengerDefaultRuby /usr/bin/ruby
&lt;/IfModule></code></pre>

We now need to create a symbolic link to Apache2 

<pre class="wp-block-code"><code>$ sudo ln -s /usr/share/redmine/public /var/www/html/redmine</code></pre>

We need to create a virtualhost file for Redmine and configure it to your domain name or server 

<pre class="wp-block-code"><code>$ sudo vi /etc/apache2/sites-available/redmine.conf</code></pre>

Add the following content. Replace projects.example.com with your domain name or server.

<pre class="wp-block-code"><code>ServerAdmin REDMINE_PASSSWORD
  DocumentRoot /var/www/html/redmine
  ServerName SERVER_IP_ADDRESS
RailsBaseURI /redmine
    PassengerResolveSymlinksInDocumentRoot on
  
  ErrorLog ${APACHE_LOG_DIR}/error.log
  CustomLog ${APACHE_LOG_DIR}/access.log combined
</code></pre>

Next, create and set the ownership of the Gemfile.lock file so that the www-data user of apache can access it.

<pre class="wp-block-code"><code>$ sudo touch /usr/share/redmine/Gemfile.lock
$ sudo chown www-data:www-data /usr/share/redmine/Gemfile.lock</code></pre>

Next, restart Apache 

<pre class="wp-block-code"><code>&lt;strong>$ sudo a2ensite redmine.conf&lt;/strong>
Enabling site redmine.
To activate the new configuration, you need to run:
  systemctl reload apache2

&lt;strong>$ systemctl restart apache2&lt;/strong></code></pre>

Check the apache status again to see if it is running.

If the status is up and running then we are good to continue.

Open your browser and type **<IP_address>/redmine** <figure class="wp-block-image size-large">

<img decoding="async" loading="lazy" width="810" height="464" src="https://i0.wp.com/nextgentips.com/wp-content/uploads/2022/01/Screenshot-from-2022-01-27-22-01-31.png?resize=810%2C464&#038;ssl=1" alt="Redmine" class="wp-image-1073" srcset="https://i0.wp.com/nextgentips.com/wp-content/uploads/2022/01/Screenshot-from-2022-01-27-22-01-31.png?resize=1024%2C587&ssl=1 1024w, https://i0.wp.com/nextgentips.com/wp-content/uploads/2022/01/Screenshot-from-2022-01-27-22-01-31.png?resize=300%2C172&ssl=1 300w, https://i0.wp.com/nextgentips.com/wp-content/uploads/2022/01/Screenshot-from-2022-01-27-22-01-31.png?resize=768%2C440&ssl=1 768w, https://i0.wp.com/nextgentips.com/wp-content/uploads/2022/01/Screenshot-from-2022-01-27-22-01-31.png?w=1210&ssl=1 1210w" sizes="(max-width: 810px) 100vw, 810px" data-recalc-dims="1" /> <figcaption>Redmine</figcaption></figure> 

You can log in with **admin** and root as **admin**

### 7. Setup Redmine Email {#7-setup-redmine-email.wp-block-heading}

Whenever you want to integrate email services with Redmine, we can use Postfix or exim4 services but will recommend postfix because of the incompatibilities with exim4 with other mail services. 

<pre class="wp-block-code"><code>$ sudo apt-get install postfix</code></pre>

Sample output.

<pre class="wp-block-code"><code>....
After modifying main.cf, be sure to run 'systemctl reload postfix'.

Created symlink /etc/systemd/system/multi-user.target.wants/postfix.service → /lib/systemd/system/postfix.service.
Processing triggers for ufw (0.36-6ubuntu1) ...
Processing triggers for systemd (245.4-4ubuntu3.15) ...
Processing triggers for man-db (2.9.1-1) ...
Processing triggers for rsyslog (8.2001.0-1ubuntu1.1) ...
Processing triggers for libc-bin (2.31-0ubuntu9.2) ...</code></pre>

After you have installed postfix, you need to tell Redmine about it. To do so create a configuration.yml file on **/etc/redmine/default/configuration.yml**

<pre class="wp-block-code"><code>$ sudo vi /etc/redmine/default/configuration.yml</code></pre>

Insert the following content 

<pre class="wp-block-code"><code>production:
  email_delivery:
    delivery_method: :sendmail</code></pre>

Save and restart the apache for the changes to take effect.

<pre class="wp-block-code"><code>$ sudo systemctl restart apache2</code></pre>

## Conclusion {#conclusion.wp-block-heading}

Congratulations. You have successfully installed Redmine on Ubuntu 20.04. If you experience any problem do let us know or consult <a href="https://www.redmine.org/projects/redmine/wiki/howto_install_redmine_on_ubuntu_step_by_step" target="_blank" rel="noreferrer noopener" title="Redmine documentation">Redmine documentation</a>.