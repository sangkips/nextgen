---
title: How to install Oracle Java SE 17 on Ubuntu 20.04
author: Kipkoech Sang
type: post
date: 2022-01-13T10:29:36+00:00
url: /2022/01/13/how-to-install-oracle-java-se-17-on-ubuntu-20-04/
rank_math_news_sitemap_robots:
  - index
rank_math_robots:
  - 'a:1:{i:0;s:5:"index";}'
rank_math_analytic_object_id:
  - 36
rank_math_internal_links_processed:
  - 1
post_view:
  - 1
is_post_view:
  - 'a:1:{i:0;s:29:"119.82.121.137, 13.248.107.42";}'
categories:
  - Programming

---
In todays guide, we are going to learn how we can install Java SE 17 on Ubuntu 20.04. Java is widely used in programs like Cassandra, Graylog, Wine etc.

Java delivers thousands of performance, stability, and security updates that is the reason why java is widely used and has a larger community base worldwide.

## Java 17 new features  {.wp-block-heading}

Java 17 includes new language enhancement, updates to the libraries, updates to the new Apple computers, removals, and deprecations of legacy features. Updates include the following:

  * Introduction of sealed classes. That is, it restrict which other classes or interfaces may extend or implement them.
  * It introduces restore always-strict floating-point semantics 
  * Introduction of enhanced pseudo random number generator
  * Introduction of new MacOS rendering pipeline using the Apple metal API

It also deprecated some old features such as:

  * Deprecated the java Applet API 
  * It removed Remote Method Invocation activation
  * Removes the experimental Ahead Of Time and Just In Time compiler
  * Deprecated the Security Manager which has been the conerstone of securing client side java code.

## Prerequisites  {.wp-block-heading}

  * Have a root user account while running the installation
  * Have Ubuntu 20.04 LTS x64 architecture 

Java 17 can be installed in two different ways, first, you can install using its PPA method, secondly, you can install by downloading it first then extracting content to /opt/ directory.

## Installation Java SE 17 on Ubuntu 20.04/21.04 {.wp-block-heading}

First we can run system update and upgrade 

<pre class="wp-block-code"><code>$ sudo apt update
$ sudo apt upgrade -y</code></pre>

Check if java is preinstalled on your system with the following command.

<pre class="wp-block-code"><code>$ java --version</code></pre>

If you get the following output, you are good to continue with installation. If you get an alternative output you will have to remove java first 

<pre class="wp-block-code"><code>Output

Command 'java' not found, but can be installed with:

apt install openjdk-11-jre-headless  # version 11.0.13+8-0ubuntu1~20.04, or
apt install default-jre              # version 2:1.11-72
apt install openjdk-13-jre-headless  # version 13.0.7+5-0ubuntu1~20.04
apt install openjdk-16-jre-headless  # version 16.0.1+9-1~20.04
apt install openjdk-17-jre-headless  # version 17.0.1+12-1~20.04
apt install openjdk-8-jre-headless   # version 8u312-b07-0ubuntu1~20.04</code></pre>

## Installing Java 17 on Ubuntu 20.04 {.wp-block-heading}

So now I am going to install java 17 from the Ubuntu 20.04 repository. 

We are going to install both jdk and jre with the following command.

<pre class="wp-block-code"><code>$ sudo apt install openjdk-17-jdk openjdk-17-jre</code></pre>

This installation comes with a lot of dependencies 

<pre class="wp-block-code"><code>Output
The following additional packages will be installed:
  adwaita-icon-theme at-spi2-core ca-certificates-java fontconfig fontconfig-config fonts-dejavu-core
  fonts-dejavu-extra gtk-update-icon-cache hicolor-icon-theme humanity-icon-theme java-common
  libatk-bridge2.0-0 libatk-wrapper-java libatk-wrapper-java-jni libatk1.0-0 libatk1.0-data libatspi2.0-0
  libavahi-client3 libavahi-common-data libavahi-common3 libcairo-gobject2 libcairo2 libcups2 libdatrie1
  libdrm-amdgpu1 libdrm-intel1 libdrm-nouveau2 libdrm-radeon1 libfontconfig1 libfontenc1 libgail-common
  libgail18 libgdk-pixbuf2.0-0 libgdk-pixbuf2.0-bin libgdk-pixbuf2.0-common libgif7 libgl1 libgl1-mesa-dri
  libglapi-mesa libglvnd0 libglx-mesa0 libglx0 libgraphite2-3 libgtk2.0-0 libgtk2.0-bin libgtk2.0-common
  libharfbuzz0b libice-dev libice6 libjbig0 libjpeg-turbo8 libjpeg8 liblcms2-2 libllvm12 libpango-1.0-0
  libpangocairo-1.0-0 libpangoft2-1.0-0 libpciaccess0 libpcsclite1 libpixman-1-0 libpthread-stubs0-dev
  librsvg2-2 librsvg2-common libsensors-config libsensors5 libsm-dev libsm6 libthai-data libthai0 libtiff5
  libvulkan1 libwayland-client0 libwebp6 libx11-dev libx11-xcb1 libxau-dev libxaw7 libxcb-dri2-0 libxcb-dri3-0
  libxcb-glx0 libxcb-present0 libxcb-randr0 libxcb-render0 libxcb-shape0 libxcb-shm0 libxcb-sync1
  libxcb-xfixes0 libxcb1-dev libxcomposite1 libxcursor1 libxdamage1 libxdmcp-dev libxfixes3 libxft2 libxi6
  libxinerama1 libxkbfile1 libxmu6 libxpm4 libxrandr2 libxrender1 libxshmfence1 libxt-dev libxt6 libxtst6
  libxv1 libxxf86dga1 libxxf86vm1 mesa-vulkan-drivers openjdk-17-jdk-headless openjdk-17-jre-headless
  ubuntu-mono x11-common x11-utils x11proto-core-dev x11proto-dev xorg-sgml-doctools xtrans-dev
Suggested packages:
  default-jre cups-common gvfs libice-doc liblcms2-utils pcscd librsvg2-bin lm-sensors libsm-doc libx11-doc
  libxcb-doc libxt-doc openjdk-17-demo openjdk-17-source visualvm libnss-mdns fonts-ipafont-gothic
  fonts-ipafont-mincho fonts-wqy-microhei | fonts-wqy-zenhei fonts-indic mesa-utils
The following NEW packages will be installed:
  adwaita-icon-theme at-spi2-core ca-certificates-java fontconfig fontconfig-config fonts-dejavu-core
  fonts-dejavu-extra gtk-update-icon-cache hicolor-icon-theme humanity-icon-theme java-common
  libatk-bridge2.0-0 libatk-wrapper-java libatk-wrapper-java-jni libatk1.0-0 libatk1.0-data libatspi2.0-0
  libavahi-client3 libavahi-common-data libavahi-common3 libcairo-gobject2 libcairo2 libcups2 libdatrie1
  libdrm-amdgpu1 libdrm-intel1 libdrm-nouveau2 libdrm-radeon1 libfontconfig1 libfontenc1 libgail-common
  libgail18 libgdk-pixbuf2.0-0 libgdk-pixbuf2.0-bin libgdk-pixbuf2.0-common libgif7 libgl1 libgl1-mesa-dri
  libglapi-mesa libglvnd0 libglx-mesa0 libglx0 libgraphite2-3 libgtk2.0-0 libgtk2.0-bin libgtk2.0-common
  libharfbuzz0b libice-dev libice6 libjbig0 libjpeg-turbo8 libjpeg8 liblcms2-2 libllvm12 libpango-1.0-0
  libpangocairo-1.0-0 libpangoft2-1.0-0 libpciaccess0 libpcsclite1 libpixman-1-0 libpthread-stubs0-dev
  librsvg2-2 librsvg2-common libsensors-config libsensors5 libsm-dev libsm6 libthai-data libthai0 libtiff5
  libvulkan1 libwayland-client0 libwebp6 libx11-dev libx11-xcb1 libxau-dev libxaw7 libxcb-dri2-0 libxcb-dri3-0
  libxcb-glx0 libxcb-present0 libxcb-randr0 libxcb-render0 libxcb-shape0 libxcb-shm0 libxcb-sync1
  libxcb-xfixes0 libxcb1-dev libxcomposite1 libxcursor1 libxdamage1 libxdmcp-dev libxfixes3 libxft2 libxi6
  libxinerama1 libxkbfile1 libxmu6 libxpm4 libxrandr2 libxrender1 libxshmfence1 libxt-dev libxt6 libxtst6
  libxv1 libxxf86dga1 libxxf86vm1 mesa-vulkan-drivers openjdk-17-jdk openjdk-17-jdk-headless openjdk-17-jre
  openjdk-17-jre-headless ubuntu-mono x11-common x11-utils x11proto-core-dev x11proto-dev xorg-sgml-doctools
  xtrans-dev
0 upgraded, 120 newly installed, 0 to remove and 0 not upgraded.
Need to get 339 MB of archives.
After this operation, 985 MB of additional disk space will be used.
Do you want to continue? &#91;Y/n] y</code></pre>

Check the java version inorder to confirm the installation.

<pre class="wp-block-code"><code>$ java --version
openjdk 17.0.1 2021-10-19
OpenJDK Runtime Environment (build 17.0.1+12-Ubuntu-120.04)
OpenJDK 64-Bit Server VM (build 17.0.1+12-Ubuntu-120.04, mixed mode, sharing)</code></pre>

## Testing Java 17 on Ubuntu 20.04 {.wp-block-heading}

Lets test if java is working correctly. To do that we are going to run our first hello world program

<pre class="wp-block-code"><code>$ sudo nano hello.java</code></pre>

Add the following to your hello.java

<pre class="wp-block-code"><code>public class hello {
    public static void main(String&#91;] args) {
        System.out.println("Hello, World! from Nextgentips"); 
    }
}</code></pre>

You need to compile the java programs in order to show the output. To run the compilation run the following code

<pre class="wp-block-code"><code>$ javac hello.java</code></pre>

If you don&#8217;t run into any errors then it&#8217;s time to run your first java program.

<pre class="wp-block-code"><code>$ java hello
Hello, World! from Nextgentips</code></pre>

## Setting Java 17 environment {.wp-block-heading}

To ensure that you cant run into problems while running java programs, we need to set JAVA_HOME environment where java will be executed. If you don&#8217;t set this environment you will encounter errors in the future. To avoid that lets set the environment.

Lets check where we have our java 17 with the following command.

<pre class="wp-block-code"><code>$ sudo update-alternatives --config java</code></pre>

You will get the path like this 

<pre class="wp-block-code"><code>Output
There is only one alternative in link group java (providing /usr/bin/java): &lt;strong>/usr/lib/jvm/java-17-openjdk-amd64/bin/java&lt;/strong>
Nothing to configure.</code></pre>

So lets set this in our **/etc/environment** 

<pre class="wp-block-code"><code>$ sudo nano /etc/environment </code></pre>

Add the following 

<pre class="wp-block-code"><code>export JAVA_HOME=/usr/lib/jvm/java-17-openjdk-amd64/bin/java</code></pre>

Save and exit 

To apply the changes use the following command 

<pre class="wp-block-code"><code>$ source /etc/environment</code></pre>

To check if the environment path is correct use the following command 

<pre class="wp-block-code"><code>$ echo $JAVA_HOME
/usr/lib/jvm/java-17-openjdk-amd64/bin/java</code></pre>

If you are getting empty results know that you have not apply the changes as required, go back and do **source /etc/environment** 

From here now you will not be required to set the environment every time you install another java version.

## Conclusion {.wp-block-heading}

You have successfully installed Java 17 on Ubuntu 20.04. In case of difficulty consult the [documentation][1]

 [1]: https://docs.oracle.com/cd/E19182-01/821-0917/inst_jdk_javahome_t/index.html "documentation"