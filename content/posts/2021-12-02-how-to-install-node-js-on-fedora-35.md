---
title: How to install Node.js on Fedora 35
author: Kipkoech Sang
type: post
date: 2021-12-02T15:58:11+00:00
url: /2021/12/02/how-to-install-node-js-on-fedora-35/
entry_views:
  - 2
view_ip:
  - 'a:2:{i:0;s:15:"114.119.132.163";i:1;s:14:"159.196.12.150";}'
rank_math_news_sitemap_robots:
  - index
rank_math_robots:
  - 'a:1:{i:0;s:5:"index";}'
rank_math_analytic_object_id:
  - 78
rank_math_internal_links_processed:
  - 1
categories:
  - Programming

---
In today&#8217;s guide, I am going to take you through the installation of node.js on Fedora 35.

Node.js is an open-source cross-platform, backend javascript runtime environment that runs on the V8 engine and executes javascript code outside of a web browser. 

A Node.js app runs in a single process, without creating a new thread for every request. It provides a set of asynchronous I/O primitives in its standard library that prevent javascript code from blocking and generally, libraries from node.js are written using non-blocking paradigms, making blocking behavior the exceptions rather than the norm.

When Node.js performs an I/O operation, like reading from the network, accessing a database or the filesystem, instead of blocking the thread and wasting CPU cycles waiting, Node.js will resume the operations when the response comes back. This allows Node.js to handle thousands of concurrent connections with a single server without introducing the burden of managing thread concurrency, which could be a significant source of bugs. 

**[nvm][1]** is used to run node.js packages. It allows you to easily switch node.js versions and install new versions to try and easily roll back if something went wrong. 

When Node.js is installed, you will have access to the node executable program in the command line. 

## Prerequisites  {.wp-block-heading}

  1. Have a user with sudo privileges 
  2. Have a Fedora 35 workstation up and running.
  3. Have basic terminal knowledge.

In this tutorial, I will show you different ways you can install Node.js. We will cover installing Node.js using Package manager, snap, and Fast Node Manager (FNM).

## Installing Node.js Via Package manager {.wp-block-heading}

Node.js is available as a module called nodejs in Fedora. Fedora 35 now ships with Node.js 16.x as the default Node.js JavaScript server-side engine.

## 1. Run system updates. {.wp-block-heading}

Let&#8217;s first update our repositories to make them up to date. Type the following command on your terminal.

<pre class="wp-block-code"><code>$ sudo dnf update -y</code></pre>

## 2. List Available Streams of Node.js {.wp-block-heading}

When updates are complete you can now list available streams of nodes on Fedora 35. Input the following command in your terminal. 

<pre class="wp-block-code"><code>$ sudo dnf module list nodejs</code></pre>

You will get the following output.

<pre class="wp-block-code"><code>Last metadata expiration check: 0:04:17 ago on Thu 02 Dec 2021 01:58:23 PM UTC.
Fedora Modular 35 - x86_64
Name               Stream             Profiles                                      Summary                      
nodejs             12                 default &#91;d], development, minimal             Javascript runtime           
nodejs             14                 default, development, minimal                 Javascript runtime           
nodejs             15                 default, development, minimal                 Javascript runtime           
nodejs             16                 default, development, minimal                 Javascript runtime           

Fedora Modular 35 - x86_64 - Updates
Name               Stream             Profiles                                      Summary                      
nodejs             12                 default &#91;d], development, minimal             Javascript runtime           
nodejs             14                 default, development, minimal                 Javascript runtime           
nodejs             15                 default, development, minimal                 Javascript runtime           
nodejs             16                 default, development, minimal                 Javascript runtime           

Hint: &#91;d]efault, &#91;e]nabled, &#91;x]disabled, &#91;i]nstalled</code></pre>

## 3. Install Node.js 16 on Fedora 35 {.wp-block-heading}

As of this writing, Node.js 16 is the default version. To install Node.js 16 run the following command on the terminal.

<pre class="wp-block-code"><code>$ sudo dnf module install nodejs:16/development</code></pre>

Use development if you want to install the development version. 

The sample output will look like this.

<pre class="wp-block-code"><code>DigitalOcean Droplet Agent                                                        62 kB/s | 3.3 kB     00:00    
Dependencies resolved.
=================================================================================================================
 Package                   Arch      Version                                            Repository          Size
=================================================================================================================
Installing group/module packages:
 nodejs                    x86_64    1:16.11.1-1.module_f35+13202+236ca42b              updates-modular    198 k
 nodejs-devel              x86_64    1:16.11.1-1.module_f35+13202+236ca42b              updates-modular    140 k
 npm                       x86_64    1:8.0.0-1.16.11.1.1.module_f35+13202+236ca42b      updates-modular    1.7 M
Installing dependencies:
 brotli                    x86_64    1.0.9-6.fc35                                       fedora             314 k
 brotli-devel              x86_64    1.0.9-6.fc35                                       fedora              32 k
 dwz                       x86_64    0.14-2.fc35                                        fedora             127 k
 efi-srpm-macros           noarch    5-4.fc35                                           fedora              22 k
 fonts-srpm-macros         noarch    1:2.0.5-6.fc35                                     fedora              27 k
 fpc-srpm-macros           noarch    1.3-4.fc35                                         fedora             7.6 k
 ghc-srpm-macros           noarch    1.5.0-5.fc35                                       fedora             7.8 k
 gnat-srpm-macros          noarch    4-14.fc35                                          fedora             8.2 k
 go-srpm-macros            noarch    3.0.11-2.fc35                                      fedora              25 k
 kernel-srpm-macros        noarch    1.0-6.fc35                                         fedora             8.0 k
 libpkgconf                x86_64    1.8.0-1.fc35                                       fedora              36 k
 libuv                     x86_64    1:1.42.0-2.module_f35+12941+0548739f               fedora-modular     148 k
 libuv-devel               x86_64    1:1.42.0-2.module_f35+12941+0548739f               fedora-modular      28 k
 lua-srpm-macros           noarch    1-5.fc35                                           fedora             8.4 k
 nim-srpm-macros           noarch    3-5.fc35                                           fedora             8.4 k
 nodejs-libs               x86_64    1:16.11.1-1.module_f35+13202+236ca42b              updates-modular     14 M
 nodejs-packaging          noarch    2021.06-3.module_f35+12598+6f18cc3b                fedora-modular      20 k
 ocaml-srpm-macros         noarch    6-5.fc35                                           fedora             7.8 k
 openblas-srpm-macros      noarch    2-10.fc35                                          fedora             7.4 k
 openssl                   x86_64    1:1.1.1l-2.fc35                                    fedora             659 k
 openssl-devel             x86_64    1:1.1.1l-2.fc35                                    fedora             2.2 M
 perl-srpm-macros          noarch    1-42.fc35                                          fedora             8.3 k
 pkgconf                   x86_64    1.8.0-1.fc35                                       fedora              41 k
 pkgconf-m4                noarch    1.8.0-1.fc35                                       fedora              14 k
 pkgconf-pkg-config        x86_64    1.8.0-1.fc35                                       fedora              10 k
 python-srpm-macros        noarch    3.10-10.fc35                                       updates             25 k
 qt5-srpm-macros           noarch    5.15.2-3.fc35                                      fedora             8.2 k
 redhat-rpm-config         noarch    199-1.fc35                                         updates             65 k
 rpmautospec-rpm-macros    noarch    0.2.5-1.fc35                                       fedora             9.9 k
 rust-srpm-macros          noarch    20-1.fc35                                          updates            9.4 k
 unzip                     x86_64    6.0-53.fc35                                        fedora             179 k
 zip                       x86_64    3.0-31.fc35                                        fedora             257 k
 zlib-devel                x86_64    1.2.11-30.fc35                                     fedora              44 k
Installing weak dependencies:
 nodejs-docs               noarch    1:16.11.1-1.module_f35+13202+236ca42b              updates-modular    6.5 M
 nodejs-full-i18n          x86_64    1:16.11.1-1.module_f35+13202+236ca42b              updates-modular    7.8 M
Installing module profiles:
 nodejs/development                                                                                             
Enabling module streams:
 nodejs                              16                                                                         

Transaction Summary
=================================================================================================================
Install  38 Packages

Total download size: 35 M
Installed size: 165 M
Is this ok &#91;y/N]: </code></pre>

Press Y to allow installation to continue.

To use node.js type node on your terminal.

<pre class="wp-block-code"><code>$ node
Welcome to Node.js v16.11.1.
Type ".help" for more information.
&gt; </code></pre>

To check the version you have installed, use **node -v**.

<pre class="wp-block-code"><code>$ node -v
v16.11.1</code></pre>

## Install Node.js via Snap. {.wp-block-heading}

Snaps are applications packaged with all their dependencies to run on all Linux platforms from a single build only. 

First, you need to enable snapd on Fedora 35.

## 1. Enable Snapd on Fedora 35 {.wp-block-heading}

Install snap with the following command on your terminal.

<pre class="wp-block-code"><code>$ sudo dnf install snapd</code></pre>

The output will be as follows:

<pre class="wp-block-code"><code>Last metadata expiration check: 0:02:34 ago on Thu 02 Dec 2021 02:37:20 PM UTC.
Dependencies resolved.
=================================================================================================================
 Package                              Architecture   Version                               Repository       Size
=================================================================================================================
Installing:
 &lt;strong>snapd                                x86_64         2.53.2-1.fc35                         updates          14 M&lt;/strong>
Installing dependencies:
 bash-completion                      noarch         1:2.11-3.fc35                         fedora          291 k
 kernel-debug-core                    x86_64         5.15.5-200.fc35                       updates          37 M
 kernel-debug-modules                 x86_64         5.15.5-200.fc35                       updates          33 M
 libpkgconf                           x86_64         1.8.0-1.fc35                          fedora           36 k
 pkgconf                              x86_64         1.8.0-1.fc35                          fedora           41 k
 pkgconf-m4                           noarch         1.8.0-1.fc35                          fedora           14 k
 pkgconf-pkg-config                   x86_64         1.8.0-1.fc35                          fedora           10 k
 policycoreutils-python-utils         noarch         3.3-1.fc35                            updates          72 k
 snap-confine                         x86_64         2.53.2-1.fc35                         updates         2.3 M
 snapd-selinux                        noarch         2.53.2-1.fc35                         updates         232 k
 squashfs-tools                       x86_64         4.5-3.20210913gite048580.fc35         fedora          204 k

Transaction Summary
=================================================================================================================
Install  12 Packages

Total download size: 87 M
Installed size: 176 M
Is this ok &#91;y/N]: y</code></pre>

Press Y to allow installation to continue and then you can restart your system for the changes to take effect.

## 2. Enable Snap classic support  {.wp-block-heading}

To enable snap classic support, create a symbolic link between /var/lib/snapd/snap and /snap.

<pre class="wp-block-code"><code>$ sudo ln -s /var/lib/snapd/snap /snap</code></pre>

## 3. Install Node.js on Fedora 35 {.wp-block-heading}

To install node using snap, use the following command on your terminal.

<pre class="wp-block-code"><code>$ sudo snap install node --classic</code></pre>

If you getting an error message &#8220;

error: system does not fully support snapd: cannot mount squashfs image using &#8220;squashfs&#8221;: mount: /tmp/sanity-mountpoint-309425902: unknown filesystem type &#8216;squashfs'&#8221;

This means the dnf didn&#8217;t pull kernel modules correctly. What you need to do is to install kernel-modules.

<pre class="wp-block-code"><code>$ sudo dnf install kernel-modules -y</code></pre>

You will get the following output.

<pre class="wp-block-code"><code>DigitalOcean Droplet Agent                                                        44 kB/s | 3.3 kB     00:00    
Dependencies resolved.
=================================================================================================================
 Package                       Architecture          Version                        Repository              Size
=================================================================================================================
Installing:
 kernel-modules                x86_64                5.15.5-200.fc35                updates                 33 M

Transaction Summary
=================================================================================================================
Install  1 Package

Total download size: 33 M
Installed size: 32 M
Downloading Packages:
kernel-modules-5.15.5-200.fc35.x86_64.rpm                                         68 MB/s |  33 MB     00:00    
-----------------------------------------------------------------------------------------------------------------
Total                                                                            1.8 MB/s |  33 MB     00:17     
Running transaction check
Transaction check succeeded.
Running transaction test
Transaction test succeeded.
Running transaction
  Preparing        :                                                                                         1/1 
  Installing       : kernel-modules-5.15.5-200.fc35.x86_64                                                   1/1 
  Running scriptlet: kernel-modules-5.15.5-200.fc35.x86_64                                                   1/1 
  Verifying        : kernel-modules-5.15.5-200.fc35.x86_64                                                   1/1 

Installed:
  kernel-modules-5.15.5-200.fc35.x86_64                                                                          

Complete!</code></pre>

restart your system again to load kernel modules and again run the snap installer this time round it will go through successfully.

<pre class="wp-block-code"><code>Output
2021-12-02T15:50:09Z INFO Waiting for automatic snapd restart...
node (16/stable) 16.13.1 from OpenJS Foundation (iojs✓) installed</code></pre>

For snap it&#8217;s such simple to do the installation with and also the packages are updated automatically when new changes are pushed to productions.

Check the version of node installed;

<pre class="wp-block-code"><code>$ node -v
v16.13.1</code></pre>

Run node on the terminal with the following command;

<pre class="wp-block-code"><code>$ node 
Welcome to Node.js v16.13.1.
Type ".help" for more information.
&gt; </code></pre>

## Conclusion {.wp-block-heading}

The above process had taught us how to install Node.js using snap and Node Package manager. Congratulations and happy coding.

 [1]: https://github.com/nvm-sh/nvm