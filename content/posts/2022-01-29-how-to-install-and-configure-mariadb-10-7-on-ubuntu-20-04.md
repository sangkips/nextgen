---
title: How to install and configure MariaDB 10.7 on Ubuntu 20.04
author: Kipkoech Sang
type: post
date: 2022-01-29T10:06:36+00:00
url: /2022/01/29/how-to-install-and-configure-mariadb-10-7-on-ubuntu-20-04/
rank_math_news_sitemap_robots:
  - index
rank_math_robots:
  - 'a:1:{i:0;s:5:"index";}'
rank_math_analytic_object_id:
  - 19
rank_math_internal_links_processed:
  - 1
categories:
  - Database

---
In this guide we are going to install MariaDB 10.7 development version, but first what is MariaDB?

MariaDB Server is one of the most popular open-source relational databases. It’s made by the original developers of MySQL and guaranteed to stay open source. It is part of most cloud offerings and the default in most Linux distributions.

It is built upon the values of performance, stability, and openness, and MariaDB Foundation ensures contributions will be accepted on technical merit. Recent **new functionality** includes **advanced clustering with&nbsp;[Galera Cluster 4][1]**, **compatibility features with Oracle Database** and **Temporal Data Tables**, allowing one to query the data as it stood at any point in the past.

You need to have Ubuntu 20.04 server with non-root administrative privileges and a firewall configured with UFW. Check out this for&nbsp;[initial server setup guide for Ubuntu 20.04.][2]

## Related articles  {#related-articles.wp-block-heading}

  * [How to Install and Configure MariaDB 10.7 on Fedora 35][3]

## Installing MariaDB 10.7 {#installing-mariadb-10-7.wp-block-heading}

### 1. Run system updates  {#1-run-system-updates.wp-block-heading}

Begin by updating your apt repository with the following command on your terminal

<pre class="wp-block-code"><code>$ sudo apt update && apt upgrade -y</code></pre>

### 2. Install MariaDB dependencies  {#2-install-mariadb-dependencies.wp-block-heading}

After updates are complete you can then install the MariaDB server by adding the apt repository to our system and also add GPG key.

Install dependencies 

<pre class="wp-block-code"><code>$ sudo apt install software-properties-common dirmngr apt-transport-https</code></pre>

Sample output.

<pre class="wp-block-code"><code>Reading package lists... Done
Building dependency tree       
Reading state information... Done
dirmngr is already the newest version (2.2.19-3ubuntu2.1).
dirmngr set to manually installed.
software-properties-common is already the newest version (0.99.9.8).
software-properties-common set to manually installed.
apt-transport-https is already the newest version (2.0.6).
0 upgraded, 0 newly installed, 0 to remove and 0 not upgraded.</code></pre>

**Add GPG keys**

<pre class="wp-block-code"><code>$ sudo apt-key adv --fetch-keys 'https://mariadb.org/mariadb_release_signing_key.asc'</code></pre>

Sample output will look like this;

<pre class="wp-block-code"><code>$ Executing: /tmp/apt-key-gpghome.9NqByLiyl1/gpg.1.sh --fetch-keys https://mariadb.org/mariadb_release_signing_key.asc
gpg: requesting key from 'https://mariadb.org/mariadb_release_signing_key.asc'
gpg: key F1656F24C74CD1D8: "MariaDB Signing Key &lt;signing-key@mariadb.org&gt;" not changed
gpg: Total number processed: 1
gpg:              unchanged: 1</code></pre>

Lastly, add the apt repository to our system.

<pre class="wp-block-code"><code>$ sudo add-apt-repository 'deb &#91;arch=amd64,arm64,ppc64el,s390x] https://mariadb.mirror.liquidtelecom.com/repo/10.7/ubuntu focal main'</code></pre>

**NB: Change mirror location to reflect where you are downloading from**

Run system updates again

<pre class="wp-block-code"><code>$ sudo apt update</code></pre>

We can check to see if the repository has been added.

<pre class="wp-block-code"><code>$ apt policy mariadb-server</code></pre>

Output

<pre class="wp-block-code"><code>mariadb-server:
  Installed: (none)
  Candidate: 1:10.7.1+maria~focal
  Version table:
     1:10.7.1+maria~focal 500
        500 https://mariadb.mirror.liquidtelecom.com/repo/10.7/ubuntu focal/main amd64 Packages
        500 https://mariadb.mirror.liquidtelecom.com/repo/10.7/ubuntu focal/main ppc64el Packages
        500 https://mariadb.mirror.liquidtelecom.com/repo/10.7/ubuntu focal/main arm64 Packages
        500 https://mariadb.mirror.liquidtelecom.com/repo/10.7/ubuntu focal/main s390x Packages
     1:10.3.32-0ubuntu0.20.04.1 500
        500 http://mirrors.digitalocean.com/ubuntu focal-updates/universe amd64 Packages
        500 http://security.ubuntu.com/ubuntu focal-security/universe amd64 Packages
     1:10.3.22-1ubuntu1 500
        500 http://mirrors.digitalocean.com/ubuntu focal/universe amd64 Packages</code></pre>

### 3. Install Mariadb 10.7 on Ubuntu 20.04 {#3-install-mariadb-10-7-on-ubuntu-20-04.wp-block-heading}

Install MariaDB with the following command 

<pre class="wp-block-code"><code>$ sudo apt install mariadb-server mariadb-client</code></pre>

The output will look like this

<pre class="wp-block-code"><code>The following additional packages will be installed:
  galera-4 libcgi-fast-perl libcgi-pm-perl libconfig-inifiles-perl libdbd-mariadb-perl libdbi-perl libencode-locale-perl
  libfcgi-perl libhtml-parser-perl libhtml-tagset-perl libhtml-template-perl libhttp-date-perl libhttp-message-perl
  libio-html-perl liblwp-mediatypes-perl libmariadb3 libmysqlclient21 libpmem1 libterm-readkey-perl libtimedate-perl
  liburi-perl mariadb-client-10.7 mariadb-client-core-10.7 mariadb-common mariadb-server-10.7 mariadb-server-core-10.7
  mysql-common socat
Suggested packages:
  libclone-perl libmldbm-perl libnet-daemon-perl libsql-statement-perl libdata-dump-perl libipc-sharedcache-perl libwww-perl
  mailx mariadb-test
The following NEW packages will be installed:
  galera-4 libcgi-fast-perl libcgi-pm-perl libconfig-inifiles-perl libdbd-mariadb-perl libdbi-perl libencode-locale-perl
  libfcgi-perl libhtml-parser-perl libhtml-tagset-perl libhtml-template-perl libhttp-date-perl libhttp-message-perl
  libio-html-perl liblwp-mediatypes-perl libmariadb3 libmysqlclient21 libpmem1 libterm-readkey-perl libtimedate-perl
  liburi-perl mariadb-client mariadb-client-10.7 mariadb-client-core-10.7 mariadb-common mariadb-server mariadb-server-10.7
  mariadb-server-core-10.7 mysql-common socat
0 upgraded, 30 newly installed, 0 to remove and 0 not upgraded.
Need to get 29.4 MB of archives.
After this operation, 230 MB of additional disk space will be used.
Do you want to continue? &#91;Y/n] </code></pre>

### 3. Initial MariaDB configuration {#3-initial-mariadb-configuration.wp-block-heading}

The installation process will not ask you to set a password and this leaves MariaDB insecure. We will use a script that the MariaDB-server package provides strict access to the server and remove unused accounts. Read more about the security script&nbsp;[here][4].

To ensure that our database is secure we need to run the **mysql\_secure\_installation** command to remove all remote root login, remove anonymous users, etc.

<pre class="wp-block-code"><code>$ sudo mysql_secure_installation</code></pre>

The following sample output will be displayed. 

<pre class="wp-block-code"><code>NOTE: RUNNING ALL PARTS OF THIS SCRIPT IS RECOMMENDED FOR ALL MariaDB
      SERVERS IN PRODUCTION USE!  PLEASE READ EACH STEP CAREFULLY!

In order to log into MariaDB to secure it, we'll need the current
password for the root user. If you've just installed MariaDB, and
haven't set the root password yet, you should just press enter here.

Enter current password for root (enter for none): 
OK, successfully used password, moving on...

Setting the root password or using the unix_socket ensures that nobody
can log into the MariaDB root user without the proper authorisation.

You already have your root account protected, so you can safely answer 'n'.

Switch to unix_socket authentication &#91;Y/n] n
 ... skipping.

You already have your root account protected, so you can safely answer 'n'.

Change the root password? &#91;Y/n] n
 ... skipping.

By default, a MariaDB installation has an anonymous user, allowing anyone
to log into MariaDB without having to have a user account created for
them.  This is intended only for testing, and to make the installation
go a bit smoother.  You should remove them before moving into a
production environment.

Remove anonymous users? &#91;Y/n] y
 ... Success!

Normally, root should only be allowed to connect from 'localhost'.  This
ensures that someone cannot guess at the root password from the network.

Disallow root login remotely? &#91;Y/n] y
 ... Success!

By default, MariaDB comes with a database named 'test' that anyone can
access.  This is also intended only for testing, and should be removed
before moving into a production environment.

Remove test database and access to it? &#91;Y/n] y
 - Dropping test database...
 ... Success!
 - Removing privileges on test database...
 ... Success!

Reloading the privilege tables will ensure that all changes made so far
will take effect immediately.

Reload privilege tables now? &#91;Y/n] y
 ... Success!

Cleaning up...

All done!  If you've completed all of the above steps, your MariaDB
installation should now be secure.

Thanks for using MariaDB!</code></pre>

Next is to **enable MariaDB database**

<pre class="wp-block-code"><code>$ sudo systemctl enable mariadb</code></pre>

**Start MariaDB database** 

<pre class="wp-block-code"><code>$ sudo systemctl start mariadb</code></pre>

Check the **status of the MariaDB database**

<pre class="wp-block-code"><code>$ sudo systemctl status mariadb</code></pre>

<pre class="wp-block-code"><code>● mariadb.service - MariaDB 10.7.1 database server
     Loaded: loaded (/lib/systemd/system/mariadb.service; enabled; vendor preset: enabled)
    Drop-In: /etc/systemd/system/mariadb.service.d
             └─migrated-from-my.cnf-settings.conf
     Active: active (running) since Sat 2022-01-29 09:46:56 UTC; 3min 34s ago
       Docs: man:mariadbd(8)
             https:&#47;&#47;mariadb.com/kb/en/library/systemd/
   Main PID: 3884 (mariadbd)
     Status: "Taking your SQL requests now..."
      Tasks: 9 (limit: 1131)
     Memory: 74.1M
     CGroup: /system.slice/mariadb.service
             └─3884 /usr/sbin/mariadbd

Jan 29 09:47:00 ubuntu /etc/mysql/debian-start&#91;3903]: sys
Jan 29 09:47:00 ubuntu /etc/mysql/debian-start&#91;3903]: Phase 6/7: Checking and upgrading tables
Jan 29 09:47:00 ubuntu /etc/mysql/debian-start&#91;3903]: Processing databases
Jan 29 09:47:00 ubuntu /etc/mysql/debian-start&#91;3903]: information_schema
Jan 29 09:47:00 ubuntu /etc/mysql/debian-start&#91;3903]: performance_schema
Jan 29 09:47:00 ubuntu /etc/mysql/debian-start&#91;3903]: sys
Jan 29 09:47:00 ubuntu /etc/mysql/debian-start&#91;3903]: sys.sys_config                                     OK
Jan 29 09:47:00 ubuntu /etc/mysql/debian-start&#91;3903]: Phase 7/7: Running 'FLUSH PRIVILEGES'
Jan 29 09:47:00 ubuntu /etc/mysql/debian-start&#91;3903]: OK
Jan 29 09:47:00 ubuntu /etc/mysql/debian-start&#91;4286]: Triggering myisam-recover for all MyISAM tables and aria-recover for all&gt;
lines 1-24/24 (END)</code></pre>

## Test MariaDB 10.7  {#test-mariadb-10-7.wp-block-heading}

To test MariaDB we can create a database. Login to MariaDB console with the following command.

<pre class="wp-block-code"><code>$ mysql -u root -p</code></pre>

<pre class="wp-block-code"><code>Welcome to the MariaDB monitor.  Commands end with ; or \g.
Your MariaDB connection id is 51
Server version: 10.7.1-MariaDB-1:10.7.1+maria~focal mariadb.org binary distribution

Copyright (c) 2000, 2018, Oracle, MariaDB Corporation Ab and others.

Type 'help;' or '\h' for help. Type '\c' to clear the current input statement.

MariaDB &#91;(none)]&gt; show databases;
+--------------------+
| Database           |
+--------------------+
| information_schema |
| mysql              |
| performance_schema |
| sys                |
+--------------------+
4 rows in set (0.000 sec)

MariaDB &#91;(none)]&gt; </code></pre>

## Conclusion {#conclusion.wp-block-heading}

We have learned how to install MariaDB on Ubuntu 21.04 server and secured it using the&nbsp;`mysql_secure_installation`&nbsp;script. You can now practice running SQL queries to learn more. I hope you enjoyed it and you have learned something new.

 [1]: https://galeracluster.com/library/whats-new.html
 [2]: https://nextgentips.com/2021/10/10/initial-server-setup-for-ubuntu-20-04/
 [3]: https://nextgentips.com/2021/12/25/how-to-install-and-configure-mariadb-10-7-on-fedora-35/
 [4]: https://mariadb.com/kb/en/mysql_secure_installation/