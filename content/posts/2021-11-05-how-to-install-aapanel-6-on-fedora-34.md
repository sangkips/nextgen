---
title: How to install aapanel 6 on Fedora 34/35
author: Kipkoech Sang
type: post
date: 2021-11-05T13:34:06+00:00
url: /2021/11/05/how-to-install-aapanel-6-on-fedora-34/
rank_math_news_sitemap_robots:
  - index
rank_math_robots:
  - 'a:1:{i:0;s:5:"index";}'
rank_math_analytic_object_id:
  - 110
rank_math_internal_links_processed:
  - 1
categories:
  - Linux

---
In this tutorial guide, we will learn how to install aapanel in Fedora 34/35.

aapanel is a free open source hosting control panel, it manages the servers through the graphical user interface. It is an international version of BAOTA. aapanel provides one-click functions such as one-click LAMP install, one-click deployment of SSL, remote backup, etc. It is a server software system that offers ideal support for Linux systems.

The reason why over 2.1 M users have installed aapanel is that:

  * Integration with file managers is easy, that is why it support uploading, downloading, decompression and file editing capabilities.
  * It provides SSH open and close services, SSH port changes, firewall port release operation
  * Its ability to create and manage websites, FTP and manage databases gives an edge with other providers.
  * It has extremely fast and convenient one-key configuration management of SSL, server, and backup.

## Requirements  {#requirements.wp-block-heading}

  * Clean operating system which has not installed Apache/Nginx/PHP/MySQL.
  * Have a ram of atleast 512MB
  * Good to have basic Linux commands to run the tasks 

## Installing aapanel on fedora 35 {#installing-aapanel-on-fedora-35.wp-block-heading}

The first thing to do before installation is to ensure that the system is up to date. Updating the system ensures that the packages receive recent updates. We can do the updates by issuing the following command on the terminal.

<pre class="wp-block-code"><code>$ sudo dnf update -y</code></pre>

When the updates are complete, we can now start the installation of aapanel. We need to install wget to enable us to download the application. Type the following into the terminal.

<pre class="wp-block-code"><code># sudo dnf install wget -y
DigitalOcean Droplet Agent                                                        73 kB/s | 3.3 kB     00:00    
Dependencies resolved.
=================================================================================================================
 Package                Architecture             Version                         Repository                 Size
=================================================================================================================
Installing:
 wget                   x86_64                   1.21.2-2.fc34                   updates                   805 k

Transaction Summary
=================================================================================================================
Install  1 Package

Total download size: 805 k
Installed size: 3.2 M
Downloading Packages:
wget-1.21.2-2.fc34.x86_64.rpm                                                    3.1 MB/s | 805 kB     00:00    
-----------------------------------------------------------------------------------------------------------------
Total                                                                            2.3 MB/s | 805 kB     00:00     
Running transaction check
Transaction check succeeded.
Running transaction test
Transaction test succeeded.
Running transaction
  Preparing        :                                                                                         1/1 
  Installing       : wget-1.21.2-2.fc34.x86_64                                                               1/1 
  Running scriptlet: wget-1.21.2-2.fc34.x86_64                                                               1/1 
  Verifying        : wget-1.21.2-2.fc34.x86_64                                                               1/1 

Installed:
  wget-1.21.2-2.fc34.x86_64                                                                                      

Complete!</code></pre>

Now that we have installed wget, we can then proceed to install the script necessary to run the aapanel. Use the following code to do so.

<pre class="wp-block-code"><code>$ wget -O install.sh http://www.aapanel.com/script/install_6.0_en.sh</code></pre>

Sample output

<pre class="wp-block-code"><code>...
--2021-11-05 12:35:16--  http://www.aapanel.com/script/install_6.0_en.sh
Resolving www.aapanel.com (www.aapanel.com)... 107.151.154.153
Connecting to www.aapanel.com (www.aapanel.com)|107.151.154.153|:80... connected.
HTTP request sent, awaiting response... 200 OK
Length: 26597 (26K) &#91;application/octet-stream]
Saving to: ‘install.sh’

install.sh                   100%&#91;===========================================&gt;]  25.97K  --.-KB/s    in 0.06s   

2021-11-05 12:35:16 (424 KB/s) - ‘install.sh’ saved &#91;26597/26597]</code></pre>

Proceed to install the script using this command:

<pre class="wp-block-code"><code>$ bash install.sh aapanel</code></pre>

Press yes to allow the installation to continue.

Sample output

<pre class="wp-block-code"><code>.....
Installed:
  firewalld-0.9.4-1.fc34.noarch                            firewalld-filesystem-0.9.4-1.fc34.noarch              
  gobject-introspection-1.68.0-4.fc34.x86_64               ipset-7.11-1.fc34.x86_64                              
  ipset-libs-7.11-1.fc34.x86_64                            iptables-nft-1.8.7-8.fc34.x86_64                      
  libnftnl-1.1.9-2.fc34.x86_64                             nftables-1:0.9.8-3.fc34.x86_64                        
  python3-decorator-4.4.2-4.fc34.noarch                    python3-firewall-0.9.4-1.fc34.noarch                  
  python3-gobject-base-3.40.1-1.fc34.x86_64                python3-nftables-1:0.9.8-3.fc34.x86_64                
  python3-slip-0.6.4-22.fc34.noarch                        python3-slip-dbus-0.6.4-22.fc34.noarch                

Complete!
success
==================================================================
Congratulations! Installed successfully!
==================================================================
aaPanel Internet Address: http://157.230.239.171:8888/5702483e
aaPanel Internal Address: http://157.230.239.171:8888/5702483e
username: vc3i8kf5
password: 0eac7e83
Warning:
If you cannot access the panel, 
release the following port (8888|888|80|443|20|21) in the security group
==================================================================
Time consumed: 7 Minute!</code></pre>

You are now good to use aapanel. Launch the application and start using it. To launch the application, go to your preferred browser and type in the internet address given on your search bar, and use the login credentials provided.

Login and proceed to install one-click LNMP/LAMP. Give it time to install before proceeding with configurations.

Inside the aapanel dashboard<figure class="wp-block-image size-large">

<img decoding="async" loading="lazy" width="810" height="365" src="https://i0.wp.com/nextgentips.com/wp-content/uploads/2021/11/Screenshot-from-2021-11-26-13-46-44-1.png?resize=810%2C365&#038;ssl=1" alt="aapanel dashboard" class="wp-image-747" srcset="https://i0.wp.com/nextgentips.com/wp-content/uploads/2021/11/Screenshot-from-2021-11-26-13-46-44-1.png?resize=1024%2C462&ssl=1 1024w, https://i0.wp.com/nextgentips.com/wp-content/uploads/2021/11/Screenshot-from-2021-11-26-13-46-44-1.png?resize=300%2C135&ssl=1 300w, https://i0.wp.com/nextgentips.com/wp-content/uploads/2021/11/Screenshot-from-2021-11-26-13-46-44-1.png?resize=768%2C347&ssl=1 768w, https://i0.wp.com/nextgentips.com/wp-content/uploads/2021/11/Screenshot-from-2021-11-26-13-46-44-1.png?w=1179&ssl=1 1179w" sizes="(max-width: 810px) 100vw, 810px" data-recalc-dims="1" /> <figcaption>aapanel dashboard</figcaption></figure> 

In order to check whether the configurations are correct use the following command to do so:

<pre class="wp-block-code"><code>$ sudo dnf check-update</code></pre>

Whenever you want to start the aapanel service do the following:

<pre class="wp-block-code"><code>$ sudo service bt start</code></pre>

You can also stop the service with the following command:

<pre class="wp-block-code"><code>$ sudo service bt stop</code></pre>

Similarly, you can restart the service with the following command:

<pre class="wp-block-code"><code>$ sudo service bt restart</code></pre>

## Conclusion {#conclusion.wp-block-heading}

You have successfully installed aapanel, you can proceed to experiment with it now. Consult the [aapanel documentation][1] in case you have an issue.

In case you want to install Apache, MySQL inside aapanel dashboard check the following article

  * [How to Install Apache, MYSQL, PHP on Ubuntu 20.04][2]

 [1]: https://www.aapanel.com/index.html
 [2]: https://nextgentips.com/2021/10/11/how-to-install-apache-mysql-php-on-ubuntu-20-04/