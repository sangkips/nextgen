---
title: How to Install and Enable SSH server on Fedora 35
author: Kipkoech Sang
type: post
date: 2022-04-04T09:32:17+00:00
url: /2022/04/04/how-to-install-and-enable-ssh-server-on-fedora-35/
rank_math_seo_score:
  - 54
rank_math_internal_links_processed:
  - 1
rank_math_primary_category:
  - 6
rank_math_focus_keyword:
  - install and enable ssh server on fedora 35
rank_math_analytic_object_id:
  - 163
categories:
  - Linux

---
<a href="https://www.ssh.com/academy/ssh/server" target="_blank" rel="noreferrer noopener">Secure Shell Protocol</a> (SSH) is a cryptographic network protocol for operating network services securely over an unsecured network. Notably used **for remote logins and command-line execution**. **SSH protects the privacy and integrity of the transferred data or files over a network. It helps block sniffing attacks on the network.** In this tutorial, I will show you how to install and enable the SSH server on fedora 35.

Before we can install the SSH server, we first need to understand how this server works. It mostly works on the client/server model. The SSH client initiates the setup of a secure connection and the SSH server listens to the incoming connection on TCP port 22 and makes a response. The server then authenticates itself to the client by providing the public key. This allows the client to know it is communicating with the correct server. 

Most Linux systems come with pre-installed open-source **<a href="https://en.wikipedia.org/wiki/OpenSSH" target="_blank" rel="noreferrer noopener">OpenSSH</a>**. OpenSSH is developed as part of the OpenBSD project. 

OpenSSH comes with the following command-line utilities and daemon.

  * Secure Copy Protocol (SCP)
  * Secure File Transfer Protocol (SFTP)
  * SSH
  * SSH-add and SSH-agent
  * SSH-keygen
  * SSH-keyscan

## Install OpenSSH on Fedora 35 {.wp-block-heading}

First, we need to log in as root to perform this operation. and then update your system repositories.

<pre class="wp-block-code"><code>dnf update -y</code></pre>

### 1. Check the OpenSSH file directory {.wp-block-heading}

When the updates are complete, proceed to check if Openssh is in your default Fedora 35 repositories.

<pre class="wp-block-code"><code>rpm -qa | grep openssh-server
openssh-server-8.7p1-3.fc35.x86_64</code></pre>

If it happens that you don&#8217;t see the output, as shown above, then you need to proceed and install them.

### 2. Install OpenSSH-server on Fedora 35. {.wp-block-heading}

To install openssh-server you need to run the following command on your terminal as root.

<pre class="wp-block-code"><code>&lt;strong>dnf -y install openssh-server&lt;/strong>
Package openssh-server-8.7p1-3.fc35.x86_64 is already installed.
Dependencies resolved.
Nothing to do.
Complete!</code></pre>

### 3. Configuring an OpenSSH {.wp-block-heading}

### Enable Openssh to start on boot  {.wp-block-heading}

If you check the status of the **sshd** service now you will find that the service isn&#8217;t running, so we need to enable it first then start the service.

<pre class="wp-block-code"><code>sudo systemctl enable sshd</code></pre>

After you have enabled it, you need to start the service 

<pre class="wp-block-code"><code>sudo systemctl start sshd</code></pre>

Lastly, you need to check the status if sshd is actually running.

<pre class="wp-block-code"><code>sudo systemctl status sshd.
● sshd.service - OpenSSH server daemon
     Loaded: loaded (/usr/lib/systemd/system/sshd.service; enabled; vendor preset: enabled)
     Active: active (running) since Mon 2022-04-04 08:50:25 UTC; 1min 51s ago
       Docs: man:sshd(8)
             man:sshd_config(5)
   Main PID: 14899 (sshd)
      Tasks: 1 (limit: 1112)
     Memory: 1.1M
        CPU: 102ms
     CGroup: /system.slice/sshd.service
             └─14899 "sshd: /usr/sbin/sshd -D &#91;listener] 0 of 10-100 startups"

Apr 04 08:50:25 fedora sshd&#91;14899]: Server listening on 0.0.0.0 port 22.
Apr 04 08:50:25 fedora sshd&#91;14899]: Server listening on :: port 22.
Apr 04 08:50:32 fedora sshd&#91;17488]: Invalid user zeg from 20.84.65.175 port 33352
Apr 04 08:50:32 fedora sshd&#91;17488]: Received disconnect from 20.84.65.175 port 33352:11: Bye Bye &#91;preauth]
Apr 04 08:50:32 fedora sshd&#91;17488]: Disconnected from invalid user zeg 20.84.65.175 port 33352 &#91;preauth]
Apr 04 08:50:37 fedora sshd&#91;19004]: Invalid user drikesmedia from 201.217.195.226 port 51452
Apr 04 08:50:37 fedora sshd&#91;19004]: Received disconnect from 201.217.195.226 port 51452:11: Bye Bye &#91;preauth]
Apr 04 08:50:37 fedora sshd&#91;19004]: Disconnected from invalid user drikesmedia 201.217.195.226 port 51452 &#91;preauth]</code></pre>

To stop running sshd daemon issue the following command.

<pre class="wp-block-code"><code>sudo systemctl stop sshd</code></pre>

If you check the status again you should be able to see status not active.

<pre class="wp-block-code"><code>systemctl status sshd
○ sshd.service - OpenSSH server daemon
     Loaded: loaded (/usr/lib/systemd/system/sshd.service; enabled; vendor preset: enabled)
     Active: inactive (dead) since Mon 2022-04-04 08:59:29 UTC; 1min 13s ago
       Docs: man:sshd(8)
             man:sshd_config(5)
    Process: 14899 ExecStart=/usr/sbin/sshd -D $OPTIONS (code=exited, status=0/SUCCESS)
   Main PID: 14899 (code=exited, status=0/SUCCESS)
        CPU: 533ms

Apr 04 08:57:17 fedora sshd&#91;19043]: Invalid user dziq from 40.121.14.28 port 54990
Apr 04 08:57:17 fedora sshd&#91;19043]: Received disconnect from 40.121.14.28 port 54990:11: Bye Bye &#91;preauth]
Apr 04 08:57:17 fedora sshd&#91;19043]: Disconnected from invalid user dziq 40.121.14.28 port 54990 &#91;preauth]
Apr 04 08:59:28 fedora sshd&#91;19046]: Invalid user abe from 188.166.243.218 port 35016
Apr 04 08:59:28 fedora sshd&#91;19046]: Received disconnect from 188.166.243.218 port 35016:11: Bye Bye &#91;preauth]
Apr 04 08:59:28 fedora sshd&#91;19046]: Disconnected from invalid user abe 188.166.243.218 port 35016 &#91;preauth]
Apr 04 08:59:29 fedora sshd&#91;14899]: Received signal 15; terminating.
Apr 04 08:59:29 fedora systemd&#91;1]: Stopping OpenSSH server daemon...
Apr 04 08:59:29 fedora systemd&#91;1]: sshd.service: Deactivated successfully.
Apr 04 08:59:29 fedora systemd&#91;1]: Stopped OpenSSH server daemon.</code></pre>

If you try connecting to a remote server you will see the following. I am using ubutnu 20.04 desktop to connect to my Fedora 35 server.

<pre class="wp-block-code"><code>ssh root@139.59.138.27</code></pre>

The **root** is the account am in and **139.59.138.27** is the IP address of my Fedora 35 server.<figure class="wp-block-image size-full is-resized">

<img decoding="async" loading="lazy" src="https://i0.wp.com/nextgentips.com/wp-content/uploads/2022/04/Screenshot-from-2022-04-04-12-06-28-1-1.png?resize=721%2C459&#038;ssl=1" alt="SSH connection" class="wp-image-1279" width="721" height="459" srcset="https://i0.wp.com/nextgentips.com/wp-content/uploads/2022/04/Screenshot-from-2022-04-04-12-06-28-1-1.png?w=721&ssl=1 721w, https://i0.wp.com/nextgentips.com/wp-content/uploads/2022/04/Screenshot-from-2022-04-04-12-06-28-1-1.png?resize=300%2C191&ssl=1 300w" sizes="(max-width: 721px) 100vw, 721px" data-recalc-dims="1" /> <figcaption>SSH connection</figcaption></figure> 

### 4. Requiring SSH remote connections  {.wp-block-heading}

For SSH to be effective, you don&#8217;t need to use an insecure connection. Disable the following services: telnet, rsh, rlogin and VSFTP.

Even though these services are not installed on Fedora by default, we need to make sure it is not running for sure. Stop them using the following commands.

<pre class="wp-block-code"><code>sudo systemctl stop telnet
sudo systemctl stop rsh
sudo systemctl stop rlogin
sudo systemctl stop vsftp</code></pre>

And if you want to disable these services from running from start-up do the following:

<pre class="wp-block-code"><code>sudo systemctl disable telnet
sudo systemctl disable rsh
sudo systemctl disable rlogin
sudo systemctl disable vsftp</code></pre>

## Conclusion {.wp-block-heading}

To wrap it up SSHing is two-way traffic that creates a request, and gets back the request signal from the server. And you have to create an SSH key pair on your system first to enable you to ssh into another host.