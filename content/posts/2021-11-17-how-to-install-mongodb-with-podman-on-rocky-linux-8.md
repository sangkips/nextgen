---
title: How to Install MongoDB with Podman on Rocky Linux 8
author: Kipkoech Sang
type: post
date: 2021-11-17T12:33:09+00:00
url: /2021/11/17/how-to-install-mongodb-with-podman-on-rocky-linux-8/
rank_math_news_sitemap_robots:
  - index
rank_math_robots:
  - 'a:1:{i:0;s:5:"index";}'
rank_math_analytic_object_id:
  - 98
rank_math_internal_links_processed:
  - 1
categories:
  - Database

---
In this tutorial I will be showing you how to install MongoDB with Podman on Rocky Linux.

Mongodb is an open source NoSQL database that provides high throughput for data driven applications. Unlike relational databases such as MySQL, Oracle and SQL server which store data in tables according to a rigid schema, MongoDB stores data in documents with flexible schema.

Podman is a daemonless, open source, Linux native tool designed to make it easy to find, run, build, share and deploy applications using Open Container Initiative (OCI) containers and container images.&nbsp;

## Related Articles  {.wp-block-heading}

  * [How to install Podman 3 on Debian 11][1]
  * [How to install MongoDB 5 on Ubuntu 21.04][2]
  * [How to Deploy MongoDB with Docker][3]
  * [How to install MongoDB 4.4 on Fedora 35][4]

## Prerequisites  {.wp-block-heading}

  * Rocky Linux server
  * User with sudo privileges 
  * Internet connection

## Table of Contents {.wp-block-heading}

  1. Update your system repositories 
  2. Install Podman
  3. Pull MongoDB using Podman
  4. Create MongoDB container 
  5. Conclusion

## 1. Install Podman {.wp-block-heading}

We need to first install podman into our system with the following command.

<pre class="wp-block-code"><code>$ sudo dnf install podman -y</code></pre>

Sample output will be as follows 

<pre class="wp-block-code"><code>Installed:
  conmon-2:2.0.29-1.module+el8.5.0+710+4c471e88.x86_64                                                           
  container-selinux-2:2.167.0-1.module+el8.5.0+710+4c471e88.noarch                                               
  containernetworking-plugins-1.0.0-1.module+el8.5.0+710+4c471e88.x86_64                                         
  containers-common-2:1-2.module+el8.5.0+710+4c471e88.noarch                                                     
  criu-3.15-3.module+el8.5.0+710+4c471e88.x86_64                                                                 
  fuse-common-3.2.1-12.el8.x86_64                                                                                
  fuse-overlayfs-1.7.1-1.module+el8.5.0+710+4c471e88.x86_64                                                      
  fuse3-3.2.1-12.el8.x86_64                                                                                      
  fuse3-libs-3.2.1-12.el8.x86_64                                                                                 
  iptables-1.8.4-20.el8.x86_64                                                                                   
  libnet-1.1.6-15.el8.x86_64                                                                                     
  libnetfilter_conntrack-1.0.6-5.el8.x86_64                                                                      
  libnfnetlink-1.0.1-13.el8.x86_64                                                                               
  libnftnl-1.1.5-4.el8.x86_64                                                                                    
  libslirp-4.4.0-1.module+el8.5.0+710+4c471e88.x86_64                                                            
  nftables-1:0.9.3-21.el8.x86_64                                                                                 
  podman-3.3.1-9.module+el8.5.0+710+4c471e88.x86_64                                                              
  podman-catatonit-3.3.1-9.module+el8.5.0+710+4c471e88.x86_64                                                    
  protobuf-c-1.3.0-6.el8.x86_64                                                                                  
  runc-1.0.2-1.module+el8.5.0+710+4c471e88.x86_64                                                                
  slirp4netns-1.1.8-1.module+el8.5.0+710+4c471e88.x86_64                                                         

Complete!</code></pre>

Check version of Podman

<pre class="wp-block-code"><code>$ podman -v
podman version 3.3.1</code></pre>

## 2. Pull MongoD using Podman {.wp-block-heading}

<pre class="wp-block-code"><code>$ podman pull mongo</code></pre>

Choose where you want to pull your image from. For my case I chose Docker, you can use Fedora, Redhat or Centos whichever distro you prefer.

For Docker the output should be like this

<pre class="wp-block-code"><code>✔ docker.io/library/mongo:latest
Trying to pull docker.io/library/mongo:latest...
Getting image source signatures
Copying blob 90eb44ebc60b done  
Copying blob 7b1a6ab2e44d done  
Copying blob 019496b6c44a done  
Copying blob c0df4f407f69 done  
Copying blob 5085b59f2efb done  
Copying blob c7499923d022 done  
Copying blob 351daa315b6c done  
Copying blob 5b6df31e95f8 done  
Copying blob 98e820b4cad7 done  
Copying blob e82745116109 done  
Copying config 4253856b25 done  
Writing manifest to image destination
Storing signatures
4253856b25700502a6ddc7fdb38aa0fe2aa84b4d404e82c9a24b9f791e99367b</code></pre>

Check to see the images 

<pre class="wp-block-code"><code>$ podman images 
REPOSITORY               TAG         IMAGE ID      CREATED      SIZE
docker.io/library/mongo  latest      4253856b2570  8 hours ago  706 MB</code></pre>

You can test mongodb now with the following:

<pre class="wp-block-code"><code>$ podman run --rm -it mongo</code></pre>

## 3. Create MongoDB Container  {.wp-block-heading}

Create a directory to store MongoDB data so that in case you delete the container you will still got your data intact.

<pre class="wp-block-code"><code>$ sudo mkdir data
$cd data</code></pre>

After you have cd into the directory, now you can create your MongoDB container with thefollowing command:

<pre class="wp-block-code"><code>$ podman run --name nextgentipsmongo -d -p 27017:27017 -v $(pwd):/data/db:Z mongo
d495b10f1accd425bd1d13748994b196f4c800344904911a0691146f57bc764e</code></pre>

Let&#8217;s look at each command variable:

  * **&#8211;name**: describe the name of the project i.e nextgentipsmongo
  * **-d**: run your container in detached mode i.e in the background 
  * **-p** **27017:27017**: map port 27017 of the host to port 27017 on the container.
  * -v ([volume][5]): create persisting data generated and used by podman containers.
  * **$(pwd):/data/db**: shows the directory to to store your data.
  * **:Z**&#8211; tells podman that the volume content will be shared between containers 

## 4. Test MongoDB  {.wp-block-heading}

We can test our container to see if it is working with the following command; If it was installed correctly it should show the mongo shell where you can now perform your database routines.

<pre class="wp-block-code"><code>$ podman run --rm -it --network host mongo mongo
MongoDB shell version v5.0.4
connecting to: mongodb://127.0.0.1:27017/?compressors=disabled&gssapiServiceName=mongodb
Implicit session: session { "id" : UUID("9a9cc44d-696a-4370-ab0c-bb2d9fe12ded") }
MongoDB server version: 5.0.4
================
Warning: the "mongo" shell has been superseded by "mongosh",
which delivers improved usability and compatibility.The "mongo" shell has been deprecated and will be removed in
an upcoming release.
For installation instructions, see
https:&#47;&#47;docs.mongodb.com/mongodb-shell/install/
================
Welcome to the MongoDB shell.
For interactive help, type "help".
For more comprehensive documentation, see
        https://docs.mongodb.com/
Questions? Try the MongoDB Developer Community Forums
        https://community.mongodb.com
---
The server generated these startup warnings when booting: 
        2021-11-17T11:01:45.156+00:00: Access control is not enabled for the database. Read and write access to data and configuration is unrestricted
        2021-11-17T11:01:45.156+00:00: /sys/kernel/mm/transparent_hugepage/enabled is 'always'. We suggest setting it to 'never'
---
---
        Enable MongoDB's free cloud-based monitoring service, which will then receive and display
        metrics about your deployment (disk utilization, CPU, operation statistics, etc).

        The monitoring data will be available on a MongoDB website with a unique URL accessible to you
        and anyone you share the URL with. MongoDB may use this information to make product
        improvements and to suggest MongoDB products and deployment options to you.

        To enable free monitoring, run the following command: db.enableFreeMonitoring()
        To permanently disable this reminder, run the following command: db.disableFreeMonitoring()
---
&gt; db.test
test.test</code></pre>

## 5. Conclusion {.wp-block-heading}

Congrartulation you have successfully install MongoDB with Podman on a RockyLinux platform. You can now move ahead and connect your database to your project.

 [1]: https://nextgentips.com/2021/10/22/how-to-install-podman-3-on-debian-11/
 [2]: https://nextgentips.com/2021/10/26/how-to-install-mongodb-5-on-ubuntu-21-04/
 [3]: https://nextgentips.com/2021/10/26/how-to-deploy-mongodb-with-docker/
 [4]: https://nextgentips.com/2021/11/17/how-to-install-mongodb-4-4-on-fedora-35/
 [5]: https://docs.docker.com/storage/volumes/