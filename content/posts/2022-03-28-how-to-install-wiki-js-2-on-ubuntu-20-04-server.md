---
title: How to install Wiki.js 2 on Ubuntu 20.04|22.04 server
author: Kipkoech Sang
type: post
date: 2022-03-28T10:23:43+00:00
url: /2022/03/28/how-to-install-wiki-js-2-on-ubuntu-20-04-server/
rank_math_seo_score:
  - 71
rank_math_focus_keyword:
  - Wiki.js 2
rank_math_internal_links_processed:
  - 1
rank_math_primary_category:
  - 6
rank_math_analytic_object_id:
  - 161
categories:
  - Linux

---
Wiki.js is open-source wiki software, it&#8217;s powerful and easily extensible, it&#8217;s an engine running on node.js and is written in javascript. It&#8217;s available as a self-hosted solution or as a single-click install from Digital Ocean and AWS marketplace.

## Wiki.js 2 features {.wp-block-heading}

  * It works on any virtual environment and supports many database platforms such as Postgresql, MariaDB, Mysql, SQLite, Ms Sql.
  * It has an extensive and intuitive admin area where you do all your administration duties at a go.
  * It is built with performance because of its node.js integration. 
  * It is easily customizable.
  * It can scale easily

## Wiki.js 2 requirements  {.wp-block-heading}

  * Server: 2 cores or more and 1 GB RAM
  * At least 1 GB of storage space
  * Good internet access
  * Database: Postgresql 9.5 or later, MariaDB 10.2 or later, MySQL 8 or later, MS SQL Server 2012 or later, SQLite 3.9 or later.
  * Node.js 10 version 10 or later, 12 version 12 or later, 14 version 14 or later, and 16 version 16 or later.

Also, you can check out these articles for node.js, PostgreSQL installation.

  * <a href="https://nextgentips.com/2021/10/09/how-to-install-postgresql-on-ubuntu-20-04/" target="_blank" rel="noreferrer noopener">How to install PostgreSQL 14 on Ubuntu 20.04|21.10</a>
  * <a href="https://nextgentips.com/2021/12/26/how-to-install-node-js-17-on-ubuntu-20-04/" target="_blank" rel="noreferrer noopener">How to install Node.js 17 on Ubuntu 20.04</a>

## Install Wiki.js 2 on Ubuntu 20.04 {.wp-block-heading}

### 1. Update system repositories  {.wp-block-heading}

The first thing to do is to update your system repositories so that once you start your installation of Wiki.js 2 you will not run into errors early on. So to update your repo run the following command.

<pre class="wp-block-code"><code>sudo apt -qqy update -y</code></pre>

Then we need to enable our system to be running updates automatically with this command.

<pre class="wp-block-code"><code>sudo DEBIAN_FRONTEND=noninteractive apt-get -qqy -o Dpkg::Options::='--force-confdef' -o Dpkg::Options::='--force-confold' dist-upgrade</code></pre>

### 2. Install Docker  {.wp-block-heading}

We need to have Docker because it is a highly recommended platform to run wiki.js. So to install Docker check this article on <a href="https://nextgentips.com/2022/01/01/how-to-install-and-configure-docker-ce-on-ubuntu-20-04/" target="_blank" rel="noreferrer noopener">how to install docker on Ubuntu 20.04</a>.

#### Install Docker Dependencies  {.wp-block-heading}

To start installing Docker, we first need to have dependencies. To install dependencies run the following command.

<pre class="wp-block-code"><code>sudo apt -qqy -o Dpkg::Options::='--force-confdef' -o Dpkg::Options::='--force-confold' install apt-transport-https ca-certificates curl gnupg-agent software-properties-common openssl</code></pre>

Sample output

<pre class="wp-block-code"><code>&lt;strong>ca-certificates&lt;/strong> is already the newest version (20210119~20.04.2).
&lt;strong>curl&lt;/strong> is already the newest version (7.68.0-1ubuntu2.7).
curl set to manually installed.
&lt;strong>openssl&lt;/strong> is already the newest version (1.1.1f-1ubuntu2.12).
openssl set to manually installed.
&lt;strong>software-properties-common&lt;/strong> is already the newest version (0.99.9.8).
software-properties-common set to manually installed.
apt-transport-https is already the newest version (2.0.6).
The following package was automatically installed and is no longer required:
  libfwupdplugin1
Use 'sudo apt autoremove' to remove it.
The following NEW packages will be installed:
  &lt;strong>gnupg-agent&lt;/strong>
0 upgraded, 1 newly installed, 0 to remove and 0 not upgraded.
Need to get 5232 B of archives.
After this operation, 46.1 kB of additional disk space will be used.
Selecting previously unselected package gnupg-agent.
(Reading database ... 94889 files and directories currently installed.)
Preparing to unpack .../gnupg-agent_2.2.19-3ubuntu2.1_all.deb ...
Unpacking gnupg-agent (2.2.19-3ubuntu2.1) ...
Setting up gnupg-agent (2.2.19-3ubuntu2.1) ...</code></pre>

#### Add Registry Key and add repository deb package  {.wp-block-heading}

To register the Docker package registry we need to add a signing key and add it to the deb package repository.

<pre class="wp-block-code"><code>curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -</code></pre>

Add the package to the apt repository.

<pre class="wp-block-code"><code>sudo add-apt-repository "deb &#91;arch=amd64] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable"</code></pre>

After this, run the system repository and install docker-ce

<pre class="wp-block-code"><code>sudo apt -qqy update
# install docker
sudo apt -qqy -o Dpkg::Options::='--force-confdef' -o Dpkg::Options::='--force-confold' install docker-ce docker-ce-cli containerd.io</code></pre>

### 3. Setup Wiki.js 2 containers  {.wp-block-heading}

First, we need to create a wiki.js 2 installation directory with the following command.

<pre class="wp-block-code"><code>mkdir -p /etc/wiki</code></pre>

Now we need to generate a database secret like this 

<pre class="wp-block-code"><code>openssl rand -base64 32 &gt; /etc/wiki/.db-secret</code></pre>

Then we need to create docker internal networks for wiki.js 2 to use.

<pre class="wp-block-code"><code>docker network create wikinet</code></pre>

Then create a data volume for PostgreSQL. 

<pre class="wp-block-code"><code>docker volume create pgdata</code></pre>

And lastly, create the actual containers like this.

For database container

<pre class="wp-block-code"><code>docker create --name=db -e POSTGRES_DB=wiki -e POSTGRES_USER=wiki -e POSTGRES_PASSWORD_FILE=/etc/wiki/.db-secret -v /etc/wiki/.db-secret:/etc/wiki/.db-secret:ro -v pgdata:/var/lib/postgresql/data --restart=unless-stopped -h db --network=wikinet postgres:14</code></pre>

Output

<pre class="wp-block-code"><code>Unable to find image 'postgres:14' locally
14: Pulling from library/postgres
ae13dd578326: Pull complete 
723e40c35aaf: Pull complete 
bf97ae6a09b4: Pull complete 
2c965b3c8cbd: Pull complete 
c3cefa46a015: Pull complete 
64a7315fc25c: Pull complete 
b9846b279f7d: Pull complete 
ed988fb8e7d9: Pull complete 
ed4bb4fd8bb5: Pull complete 
ead27f1733c8: Pull complete 
7d493bacd383: Pull complete 
0920535e8417: Pull complete 
db76d5bdbf2c: Pull complete 
Digest: sha256:14a621708fddaf5620fb3dea3e313e8afc3615b6fe0b824ff929692823952144
Status: Downloaded newer image for postgres:14
4f1744020e8ee16c6180cbb469f527fc04a83f47224bb9a0b84c7e1aeeb6a9af</code></pre>

For Wiki container 

<pre class="wp-block-code"><code>docker create --name=wiki -e DB_TYPE=postgres -e DB_HOST=db -e DB_PORT=5432 -e DB_PASS_FILE=/etc/wiki/.db-secret -v /etc/wiki/.db-secret:/etc/wiki/.db-secret:ro -e DB_USER=wiki -e DB_NAME=wiki -e UPGRADE_COMPANION=1 --restart=unless-stopped -h wiki --network=wikinet -p 80:3000 -p 443:3443 ghcr.io/requarks/wiki:2</code></pre>

<pre class="wp-block-code"><code># output
Unable to find image 'ghcr.io/requarks/wiki:2' locally
2: Pulling from requarks/wiki
59bf1c3509f3: Pull complete 
683dd8c3cc08: Pull complete 
ae5b2724f19b: Pull complete 
39190df3f477: Pull complete 
d4cce20361a2: Pull complete 
4f4fb700ef54: Pull complete 
85ad99a67650: Pull complete 
11d5b98621fc: Pull complete 
444c84d6e4a9: Pull complete 
e90aa5f15768: Pull complete 
aaa0b712706c: Pull complete 
f54da04ffb2d: Pull complete 
13bd7779ec0d: Pull complete 
Digest: sha256:6e2cd07652e3a96d305cb8ddd3e6ea649d99ecb3df2988f733ac621b2cbf910e
Status: Downloaded newer image for ghcr.io/requarks/wiki:2
1a12af5878318163fcf6a1d8805a9ec3421df6528fb279d142c88d147e0e407e</code></pre>

For wiki update companion

<pre class="wp-block-code"><code>docker create --name=wiki-update-companion -v /var/run/docker.sock:/var/run/docker.sock:ro --restart=unless-stopped -h wiki-update-companion --network=wikinet requarks/wiki-update-companion:latest</code></pre>

<pre class="wp-block-code"><code># output
Unable to find image 'requarks/wiki-update-companion:latest' locally
latest: Pulling from requarks/wiki-update-companion
000eee12ec04: Pull complete 
db438065d064: Pull complete 
e345d85b1d3e: Pull complete 
f6285e273036: Pull complete 
2354ee191574: Pull complete 
7c8b4b598d87: Pull complete 
e436df127cfa: Pull complete 
Digest: sha256:ef3c818540fd919b1cda73eef1bc83cb8b08f76e8c26c4ad291a9fa489ba0299
Status: Downloaded newer image for requarks/wiki-update-companion:latest
093d83bfc4903837378bf55c495380b53822beb63b932999c8f5b93fb6647862</code></pre>

### 4. Setup firewall {.wp-block-heading}

We need to allow, https, http and ssh rules to pass the firewall filter. this will allow us ssh from any remote machine and be able to access the interface of the wiki.js easily.

<pre class="wp-block-code"><code>sudo ufw allow ssh 
sudo allow http 
sudo allow https
sudo ufw --force enable</code></pre>

### 5. Install and configure NGINX {.wp-block-heading}

Nginx is not necessarily needed but it is highly recommended to put it in front of wiki.js. This enables the usage of SSL, caching parameters, etc.

<pre class="wp-block-code"><code>sudo apt install nginx</code></pre>

Adjust the firewall setting to allow Nginx entry.

<pre class="wp-block-code"><code>sudo ufw allow 'Nginx HTTP'</code></pre>

Then we need to enable the service.

<pre class="wp-block-code"><code>sudo systemctl enable nginx
sudo systemctl start nginx.service
sudo systemctl status nginx.service</code></pre>

### 6. Start the Wiki.js containers  {.wp-block-heading}

Head over to your terminal and start the docker containers we have created.

<pre class="wp-block-code"><code>docker start db
docker start wiki
docker start wiki-update-companion</code></pre>

Now that we have started the docker containers, head over to your favorite browser and access the wiki.js interface.

<pre class="wp-block-code"><code>http:&#47;&#47;&lt;your-ip-address&gt;</code></pre><figure class="wp-block-image size-large">

<img decoding="async" loading="lazy" width="810" height="464" src="https://i0.wp.com/nextgentips.com/wp-content/uploads/2022/03/Screenshot-from-2022-03-28-13-11-26.png?resize=810%2C464&#038;ssl=1" alt="Wiki.js 2 login page " class="wp-image-1233" srcset="https://i0.wp.com/nextgentips.com/wp-content/uploads/2022/03/Screenshot-from-2022-03-28-13-11-26.png?resize=1024%2C586&ssl=1 1024w, https://i0.wp.com/nextgentips.com/wp-content/uploads/2022/03/Screenshot-from-2022-03-28-13-11-26.png?resize=300%2C172&ssl=1 300w, https://i0.wp.com/nextgentips.com/wp-content/uploads/2022/03/Screenshot-from-2022-03-28-13-11-26.png?resize=768%2C440&ssl=1 768w, https://i0.wp.com/nextgentips.com/wp-content/uploads/2022/03/Screenshot-from-2022-03-28-13-11-26.png?w=1294&ssl=1 1294w" sizes="(max-width: 810px) 100vw, 810px" data-recalc-dims="1" /> <figcaption>Wiki.js 2 login page </figcaption></figure> <figure class="wp-block-image size-large"><img decoding="async" loading="lazy" width="810" height="464" src="https://i0.wp.com/nextgentips.com/wp-content/uploads/2022/03/Screenshot-from-2022-03-28-13-13-43.png?resize=810%2C464&#038;ssl=1" alt="Wiki.js 2 dashboard" class="wp-image-1234" srcset="https://i0.wp.com/nextgentips.com/wp-content/uploads/2022/03/Screenshot-from-2022-03-28-13-13-43.png?resize=1024%2C586&ssl=1 1024w, https://i0.wp.com/nextgentips.com/wp-content/uploads/2022/03/Screenshot-from-2022-03-28-13-13-43.png?resize=300%2C172&ssl=1 300w, https://i0.wp.com/nextgentips.com/wp-content/uploads/2022/03/Screenshot-from-2022-03-28-13-13-43.png?resize=768%2C440&ssl=1 768w, https://i0.wp.com/nextgentips.com/wp-content/uploads/2022/03/Screenshot-from-2022-03-28-13-13-43.png?w=1294&ssl=1 1294w" sizes="(max-width: 810px) 100vw, 810px" data-recalc-dims="1" /><figcaption>Wiki.js 2 dashboad</figcaption></figure> 

## Conclusion {.wp-block-heading}

Congratulations you have successfully installed Wiki.js 2 on Ubuntu 20.04. If in case you face any problems consult <a href="https://docs.requarks.io/" target="_blank" rel="noreferrer noopener">Wiki.js documentation</a>.