---
title: How to install go1.19 on Ubuntu 22.04
author: Kipkoech Sang
type: post
date: 2022-06-27T11:48:39+00:00
url: /2022/06/27/how-to-install-go1-19beta-on-ubuntu-22-04/
categories:
  - Programming

---
In this tutorial, we are going to explore how to install go on Ubuntu 22.04

<a href="https://go.dev/doc/" target="_blank" rel="noreferrer noopener">Golang</a> is an open-source programming language that is easy to learn and use. It is built-in concurrency and a robust standard library. It is reliable and builds fast, and efficient software that scales fast.

Its concurrency mechanisms make it easy to write programs that get the most out of multicore and networked machines, while its novel-type systems enable flexible and modular program constructions.

Go compiles quickly to machine code and has the convenience of garbage collection and the power of run-time reflection.

In this guide, we are going to learn how to install Golang 1.19.4 on Ubuntu 22.04.

## Related Articles {.wp-block-heading}

  * [How to install Go 1.18 on Fedora 35][1]
  * [How to install Go 1.17 on Ubuntu 20.04][2]

## Installing Go 1.19.4 on Ubuntu 22.04 {.wp-block-heading}

## 1. Run system updates {.wp-block-heading}

To begin with, we need to run system updates in order to make our current repositories up to date. To do so we need to run the following command on our terminal.

<pre class="wp-block-code"><code>sudo apt update && apt upgrade -y</code></pre>

When both upgrades and updates are complete, go ahead to download go from its download page.

## 2. Installing Go {.wp-block-heading}

To install go we need to download it by going to the <a href="https://go.dev/doc/install" target="_blank" rel="noopener" title="">download page</a>. we are going to download the release. We are going to use the wget command to download.

<pre class="wp-block-code"><code>$ wget https://go.dev/dl/go1.19.4.linux-amd64.tar.gz</code></pre>

when the download is complete, extract the archive downloaded to your desired location. I will be using** /usr/local** directory. First delete the directory if you have installed Golang before.

<pre class="wp-block-code"><code>$ sudo rm -rf /usr/local/go && tar -C /usr/local -xzf go1.19.4.linux-amd64.tar.gz
</code></pre>

After extraction is complete move ahead and set up the go environment.

## 3. Setting Go Environment {.wp-block-heading}

To set up go environment we need to define the root of Golang packages. We normally use **GOROOT **and **GOPATH** to define that environment. We need to set up the **GOROOT** location where the packages are installed.

<pre class="wp-block-code"><code>export GOROOT=/usr/local/go</code></pre>

Next, we will need to set up the GOPATH. Let’s set up GOPATH in the $HOME/go directory.

<pre class="wp-block-code"><code>export GOPATH=$HOME/go</code></pre>

Now we need to append the go binary PATH so that we can be able to access the program system-wide. Use the following command.

<pre class="wp-block-code"><code>export PATH=$GOPATH/bin:$GOROOT/bin:$PATH</code></pre>

To apply the changes we have made above, run the following command:

<pre class="wp-block-code"><code>source ~/.bashrc</code></pre>

Lastly, we can do the verification with the following;

<pre class="wp-block-code"><code>$ go version
go version go1.19.4 linux/amd64</code></pre>

Let’s create a program to check all our settings. We will create a simple one.

Create a file main.go on the main directory.

<pre class="wp-block-code"><code>touch main.go</code></pre>

On your favorite text editor do the following

<pre class="wp-block-code"><code>sudo nano main.go</code></pre>

Insert the below code

<pre class="wp-block-code"><code>$ package main

import "fmt"

func main(){
	fmt.Println("This is go1.19.4 on Ubuntu 22.04")
}</code></pre>

To run the program use the following command;

<pre class="wp-block-code"><code>$ go run main.go
This is go1.19.4 on Ubuntu 22.04</code></pre>

## Conclusion {.wp-block-heading}

Now you know how to install Golang 1.19.4 in Ubuntu 22.04. You can consult the <a href="https://go.dev/dl/" target="_blank" rel="noreferrer noopener">go documentation</a> in case you have any problems.

 [1]: https://nextgentips.com/2021/12/21/how-to-install-go-1-18-on-fedora-35/
 [2]: https://nextgentips.com/2021/12/11/how-to-install-go-1-17-on-ubuntu-20-04/