---
title: How to perform CRUD operations in Django
author: Kipkoech Sang
type: post
date: 2022-08-23T12:40:48+00:00
url: /2022/08/23/how-to-perform-crud-operations-in-django/
categories:
  - Uncategorized

---
In this tutorial, we will learn how to operate CRUD functionalities in Django. CRUD functionalities are Create, Read, Update and Delete. These are the main building blocks of any project. At some point you need to perform read operations from the database, you want to do an update or you wish to delete an item from the database. All these are possible thanks to CRUD operations.

In this tutorial, we are building on top of these lectures on <a href="https://nextgentips.com/2022/05/03/how-to-set-up-a-python-django-application-using-django-4-0/" target="_blank" rel="noreferrer noopener">How to set up a Python Django Application using Django 4.0</a> and <a href="https://nextgentips.com/2022/05/24/how-to-create-a-django-project-using-templates/" target="_blank" rel="noreferrer noopener">How to create Django Templates</a>. So make sure to check out first.

If you want to follow along make sure you have the project setup complete, the templates are working fine, the database is up and running and lastly have some data on the database.

## Steps to follow {.wp-block-heading}

### 1. Create a Project {.wp-block-heading}

To create a project go to your terminal and create a folder name your project whatever you like, the create a virtual environment where all your installations will be staying. To create a virtual environment do the following.

<pre class="wp-block-code"><code>$ mkdir crud_example
$ cd crud_example
#create a virtual environment 
$ python3 -m venv env #env is the name of the virual environment.</code></pre>

To make your work easier open the containing folder in your favorite text editor, for me I will be using Pycharm. Activate the virtual environment as created above.

<pre class="wp-block-code"><code>$ source env/bin/activate</code></pre>

### 2. Install Django  {.wp-block-heading}

To install Django in your project, use the following command.

<pre class="wp-block-code"><code>$ pip install django</code></pre>

After Django has been installed make sure you do `<mark style="background-color:#abb8c3" class="has-inline-color">pip freeze > requirements.txt</mark>` in order to store all your installed files. This requiremets.txt file is necessary so that someone else who will come back later and want to use your project will just install the requirements from one file.

<pre class="wp-block-code"><code>$ pip freeze &gt; requirements.txt</code></pre>

### 3. Create a Django project and app {.wp-block-heading}

The next step is to create a Django project and an app. To create a project using the following command.

<pre class="wp-block-code"><code>$ django-admin startproject myproject .</code></pre>

Make sure to include the period in the end. It tells Django to create a project in the current directory.

After the project has been created, we need to create an app. To do so use the following command.

<pre class="wp-block-code"><code>$ python manage.py startapp project</code></pre>

When the app has been created, go to the <mark style="background-color:#abb8c3" class="has-inline-color">`settings.py` </mark>file and add your app to the installed apps

<pre class="wp-block-code"><code>#installed apps 
...
'project.apps.ProjectConfig',</code></pre>

Still inside the `settings.py` file create a database of your choice, be it Mysql, PostgreSQL, or db sqlite. For my case, I will be using PostgreSQL. You may want to leave db sqlite and use it for testing purposes, but you will need PostgreSQL or MySQL for production. 

### 4. Create PostgreSQL database {.wp-block-heading}

To use PostgreSQL, we need to create it first in our system and then tell django to use it. To create a Postgresql do the following.

Open psql as Postgres user 

<pre class="wp-block-code"><code>sudo -u postgres psql
postgres=# </code></pre>

To create a database, we use the following command inside postgres.

<pre class="wp-block-code"><code>CREATE DATABASE project_db;</code></pre>

Project is the name of the database.

After creating the database, create the user 

<pre class="wp-block-code"><code>CREATE USER projectuser WITH PASSWORD 'strpassword';</code></pre>

Then we need to grant privileges to the user created.

<pre class="wp-block-code"><code>GRANT ALL PRIVILEGES ON DATABASE project_db TO projectuser;</code></pre>

Lastly quit the Postgres with `<mark style="background-color:#abb8c3" class="has-inline-color">\q</mark>`

We now need to go back to our project and inside `<mark style="background-color:#abb8c3" class="has-inline-color">settings.py</mark>` look for databases and change from db sqlite to PostgreSQL

<pre class="wp-block-code"><code>DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql',
        'NAME': 'project_db',
        'USER': 'projectuser',
        'PASSWORD': 'strpasswrd',
        'HOST': 'localhost',
        'PORT': '',
    }
}</code></pre>

Before we can connect to PostgreSQL, we need to install `<mark style="background-color:#abb8c3" class="has-inline-color">psycopg2</mark>`. 

<pre class="wp-block-code"><code>pip install psycopg2-binary</code></pre>

Add it to the requirements.txt file 

### Create superuser  {.wp-block-heading}

To create a superuser in Django we use the following command. Make sure to follow the prompts in order to create a user, email, and password

<pre class="wp-block-code"><code>python manage.py createsupeuser</code></pre>

Add some data into the tables to play with.

### 5. Views, models, and Urls {.wp-block-heading}

Let&#8217;s start with creating models. Models give us the data and how our database will be structured. For this, I will use a simple example.

<pre class="wp-block-code"><code>#models 
from django.db import models
import uuid

class Project(models.Model):
    title = models.CharField(max_length=255)
    description = models.TextField(null=True, blank=True)
    id = models.UUIDField(default=uuid.uuid4, unique=True, primary_key=True, editable=False)

    def __str__(self):
        return self.title</code></pre>

After this make sure you run `<mark style="background-color:#abb8c3" class="has-inline-color">makemigrations and migrate</mark>` in order to populate the database.

<pre class="wp-block-code"><code>$ python manage.py makemigrations
$ python manage.py migrate</code></pre>

### Create urls.py and populate {.wp-block-heading}

We need to create a `<mark style="background-color:#abb8c3" class="has-inline-color">urls.py</mark>` file inside our app because Django doesn&#8217;t create it for us. 

Your project/urls.py file should look like this 

<pre class="wp-block-code"><code>from django.urls import path
from . import views

urlpatterns = &#91;
]</code></pre>

For the main **myproject/urls.py** it should look like this 

<pre class="wp-block-code"><code>from django.contrib import admin
from django.urls import path, include

urlpatterns = &#91;
    path('admin/', admin.site.urls),
    path('', include('project.urls'))# add this to serve the app urls 
]</code></pre>

### Create Views  {.wp-block-heading}

This is the engine of your project. To create views use the following

<pre class="wp-block-code"><code>from django.shortcuts import render

def index(request):
    context = {   
    }
    return render(request, 'project/index.html', context)</code></pre>

### 5. Django Templates  {.wp-block-heading}

From this part, we need to create templates and from the beginning, I had cover templates. <a href="https://nextgentips.com/2022/05/24/how-to-create-a-django-project-using-templates/" target="_blank" rel="noreferrer noopener">How to create Django Templates</a>.

Create a templates folder and add all the HTML in it. Create an index.html, projectfile.html, and delete.html files.

### 6. CRUD functionalities  {.wp-block-heading}

### Read Functionality  {.wp-block-heading}

We can start with Read functionalities. Let&#8217;s display what is in the database in our home. 

Remember the view we had created earlier, we are going to work from there. To understand more make sure you play with <a href="https://docs.djangoproject.com/en/4.1/topics/db/queries/" target="_blank" rel="noreferrer noopener">Django ORM</a>. 

This will be our final views.py for Read operations.

<pre class="wp-block-code"><code>from django.shortcuts import render
from .models import Project

def index(request):
    projects = Project.objects.all()# fetch all the data from the database

    context = {
        'projects': projects
    }
    return render(request, 'project/index.html', context)</code></pre>

### Create functionality {.wp-block-heading}

To use create functionality, we need to have a form where we input our data. So now head over to our app and add **forms.py** file and populate it with the following:

<pre class="wp-block-code"><code>from django.forms import ModelForm

from app.models import Project


class ProjectForm(ModelForm):
    class Meta:
        model = Project
        fields = '__all__'
       </code></pre>

Then go to **views.py** file and add this also

<pre class="wp-block-code"><code>def createProject(request):
    form = ProjectForm()

    if request.method == 'POST':
        form = ProjectForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('home')
    context = {
        'form': form
    }
    return render(request, 'project/projectfile.html', context)</code></pre>

Create urls to serve this view like this:

<pre class="wp-block-code"><code>from django.urls import path
from . import views

urlpatterns = &#91;
    path('', views.index, name='home'),
    path('create/', views.createProject, name='create-file'),
]</code></pre>

Create **Projectfile.html** and populate with this:

<pre class="wp-block-code"><code>&lt;form method="POST" action=""&gt;
    {% csrf_token %}

    {{ form.as_p }}

    &lt;input type="submit" value="Submit"&gt;
&lt;/form&gt;</code></pre>

### Update functionality {.wp-block-heading}

We are still going to use the form we created earlier. What is different only is the addition of a primary key to help us know the section we are updating.

On **views.py** file add the following

<pre class="wp-block-code"><code>def updateProject(request, pk):
    project = Project.objects.get(id=pk)
    form = ProjectForm(instance=project)

    if request.method == 'POST':
        form = ProjectForm(request.POST, instance=project)
        if form.is_valid():
            form.save()
            return redirect('home')

    context = {
        'form': form
    }
    return render(request, 'project/projectfile.html', context)</code></pre>

Here I am still using the same projectfile.html file because its the same form am working on.

On project/urls.py file add this URL link.

<pre class="wp-block-code"><code>path('update/&lt;str:pk&gt;/', views.updateProject, name='update-file'),</code></pre>

Inside index.html file we need to add the edit link so that we can redirect to where we need to edit.

This should be the final index.html file.

<pre class="wp-block-code"><code>&lt;h1&gt;Projects&lt;/h1&gt;
 
&lt;table&gt;
    &lt;thead&gt;
    &lt;tr&gt;

        &lt;th&gt;Title&lt;/th&gt;
        &lt;th&gt;Description&lt;/th&gt;
        &lt;th&gt;Action&lt;/th&gt;
        &lt;th&gt;Action&lt;/th&gt;
    &lt;/tr&gt;
    &lt;/thead&gt;

    &lt;tbody&gt;
    {% for project in projects %} # looping inside the function in django using tags 
    &lt;tr&gt;

        &lt;td&gt;{{project.title }}&lt;/td&gt;
        &lt;td&gt;{{project.description }}&lt;/td&gt;
        &lt;td&gt;&lt;a href="{% url 'update-file' project.id %}"&gt;Edit&lt;/a&gt;&lt;/td&gt;
        &lt;td&gt;&lt;a href="{% url 'delete-file' project.id %}"&gt;Delete&lt;/a&gt;&lt;/td&gt;
    &lt;/tr&gt;
    {% endfor %}
    &lt;/tbody&gt;
&lt;/table&gt;</code></pre>

### Delete functionality {.wp-block-heading}

Delete functionality is the last and easiest of the three above. What we need to have is the deletefile.html and delete project view.

Let&#8217;s start with the deletefile.html. Populate it with the following.

<pre class="wp-block-code"><code>&lt;h1&gt;Delete&lt;/h1&gt;

&lt;form method="POST" action=""&gt;
    {% csrf_token %}
    &lt;p&gt;Are you sure you want to delete "{{object}}"?&lt;/p&gt;
    &lt;a href="{% url 'home' %}"&gt;Go back&lt;/a&gt;# using url tags 

    &lt;input type="submit" value="Delete"&gt;
&lt;/form&gt;</code></pre>

Our URL is this.

<pre class="wp-block-code"><code>path('delete/&lt;str:pk&gt;/', views.deleteProject, name='delete-file')</code></pre>

Create a delete view like this 

<pre class="wp-block-code"><code>def deleteProject(request, pk):
    project = Project.objects.get(id=pk)
    if request.method == 'POST':
        project.delete()
        return redirect('home')
    context = {
        'object': project
    }

    return render(request, 'project/deletefile.html', context)</code></pre>

## Conclusion  {.wp-block-heading}

We have successfully implemented the CRUD operation in Django. In case of any difficulty refer to my <a href="https://github.com/sangkips/crud-operation" target="_blank" rel="noreferrer noopener">Github repo</a> for the full codebase.