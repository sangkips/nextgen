---
title: How to install and configure Flatpak on Ubuntu 20.04
author: Kipkoech Sang
type: post
date: 2021-11-22T12:44:02+00:00
url: /2021/11/22/how-to-install-and-configure-flatpak-on-ubuntu-20-04/
rank_math_news_sitemap_robots:
  - index
rank_math_robots:
  - 'a:1:{i:0;s:5:"index";}'
rank_math_analytic_object_id:
  - 91
rank_math_internal_links_processed:
  - 1
categories:
  - Linux

---
In this tutorial, we are going to learn how to install Flatpak on Ubuntu 20.04.

Flatpak is a utility for software deployment and package management for Linux. Flatpak offers a sandbox environment in which users can run application software in isolation from the rest of the system.

Flatpak can be used by all kinds of desktop environments and aims to be agnostic as possible regarding how applications are built. 

## Prerequisites  {.wp-block-heading}

  * Have Ubuntu 20.04 server up and running 
  * User with sudo privileges 
  * Have basic knowlege of terminal commands 

## Table of Contents {.wp-block-heading}

  1. Run system updates 
  2. Add necessary repository into the system
  3. Install Flatpak
  4. Install Flatpak support for GNOME
  5. Reboot the system
  6. Conclusion

## 1. Run system updates  {.wp-block-heading}

To begin our installation, we need to run system-wide updates, Updates are necessary for every system in order to make the system packages up to date. 

<pre class="wp-block-code"><code>$ sudo apt update
$ sudo apt upgrade -y</code></pre>

After the updates are complete we can proceed to install the repository necessary for Flatpak installation.

## 2. Add alexlarsson/flatpak PPA repository into the system. {.wp-block-heading}

We need to install the following PPA into our repository. This will fetch the installation packages from that repository and install them into our system. Use the following command to do so;

<pre class="wp-block-code"><code>$ sudo add-apt-repository ppa:alexlarsson/flatpak</code></pre>

You will get the following output;

<pre class="wp-block-code"><code>Linux application sandboxing and distribution framework
 More info: https://launchpad.net/~alexlarsson/+archive/ubuntu/flatpak
Press &#91;ENTER] to continue or Ctrl-c to cancel adding it.

Hit:1 http://security.ubuntu.com/ubuntu focal-security InRelease
Hit:2 http://ppa.launchpad.net/alexlarsson/flatpak/ubuntu focal InRelease                                       
Hit:3 https://repos-droplet.digitalocean.com/apt/droplet-agent main InRelease                                   
Hit:4 http://mirrors.digitalocean.com/ubuntu focal InRelease                          
Hit:5 http://mirrors.digitalocean.com/ubuntu focal-updates InRelease
Hit:6 http://mirrors.digitalocean.com/ubuntu focal-backports InRelease
Reading package lists... Done</code></pre>

You can run system updates again to effect the changes introduced.

Now we can install Flatpak after updates are complete.

## 3. Install Flatpak {.wp-block-heading}

Flatpak can be installed after all the prerequisites have been met. To install Flatpak run the following command in your terminal;

<pre class="wp-block-code"><code>$ sudo apt install flatpak</code></pre>

Press Y to accept the instalaltion. 

<pre class="wp-block-code"><code>Sample output
Reading package lists... Done
Building dependency tree       
Reading state information... Done
The following additional packages will be installed:
  adwaita-icon-theme at-spi2-core bubblewrap desktop-file-utils fontconfig fontconfig-config fonts-dejavu-core
  gnome-desktop3-data gtk-update-icon-cache hicolor-icon-theme humanity-icon-theme libappstream-glib8
  libatk-bridge2.0-0 libatk1.0-0 libatk1.0-data libatspi2.0-0 libavahi-client3 libavahi-common-data
  libavahi-common3 libavahi-glib1 libcairo-gobject2 libcairo2 libcolord2 libcups2 libdatrie1 libepoxy0
  libfontconfig1 libgdk-pixbuf2.0-0 libgdk-pixbuf2.0-bin libgdk-pixbuf2.0-common libgnome-desktop-3-19
  libgraphite2-3 libgtk-3-0 libgtk-3-bin libgtk-3-common libharfbuzz0b libjbig0 libjpeg-turbo8 libjpeg8
  liblcms2-2 libostree-1-1 libpango-1.0-0 libpangocairo-1.0-0 libpangoft2-1.0-0 libpixman-1-0 librest-0.7-0
  librsvg2-2 librsvg2-common libsoup-gnome2.4-1 libthai-data libthai0 libtiff5 libwayland-client0
  libwayland-cursor0 libwayland-egl1 libwebp6 libxcb-render0 libxcb-shm0 libxcomposite1 libxcursor1 libxdamage1
  libxfixes3 libxi6 libxinerama1 libxkbcommon0 libxrandr2 libxrender1 libxtst6 p11-kit p11-kit-modules
  ubuntu-mono x11-common xdg-desktop-portal xdg-desktop-portal-gtk
Suggested packages:
  avahi-daemon colord cups-common gvfs liblcms2-utils librsvg2-bin evince
The following NEW packages will be installed:
  adwaita-icon-theme at-spi2-core bubblewrap desktop-file-utils flatpak fontconfig fontconfig-config
  fonts-dejavu-core gnome-desktop3-data gtk-update-icon-cache hicolor-icon-theme humanity-icon-theme
  libappstream-glib8 libatk-bridge2.0-0 libatk1.0-0 libatk1.0-data libatspi2.0-0 libavahi-client3
  libavahi-common-data libavahi-common3 libavahi-glib1 libcairo-gobject2 libcairo2 libcolord2 libcups2
  libdatrie1 libepoxy0 libfontconfig1 libgdk-pixbuf2.0-0 libgdk-pixbuf2.0-bin libgdk-pixbuf2.0-common
  libgnome-desktop-3-19 libgraphite2-3 libgtk-3-0 libgtk-3-bin libgtk-3-common libharfbuzz0b libjbig0
  libjpeg-turbo8 libjpeg8 liblcms2-2 libostree-1-1 libpango-1.0-0 libpangocairo-1.0-0 libpangoft2-1.0-0
  libpixman-1-0 librest-0.7-0 librsvg2-2 librsvg2-common libsoup-gnome2.4-1 libthai-data libthai0 libtiff5
  libwayland-client0 libwayland-cursor0 libwayland-egl1 libwebp6 libxcb-render0 libxcb-shm0 libxcomposite1
  libxcursor1 libxdamage1 libxfixes3 libxi6 libxinerama1 libxkbcommon0 libxrandr2 libxrender1 libxtst6 p11-kit
  p11-kit-modules ubuntu-mono x11-common xdg-desktop-portal xdg-desktop-portal-gtk
0 upgraded, 75 newly installed, 0 to remove and 0 not upgraded.
Need to get 17.9 MB of archives.
After this operation, 84.9 MB of additional disk space will be used.
Do you want to continue? &#91;Y/n] y</code></pre>

After the installation, we can confirm the version of Flatpak installed.

<pre class="wp-block-code"><code>$ flatpak --version
Flatpak 1.11.2</code></pre>

Next, we can install Flatpak GNOME support because Flatpak runs on the desktop environment

## 4. Install Flatpak GNOME packages  {.wp-block-heading}

<pre class="wp-block-code"><code>$ sudo apt install gnome-software-plugin-flatpak</code></pre>

This process will install many dependencies, wait for them to complete.

<pre class="wp-block-code"><code>Sample output
Setting up libcairo-gobject-perl (1.005-2) ...
Setting up libglib-object-introspection-perl (0.048-2build1) ...
Setting up libgtk3-perl (0.037-1) ...
Processing triggers for desktop-file-utils (0.24-1ubuntu3) ...
Processing triggers for mime-support (3.64ubuntu1) ...
Processing triggers for hicolor-icon-theme (0.17-2) ...
Processing triggers for libglib2.0-0:amd64 (2.64.6-1~ubuntu20.04.4) ...
Processing triggers for libc-bin (2.31-0ubuntu9.2) ...
Processing triggers for man-db (2.9.1-1) ...
Setting up pinentry-gnome3 (1.1.0-3build1) ...
Processing triggers for dbus (1.12.16-2ubuntu2.1) ...
Processing triggers for shared-mime-info (1.15-1) ...
Setting up libgoa-1.0-0b:amd64 (3.36.1-0ubuntu1) ...
Setting up gnome-startup-applications (3.36.0-2ubuntu1) ...
Setting up gnome-keyring (3.36.0-1ubuntu1) ...
Setting up gir1.2-goa-1.0:amd64 (3.36.1-0ubuntu1) ...
Setting up software-properties-gtk (0.99.9.8) ...
Setting up gnome-software (3.36.1-0ubuntu0.20.04.0) ...
Setting up gnome-software-plugin-flatpak (3.36.1-0ubuntu0.20.04.0) ...
Setting up gnome-software-plugin-snap (3.36.1-0ubuntu0.20.04.0) ...</code></pre>

## 5. Reboot the system. {.wp-block-heading}

We can reboot the system so that changes can take effect. Use the following command;

<pre class="wp-block-code"><code>$ sudo reboot -n</code></pre>

Now we can explore how to use Flatpak.

## 6. Flatpak usage {.wp-block-heading}

Flatpak can be used like other packages used for running installation such as snap. We use Flatpak like this;

<pre class="wp-block-code"><code>$ flatpak &#91;OPTION] command</code></pre>

To start installing applications using Flatpak we need to instruct Flatpak to retrieve them from the [Flathub][1] store. First, add Flathub remote repository to the system then you now install apps using Flatpak.

<pre class="wp-block-code"><code>$ sudo flatpak remote-add --if-not-exists flathub https://flathub.org/repo/flathub.flatpakrepo</code></pre>

Now you can install any app found on Flathub. Let&#8217;s say we want to install Vlc on our system using Flatpak, do the following;

<pre class="wp-block-code"><code>$ flatpak install flathub org.videolan.VLC</code></pre>

<pre class="wp-block-code"><code>Sample output
Looking for matches…
Required runtime for org.videolan.VLC/x86_64/stable (runtime/org.kde.Platform/x86_64/5.15-21.08) found in remote flathub
Do you want to install it? &#91;Y/n]: y

org.videolan.VLC permissions:
    ipc                    network                pulseaudio              x11       devices
    file access &#91;1]        dbus access &#91;2]        bus ownership &#91;3]

    &#91;1] host, xdg-config/kdeglobals:ro, xdg-run/gvfs
    &#91;2] com.canonical.AppMenu.Registrar, org.freedesktop.Notifications, org.freedesktop.ScreenSaver,
        org.freedesktop.secrets, org.kde.kwalletd, org.kde.kwalletd5, org.mpris.MediaPlayer2.Player
    &#91;3] org.mpris.MediaPlayer2.vlc


        ID                                         Branch           Op       Remote        Download
        ID                                         Branch           Op       Remote        Download
 1. &#91;✓] org.freedesktop.Platform.GL.default        21.08            i        flathub       128.3 MB / 128.6 MB
 2. &#91;✓] org.freedesktop.Platform.openh264          2.0              i        flathub         1.5 MB / 1.5 MB
 3. &#91;✓] org.kde.KStyle.Adwaita                     5.15-21.08       i        flathub         6.7 MB / 6.7 MB
 4. &#91;✓] org.kde.Platform.Locale                    5.15-21.08       i        flathub       153.8 MB / 344.1 MB
 5. &#91;✓] org.kde.Platform                           5.15-21.08       i        flathub       240.0 MB / 302.3 MB
 6. &#91;✓] org.videolan.VLC.Locale                    stable           i        flathub        13.5 MB / 13.4 MB
 7. &#91;✓] org.videolan.VLC                           stable           i        flathub        24.0 MB / 28.5 MB

Installing 7/7… ████████████████████ 100%  24.0 MB/s</code></pre>

Follow the prompts to accept the installation of Vlc.

To run the application using Flatpak, you can run it like this:

<pre class="wp-block-code"><code>$ flatpak run org.videolan.VLC</code></pre>

## 7. Conclusion {.wp-block-heading}

We have successfully installed and run apps using Flatpak on Ubuntu 20.04. Continue exploring to learn more.

 [1]: https://flathub.org/home