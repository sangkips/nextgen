---
title: How to install Flatpak on Ubuntu 22.04
author: Kipkoech Sang
type: post
date: 2022-06-19T09:50:58+00:00
url: /2022/06/19/how-to-install-flatpak-on-ubuntu-22-04/
rank_math_internal_links_processed:
  - 1
rank_math_seo_score:
  - 58
rank_math_focus_keyword:
  - flatpak on ubuntu 22.04,flatpak
rank_math_primary_category:
  - 6
rank_math_analytic_object_id:
  - 185
categories:
  - Linux

---
Flatpak is a utility for software deployment and package management for Linux. Flatpak offers a sandbox environment in which users can run application software in isolation from the rest of the system.

Flatpak can be used by all types of desktop environments and aims to be as agnostic as possible regarding how applications are built.

In this tutorial, we are going to learn how to install Flatpak on Ubuntu 22.04.

## Prerequisites {.wp-block-heading}

  * Have Ubuntu 22.04 server up and running
  * User with sudo privileges
  * Have basic knowledge of terminal commands

## Table of Contents {.wp-block-heading}

  1. Run system updates
  2. Install Flatpak
  3. Install Flatpak support for GNOME
  4. Reboot the system
  5. Conclusion

### 1. Run system updates {.wp-block-heading}

To begin our installation, we need to run system-wide updates, Updates are necessary for every system in order to make the system packages up to date.

<pre class="wp-block-code"><code>$ sudo apt update && apt upgrade -y</code></pre>

After the updates are complete we can proceed to install the Flatpak

### 2. Install Flatpak {.wp-block-heading}

Flatpak can be installed after all the prerequisites have been met. To install Flatpak run the following command in your terminal;

<pre class="wp-block-code"><code>$ sudo apt install flatpak</code></pre>

Press Y to accept the instalaltion.

<pre class="wp-block-code"><code>Sample output  
The following additional packages will be installed:
  adwaita-icon-theme alsa-topology-conf alsa-ucm-conf at-spi2-core bubblewrap dconf-gsettings-backend dconf-service desktop-file-utils
  fontconfig fontconfig-config fonts-dejavu-core glib-networking glib-networking-common glib-networking-services gsettings-desktop-schemas
  gtk-update-icon-cache hicolor-icon-theme humanity-icon-theme libappstream-glib8 libasound2 libasound2-data libasyncns0 libatk-bridge2.0-0
  libatk1.0-0 libatk1.0-data libatspi2.0-0 libavahi-client3 libavahi-common-data libavahi-common3 libavahi-glib1 libcairo-gobject2
  libcairo2 libcanberra0 libcolord2 libcups2 libdatrie1 libdconf1 libdeflate0 libepoxy0 libflac8 libfontconfig1 libfuse2
  libgdk-pixbuf-2.0-0 libgdk-pixbuf2.0-bin libgdk-pixbuf2.0-common libgraphite2-3 libgtk-3-0 libgtk-3-bin libgtk-3-common libharfbuzz0b
  libjbig0 libjpeg-turbo8 libjpeg8 liblcms2-2 libltdl7 libmalcontent-0-0 libogg0 libopus0 libostree-1-1 libpango-1.0-0 libpangocairo-1.0-0
  libpangoft2-1.0-0 libpipewire-0.3-0 libpipewire-0.3-common libpipewire-0.3-modules libpixman-1-0 libproxy1v5 libpulse0 librsvg2-2
  librsvg2-common libsndfile1 libsoup2.4-1 libsoup2.4-common libspa-0.2-modules libtdb1 libthai-data libthai0 libtiff5 libvorbis0a
  libvorbisenc2 libvorbisfile3 libwayland-client0 libwayland-cursor0 libwayland-egl1 libwebp7 libwebrtc-audio-processing1 libx11-xcb1
  libxcb-render0 libxcb-shm0 libxcomposite1 libxcursor1 libxdamage1 libxfixes3 libxi6 libxinerama1 libxkbcommon0 libxrandr2 libxrender1
  libxtst6 p11-kit p11-kit-modules pipewire pipewire-bin pipewire-media-session rtkit session-migration sound-theme-freedesktop ubuntu-mono
  x11-common xdg-dbus-proxy xdg-desktop-portal xdg-desktop-portal-gtk
Suggested packages:
  avahi-daemon malcontent-gui libasound2-plugins alsa-utils libcanberra-gtk0 libcanberra-pulse colord cups-common gvfs liblcms2-utils
  opus-tools pulseaudio librsvg2-bin accountsservice evince xdg-desktop-portal-gnome</code></pre>

After the installation, we can confirm the version of Flatpak installed.

<pre class="wp-block-code"><code>$ flatpak --version
Flatpak 1.12.7</code></pre>

Next, we can install Flatpak GNOME support because Flatpak runs on the desktop environment

### 3. Install Flatpak GNOME packages {.wp-block-heading}

<pre class="wp-block-code"><code>$ sudo apt install gnome-software-plugin-flatpak</code></pre>

This process will install many dependencies, and wait for them to complete.

<pre class="wp-block-code"><code>Sample output
appstream apt-config-icons aspell aspell-en dictionaries-common emacsen-common enchant-2 gcr gir1.2-atk-1.0 gir1.2-freedesktop
  gir1.2-gdkpixbuf-2.0 gir1.2-goa-1.0 gir1.2-gtk-3.0 gir1.2-handy-1 gir1.2-harfbuzz-0.0 gir1.2-pango-1.0 gir1.2-snapd-1 gnome-desktop3-data
  gnome-keyring gnome-keyring-pkcs11 gnome-session-bin gnome-session-common gnome-software gnome-software-common gnome-software-plugin-snap
  gnome-startup-applications hunspell-en-us libaspell15 libcairo-gobject-perl libcairo-perl libdrm-amdgpu1 libdrm-intel1 libdrm-nouveau2
  libdrm-radeon1 libegl-mesa0 libegl1 libenchant-2-2 libextutils-depends-perl libflatpak0 libgbm1 libgck-1-0 libgcr-base-3-1 libgcr-ui-3-1
  libgl1 libgl1-amber-dri libgl1-mesa-dri libglapi-mesa libgles2 libglib-object-introspection-perl libglib-perl libglvnd0 libglx-mesa0
  libglx0 libgnome-desktop-3-19 libgoa-1.0-0b libgoa-1.0-common libgspell-1-2 libgspell-1-common libgtk3-perl libhandy-1-0
  libhunspell-1.7-0 libice6 libimobiledevice6 libllvm13 libpam-gnome-keyring libpangoxft-1.0-0 libpciaccess0 libplist3 libsecret-1-0
  libsecret-common libsensors-config libsensors5 libsm6 libsnapd-glib1 libupower-glib3 libusbmuxd6 libvulkan1 libwayland-server0
  libxcb-dri2-0 libxcb-dri3-0 libxcb-glx0 libxcb-present0 libxcb-randr0 libxcb-sync1 libxcb-xfixes0 libxft2 libxkbregistry0 libxshmfence1
  libxxf86vm1 mesa-vulkan-drivers pinentry-gnome3 python3-dateutil python3-software-properties python3-xkit software-properties-common
  software-properties-gtk ubuntu-advantage-desktop-daemon ubuntu-drivers-common upower usbmuxd
Suggested packages:
  aspell-doc spellutils wordlist apt-config-icons-hidpi hunspell openoffice.org-hunspell | openoffice.org-core libfont-freetype-perl
  libenchant-2-voikko libxml-libxml-perl libusbmuxd-tools lm-sensors pinentry-doc python3-aptdaemon.pkcompat
The following NEW packages will be installed:
  appstream apt-config-icons aspell aspell-en dictionaries-common emacsen-common enchant-2 gcr gir1.2-atk-1.0 gir1.2-freedesktop
  gir1.2-gdkpixbuf-2.0 gir1.2-goa-1.0 gir1.2-gtk-3.0 gir1.2-handy-1 gir1.2-harfbuzz-0.0 gir1.2-pango-1.0 gir1.2-snapd-1 gnome-desktop3-data
  gnome-keyring gnome-keyring-pkcs11 gnome-session-bin gnome-session-common gnome-software gnome-software-common
  gnome-software-plugin-flatpak gnome-software-plugin-snap gnome-startup-applications hunspell-en-us libaspell15 libcairo-gobject-perl
  libcairo-perl libdrm-amdgpu1 libdrm-intel1 libdrm-nouveau2 libdrm-radeon1 libegl-mesa0 libegl1 libenchant-2-2 libextutils-depends-perl
  libflatpak0 libgbm1 libgck-1-0 libgcr-base-3-1 libgcr-ui-3-1 libgl1 libgl1-amber-dri libgl1-mesa-dri libglapi-mesa libgles2
  libglib-object-introspection-perl libglib-perl libglvnd0 libglx-mesa0 libglx0 libgnome-desktop-3-19 libgoa-1.0-0b libgoa-1.0-common
  libgspell-1-2 libgspell-1-common libgtk3-perl libhandy-1-0 libhunspell-1.7-0 libice6 libimobiledevice6 libllvm13 libpam-gnome-keyring
  libpangoxft-1.0-0 libpciaccess0 libplist3 libsecret-1-0 libsecret-common libsensors-config libsensors5 libsm6 libsnapd-glib1
  libupower-glib3 libusbmuxd6 libvulkan1 libwayland-server0 libxcb-dri2-0 libxcb-dri3-0 libxcb-glx0 libxcb-present0 libxcb-randr0
  libxcb-sync1 libxcb-xfixes0 libxft2 libxkbregistry0 libxshmfence1 libxxf86vm1 mesa-vulkan-drivers pinentry-gnome3 python3-dateutil
  python3-xkit software-properties-gtk ubuntu-advantage-desktop-daemon ubuntu-drivers-common upower usbmuxd
The following packages will be upgraded:
  python3-software-properties software-properties-common
2 upgraded, 99 newly installed, 0 to remove and 53 not upgraded.
Need to get 46.4 MB of archives.
After this operation, 196 MB of additional disk space will be used.
Do you want to continue? &#91;Y/n] y</code></pre>

### 4. Reboot the system. {.wp-block-heading}

We can reboot the system so that changes can take effect. Use the following command;

<pre class="wp-block-code"><code>$ sudo reboot -n</code></pre>

Now we can explore how to use Flatpak.

### 5. Flatpak usage {.wp-block-heading}

Flatpak can be used like other packages used for running installation such as snap. We use Flatpak like this;

<pre class="wp-block-code"><code>$ flatpak &#91;OPTION] command</code></pre>

To start installing applications using Flatpak we need to instruct Flatpak to retrieve them from the <a href="https://flathub.org/home" target="_blank" rel="noreferrer noopener">Flathub </a>store. First, add Flathub remote repository to the system then you now install apps using Flatpak.

<pre class="wp-block-code"><code>$ sudo flatpak remote-add --if-not-exists flathub https://flathub.org/repo/flathub.flatpakrepo</code></pre>

Now you can proceed to install any app found on Flathub. Let’s say we want to install Vlc on our system using Flatpak, we can do the following;

<pre class="wp-block-code"><code>$ flatpak install flathub org.videolan.VLC</code></pre>

<pre class="wp-block-code"><code>Sample output
Looking for matches…
Required runtime for org.videolan.VLC/x86_64/stable (runtime/org.kde.Platform/x86_64/5.15-21.08) found in remote flathub
Do you want to install it? &#91;Y/n]: y

org.videolan.VLC permissions:
    ipc                    network                  pulseaudio       x11       devices       file access &#91;1]
    dbus access &#91;2]        bus ownership &#91;3]

    &#91;1] host, xdg-config/kdeglobals:ro, xdg-run/gvfs
    &#91;2] com.canonical.AppMenu.Registrar, org.freedesktop.Notifications, org.freedesktop.ScreenSaver, org.freedesktop.secrets,
        org.kde.kconfig.notify, org.kde.kwalletd, org.kde.kwalletd5, org.mpris.MediaPlayer2.Player
    &#91;3] org.mpris.MediaPlayer2.vlc


        ID                                             Branch                Op            Remote             Download
        ID                                             Branch                Op            Remote             Download
 1. &#91;✓] org.freedesktop.Platform.GL.default            21.08                 i             flathub            132.0 MB / 131.3 MB
 2. &#91;✓] org.freedesktop.Platform.openh264              2.0                   i             flathub              1.8 MB / 1.5 MB
 3. &#91;✓] org.kde.KStyle.Adwaita                         5.15-21.08            i             flathub              6.7 MB / 6.6 MB
 4. &#91;✓] org.kde.Platform.Locale                        5.15-21.08            i             flathub            162.7 MB / 345.4 MB
 5. &#91;✓] org.kde.Platform                               5.15-21.08            i             flathub            252.7 MB / 306.1 MB
 6. &#91;✓] org.videolan.VLC.Locale                        stable                i             flathub             13.5 MB / 13.4 MB
 7. &#91;✓] org.videolan.VLC                               stable                i             flathub             39.3 MB / 31.5 MB

Installation complete.</code></pre>

Follow the prompts to accept the installation of Vlc.

To run the application using Flatpak, you can run it like this:

<pre class="wp-block-code"><code>$ flatpak run org.videolan.VLC</code></pre>

## Conclusion {.wp-block-heading}

We have successfully installed and run apps using Flatpak on Ubuntu 22.04. Continue exploring to learn more.