---
title: How to install Grafana 8 on Ubuntu 21.04
author: Kipkoech Sang
type: post
date: 2021-10-30T20:28:38+00:00
url: /2021/10/30/how-to-install-grafana-8-on-ubuntu-21-04/
ocean_gallery_link_images:
  - off
ocean_disable_margins:
  - enable
ocean_display_top_bar:
  - default
ocean_display_header:
  - default
ocean_disable_title:
  - default
ocean_disable_heading:
  - default
ocean_disable_breadcrumbs:
  - default
ocean_display_footer_widgets:
  - default
ocean_display_footer_bottom:
  - default
ocean_link_format_target:
  - self
ocean_quote_format_link:
  - post
rank_math_news_sitemap_robots:
  - index
rank_math_robots:
  - 'a:1:{i:0;s:5:"index";}'
rank_math_analytic_object_id:
  - 116
rank_math_internal_links_processed:
  - 1
categories:
  - Monitoring

---
In this tutorial guide we are going to learn how to install Grafana 8 on Ubuntu 21.04 server. Grafana is a complete observability stack that allows you to monitor and analyze logs, metrics and traces.

Grafana allows you to query, visualize, alert on and understand your data insight. Grafana can create, explore and create beautiful dashboards that can be shared with your teams.

## Related Contents {.wp-block-heading}

  * [How to install Nagios Core on Ubuntu 20.04][1]
  * [How to install Prometheus 2.30.3 Server in Ubuntu 20.04][2]

## Grafana Components {.wp-block-heading}

  * **Grafana Open source**. This is an open source visualization and analytics software. It allows one to query, visualize, alert on and check for traces every where. It provides you with tools to turn your time series database to impactful graphs which are easy to interpret.
  * **Grafana Loki.** It is an open source set of components that can be composed into a fully featured logging stack.
  * **Grafana Tembo.** This is an open source easy to use and high volume distributed tracing backend 

## Why Grafana? {.wp-block-heading}

Grafana have been widely adopted by big companies due to the following reasons:

  * Grafana provides data for everyone. Grafana was built with a philosophy that data should be accessible to everyone. By providing data to everyone it provides a way that everyone can give opinion and recommendations of what should be done with regard to the datasets available to everyone.
  * Grafana unify your data, not databases. Grafana provides a way of unifying all your data wherever it exists and show them in a single dashboard.
  * Grafana provides for data flexibility and versatility. Grafana translates and transform any of your data into flexible and versatile dashboards. Grafana allows you to build dashboards specifically for you according to data available. You can create customized data according to your needs.
  * Grafana provides dashboards for everyone. Grafana provides you with an option to share your dashboard with every team, providing good decision making.

## Grafana Features  {.wp-block-heading}

Grafana has many features such as:

  * **Dashboard templating**. Dashboards allows users to build visualization graphs that is shareable with other groups. Dashboard is the most convenient way to create all the data pool that can be adhered easily.
  * **Annotations**. This feature allows you to mark graphs so that in case of a problem you know where to return to. This features helps a lot in case their error with your tabulations.
  * **Kiosk mode and playlist.** playlist is good for rolling coverage. Let&#8217;s say you want to display your contents on a bigger screen, you will use playlist to do that, it can play all what you have all the time required. Kiosk mode on the other hand lets you show the interface elements that is needed in view-mode only.
  * **Alerting.** Grafana alerting is the most important feature because it let you know when something went wrong. Think of database error, it will alert you on the severance of the error and what measures need to be taken care of.
  * **Plugins.** You can extend grafana functionalities with the help of plugins. Get interface with other products through plugins. This has made it possible for grafana to be widely used.

## Installing Grafana on Ubuntu 21.04 {.wp-block-heading}

## 1. Run system update {.wp-block-heading}

To install grafana, we have to run system updates, to do that use the following command:

<pre class="wp-block-code"><code>$ sudo apt update</code></pre>

Afer the update is complete do run upgrade every package which might have been outdated. Use the following command to do so.

<pre class="wp-block-code"><code>$ sudo apt upgrade -y</code></pre>

## Install Software-properties-common {.wp-block-heading}

Software properties allow for easy management of PPA in the system. Use the following command to do so

<pre class="wp-block-code"><code>$ sudo apt install software-properties-common</code></pre>

<pre class="wp-block-code"><code># sudo apt install software-properties-common
Reading package lists... Done
Building dependency tree... Done
Reading state information... Done
software-properties-common is already the newest version (0.99.10).
software-properties-common set to manually installed.
The following package was automatically installed and is no longer required:
  net-tools
Use 'sudo apt autoremove' to remove it.
0 upgraded, 0 newly installed, 0 to remove and 0 not upgraded.</code></pre>

From the above we can see that software-properties-common is there by default on Ubuntu 21.04. Let&#8217;s go ahead and add GPG keys to Grafana.

## 2. Add Grafana to APT Repository {.wp-block-heading}

First we will need to add GPG keys to our APT sources keyring. Use the following command to accomplish this

<pre class="wp-block-code"><code>$ wget -q -O - https://packages.grafana.com/gpg.key | sudo apt-key add -</code></pre>

You will get ok as the ouput. After this we can now add Grafana to the APT repository. Use the following command to do so

<pre class="wp-block-code"><code>$ sudo add-apt-repository "deb https://packages.grafana.com/oss/deb stable main"</code></pre>

Press Enter to continue with the installation.

<pre class="wp-block-code"><code># sudo add-apt-repository "deb https://packages.grafana.com/oss/deb stable main"
Repository: 'deb https://packages.grafana.com/oss/deb stable main'
Description:
Archive for codename: stable components: main
More info: https://packages.grafana.com/oss/deb
Adding repository.
Press &#91;ENTER] to continue or Ctrl-c to cancel.</code></pre>

It is now high time to install Grafana into our system. We have completed all the prerequisites, but first we need to update our system for the new changes to take effect.

<pre class="wp-block-code"><code>$ sudo apt update</code></pre>

Then after the updates are complete install Grafana. Do this on the terminal

## 3. Install Grafana  {.wp-block-heading}

<pre class="wp-block-code"><code>$ sudo apt install grafana -y</code></pre>

**-y** instructs the system continue with installation without asking you to press enter in order to accept the installation.

## 4. Verify the installation {.wp-block-heading}

Use the following to verify your installation

<pre class="wp-block-code"><code>$ grafana-server -v</code></pre>

Sample output

<pre class="wp-block-code"><code># grafana-server -v
Version 8.2.2 (commit: 6232fe07c0, branch: HEAD)</code></pre>

## Start Grafana Service  {.wp-block-heading}

Grafana needs to up and running for it to help us with monitoring, do start the service with the following command

<pre class="wp-block-code"><code>$ sudo systemctl start grafana-server</code></pre>

Enable the service to run on start with the following command:

<pre class="wp-block-code"><code>$ sudo systemctl enable grafana-server </code></pre>

Sample Output

<pre class="wp-block-code"><code># sudo systemctl enable grafana-server
Synchronizing state of grafana-server.service with SysV service script with /lib/systemd/systemd-sysv-install.
Executing: /lib/systemd/systemd-sysv-install enable grafana-server
Created symlink /etc/systemd/system/multi-user.target.wants/grafana-server.service → /lib/systemd/system/grafana-server.service.</code></pre>

You can verify the status of the Grafana with the following comand:

<pre class="wp-block-code"><code>$ sudo systemctl status grafana-server</code></pre>

Sample Output

<pre class="wp-block-code"><code># sudo systemctl status grafana-server
● grafana-server.service - Grafana instance
     Loaded: loaded (/lib/systemd/system/grafana-server.service; disabled; vendor preset: enabled)
     Active: active (running) since Sat 2021-10-30 19:42:42 UTC; 2min 40s ago
       Docs: http://docs.grafana.org
   Main PID: 2000 (grafana-server)
      Tasks: 8 (limit: 2330)
     Memory: 26.9M
     CGroup: /system.slice/grafana-server.service
             └─2000 /usr/sbin/grafana-server --config=/etc/grafana/grafana.ini --pidfile=/run/grafana/grafana-se&gt;

Oct 30 19:42:44 ubuntu-21 grafana-server&#91;2000]: t=2021-10-30T19:42:44+0000 lvl=info msg="Executing migration" lo&gt;
Oct 30 19:42:44 ubuntu-21 grafana-server&#91;2000]: t=2021-10-30T19:42:44+0000 lvl=info msg="migrations completed" l&gt;
Oct 30 19:42:44 ubuntu-21 grafana-server&#91;2000]: t=2021-10-30T19:42:44+0000 lvl=info msg="Created default admin" &gt;
Oct 30 19:42:44 ubuntu-21 grafana-server&#91;2000]: t=2021-10-30T19:42:44+0000 lvl=info msg="Created default organiz&gt;
Oct 30 19:42:44 ubuntu-21 grafana-server&#91;2000]: t=2021-10-30T19:42:44+0000 lvl=info msg="Starting plugin search"&gt;
Oct 30 19:42:44 ubuntu-21 grafana-server&#91;2000]: t=2021-10-30T19:42:44+0000 lvl=info msg="Registering plugin" log&gt;
Oct 30 19:42:44 ubuntu-21 grafana-server&#91;2000]: t=2021-10-30T19:42:44+0000 lvl=info msg="External plugins direct&gt;
Oct 30 19:42:44 ubuntu-21 grafana-server&#91;2000]: t=2021-10-30T19:42:44+0000 lvl=info msg="Live Push Gateway initi&gt;
Oct 30 19:42:44 ubuntu-21 grafana-server&#91;2000]: t=2021-10-30T19:42:44+0000 lvl=info msg="Writing PID file" logge&gt;
Oct 30 19:42:44 ubuntu-21 grafana-server&#91;2000]: t=2021-10-30T19:42:44+0000 lvl=info msg="HTTP Server Listen" log&gt;</code></pre>

Installation is complete for now but where do we from here.. You are wondering what if i want to access the user interface of the Grafana. Don&#8217;t worry. Grafana can be accessed through port 3000 on the browser. But wait will it work really? Check it out first: Type your IP address:3000 on your browser to see what happens.<figure class="wp-block-image size-full">

<img decoding="async" loading="lazy" width="690" height="299" src="https://i0.wp.com/nextgentips.com/wp-content/uploads/2021/10/Screenshot-from-2021-10-30-23-04-06.png?resize=690%2C299&#038;ssl=1" alt="" class="wp-image-479" srcset="https://i0.wp.com/nextgentips.com/wp-content/uploads/2021/10/Screenshot-from-2021-10-30-23-04-06.png?w=690&ssl=1 690w, https://i0.wp.com/nextgentips.com/wp-content/uploads/2021/10/Screenshot-from-2021-10-30-23-04-06.png?resize=300%2C130&ssl=1 300w" sizes="(max-width: 690px) 100vw, 690px" data-recalc-dims="1" /> </figure> 

It shows an errror because we have not allowed the rules on the firewall. So let&#8217;s allow tcp access to the firewall. We can use the following command:

<pre class="wp-block-code"><code>$ sudo ufw allow proto tcp from any to any port 3000</code></pre>

<pre class="wp-block-code"><code># sudo ufw allow proto tcp from any to any port 3000
Rules updated
Rules updated (v6)</code></pre>

when you type your address:3000 this time round Grafana interface will pop up as show below<figure class="wp-block-image size-full">

<img decoding="async" loading="lazy" width="731" height="547" src="https://i0.wp.com/nextgentips.com/wp-content/uploads/2021/10/Screenshot-from-2021-10-30-23-14-10.png?resize=731%2C547&#038;ssl=1" alt="" class="wp-image-480" srcset="https://i0.wp.com/nextgentips.com/wp-content/uploads/2021/10/Screenshot-from-2021-10-30-23-14-10.png?w=731&ssl=1 731w, https://i0.wp.com/nextgentips.com/wp-content/uploads/2021/10/Screenshot-from-2021-10-30-23-14-10.png?resize=300%2C224&ssl=1 300w" sizes="(max-width: 731px) 100vw, 731px" data-recalc-dims="1" /> </figure> 

Default username and password is **admin**, you will be created with the following Grafana interface.<figure class="wp-block-image size-large">

<img decoding="async" loading="lazy" width="810" height="464" src="https://i0.wp.com/nextgentips.com/wp-content/uploads/2021/10/Screenshot-from-2021-10-30-23-16-11-1024x586.png?resize=810%2C464&#038;ssl=1" alt="" class="wp-image-481" srcset="https://i0.wp.com/nextgentips.com/wp-content/uploads/2021/10/Screenshot-from-2021-10-30-23-16-11.png?resize=1024%2C586&ssl=1 1024w, https://i0.wp.com/nextgentips.com/wp-content/uploads/2021/10/Screenshot-from-2021-10-30-23-16-11.png?resize=300%2C172&ssl=1 300w, https://i0.wp.com/nextgentips.com/wp-content/uploads/2021/10/Screenshot-from-2021-10-30-23-16-11.png?resize=768%2C440&ssl=1 768w, https://i0.wp.com/nextgentips.com/wp-content/uploads/2021/10/Screenshot-from-2021-10-30-23-16-11.png?w=1294&ssl=1 1294w" sizes="(max-width: 810px) 100vw, 810px" data-recalc-dims="1" /> <figcaption>Grafana Dashboard</figcaption></figure> 

## Conclusion {.wp-block-heading}

Congratulations! You have installed Grafana monitoring tool and now you can start working on your project because you are covered with alerting tool. In case you find any problem always consult [Grafana Documentation.][3]

 [1]: https://nextgentips.com/2021/10/12/how-to-install-nagios-on-ubuntu-20-04/
 [2]: https://nextgentips.com/2021/10/07/how-to-install-prometheus-server-in-ubuntu-20-04/
 [3]: https://grafana.com/docs/grafana/latest/