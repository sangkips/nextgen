---
title: How to install Insomnia on Ubuntu 22.04
author: Kipkoech Sang
type: post
date: 2022-06-23T11:36:11+00:00
url: /2022/06/23/how-to-install-insomnia-on-ubuntu-22-04/
categories:
  - Linux

---
Insomnia is an open-source, cross-platform API client for GraphQL, REST, and gRPC. It&#8217;s a free cross-platform desktop application that takes the pain out of interacting with the designing HTTP-based APIs.

Insomnia is an API client that allows you to send requests outside your terminal without writing code. What you need to know in the API world are the **requests and endpoints.**

An **Endpoint** is a requestable URL. Think of it like this, you have a domain name and inside that domain name, there are a few routes pointers taking you to a certain page. For example, for `<strong>nextgentips.com</strong>`, we have an endpoint like `<strong>category/monitoring</strong>`.

A **Request** is a structured way to communicate with an API. The request makes use of the GET, POST, PUT, PATCH, and DELETE.

  * GET: returns information from an API.
  * POST: sends data entered and returns a confirmation through an HTTP response message.
  * PUT/PATCH: Modifies a record that has been requested by the user.
  * DELETE: Removes the record from the database

## Install Insomnia on Ubuntu 22.04 {.wp-block-heading}

### 1. Run system updates {.wp-block-heading}

To begin with we are going to run system updates in order to make our repositories up to date.

<pre class="wp-block-code"><code>sudo apt update && apt upgrade -y</code></pre>

### 2. Add insomnia to Debeian package repository {.wp-block-heading}

We need to add Insomnia to our repository so that it will be easier to install. So to add the repo, we can use the following command.

<pre class="wp-block-code"><code>$ echo "deb &#91;trusted=yes arch=amd64] https://download.konghq.com/insomnia-ubuntu/ default all" \
    | sudo tee -a /etc/apt/sources.list.d/insomnia.list</code></pre>

Then we need to update our repositories again for the changes to take effect.

<pre class="wp-block-code"><code>$ sudo apt update
Get:1 http://mirrors.digitalocean.com/ubuntu jammy InRelease &#91;270 kB]
Hit:2 https://repos-droplet.digitalocean.com/apt/droplet-agent main InRelease                                                               
Hit:3 http://mirrors.digitalocean.com/ubuntu jammy-updates InRelease                                                         
Hit:4 http://mirrors.digitalocean.com/ubuntu jammy-backports InRelease                                                       
Get:5 http://security.ubuntu.com/ubuntu jammy-security InRelease &#91;110 kB]                                                    
&lt;strong>Ign:6 https://download.konghq.com/insomnia-ubuntu default InRelease                 
Get:7 https://download.konghq.com/insomnia-ubuntu default Release &#91;1328 B]                            
Ign:8 https://download.konghq.com/insomnia-ubuntu default Release.gpg                                
Get:9 https://download.konghq.com/insomnia-ubuntu default/all amd64 Packages &#91;1607 B]&lt;/strong>
Fetched 383 kB in 8s (49.7 kB/s)
Reading package lists... Done
Building dependency tree... Done
Reading state information... Done
All packages are up to date.</code></pre>

### 3. Install Insomnia on Ubuntu 22.04 {.wp-block-heading}

To install insomnia just run `<strong>apt install insomnia</strong>` on your terminal. 

<pre class="wp-block-code"><code>$ sudo apt install insomnia -y</code></pre>

Sample output will look like this.

<pre class="wp-block-code"><code>The following additional packages will be installed:
  adwaita-icon-theme at-spi2-core cpp cpp-11 dconf-gsettings-backend dconf-service fontconfig fontconfig-config fonts-dejavu-core
  gcc-11-base gsettings-desktop-schemas gtk-update-icon-cache hicolor-icon-theme humanity-icon-theme libappindicator3-1 libatk-bridge2.0-0
  libatk1.0-0 libatk1.0-data libatspi2.0-0 libauthen-sasl-perl libavahi-client3 libavahi-common-data libavahi-common3 libcairo-gobject2
  libcairo2 libclone-perl libcolord2 libcups2 libdata-dump-perl libdatrie1 libdbusmenu-glib4 libdbusmenu-gtk3-4 libdconf1 libdeflate0
  libdrm-amdgpu1 libdrm-intel1 libdrm-nouveau2 libdrm-radeon1 libencode-locale-perl libepoxy0 libfile-basedir-perl
  libfile-desktopentry-perl libfile-listing-perl libfile-mimeinfo-perl libfont-afm-perl libfontconfig1 libfontenc1 libgdk-pixbuf-2.0-0
  libgdk-pixbuf2.0-bin libgdk-pixbuf2.0-common libgl1 libgl1-amber-dri libgl1-mesa-dri libglapi-mesa libglvnd0 libglx-mesa0 libglx0
  libgraphite2-3 libgtk-3-0 libgtk-3-bin libgtk-3-common libgtkd-3-0 libharfbuzz0b libhtml-form-perl libhtml-format-perl
  libhtml-parser-perl libhtml-tagset-perl libhtml-tree-perl libhttp-cookies-perl libhttp-daemon-perl libhttp-date-perl libhttp-message-perl
  libhttp-negotiate-perl libice6 libio-html-perl libio-socket-ssl-perl libio-stringy-perl libipc-system-simple-perl libisl23 libjbig0
  libjpeg-turbo8 libjpeg8 liblcms2-2 libllvm11 libllvm13 liblwp-mediatypes-perl liblwp-protocol-https-perl libmailtools-perl libmpc3
  libnet-dbus-perl libnet-http-perl libnet-smtp-ssl-perl libnet-ssleay-perl libnotify4 libpango-1.0-0 libpangocairo-1.0-0 libpangoft2-1.0-0
  libpciaccess0 libphobos2-ldc-shared98 libpixman-1-0 librsvg2-2 librsvg2-common libsecret-1-0 libsecret-common libsensors-config
  libsensors5 libsm6 libthai-data libthai0 libtie-ixhash-perl libtiff5 libtimedate-perl libtry-tiny-perl liburi-perl libvte-2.91-0
  libvte-2.91-common libvted-3-0 libvulkan1 libwayland-client0 libwayland-cursor0 libwayland-egl1 libwebp7 libwww-perl
  libwww-robotrules-perl libx11-protocol-perl libx11-xcb1 libxaw7 libxcb-dri2-0 libxcb-dri3-0 libxcb-glx0 libxcb-present0 libxcb-randr0
  libxcb-render0 libxcb-shape0 libxcb-shm0 libxcb-sync1 libxcb-xfixes0 libxcomposite1 libxcursor1 libxdamage1 libxfixes3 libxft2 libxi6
  libxinerama1 libxkbcommon0 libxkbfile1 libxml-parser-perl libxml-twig-perl libxml-xpathengine-perl libxmu6 libxpm4 libxrandr2 libxrender1
  libxshmfence1 libxss1 libxt6 libxtst6 libxv1 libxxf86dga1 libxxf86vm1 mesa-vulkan-drivers perl-openssl-defaults session-migration tilix
  tilix-common ubuntu-mono x11-common x11-utils x11-xserver-utils xdg-utils</code></pre>

To check if Insomnia is installed go to the `<strong>show applications</strong>` and **search** for insomia from the installed apps.

<pre class="wp-block-code"><code>show applications > search > insomnia</code></pre><figure class="wp-block-image size-large">

<img decoding="async" loading="lazy" width="810" height="367" src="https://i0.wp.com/nextgentips.com/wp-content/uploads/2022/06/Screenshot-from-2022-06-23-14-28-08.png?resize=810%2C367&#038;ssl=1" alt="Insomnia " class="wp-image-1406" srcset="https://i0.wp.com/nextgentips.com/wp-content/uploads/2022/06/Screenshot-from-2022-06-23-14-28-08.png?resize=1024%2C464&ssl=1 1024w, https://i0.wp.com/nextgentips.com/wp-content/uploads/2022/06/Screenshot-from-2022-06-23-14-28-08.png?resize=300%2C136&ssl=1 300w, https://i0.wp.com/nextgentips.com/wp-content/uploads/2022/06/Screenshot-from-2022-06-23-14-28-08.png?resize=768%2C348&ssl=1 768w, https://i0.wp.com/nextgentips.com/wp-content/uploads/2022/06/Screenshot-from-2022-06-23-14-28-08.png?w=1184&ssl=1 1184w" sizes="(max-width: 810px) 100vw, 810px" data-recalc-dims="1" /> <figcaption>Insomnia </figcaption></figure> 

## Installing Insomnia via Snap. {.wp-block-heading}

Another way to install Insomnia and the most effective way is to use Snap because Snap do the app updates automatically, it will remove the hustle of updating yourself. To install using snap run the following command.

<pre class="wp-block-code"><code>$ sudo snap install insomnia</code></pre>

That is it for today, enjoy your REST API management.