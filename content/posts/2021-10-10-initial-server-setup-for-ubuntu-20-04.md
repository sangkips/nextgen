---
title: How to setup Ubuntu 20.04 Server for the first time
author: Kipkoech Sang
type: post
date: 2021-10-10T08:44:38+00:00
url: /2021/10/10/initial-server-setup-for-ubuntu-20-04/
site-sidebar-layout:
  - default
site-content-layout:
  - default
theme-transparent-header-meta:
  - default
ocean_gallery_link_images:
  - off
ocean_disable_margins:
  - enable
ocean_display_top_bar:
  - default
ocean_display_header:
  - default
ocean_disable_title:
  - default
ocean_disable_heading:
  - default
ocean_disable_breadcrumbs:
  - default
ocean_display_footer_widgets:
  - default
ocean_display_footer_bottom:
  - default
ocean_link_format_target:
  - self
ocean_quote_format_link:
  - post
rank_math_news_sitemap_robots:
  - index
rank_math_robots:
  - 'a:1:{i:0;s:5:"index";}'
rank_math_analytic_object_id:
  - 141
rank_math_internal_links_processed:
  - 1
categories:
  - Linux

---
In this guide we are going to learn how to set up your Ubuntu server after fresh install. 

## Logging as root into the server {.wp-block-heading}

After fresh installation, the server usually comes with a **root** account which can be used to log in to your server. Root account has all the privileges which if you are not careful with it you can do more harm to your system. So you are discouraged from using it. So the first thing you can do is to create a regular user and give **[sudo][1]** permissions so that they may run administrative commands with limitations.

To start off, you will need to log in to your server. Ensure you know your server public IP address. Use the following command to log in as root to your server.

<pre class="wp-block-code"><code>$ ssh root@&lt;public_IP_Address&gt;</code></pre>

Public IP Address is your server&#8217;s IP address. Accept the warnings that comes with authenticity and input your root password. Alternatively, if you are using SSH key to authenticate, provide your passphrase. 

## Creating new Server User {.wp-block-heading}

Once logged in as root the next thing is to create a user with sudo privileges. Let us create our new user with the following command. Open terminal as root 

<pre class="wp-block-code"><code>$ adduser nextgentips</code></pre><figure class="wp-block-image size-full">

<img decoding="async" loading="lazy" width="623" height="416" src="https://i0.wp.com/nextgentips.com/wp-content/uploads/2021/10/Screenshot-from-2021-10-10-10-07-41.png?resize=623%2C416&#038;ssl=1" alt="" class="wp-image-169" srcset="https://i0.wp.com/nextgentips.com/wp-content/uploads/2021/10/Screenshot-from-2021-10-10-10-07-41.png?w=623&ssl=1 623w, https://i0.wp.com/nextgentips.com/wp-content/uploads/2021/10/Screenshot-from-2021-10-10-10-07-41.png?resize=300%2C200&ssl=1 300w, https://i0.wp.com/nextgentips.com/wp-content/uploads/2021/10/Screenshot-from-2021-10-10-10-07-41.png?resize=24%2C16&ssl=1 24w, https://i0.wp.com/nextgentips.com/wp-content/uploads/2021/10/Screenshot-from-2021-10-10-10-07-41.png?resize=36%2C24&ssl=1 36w, https://i0.wp.com/nextgentips.com/wp-content/uploads/2021/10/Screenshot-from-2021-10-10-10-07-41.png?resize=48%2C32&ssl=1 48w" sizes="(max-width: 623px) 100vw, 623px" data-recalc-dims="1" /> <figcaption>Adding new User </figcaption></figure> 

## Granting Administrative privileges  {.wp-block-heading}

The user created above have regular privileges, for it to perform administrative tasks, sudo privileges must be assigned. The new user must prefix sudo in every command typed to gain all administrative roles.

To add new user to these privileges, you must add the new user to sudo group. 

As root user run the following command to add sudo group to the user.

<pre class="wp-block-code"><code>$ usermod -aG sudo nextgentips</code></pre>

**-a** stands for append 

&#8211;**G** argument tells the usermod to change a users group settings.

## Set up Basic Firewall {.wp-block-heading}

A firewall is a network security system that monitors and controls incoming and outgoing network traffic based on predetermined security rules. A firewall typically establishes a barrier between a trusted network and an untrusted network, such as the Internet.

Ubuntu servers comes with what we call Uncomplicated firewall (UFW). We use UFW firewall to make sure only connections to certain services are only allowed. Applications register their profiles with UFW upon installation. 

<pre class="wp-block-code"><code>$ ufw app list

# shows the current available profiles </code></pre><figure class="wp-block-image size-full">

<img decoding="async" loading="lazy" width="437" height="73" src="https://i0.wp.com/nextgentips.com/wp-content/uploads/2021/10/Screenshot-from-2021-10-10-11-11-37.png?resize=437%2C73&#038;ssl=1" alt="" class="wp-image-170" srcset="https://i0.wp.com/nextgentips.com/wp-content/uploads/2021/10/Screenshot-from-2021-10-10-11-11-37.png?w=437&ssl=1 437w, https://i0.wp.com/nextgentips.com/wp-content/uploads/2021/10/Screenshot-from-2021-10-10-11-11-37.png?resize=300%2C50&ssl=1 300w, https://i0.wp.com/nextgentips.com/wp-content/uploads/2021/10/Screenshot-from-2021-10-10-11-11-37.png?resize=24%2C4&ssl=1 24w, https://i0.wp.com/nextgentips.com/wp-content/uploads/2021/10/Screenshot-from-2021-10-10-11-11-37.png?resize=36%2C6&ssl=1 36w, https://i0.wp.com/nextgentips.com/wp-content/uploads/2021/10/Screenshot-from-2021-10-10-11-11-37.png?resize=48%2C8&ssl=1 48w" sizes="(max-width: 437px) 100vw, 437px" data-recalc-dims="1" /> </figure> 

Allow SSH connections so that you can be able to login the next time. Use the following command to do that.

<pre class="wp-block-code"><code>$ ufw allow openSSH</code></pre><figure class="wp-block-image size-full">

<img decoding="async" loading="lazy" width="477" height="77" src="https://i0.wp.com/nextgentips.com/wp-content/uploads/2021/10/Screenshot-from-2021-10-10-11-19-52.png?resize=477%2C77&#038;ssl=1" alt="" class="wp-image-171" srcset="https://i0.wp.com/nextgentips.com/wp-content/uploads/2021/10/Screenshot-from-2021-10-10-11-19-52.png?w=477&ssl=1 477w, https://i0.wp.com/nextgentips.com/wp-content/uploads/2021/10/Screenshot-from-2021-10-10-11-19-52.png?resize=300%2C48&ssl=1 300w, https://i0.wp.com/nextgentips.com/wp-content/uploads/2021/10/Screenshot-from-2021-10-10-11-19-52.png?resize=24%2C4&ssl=1 24w, https://i0.wp.com/nextgentips.com/wp-content/uploads/2021/10/Screenshot-from-2021-10-10-11-19-52.png?resize=36%2C6&ssl=1 36w, https://i0.wp.com/nextgentips.com/wp-content/uploads/2021/10/Screenshot-from-2021-10-10-11-19-52.png?resize=48%2C8&ssl=1 48w" sizes="(max-width: 477px) 100vw, 477px" data-recalc-dims="1" /> </figure> 

Then you can enable the firewall with the following command:

<pre class="wp-block-code"><code>$ ufw enable</code></pre>

Let&#8217;s check the status now with the following command.

<pre class="wp-block-code"><code>$ ufw status </code></pre><figure class="wp-block-image size-full">

<img decoding="async" loading="lazy" width="740" height="179" src="https://i0.wp.com/nextgentips.com/wp-content/uploads/2021/10/Screenshot-from-2021-10-10-11-23-31.png?resize=740%2C179&#038;ssl=1" alt="" class="wp-image-172" srcset="https://i0.wp.com/nextgentips.com/wp-content/uploads/2021/10/Screenshot-from-2021-10-10-11-23-31.png?w=740&ssl=1 740w, https://i0.wp.com/nextgentips.com/wp-content/uploads/2021/10/Screenshot-from-2021-10-10-11-23-31.png?resize=300%2C73&ssl=1 300w, https://i0.wp.com/nextgentips.com/wp-content/uploads/2021/10/Screenshot-from-2021-10-10-11-23-31.png?resize=24%2C6&ssl=1 24w, https://i0.wp.com/nextgentips.com/wp-content/uploads/2021/10/Screenshot-from-2021-10-10-11-23-31.png?resize=36%2C9&ssl=1 36w, https://i0.wp.com/nextgentips.com/wp-content/uploads/2021/10/Screenshot-from-2021-10-10-11-23-31.png?resize=48%2C12&ssl=1 48w" sizes="(max-width: 740px) 100vw, 740px" data-recalc-dims="1" /> </figure> 

You are now a regular user you can SSH into your system with your credentials now by using the following command:

<pre class="wp-block-code"><code>$ ssh nextgentips@&lt;your_ip_address&gt;</code></pre>

That is all for now. You are now good to use your new user.

 [1]: https://www.sudo.ws/