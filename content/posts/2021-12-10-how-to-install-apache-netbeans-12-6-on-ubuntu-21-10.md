---
title: How to install Apache NetBeans 12.6 on Ubuntu 20.04/21.10
author: Kipkoech Sang
type: post
date: 2021-12-10T18:08:18+00:00
url: /2021/12/10/how-to-install-apache-netbeans-12-6-on-ubuntu-21-10/
entry_views:
  - 1
view_ip:
  - 'a:1:{i:0;s:13:"18.234.30.224";}'
rank_math_news_sitemap_robots:
  - index
rank_math_robots:
  - 'a:1:{i:0;s:5:"index";}'
rank_math_analytic_object_id:
  - 68
rank_math_internal_links_processed:
  - 1
categories:
  - Programming

---
In this tutorial, we are going to explore how to install Apache Netbeans 12.6 on Ubuntu 20.04.

NetBeans is an integrated development environment for java. Netbeans allows the development of applications from a set of modular software components called modules. NetBeans is part of the wider Apache family license that is why it is called Apache NetBeans.

## Features of Apache NetBeans  {.wp-block-heading}

  * It is smart and offers fast use experience. It is much more than a text editor, it offers source code highligting, it lets you refactor your code.
  * Apache NetBeans provides editors, wizards and templates to help you create applications in java, PHP, HTML etc. 
  * Apache NetBeans is a cross platform application. IIt can be installed on all platforms that supports java such as Linux,Windows, MacOS and BSD devices. Write once, run anywhere.
  * Apache Netbeans is a free and open-source meaning it has a wider community base for faster issue resolution.

## Prerequisites  {.wp-block-heading}

  * Make sure you have java8 and above installed on your system.
  * Have basic understanding of Linux terminal
  * Have Ubuntu 20.04 up and running 
  * Have a non root user 

## Install Apache Netbeans on Ubuntu 20.04 {.wp-block-heading}

## 1. Run system update. {.wp-block-heading}

In order to up to date repositories, we need to run system update from our terminal. Use the following command to do so.

<pre class="wp-block-code"><code>$ sudo apt update && apt upgrade -y</code></pre>

When updates are complete, please go ahead and install java into your system.

## 2. Install Java development kit {.wp-block-heading}

Apache NetBeans depends on java to run, so we first have to install it. Use the following command.

<pre class="wp-block-code"><code>$ sudo apt install default-jdk -y</code></pre>

Sample output.

<pre class="wp-block-code"><code>output
The following packages were automatically installed and are no longer required:
  libfprint-2-tod1 libgtkglext1 libllvm9 libminizip1 libpangox-1.0-0 shim
Use 'sudo apt autoremove' to remove them.
The following additional packages will be installed:
  default-jdk-headless default-jre default-jre-headless fonts-dejavu-extra
  libatk-wrapper-java libatk-wrapper-java-jni libice-dev libsm-dev libxt-dev
  openjdk-11-jdk openjdk-11-jdk-headless openjdk-11-jre
  openjdk-11-jre-headless
Suggested packages:
  libice-doc libsm-doc libxt-doc openjdk-11-demo openjdk-11-source visualvm
  fonts-ipafont-gothic fonts-ipafont-mincho fonts-wqy-microhei
  | fonts-wqy-zenhei
The following NEW packages will be installed:
  default-jdk default-jdk-headless default-jre default-jre-headless
  fonts-dejavu-extra libatk-wrapper-java libatk-wrapper-java-jni libice-dev
  libsm-dev libxt-dev openjdk-11-jdk openjdk-11-jdk-headless openjdk-11-jre
  openjdk-11-jre-headless
0 upgraded, 14 newly installed, 0 to remove and 143 not upgraded.
Need to get 264 MB of archives.
After this operation, 415 MB of additional disk space will be used.
Do you want to continue? &#91;Y/n] </code></pre>

Press Y to allow the installation to continue.

Verify the installed version of java with the following command.

<pre class="wp-block-code"><code>$ java --version
openjdk 11.0.11 2021-04-20
OpenJDK Runtime Environment (build 11.0.11+9-Ubuntu-0ubuntu2.20.04)
OpenJDK 64-Bit Server VM (build 11.0.11+9-Ubuntu-0ubuntu2.20.04, mixed mode, sharing)</code></pre>

## 3. Install Apache Netbeans using snap. {.wp-block-heading}

I will be using snap method because any snap packages are automatically updated every time a new release is made. To install via snap enable snapd with the following command.

<pre class="wp-block-code"><code>$ sudo apt install snapd</code></pre>

<pre class="wp-block-code"><code>output
Reading package lists... Done
Building dependency tree       
Reading state information... Done
snapd is already the newest version (2.51.1+20.04ubuntu2).
snapd set to manually installed.
0 upgraded, 0 newly installed, 0 to remove and 0 not upgraded.</code></pre>

It looks like snapd Ubuntu 20.04 comes preinstalled with snaps.

Next is to install netbeans.

## Install Apache NetBeans  {.wp-block-heading}

To install NetBeans via snap, run the following command on your terminal.

<pre class="wp-block-code"><code>$ sudo snap install netbeans --classic </code></pre>

<pre class="wp-block-code"><code>Output
netbeans 12.6 from Apache NetBeans✓ installed</code></pre>

To launch NetBeans type **netbeans** on your terminal

<pre class="wp-block-code"><code>$ netbeans </code></pre>

From here you can now set NetBeans to conform to your needs.

## 4. Conclusion. {.wp-block-heading}

I am glad you have learn how to install NetBeans using snap method. I love this method because I don&#8217;t have to run system updates every time. Repositories are automatically update whenever a new release is made.