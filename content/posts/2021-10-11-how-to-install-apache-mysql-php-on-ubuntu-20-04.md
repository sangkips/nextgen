---
title: How to Install Apache, MYSQL, PHP on Ubuntu 20.04
author: Kipkoech Sang
type: post
date: 2021-10-11T15:00:27+00:00
url: /2021/10/11/how-to-install-apache-mysql-php-on-ubuntu-20-04/
site-sidebar-layout:
  - default
site-content-layout:
  - default
theme-transparent-header-meta:
  - default
rank_math_news_sitemap_robots:
  - index
rank_math_robots:
  - 'a:1:{i:0;s:5:"index";}'
rank_math_analytic_object_id:
  - 140
rank_math_internal_links_processed:
  - 1
categories:
  - Database

---
In this blog guide we are going to learn how to install Apache, MYSQL and PHP(LAMP) stack on Ubuntu 20.04 server. You are wondering what is LAMP? LAMP stands for **L**inux operating system with [**A**pache web server][1]. Site data is stored in [**M**ySQL database][2] and dynamic content is processed by [**P**HP][3].

**Apache HTTP server** project is an open source HTTP server for modern operating systems including both UNIX and windows. The goal of this project is to provide secure, efficient and extensible server that provides HTTP services in sync with the current standards.

**MySQL** is an open source relational database management system based on SQL (Structured Query Language). A relational database management system is a software that: 

  * Enables you to implement database with tables, columns and indexes.
  * Guarantees the referential integrity between rows of various tables.
  * Updates the indexes automatically
  * Interprets the SQL query and combine information from various tables 

**PHP** is a popular scripting language that is suited to web development. It is fast, flexible and pragmatic and powers everything from small blogs to the most powerful websites.

## Prerequisites {.wp-block-heading}

  * Have an Ubuntu 20.04 server
  * Be sudo user 
  * Basic firewall installed. 

Check this out for [initial server setup guide for Ubuntu 20.04 server][4]

## Installing Apache {.wp-block-heading}

What you need to do is update your Ubuntu server repository. Type the following command in your terminal.

<pre class="wp-block-code"><code>$ sudo apt update</code></pre><figure class="wp-block-image size-full">

<img decoding="async" loading="lazy" width="725" height="256" src="https://i0.wp.com/nextgentips.com/wp-content/uploads/2021/10/Screenshot-from-2021-10-11-10-29-24.png?resize=725%2C256&#038;ssl=1" alt="" class="wp-image-176" srcset="https://i0.wp.com/nextgentips.com/wp-content/uploads/2021/10/Screenshot-from-2021-10-11-10-29-24.png?w=725&ssl=1 725w, https://i0.wp.com/nextgentips.com/wp-content/uploads/2021/10/Screenshot-from-2021-10-11-10-29-24.png?resize=300%2C106&ssl=1 300w, https://i0.wp.com/nextgentips.com/wp-content/uploads/2021/10/Screenshot-from-2021-10-11-10-29-24.png?resize=24%2C8&ssl=1 24w, https://i0.wp.com/nextgentips.com/wp-content/uploads/2021/10/Screenshot-from-2021-10-11-10-29-24.png?resize=36%2C13&ssl=1 36w, https://i0.wp.com/nextgentips.com/wp-content/uploads/2021/10/Screenshot-from-2021-10-11-10-29-24.png?resize=48%2C17&ssl=1 48w" sizes="(max-width: 725px) 100vw, 725px" data-recalc-dims="1" /> <figcaption>Updating repository</figcaption></figure> 

Once the update is complete, install Apache with the following command.

<pre class="wp-block-code"><code>$ sudo apt install apache2</code></pre>

Next, we will need to adjust the firewall to accept both HTTP and HTTPS traffic.

Check the ststus of the firewall with the following command.

<pre class="wp-block-code"><code>$ sudo ufw app list</code></pre><figure class="wp-block-image size-full">

<img decoding="async" loading="lazy" width="555" height="160" src="https://i0.wp.com/nextgentips.com/wp-content/uploads/2021/10/Screenshot-from-2021-10-11-10-57-16.png?resize=555%2C160&#038;ssl=1" alt="" class="wp-image-177" srcset="https://i0.wp.com/nextgentips.com/wp-content/uploads/2021/10/Screenshot-from-2021-10-11-10-57-16.png?w=555&ssl=1 555w, https://i0.wp.com/nextgentips.com/wp-content/uploads/2021/10/Screenshot-from-2021-10-11-10-57-16.png?resize=300%2C86&ssl=1 300w, https://i0.wp.com/nextgentips.com/wp-content/uploads/2021/10/Screenshot-from-2021-10-11-10-57-16.png?resize=24%2C7&ssl=1 24w, https://i0.wp.com/nextgentips.com/wp-content/uploads/2021/10/Screenshot-from-2021-10-11-10-57-16.png?resize=36%2C10&ssl=1 36w, https://i0.wp.com/nextgentips.com/wp-content/uploads/2021/10/Screenshot-from-2021-10-11-10-57-16.png?resize=48%2C14&ssl=1 48w" sizes="(max-width: 555px) 100vw, 555px" data-recalc-dims="1" /> </figure> 

Let&#8217;s check if Apache allows full traffic with the following command.

<pre class="wp-block-code"><code>$ sudo ufw app info "Apache full"</code></pre><figure class="wp-block-image size-full">

<img decoding="async" loading="lazy" width="694" height="204" src="https://i0.wp.com/nextgentips.com/wp-content/uploads/2021/10/Screenshot-from-2021-10-11-11-18-08.png?resize=694%2C204&#038;ssl=1" alt="" class="wp-image-178" srcset="https://i0.wp.com/nextgentips.com/wp-content/uploads/2021/10/Screenshot-from-2021-10-11-11-18-08.png?w=694&ssl=1 694w, https://i0.wp.com/nextgentips.com/wp-content/uploads/2021/10/Screenshot-from-2021-10-11-11-18-08.png?resize=300%2C88&ssl=1 300w, https://i0.wp.com/nextgentips.com/wp-content/uploads/2021/10/Screenshot-from-2021-10-11-11-18-08.png?resize=24%2C7&ssl=1 24w, https://i0.wp.com/nextgentips.com/wp-content/uploads/2021/10/Screenshot-from-2021-10-11-11-18-08.png?resize=36%2C11&ssl=1 36w, https://i0.wp.com/nextgentips.com/wp-content/uploads/2021/10/Screenshot-from-2021-10-11-11-18-08.png?resize=48%2C14&ssl=1 48w" sizes="(max-width: 694px) 100vw, 694px" data-recalc-dims="1" /> </figure> 

Run the following command to allow HTTPS and HTTP traffic to the server.

<pre class="wp-block-code"><code>$ sudo ufw allow "Apache Full"</code></pre>

<pre class="wp-block-code"><code>$ http://&lt;Your_server_IP&gt;</code></pre><figure class="wp-block-image size-full">

<img decoding="async" loading="lazy" width="804" height="632" src="https://i0.wp.com/nextgentips.com/wp-content/uploads/2021/10/Screenshot-from-2021-10-11-16-44-35.png?resize=804%2C632&#038;ssl=1" alt="" class="wp-image-179" srcset="https://i0.wp.com/nextgentips.com/wp-content/uploads/2021/10/Screenshot-from-2021-10-11-16-44-35.png?w=804&ssl=1 804w, https://i0.wp.com/nextgentips.com/wp-content/uploads/2021/10/Screenshot-from-2021-10-11-16-44-35.png?resize=300%2C236&ssl=1 300w, https://i0.wp.com/nextgentips.com/wp-content/uploads/2021/10/Screenshot-from-2021-10-11-16-44-35.png?resize=768%2C604&ssl=1 768w, https://i0.wp.com/nextgentips.com/wp-content/uploads/2021/10/Screenshot-from-2021-10-11-16-44-35.png?resize=24%2C19&ssl=1 24w, https://i0.wp.com/nextgentips.com/wp-content/uploads/2021/10/Screenshot-from-2021-10-11-16-44-35.png?resize=36%2C28&ssl=1 36w, https://i0.wp.com/nextgentips.com/wp-content/uploads/2021/10/Screenshot-from-2021-10-11-16-44-35.png?resize=48%2C38&ssl=1 48w" sizes="(max-width: 804px) 100vw, 804px" data-recalc-dims="1" /> </figure> 

## Install MYSQL {.wp-block-heading}

Use **apt** to install MySQL. MySQL organize and provide access to databases where a site can store information.

Use the folloing command to install MySQL

<pre class="wp-block-code"><code>$ sudo apt install mysql-server</code></pre>

After installation we need to run a security script that remove dangerous defaults and lock down access to your database system. 

<pre class="wp-block-code"><code>$ sudo mysql_secure_installation</code></pre>

Follow every prompt to continue.<figure class="wp-block-image size-full">

<img decoding="async" loading="lazy" width="702" height="210" src="https://i0.wp.com/nextgentips.com/wp-content/uploads/2021/10/Screenshot-from-2021-10-11-17-08-09.png?resize=702%2C210&#038;ssl=1" alt="" class="wp-image-180" srcset="https://i0.wp.com/nextgentips.com/wp-content/uploads/2021/10/Screenshot-from-2021-10-11-17-08-09.png?w=702&ssl=1 702w, https://i0.wp.com/nextgentips.com/wp-content/uploads/2021/10/Screenshot-from-2021-10-11-17-08-09.png?resize=300%2C90&ssl=1 300w, https://i0.wp.com/nextgentips.com/wp-content/uploads/2021/10/Screenshot-from-2021-10-11-17-08-09.png?resize=24%2C7&ssl=1 24w, https://i0.wp.com/nextgentips.com/wp-content/uploads/2021/10/Screenshot-from-2021-10-11-17-08-09.png?resize=36%2C11&ssl=1 36w, https://i0.wp.com/nextgentips.com/wp-content/uploads/2021/10/Screenshot-from-2021-10-11-17-08-09.png?resize=48%2C14&ssl=1 48w" sizes="(max-width: 702px) 100vw, 702px" data-recalc-dims="1" /> </figure> 

Test if you can connect to MySQL by typing in the following command.

<pre class="wp-block-code"><code>$ sudo mysql</code></pre><figure class="wp-block-image size-full">

<img decoding="async" loading="lazy" width="726" height="252" src="https://i0.wp.com/nextgentips.com/wp-content/uploads/2021/10/Screenshot-from-2021-10-11-17-16-10.png?resize=726%2C252&#038;ssl=1" alt="" class="wp-image-181" srcset="https://i0.wp.com/nextgentips.com/wp-content/uploads/2021/10/Screenshot-from-2021-10-11-17-16-10.png?w=726&ssl=1 726w, https://i0.wp.com/nextgentips.com/wp-content/uploads/2021/10/Screenshot-from-2021-10-11-17-16-10.png?resize=300%2C104&ssl=1 300w, https://i0.wp.com/nextgentips.com/wp-content/uploads/2021/10/Screenshot-from-2021-10-11-17-16-10.png?resize=24%2C8&ssl=1 24w, https://i0.wp.com/nextgentips.com/wp-content/uploads/2021/10/Screenshot-from-2021-10-11-17-16-10.png?resize=36%2C12&ssl=1 36w, https://i0.wp.com/nextgentips.com/wp-content/uploads/2021/10/Screenshot-from-2021-10-11-17-16-10.png?resize=48%2C17&ssl=1 48w" sizes="(max-width: 726px) 100vw, 726px" data-recalc-dims="1" /> </figure> 

Use the following command to exit Mysql console

<pre class="wp-block-code"><code>$ exit</code></pre>

## Installing PHP {.wp-block-heading}

PHP requires **libapache2-mod-php** package in order to integrate into Apache. It also requires **php-mysql package** in order to connect to MySQL database.

Run the following command in order to install those packages.

<pre class="wp-block-code"><code>$ sudo apt install php libapache2-mod-php php-mysql</code></pre>

## Conclusion {.wp-block-heading}

Now that you have install LAMP stack, you are now in a position to install all websites and web software.

 [1]: https://httpd.apache.org/
 [2]: https://dev.mysql.com/doc/
 [3]: https://www.php.net/
 [4]: https://nextgentips.com/2021/10/10/initial-server-setup-for-ubuntu-20-04/ "https://nextgentips.com/2021/10/10/initial-server-setup-for-ubuntu-20-04/"