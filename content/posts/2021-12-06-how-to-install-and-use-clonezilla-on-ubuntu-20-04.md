---
title: How to install and use Clonezilla on Ubuntu 20.04
author: Kipkoech Sang
type: post
date: 2021-12-06T08:28:12+00:00
url: /2021/12/06/how-to-install-and-use-clonezilla-on-ubuntu-20-04/
rank_math_news_sitemap_robots:
  - index
rank_math_robots:
  - 'a:1:{i:0;s:5:"index";}'
rank_math_analytic_object_id:
  - 75
rank_math_internal_links_processed:
  - 1
post_view:
  - 1
is_post_view:
  - 'a:1:{i:0;s:28:"168.181.124.2, 99.82.164.134";}'
categories:
  - Linux

---
[Clonezilla][1] is a suite of open-source, disk cloning programs used for bare metal backup and recovery and also used during system deployment. Clonezilla server edition uses multicast technologies to deploy a single image file to a group of computers in a local area network. 

In this tutorial guide, we are going to explore how to install and use Clonezilla on Ubuntu 20.04.

## Types of Clonezilla  {.wp-block-heading}

  * Clonezilla live. This is suitable for single machine backup and restore. It allows you to use the CD/DVD or USB flash drive to boot and run clonezilla
  * Clonezilla lite server. Suitable for massive deployments. Ity allows you to use clonezilla live to do massive clonnning.
  * Clonezilla SE (server edition). Suitable for massive deployments

Clonezilla only saves and restores only used blocks in the hard disk, therefore, increasing the clone efficiency.

## Features of Clonezilla {.wp-block-heading}

  * It supports many File sytems such as ext2, ext3, eiserfs, reiser4, xfs, jfs, btrfs, f2fs etc
  * It support both MBR and GPT partion formats of hard drive are supported. 
  * One image restoring to multiple local devices are supported 
  * Images are encrypted in the process
  * AES-256 encryption is used to secure data access, storage and transfer
  * Multicast is supported on Clonzilla SE which is suitable for massive clone.

## Prerequisites  {.wp-block-heading}

  * X86 or X86-X64 processor
  * 256 MB of RAM
  * Boot devices 
  * Ubuntu 20.04 server

## Install Clonezilla in Ubuntu 20.04 {.wp-block-heading}

## 1. Run sytem updates  {.wp-block-heading}

First, run the update command to update package repositories in order to get the latest package information.

<pre class="wp-block-code"><code>$ sudo apt update && apt upgrade -y</code></pre>

When updates are complete, you can now begin the installation.

## 2. Install Clonezilla {.wp-block-heading}

You can now run the installation program with the following command on your terminal. The program will install all the dependencies as needed by clonezilla.

<pre class="wp-block-code"><code>$ sudo apt install clonezilla -y</code></pre>

Sample Output

<pre class="wp-block-code"><code>Reading package lists... Done
Building dependency tree       
Reading state information... Done
The following additional packages will be installed:
  dialog drbl ipcalc mtools nilfs-tools partclone partimage pigz syslinux syslinux-common wakeonlan
Suggested packages:
  cifs-utils udpcast sshfs floppyd
The following NEW packages will be installed:
  clonezilla dialog drbl ipcalc mtools nilfs-tools partclone partimage pigz syslinux syslinux-common wakeonlan
0 upgraded, 12 newly installed, 0 to remove and 0 not upgraded.</code></pre>

To begin using Clonezilla, type Clonezilla on your terminal.

<pre class="wp-block-code"><code>$ clonezilla</code></pre>

You will get the following from your terminal.<figure class="wp-block-image size-large">

<img decoding="async" loading="lazy" width="810" height="583" src="https://i0.wp.com/nextgentips.com/wp-content/uploads/2021/12/Screenshot-from-2021-12-06-11-12-33.png?resize=810%2C583&#038;ssl=1" alt="Clonezilla interface" class="wp-image-796" srcset="https://i0.wp.com/nextgentips.com/wp-content/uploads/2021/12/Screenshot-from-2021-12-06-11-12-33.png?resize=1024%2C737&ssl=1 1024w, https://i0.wp.com/nextgentips.com/wp-content/uploads/2021/12/Screenshot-from-2021-12-06-11-12-33.png?resize=300%2C216&ssl=1 300w, https://i0.wp.com/nextgentips.com/wp-content/uploads/2021/12/Screenshot-from-2021-12-06-11-12-33.png?resize=768%2C553&ssl=1 768w, https://i0.wp.com/nextgentips.com/wp-content/uploads/2021/12/Screenshot-from-2021-12-06-11-12-33.png?w=1054&ssl=1 1054w" sizes="(max-width: 810px) 100vw, 810px" data-recalc-dims="1" /> <figcaption>Clonezilla interface</figcaption></figure> 

If you choose device-image you will get the following:

<pre class="wp-block-code"><code>output
Every 3.0s: ocs-scan-disk                                                       ubuntu: Mon Dec  6 08:22:33 2021

2021/12/06 08:22:33
You can insert storage device into this machine now if you want to use that, then wait for it to be detected.
Scanning devices... Available disk(s) on this machine:
===================================
Excluding busy partition or disk...
===================================
Update periodically. Press Ctrl-C to exit this window.</code></pre>

## Conclusion {.wp-block-heading}

We have completed steps on how to install Clonezilla on Ubuntu 20.04. I hope you have learned something new in the process.

 [1]: https://clonezilla.org/