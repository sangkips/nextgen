---
title: How to install Terraform on Ubuntu 22.04
author: Kipkoech Sang
type: post
date: 2022-06-17T08:54:10+00:00
url: /2022/06/17/how-to-install-terraform-on-ubuntu-22-04/
rank_math_internal_links_processed:
  - 1
rank_math_seo_score:
  - 22
rank_math_ca_keyword:
  - 'a:2:{s:7:"keyword";s:30:"install terraform ubuntu 22.04";s:7:"country";s:3:"all";}'
rank_math_primary_category:
  - 37
rank_math_focus_keyword:
  - install terraform ubuntu 22.04
rank_math_contentai_score:
  - 'a:5:{s:9:"wordCount";s:3:"100";s:9:"linkCount";s:5:"26.67";s:12:"headingCount";s:1:"0";s:10:"mediaCount";s:1:"0";s:8:"keywords";s:5:"26.32";}'
rank_math_analytic_object_id:
  - 183
categories:
  - Automation

---
Terraform is an open-source infrastructure as code software tool that provides a consistent CLI workflow to manage hundreds of cloud services. Terraform codifies cloud APIs into declarative configuration files.

In this tutorial, we are going to learn how to install Terraform on Ubuntu 22.04.

**Infrastructure as code (IAC) tools allow you to manage your infrastructure with a configuration file rather than through a graphical interface. IAC allows you to build, change and manage your infrastructure in a safe, consistent, and repeatable way by defining configurations that you can version, reuse and share.**

## Where do we use Terraform? {.wp-block-heading}

Terraform is used in many use cases such as:

  * Infrastructure as code. We use Terraform to automate and provision our infrastructure such as servers, databases, firewalls, etc. This helps to accelerate cloud adoption because manual provisioning is always slow and cumbersome.
  * Manage network infrastructure. Terraform is used to automate key networking tasks such as updating load balancer member pools.
  * For multi-cloud deployments. Terraform helps in deploying serverless functions such as AWS Lambda.

## Advantages of Using Terraform {.wp-block-heading}

  * Terraform can manage infrastructure on multiple cloud platforms.
  * The human-readable form helps you write infrastructure code quickly.
  * Terraforms state helps you to track resource changes throughout your deployments.
  * You can commit your configurations to version control to safely collaborate on infrastructure.

To deploy infrastructure with Terraform you need to understand the following aspects:

  * **Scope**: You need to identify the infrastructure for your project in advance.
  * **Author**: Write the configuration manifest for your infrastructure.
  * **Initialize**: Install all the plugins needed by Terraform to manage your infrastructure.
  * **Plan**: Preview the changes Terraform will make to match your configurations.
  * **Apply**: Make the planned changes to suit your configs 

**What is good about Terraform is that it keeps track of your infrastructure in a state file. Terraform uses the state file to determine the changes to make in your infrastructure in order to match your configurations.**

## Install Terraform on Ubuntu 22.04 {.wp-block-heading}

Before we can install Terraform, make sure your system is up to date, you have **GnuPG, software-properties-common, and curl** packages installed.

### 1. Run system updates {.wp-block-heading}

Begin by updating your system repositories to make them up to date. Use the following command to run system updates.

<pre class="wp-block-code"><code>sudo apt update && apt upgrade -y </code></pre>

### 2. Install Terraform Package dependencies {.wp-block-heading}

Install the following dependencies, software-properties-common, GnuPG, and curl on your system using the following command.

<pre class="wp-block-code"><code>sudo apt install -y gnupg software-properties-common curl</code></pre>

It will not install any packages because Ubuntu 22.04 comes preinstalled with the following packages. So what you can do is just update them.

### 3. Add Hashicorp GPG key {.wp-block-heading}

Next in line is to add the Hashicorp GPG key into our system. We are going to install using the curl command we installed earlier.

<pre class="wp-block-code"><code>curl -fsSL https://apt.releases.hashicorp.com/gpg | sudo apt-key add -</code></pre>

### 4. Add Hashicorp repository to Ubuntu 22.04 {.wp-block-heading}

Now we need to tell our system where to get installation packages from and that is by adding the Hashicorp repository to our system repositories. To add repositories, we need to use the following command.

<pre class="wp-block-code"><code>sudo apt-add-repository "deb &#91;arch=amd64] https://apt.releases.hashicorp.com $(lsb_release -cs) main"</code></pre>

Check what has been added to your system. See below output

<pre class="wp-block-code"><code>Output
Repository: 'deb &#91;arch=amd64] https://apt.releases.hashicorp.com jammy main'
Description:
Archive for codename: jammy components: main
More info: https://apt.releases.hashicorp.com
Adding repository.
Press &#91;ENTER] to continue or Ctrl-c to cancel.
Adding deb entry to /etc/apt/sources.list.d/archive_uri-https_apt_releases_hashicorp_com-jammy.list
Adding disabled deb-src entry to /etc/apt/sources.list.d/archive_uri-https_apt_releases_hashicorp_com-jammy.list
Get:1 http://mirrors.digitalocean.com/ubuntu jammy InRelease &#91;270 kB]
Hit:2 http://mirrors.digitalocean.com/ubuntu jammy-updates InRelease                                                                        
Get:3 https://apt.releases.hashicorp.com jammy InRelease &#91;10.3 kB]                                                                          
Hit:4 http://mirrors.digitalocean.com/ubuntu jammy-backports InRelease                                                                      
Hit:5 https://repos-droplet.digitalocean.com/apt/droplet-agent main InRelease                    
Hit:6 http://security.ubuntu.com/ubuntu jammy-security InRelease           
Get:7 https://apt.releases.hashicorp.com jammy/main amd64 Packages &#91;57.7 kB]
Fetched 338 kB in 7s (48.3 kB/s)
Reading package lists... Done</code></pre>

Lastly is to update our system repositories again for changes to take effect.

<pre class="wp-block-code"><code>$ sudo apt update </code></pre>

### 5. Install Terraform on Ubuntu 22.04 {.wp-block-heading}

After we have met all the requirements, we can now run terraform installation with ease. Run the following command on your terminal.

<pre class="wp-block-code"><code>$ sudo apt install terraform</code></pre>

The sample output will be like this;

<pre class="wp-block-code"><code>Output
Reading package lists... Done
Building dependency tree... Done
Reading state information... Done
The following NEW packages will be installed:
  terraform
0 upgraded, 1 newly installed, 0 to remove and 0 not upgraded.
Need to get 19.9 MB of archives.
After this operation, 62.9 MB of additional disk space will be used.
Get:1 https://apt.releases.hashicorp.com jammy/main amd64 terraform amd64 1.2.3 &#91;19.9 MB]
Fetched 19.9 MB in 0s (91.7 MB/s)
Selecting previously unselected package terraform.
(Reading database ... 93711 files and directories currently installed.)
Preparing to unpack .../terraform_1.2.3_amd64.deb ...
Unpacking terraform (1.2.3) ...
Setting up terraform (1.2.3) ...
Scanning processes...                                                                                                                        
Scanning candidates...                                                                                                                       
Scanning linux images...  </code></pre>

Now that we have installed Terraform, we can verify the installation with the following command;

<pre class="wp-block-code"><code>&lt;strong>$ terraform -help init&lt;/strong>

Usage: terraform &#91;global options] init &#91;options]

  Initialize a new or existing Terraform working directory by creating
  initial files, loading any remote state, downloading modules, etc.

  This is the first command that should be run for any new or existing
  Terraform configuration per machine. This sets up all the local data
  necessary to run Terraform that is typically not committed to version
  control.

  This command is always safe to run multiple times. Though subsequent runs
  may give errors, this command will never delete your configuration or
  state. Even so, if you have important information, please back it up prior
  to running this command, just in case.</code></pre>

Check the version of installed Terraform with `<strong>terraform -v</strong>` command.

<pre class="wp-block-code"><code>terraform -v 
Terraform v1.2.3</code></pre>

Let’s install a package using Terraform to see if it’s actually working.

First, enable tab autocompletion

<pre class="wp-block-code"><code>$ touch ~/.bashrc</code></pre>

Then install the auto-completion package with the following command;

<pre class="wp-block-code"><code>$ terraform -install-autocomplete</code></pre>

Restart your shell for changes to take effect.

### Installing Terraform using the downloaded zip file {.wp-block-heading}

Another hustle-free way to install Terraform is to download the zip file and extract it to your preferred location. Let&#8217;s see how we can do that.

  1. Go to Terraform download section and hit download amd64 to download a zip file.
  2. To unzip the file install unzip using the following command `<mark style="background-color:#ffffff" class="has-inline-color has-vivid-purple-color">apt install unzip</mark>`.
  3. To unzip the file use `<mark style="background-color:rgba(0, 0, 0, 0);color:#a054e7" class="has-inline-color">unzip terraform_1.2.3_linux_amd64.zip</mark>`
  4. Move the unzipped file to `<mark style="background-color:rgba(0, 0, 0, 0)" class="has-inline-color has-vivid-purple-color">/usr/local/bin</mark>` directory using this command `<mark style="background-color:rgba(0, 0, 0, 0)" class="has-inline-color has-vivid-purple-color">mv terraform /usr/local/bin</mark>`
  5. Terraform is now installed, you can check the version with `<mark style="background-color:rgba(0, 0, 0, 0)" class="has-inline-color has-vivid-purple-color">terraform -v</mark>` command.

To initialize a project in Terraform we use, `<strong><mark style="background-color:rgba(0, 0, 0, 0)" class="has-inline-color has-vivid-purple-color">terraform init</mark></strong>` command

<pre class="wp-block-code"><code>terraform init</code></pre>

To provision a container, use `<strong>terraform apply</strong>` command 

<pre class="wp-block-code"><code>terraform apply</code></pre>

To stop a container use `<strong>terraform destroy</strong>` command

## Conclusion {.wp-block-heading}

We have successfully installed Terraform on Ubuntu 22.04. Continue learning more about Terraform using Its rich <a href="https://learn.hashicorp.com/collections/terraform/docker-get-started" target="_blank" rel="noreferrer noopener">documentation.</a>