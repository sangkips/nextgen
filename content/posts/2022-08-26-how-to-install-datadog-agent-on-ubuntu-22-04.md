---
title: How to install Datadog Agent on Ubuntu 22.04
author: Kipkoech Sang
type: post
date: 2022-08-26T13:19:57+00:00
url: /2022/08/26/how-to-install-datadog-agent-on-ubuntu-22-04/
categories:
  - Monitoring

---
In this tutorial, we are going to learn how to install Datadog Agent on Ubuntu 22.04.

### What is Datadog? {.wp-block-heading}

Datadog is an infrastructure monitoring service, it empowers users to keep track of the cloud infrastructure. It gives analytics for all services running on your infrastructure, with the help of dashboards making it easy for one to escalate errors whenever it occurs.

Datadog helps you monitor servers, databases, and all the applications running on your infrastructure. Datadog&#8217;s seamless integration with many products makes its unique saas application. Currently, Datadog can integrate easily with over 500 products. It also supports the integration of different APIs. Another thing also is it supports other apps written in other languages. 

In summary, Datadog has the following great features.

  * It supports other apps written in other languages.
  * Dashboards can easily be customized.
  * Has real-time alerting services.
  * It automatically collects and analyzes logs and errors.
  * It has seamless integration with many APIs 
  * It can easily integrate with many products out there.

### Datadog Agent Architecture.  {.wp-block-heading}

Here we need to talk about what makes up Datadog Agent. Let&#8217;s start with Components.

**Agent Components** 

The Datadog Agent is made up of the following components:

  * **The Collector**&#8211; This is responsible for gathering system and application metrics from the machine by temporarily executing standard utilities such iostat, vmstat etc or connecting to applications monitoring interfaces over TCP and HTTP.
  * **The Forwarder**&#8211; This is responsible for buffering and communicating with Datadog hq love SSL. It does this by listening over HTTP for any incoming requests then buffer them and forward to Datadog HQ over HTTPs protocol.
  * **The Dogstatsd-** This is responsible for aggregating local metrics sent from your code. Dogstatsd is a python implementation of etsy&#8217;s statsD metric aggregation daemon. 
  * **The supervisord-** This is responsible for keeping all previous processes up and running. 

### Installing Datadog Agent on Ubuntu 22.04 {.wp-block-heading}

To install Datadog Agent you need to have the account in order to give you an API for the free trial version. So go ahead and create your account.

### 1. Agent setup {.wp-block-heading}

Once you have created your account, you will be taken to a page like the figure below where you will choose your operating system to install the agent. For us here we will choose Ubuntu. It will then gives you step by step guide on how to install the Datadog Agent for the first time and when upgrading.<figure class="wp-block-image size-full">

<img decoding="async" loading="lazy" width="810" height="490" src="https://i0.wp.com/nextgentips.com/wp-content/uploads/2022/08/Screenshot-from-2022-08-26-11-51-34.png?resize=810%2C490&#038;ssl=1" alt="Nextgentips: Datadog agent" class="wp-image-1529" srcset="https://i0.wp.com/nextgentips.com/wp-content/uploads/2022/08/Screenshot-from-2022-08-26-11-51-34.png?w=948&ssl=1 948w, https://i0.wp.com/nextgentips.com/wp-content/uploads/2022/08/Screenshot-from-2022-08-26-11-51-34.png?resize=300%2C181&ssl=1 300w, https://i0.wp.com/nextgentips.com/wp-content/uploads/2022/08/Screenshot-from-2022-08-26-11-51-34.png?resize=768%2C464&ssl=1 768w" sizes="(max-width: 810px) 100vw, 810px" data-recalc-dims="1" /> <figcaption>Nextgentips: Datadog agent</figcaption></figure> 

### 2. Install Datadog {.wp-block-heading}

To install Datadog Agent, copy the easy install script onto your terminal and press enter.

<pre class="wp-block-code"><code>$ sudo -i # to take you to root user
$ DD_AGENT_MAJOR_VERSION=7 DD_API_KEY=7b8f7f1e565c731ccb40f86834b51024 DD_SITE="datadoghq.com" bash -c "$(curl -L https://s3.amazonaws.com/dd-agent/scripts/install_script.sh)"</code></pre>

If you are successful with installation, you should be in a position to see something like this:

<pre class="wp-block-code"><code>/usr/bin/systemctl
* Starting the Datadog Agent...



Your Datadog Agent is running and functioning properly. It will continue
to run in the background and submit metrics to Datadog.

If you ever want to stop the Datadog Agent, run:

     systemctl stop datadog-agent

And to run it again run:

     systemctl start datadog-agent</code></pre>

Check the version of the Datadog installed.

<pre class="wp-block-code"><code>$ datadog-agent version
Agent 7.38.2 - Commit: ba442fd - Serialization version: v5.0.23 - Go version: go1.17.11
</code></pre>

Also, the finish icon at the bottom will automatically become active because the system will sense the existence of the agent. Press finish to take you to your Datadog dashboard.<figure class="wp-block-image size-large">

<img decoding="async" loading="lazy" width="810" height="433" src="https://i0.wp.com/nextgentips.com/wp-content/uploads/2022/08/Screenshot-from-2022-08-26-13-21-39.png?resize=810%2C433&#038;ssl=1" alt="nextgentips: Datadog Dashboard" class="wp-image-1530" srcset="https://i0.wp.com/nextgentips.com/wp-content/uploads/2022/08/Screenshot-from-2022-08-26-13-21-39.png?resize=1024%2C548&ssl=1 1024w, https://i0.wp.com/nextgentips.com/wp-content/uploads/2022/08/Screenshot-from-2022-08-26-13-21-39.png?resize=300%2C160&ssl=1 300w, https://i0.wp.com/nextgentips.com/wp-content/uploads/2022/08/Screenshot-from-2022-08-26-13-21-39.png?resize=768%2C411&ssl=1 768w, https://i0.wp.com/nextgentips.com/wp-content/uploads/2022/08/Screenshot-from-2022-08-26-13-21-39.png?w=1124&ssl=1 1124w" sizes="(max-width: 810px) 100vw, 810px" data-recalc-dims="1" /> <figcaption>nextgentips: Datadog Dashboard</figcaption></figure>