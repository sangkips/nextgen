---
title: How to Install PostgreSQL 14 on Rocky Linux 8
author: Kipkoech Sang
type: post
date: 2021-10-28T18:21:51+00:00
url: /2021/10/28/how-to-install-postgresql-14-on-rocky-linux-8/
ocean_gallery_link_images:
  - off
ocean_disable_margins:
  - enable
ocean_display_top_bar:
  - default
ocean_display_header:
  - default
ocean_disable_title:
  - default
ocean_disable_heading:
  - default
ocean_disable_breadcrumbs:
  - default
ocean_display_footer_widgets:
  - default
ocean_display_footer_bottom:
  - default
ocean_link_format_target:
  - self
ocean_quote_format_link:
  - post
entry_views:
  - 1
view_ip:
  - 'a:1:{i:0;s:13:"49.205.247.22";}'
rank_math_description:
  - "PostgreSQL is the most advanced open source relational database. It's reliable, robust and provide excellent performance."
rank_math_focus_keyword:
  - Install postgresql
rank_math_news_sitemap_robots:
  - index
rank_math_robots:
  - 'a:1:{i:0;s:5:"index";}'
rank_math_analytic_object_id:
  - 119
rank_math_internal_links_processed:
  - 1
categories:
  - Database

---
In this tutorial we are going to learn how to install PostgreSQL 14 on Rocky Linux. PostgreSQL is world&#8217;s most advanced open source relational database. It has experienced running up to 30 years that is why it has earned strong reputation for its reliability, robustness and its strong performance.

PostgreSQL 14 brings a variety of features that help developers and administrators deploy there data backed applications. PostgreSQL 14 continues to build on complex data types include access to convenient JSON files and support for non contiguous ranges of data. PostgreSQL 14 its main agenda was to provide high performance and distributed data workloads. It advances in data concurrency, high write workloads, query parallelism and logical replication.

## New features contained in PostgreSQL 14 {.wp-block-heading}

  * The SQL standard **search** and **cycle** for options for common table expressions have been implemented. When computing a tree traversal using a recursive query, you might want to order the results in either depth-first or breadth-first order.
  * B-tree index updates are managed more effectively, reducing index plot.
  * **libpq** now has the ability to pipeline multiple queries, which can boost throughput over high latency connections. libpq&nbsp;is a set of library functions that allow client programs to pass queries to the&nbsp;PostgreSQL&nbsp;backend server and to receive the results of these queries.
  * Extended statistics can now be collected on expressions, allowing better planning for complex results.
  * Improvement have been made for parallel queries, heavily concurrent workloads, partitioned tables, logical replication and vacuuming. VACUUM reclaims storage occupied by dead tuples. In normal PostgreSQL operation, tuples that are deleted or obsoleted by an update are not physically removed from their table; they remain present until a VACUUM is done. Therefore it&#8217;s necessary to do VACUUM periodically, especially on frequently-updated tables.
  * VACUUM has become more aggressive, it skips inessential cleanup if the database starts to approach a transaction ID wraparound condition.

## Prerequisites {.wp-block-heading}

  * Rocky Linux 8 server
  * Internet connection
  * Familiar to command line interface

Check the following for more open source databases

  * [Databases you can use for free][1]

## 1. Install PostgreSQL 14 {.wp-block-heading}

Run system update as a starting point with the following command:

<pre class="wp-block-code"><code>$ sudo dnf update -y</code></pre>

First check the default version of Apstream for PostgreSQL with the following command:

<pre class="wp-block-code"><code>$ sudo dnf module list postgresql</code></pre>

<pre class="wp-block-code"><code># sudo dnf module list postgresql
DigitalOcean Droplet Agent                                                        39 kB/s | 3.3 kB     00:00    
Rocky Linux 8 - AppStream
Name                  Stream           Profiles                     Summary                                      
postgresql            9.6              client, server &#91;d]           PostgreSQL server and client module          
postgresql            10 &#91;d]           client, server &#91;d]           PostgreSQL server and client module          
postgresql            12               client, server &#91;d]           PostgreSQL server and client module          
postgresql            13               client, server &#91;d]           PostgreSQL server and client module          

Hint: &#91;d]efault, &#91;e]nabled, &#91;x]disabled, &#91;i]nstalled</code></pre>

As you can see the default version of **PostgreSQL** on **Apstream** is **PostgreSQL 10** as shown by [d]. So to install PostgreSQL 14.

## 2. Add official repository {.wp-block-heading}

In order to install PostgreSQL on rocky Linux we need to add official **YUM** repository with the following command:

<pre class="wp-block-code"><code>$ sudo dnf install https://download.postgresql.org/pub/repos/yum/reporpms/EL-8-x86_64/pgdg-redhat-repo-latest.noarch.rpm</code></pre>

Sample output

<pre class="wp-block-code"><code>....
Downloading Packages:
Running transaction check
Transaction check succeeded.
Running transaction test
Transaction test succeeded.
Running transaction
  Preparing        :                                                                                         1/1 
  Installing       : pgdg-redhat-repo-42.0-20.noarch                                                         1/1 
  Verifying        : pgdg-redhat-repo-42.0-20.noarch                                                         1/1 

Installed:
  pgdg-redhat-repo-42.0-20.noarch                                                                                

Complete!</code></pre>

## 3. Install PostgreSQL 14 on Rocky Linux 8 | Alma Linux 8 {.wp-block-heading}

Disable the default PostgreSQL 10 from the system so that it can allow you install the newer version.

<pre class="wp-block-code"><code>$ sudo dnf -qy module disable postgresql</code></pre>

Sample output

<pre class="wp-block-code"><code># sudo dnf -qy module disable postgresql
Importing GPG key 0x442DF0F8:
 Userid     : "PostgreSQL RPM Building Project &lt;pgsql-pkg-yum@postgresql.org&gt;"
 Fingerprint: 68C9 E2B9 1A37 D136 FE74 D176 1F16 D2E1 442D F0F8
 From       : /etc/pki/rpm-gpg/RPM-GPG-KEY-PGDG
Importing GPG key 0x442DF0F8:
 Userid     : "PostgreSQL RPM Building Project &lt;pgsql-pkg-yum@postgresql.org&gt;"
 Fingerprint: 68C9 E2B9 1A37 D136 FE74 D176 1F16 D2E1 442D F0F8
 From       : /etc/pki/rpm-gpg/RPM-GPG-KEY-PGDG
Importing GPG key 0x442DF0F8:
 Userid     : "PostgreSQL RPM Building Project &lt;pgsql-pkg-yum@postgresql.org&gt;"
 Fingerprint: 68C9 E2B9 1A37 D136 FE74 D176 1F16 D2E1 442D F0F8
 From       : /etc/pki/rpm-gpg/RPM-GPG-KEY-PGDG
Importing GPG key 0x442DF0F8:
 Userid     : "PostgreSQL RPM Building Project &lt;pgsql-pkg-yum@postgresql.org&gt;"
 Fingerprint: 68C9 E2B9 1A37 D136 FE74 D176 1F16 D2E1 442D F0F8
 From       : /etc/pki/rpm-gpg/RPM-GPG-KEY-PGDG
Importing GPG key 0x442DF0F8:
 Userid     : "PostgreSQL RPM Building Project &lt;pgsql-pkg-yum@postgresql.org&gt;"
 Fingerprint: 68C9 E2B9 1A37 D136 FE74 D176 1F16 D2E1 442D F0F8
 From       : /etc/pki/rpm-gpg/RPM-GPG-KEY-PGDG
Importing GPG key 0x442DF0F8:
 Userid     : "PostgreSQL RPM Building Project &lt;pgsql-pkg-yum@postgresql.org&gt;"
 Fingerprint: 68C9 E2B9 1A37 D136 FE74 D176 1F16 D2E1 442D F0F8
 From       : /etc/pki/rpm-gpg/RPM-GPG-KEY-PGDG
Importing GPG key 0x442DF0F8:
 Userid     : "PostgreSQL RPM Building Project &lt;pgsql-pkg-yum@postgresql.org&gt;"
 Fingerprint: 68C9 E2B9 1A37 D136 FE74 D176 1F16 D2E1 442D F0F8
 From       : /etc/pki/rpm-gpg/RPM-GPG-KEY-PGDG</code></pre>

Then we can enable PostgreSQL 14 with the following command:

<pre class="wp-block-code"><code>$ sudo dnf -y install postgresql14 postgresql14-server</code></pre>

<pre class="wp-block-code"><code>....
Preparing        :                                                                                         1/1 
  Installing       : postgresql14-libs-14.0-1PGDG.rhel8.x86_64                                               1/5 
  Running scriptlet: postgresql14-libs-14.0-1PGDG.rhel8.x86_64                                               1/5 
  Installing       : libicu-60.3-2.el8_1.x86_64                                                              2/5 
  Running scriptlet: libicu-60.3-2.el8_1.x86_64                                                              2/5 
  Installing       : lz4-1.8.3-3.el8_4.x86_64                                                                3/5 
  Installing       : postgresql14-14.0-1PGDG.rhel8.x86_64                                                    4/5 
  Running scriptlet: postgresql14-14.0-1PGDG.rhel8.x86_64                                                    4/5 
  Running scriptlet: postgresql14-server-14.0-1PGDG.rhel8.x86_64                                             5/5 
  Installing       : postgresql14-server-14.0-1PGDG.rhel8.x86_64                                             5/5 
  Running scriptlet: postgresql14-server-14.0-1PGDG.rhel8.x86_64                                             5/5 
  Verifying        : libicu-60.3-2.el8_1.x86_64                                                              1/5 
  Verifying        : lz4-1.8.3-3.el8_4.x86_64                                                                2/5 
  Verifying        : postgresql14-14.0-1PGDG.rhel8.x86_64                                                    3/5 
  Verifying        : postgresql14-libs-14.0-1PGDG.rhel8.x86_64                                               4/5 
  Verifying        : postgresql14-server-14.0-1PGDG.rhel8.x86_64                                             5/5 

Installed:
  libicu-60.3-2.el8_1.x86_64                               lz4-1.8.3-3.el8_4.x86_64                              
  postgresql14-14.0-1PGDG.rhel8.x86_64                     postgresql14-libs-14.0-1PGDG.rhel8.x86_64             
  postgresql14-server-14.0-1PGDG.rhel8.x86_64             

Complete!</code></pre>

## 4. Enable PostgreSQL 14 service and initialize the database {.wp-block-heading}

We can initialize our database first with this command:

<pre class="wp-block-code"><code>$ sudo /usr/pgsql-14/bin/postgresql-14-setup initdb</code></pre>

You will get the following output.

<pre class="wp-block-code"><code># sudo /usr/pgsql-14/bin/postgresql-14-setup initdb
Initializing database ... OK</code></pre>

We can enable the service with the following command:

<pre class="wp-block-code"><code>$ sudo systemctl enable --now postgresql-14</code></pre>

Then you can start the service thereafter with the following command.

<pre class="wp-block-code"><code>$ sudo systemctl start postgresql-14</code></pre>

Confirm that the services is running first in order for you to continue.

<pre class="wp-block-code"><code>$ sudo systemctl status postgresql-14</code></pre>

If you get the following as output, you are on the right track

<pre class="wp-block-code"><code># sudo systemctl status postgresql-14
● postgresql-14.service - PostgreSQL 14 database server
   Loaded: loaded (/usr/lib/systemd/system/postgresql-14.service; enabled; vendor preset: disabled)
   Active: active (running) since Thu 2021-10-28 17:48:21 UTC; 8min ago
     Docs: https://www.postgresql.org/docs/14/static/
  Process: 64082 ExecStartPre=/usr/pgsql-14/bin/postgresql-14-check-db-dir ${PGDATA} (code=exited, status=0/SUCC&gt;
 Main PID: 64087 (postmaster)
    Tasks: 8 (limit: 11189)
   Memory: 16.8M
   CGroup: /system.slice/postgresql-14.service
           ├─64087 /usr/pgsql-14/bin/postmaster -D /var/lib/pgsql/14/data/
           ├─64089 postgres: logger 
           ├─64091 postgres: checkpointer 
           ├─64092 postgres: background writer 
           ├─64093 postgres: walwriter 
           ├─64094 postgres: autovacuum launcher 
           ├─64095 postgres: stats collector 
           └─64096 postgres: logical replication launcher 

Oct 28 17:48:21 rockylinux systemd&#91;1]: Starting PostgreSQL 14 database server...
Oct 28 17:48:21 rockylinux postmaster&#91;64087]: 2021-10-28 17:48:21.106 UTC &#91;64087] LOG:  redirecting log output t&gt;
Oct 28 17:48:21 rockylinux postmaster&#91;64087]: 2021-10-28 17:48:21.106 UTC &#91;64087] HINT:  Future log output will &gt;
Oct 28 17:48:21 rockylinux systemd&#91;1]: Started PostgreSQL 14 database server.</code></pre>

## Conclusion {.wp-block-heading}

Congratulations! You have installed PostgreSQL 14 on Rocky Linux 8, you have also learn why PostgreSQL is important to large data set. For more information always consult the [PostgreSQL documentation.][2]

 [1]: https://nextgentips.com/2021/10/02/databases-you-can-use-for-free/
 [2]: https://www.postgresql.org/files/documentation/pdf/14/postgresql-14-A4.pdf