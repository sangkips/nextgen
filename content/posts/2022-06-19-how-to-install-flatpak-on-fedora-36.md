---
title: How to install Flatpak on Fedora 36
author: Kipkoech Sang
type: post
date: 2022-06-19T10:52:39+00:00
url: /2022/06/19/how-to-install-flatpak-on-fedora-36/
rank_math_internal_links_processed:
  - 1
rank_math_seo_score:
  - 67
rank_math_primary_category:
  - 6
rank_math_focus_keyword:
  - flatpak on fedora 36
rank_math_analytic_object_id:
  - 186
categories:
  - Linux

---
Flatpak is a utility for software deployment and package management for Linux. Flatpak offers a sandbox environment in which users can run application software in isolation from the rest of the system.

Flatpak can be used by all types of desktop environments and aims to be as agnostic as possible regarding how applications are built.

Flatpak runtimes and applications are built as <a href="https://github.com/opencontainers/image-spec/blob/main/spec.md" target="_blank" rel="noreferrer noopener">OCI images</a> and are distributed with the Fedora registry

Flatpaks are a new way of deploying applications.

In this tutorial, we are going to learn how to install Flatpak on Fedora 36.

**Advantages of using Flatpaks on Fedora**

  * Applications can easily be updated without rebooting the system
  * Applications can easily be installed on Fedora silverblue
  * Flatpaks works along all supported Fedora versions
  * Flatpaks can be run by all users running on other distributions.

## Prerequisites {#prerequisites.wp-block-heading}

  * Have Fedora 36 workstation up and running
  * User with sudo privileges
  * Have basic knowledge of terminal commands

Related Articles

  * <a href="https://nextgentips.com/2021/11/22/how-to-install-and-configure-flatpak-on-ubuntu-20-04/" target="_blank" rel="noreferrer noopener">How to install and configure Flatpak on Ubuntu 20.04</a>

## Install Flatpak on Fedora 36 {#install-flatpak-on-fedora-35.wp-block-heading}

### 1. Install updates {#1-install-updates.wp-block-heading}

The first thing for any system is to update its repositories in order to make them up to date.

<pre class="wp-block-code"><code>$ sudo dnf update -y</code></pre>

### 2. Install Flatpak on Fedora 36 {#3-install-flatpak-on-fedora-35.wp-block-heading}

Now we can install Flatpak

<pre class="wp-block-code"><code>$ sudo dnf install flatpak-module-tools</code></pre>

Check the version of the installed Flatpak

<pre class="wp-block-code"><code>$ flatpak --version
Flatpak 1.12.7</code></pre>

### 3. Add the user to the mock group {#3-add-user-to-mock-group.wp-block-heading}

**Mock** is a tool for a reproducible build of RPM packages. It is used by the Fedora build system to populate a chroot environment which is then used in building a source RPM.

To add the user to the mock group use the following command

<pre class="wp-block-code"><code>$ sudo usermod -a -G mock $USER</code></pre>

Restart your system for the changes to take effect.

### 4. Install Flatpak runtime environment {#5-install-flatpak-runtime-environment.wp-block-heading}

Flatpak runs as a container in the Fedora platform, therefore we are supposed to enable remote testing on our system.

**Add remote testing**

<pre class="wp-block-code"><code>$ flatpak remote-add fedora-testing oci+https://registry.fedoraproject.org#testing</code></pre>

**Enable remote testing**

<pre class="wp-block-code"><code>$ flatpak remote-modify --enable fedora-testing</code></pre>

**Test Flatpak**

To know if it’s working try the following command.

<pre class="wp-block-code"><code>$ flatpak install fedora-testing org.fedoraproject.Platform/x86_64/f35</code></pre>

<pre class="wp-block-code"><code>Looking for matches…


        ID                                    Branch           Op           Remote                   Download
        ID                                    Branch           Op           Remote                   Download
 1. &#91;✓] org.fedoraproject.Platform            f35              i            fedora-testing           653.4 MB / 653.4 MB

Installing… ████████████████████ 100%  130.7 MB/s  00:00</code></pre>

### 5. Enable Flathub {#6-enable-flathub.wp-block-heading}

Flathub is an app repository in Flatpak. It’s like Dockerhub for docker images. So to install any app you want to use run the following command.

<pre class="wp-block-code"><code>$ sudo wget https://flathub.org/repo/flathub.flatpakrepo</code></pre>

For some reason, the following command can work alone on GNOME and KDE Fedora installation. If it fails for some reason add Fathub remote manually with the following command.

<pre class="wp-block-code"><code>$ flatpak remote-add --if-not-exists flathub https://flathub.org/repo/flathub.flatpakrepo</code></pre>

### 6. Install the app with Flathub {#install-app-with-flathub.wp-block-heading}

Let’s look at an example of how to install an application using Flathub. Let’s install steam

<pre class="wp-block-code"><code>$ flatpak install flathub com.discordapp.Discord</code></pre>

You will see the following

<pre class="wp-block-code"><code>Looking for matches…
Required runtime for com.discordapp.Discord/x86_64/stable (runtime/org.freedesktop.Platform/x86_64/21.08) found in remote flathub
Do you want to install it? &#91;Y/n]: y

com.discordapp.Discord permissions:
    ipc            network       pulseaudio      x11      devices      file access &#91;1]      dbus access &#91;2]
    tags &#91;3]

    &#91;1] xdg-download, xdg-pictures:ro, xdg-videos:ro
    &#91;2] com.canonical.AppMenu.Registrar, com.canonical.Unity.LauncherEntry, com.canonical.indicator.application,
        org.freedesktop.Notifications, org.kde.StatusNotifierWatcher
    &#91;3] proprietary


        ID                                             Branch            Op           Remote            Download
 1. &#91;✓] org.freedesktop.Platform.GL.default            21.08             i            flathub           131.0 MB / 131.3 MB
 2. &#91;✓] org.freedesktop.Platform.Locale                21.08             i            flathub            17.7 kB / 325.8 MB
 3. &#91;✓] org.freedesktop.Platform.openh264              2.0               i            flathub             1.5 MB / 1.5 MB
 4. &#91;✓] org.freedesktop.Platform                       21.08             i            flathub           164.1 MB / 200.3 MB
 5. &#91;✓] com.discordapp.Discord                         stable            i            flathub            80.4 MB / 81.7 MB

Installing 5/5… ████████████████████ 100%  20.1 MB/s  00:00</code></pre>

To **run steam** do the following

<pre class="wp-block-code"><code>$ flatpak run com.discordapp.Discord</code></pre>

## Conclusion {#conclusion.wp-block-heading}

We have successfully installed Flatpak on Fedora 36. To learn more about Flatpak, use its <a href="https://docs.flatpak.org/en/latest/getting-started.html" target="_blank" rel="noreferrer noopener">documentation</a>. I hope you enjoyed and learned something new.