---
title: How to install and use FastAPI
author: Kipkoech Sang
type: post
date: 2022-11-29T11:07:40+00:00
url: /2022/11/29/how-to-install-and-use-fastapi/
categories:
  - Programming

---
FastAPI is a web framework for developing RESTful APIs in Python. It is based on pydantic and type hints to validate, serialize and deserialize data and automatically auto-generate OpenAPI documents.

### Features of FastAPI {.wp-block-heading}

  * Fast
  * Fast to code with
  * Fewer bugs
  * Intuitive has a code editor completion feature
  * Easy to learn and use.
  * Has minimized code duplication
  * Robust, you get production-ready code
  * Standards-based. It&#8217;s entirely based on open standards 

### Installing FastAPI {.wp-block-heading}

To start using FastAPI, you need to install it on your system, then after that, install ASGI server to run on such Uvicorn or Hypercorn.

To install FastAPI run the following command from the terminal<mark style="background-color:#abb8c3" class="has-inline-color"> `pip install fastapi`</mark>

<pre class="wp-block-code"><code>pip install fastapi</code></pre>

Next is to install a server that is Uvicorn `<mark style="background-color:#abb8c3" class="has-inline-color">pip install "uvicorn[standard]"</mark>`

<pre class="wp-block-code"><code>pip install "uvicorn&#91;standard]"</code></pre>

That is all about the installation. Let&#8217;s see an example of FastAPI. Create a project folder and cd into the project folder. Open the project on your preferred code editor. Create a main.py file and paste it .0into the following code.

<pre class="wp-block-code"><code>from fastapi import FastAPI

app = FastAPI()

@app.get('/')
def root():
    return {'Message': 'World'</code></pre>

To run the above code use the following command `<mark style="background-color:#abb8c3" class="has-inline-color">uvicorn main:app --reload</mark>`

<pre class="wp-block-code"><code>uvicorn main:app --reload</code></pre>

<mark style="background-color:#abb8c3" class="has-inline-color">`--reload` </mark>tells the server to reload every time new changes are introduced.

If it starts the server correctly, you will be in a position to see the following as the output.

<pre class="wp-block-code"><code>INFO:     Will watch for changes in these directories: &#91;'/home/sang-pc/Documents/dev/fastapi/project']
INFO:     Uvicorn running on http://127.0.0.1:8000 (Press CTRL+C to quit)
INFO:     Started reloader process &#91;3192] using WatchFiles
INFO:     Started server process &#91;3194]
INFO:     Waiting for application startup.
INFO:     Application startup complete.
</code></pre>

To see the changes, now go to the browser and open `<mark style="background-color:#abb8c3" class="has-inline-color">http://127.0.0.1:8000</mark>`, you will see your message on the browser.

### Accessing Interactive API docs  {.wp-block-heading}

To access a more interactive API user interface, use docs UI. To access docs UI run the following from your browser.

<pre class="wp-block-code"><code>http:&#47;&#47;127.0.0.1:8000/docs</code></pre>

Yow will see a very clean UI to test your project<figure class="wp-block-image size-large">

<img decoding="async" loading="lazy" width="810" height="365" src="https://i0.wp.com/nextgentips.com/wp-content/uploads/2022/11/Screenshot-from-2022-11-29-14-02-52.png?resize=810%2C365&#038;ssl=1" alt="Nextgentips: Fastapi Swagger UI" class="wp-image-1672" srcset="https://i0.wp.com/nextgentips.com/wp-content/uploads/2022/11/Screenshot-from-2022-11-29-14-02-52.png?resize=1024%2C461&ssl=1 1024w, https://i0.wp.com/nextgentips.com/wp-content/uploads/2022/11/Screenshot-from-2022-11-29-14-02-52.png?resize=300%2C135&ssl=1 300w, https://i0.wp.com/nextgentips.com/wp-content/uploads/2022/11/Screenshot-from-2022-11-29-14-02-52.png?resize=768%2C346&ssl=1 768w, https://i0.wp.com/nextgentips.com/wp-content/uploads/2022/11/Screenshot-from-2022-11-29-14-02-52.png?w=1303&ssl=1 1303w" sizes="(max-width: 810px) 100vw, 810px" data-recalc-dims="1" /> <figcaption class="wp-element-caption">Nextgentips: Fastapi Swagger UI</figcaption></figure> 

If you love terminal, you can still get the results by using the curl command like this:

<pre class="wp-block-code"><code>curl -X 'GET' \
  'http://127.0.0.1:8000/' \
  -H 'accept: application/json'</code></pre>

From here now you can start writing your restful API logic.