---
title: How to Install and Configure MariaDB 10.7 on Fedora 35
author: Kipkoech Sang
type: post
date: 2021-12-25T07:50:49+00:00
url: /2021/12/25/how-to-install-and-configure-mariadb-10-7-on-fedora-35/
rank_math_news_sitemap_robots:
  - index
rank_math_robots:
  - 'a:1:{i:0;s:5:"index";}'
rank_math_analytic_object_id:
  - 53
rank_math_internal_links_processed:
  - 1
categories:
  - Database

---
[MariaDB][1] Server is one of the most popular open-source relational databases. It’s made by the original developers of MySQL and guaranteed to stay open source. It is part of most cloud offerings and the default in most Linux distributions.

It is built upon the values of performance, stability, and openness, and MariaDB Foundation ensures contributions will be accepted on technical merit. Recent new functionality includes advanced clustering with [Galera Cluster 4][2], compatibility features with Oracle Database and Temporal Data Tables, allowing one to query the data as it stood at any point in the past.

Welcome to today&#8217;s topic on how to install MariaDB 10.08 on Fedora 35 distribution.

## Related Articles  {.wp-block-heading}

  * [How to install MariaDB 10.7 on Ubuntu 21.04][3]

## Installing MariaDB on Fedora 35 {.wp-block-heading}

To install MariaDB 10.8 Alpha, we can first have to update the system repositories to make our system up to date.

<pre class="wp-block-code"><code>$ sudo dnf update -y</code></pre>

I am going to install MariaDB from the Modular repository in order to enable install the version I require. 

## List available versions of MariaDB. {.wp-block-heading}

To begin with we need to list the available versions of MariaDB. Open your terminal and type the following command;

<pre class="wp-block-code"><code>$ sudo dnf modular list mariadb</code></pre>

You will get the following output.

<pre class="wp-block-code"><code>Output
Fedora Modular 35 - x86_64
Name        Stream    Profiles                             Summary                                               
mariadb     10.3      client, devel, galera, server &#91;d]    MariaDB: a very fast and robust SQL database server   
mariadb     10.4      client, devel, galera, server &#91;d]    MariaDB: a very fast and robust SQL database server   
mariadb     10.5      client, devel, galera, server &#91;d]    MariaDB: a very fast and robust SQL database server   

Fedora Modular 35 - x86_64 - Updates
Name        Stream    Profiles                             Summary                                               
mariadb     10.3      client, devel, galera, server &#91;d]    MariaDB: a very fast and robust SQL database server   
mariadb     10.4      client, devel, galera, server &#91;d]    MariaDB: a very fast and robust SQL database server   
mariadb     10.5      client, devel, galera, server &#91;d]    MariaDB: a very fast and robust SQL database server   
mariadb     10.6      client, devel, galera, server        MariaDB: a very fast and robust SQL database server   
mariadb     10.7      client, devel, galera, server        MariaDB: a very fast and robust SQL database server   

Hint: &#91;d]efault, &#91;e]nabled, &#91;x]disabled, &#91;i]nstalled</code></pre>

In order to enable the MariaDB, you need to use and set stream RPMs you will have to enable with the following command;

<pre class="wp-block-code"><code>$ sudo dnf module enable mariadb:10.7</code></pre>

<pre class="wp-block-code"><code>Output
Package                   Architecture             Version                      Repository                 Size
=================================================================================================================
Enabling module streams:
 mariadb                                            10.7                                                        

Transaction Summary
=================================================================================================================

Is this ok &#91;y/N]: y
Complete!</code></pre>

Now that we have enabled the MariaDB version we would like to use, check if the server version is also available.

<pre class="wp-block-code"><code>$ sudo dnf list mariadb-server</code></pre>

<pre class="wp-block-code"><code>Output
Available Packages
mariadb-server.x86_64                    3:10.7.1-1.module_f35+13505+f0394ab9                     updates-modular</code></pre>

For us, we have our Mariadb server available.

## Install MariaDB Server {.wp-block-heading}

To install MariaDB run the following command on your terminal.

<pre class="wp-block-code"><code>$ sudo dnf module install mariadb/server</code></pre>

When the installation is complete, it&#8217;s now time to enable MariaDB.

## Configuring MariaDB 10.7 {.wp-block-heading}

Enable MariaDB service at boot. To do so run the following on your terminal.

<pre class="wp-block-code"><code>$ sudo systemctl enbale mariadb</code></pre>

Then start the MariaDB service with the following command;

<pre class="wp-block-code"><code>$ sudo systemctl start mariadb</code></pre>

Lastly, you can check the status of MariaDB with the following;

<pre class="wp-block-code"><code>$ sudo systemctl status mariadb</code></pre>

<pre class="wp-block-code"><code>Output
● mariadb.service - MariaDB 10.7 database server
     Loaded: loaded (/usr/lib/systemd/system/mariadb.service; enabled; vendor preset: disabled)
     Active: active (running) since Sat 2021-12-25 07:32:13 UTC; 1min 29s ago
       Docs: man:mariadbd(8)
             https:&#47;&#47;mariadb.com/kb/en/library/systemd/
    Process: 19900 ExecStartPre=/usr/libexec/mariadb-check-socket (code=exited, status=0/SUCCESS)
    Process: 19922 ExecStartPre=/usr/libexec/mariadb-prepare-db-dir mariadb.service (code=exited, status=0/SUCCE>
    Process: 20015 ExecStartPost=/usr/libexec/mariadb-check-upgrade (code=exited, status=0/SUCCESS)
   Main PID: 20004 (mariadbd)
     Status: "Taking your SQL requests now..."
      Tasks: 9 (limit: 1112)
     Memory: 121.1M
        CPU: 1.048s
     CGroup: /system.slice/mariadb.service
             └─20004 /usr/libexec/mariadbd --basedir=/usr

Dec 25 07:32:13 fedora mariadb-prepare-db-dir&#91;19961]: See the MariaDB Knowledgebase at https://mariadb.com/kb or>
Dec 25 07:32:13 fedora mariadb-prepare-db-dir&#91;19961]: MySQL manual for more instructions.
Dec 25 07:32:13 fedora mariadb-prepare-db-dir&#91;19961]: Please report any problems at https://mariadb.org/jira
Dec 25 07:32:13 fedora mariadb-prepare-db-dir&#91;19961]: The latest information about MariaDB is available at https>
Dec 25 07:32:13 fedora mariadb-prepare-db-dir&#91;19961]: You can find additional information about the MySQL part a>
Dec 25 07:32:13 fedora mariadb-prepare-db-dir&#91;19961]: https://dev.mysql.com
Dec 25 07:32:13 fedora mariadb-prepare-db-dir&#91;19961]: Consider joining MariaDB's strong and vibrant community:
Dec 25 07:32:13 fedora mariadb-prepare-db-dir&#91;19961]: https://mariadb.org/get-involved/
Dec 25 07:32:13 fedora mariadbd&#91;20004]: 2021-12-25  7:32:13 0 &#91;Note] /usr/libexec/mariadbd (server 10.7.1-MariaD>
Dec 25 07:32:13 fedora systemd&#91;1]: Started MariaDB 10.7 database server.</code></pre>

If it happens you are getting service not active, repeat the following steps to enable the service again.

## Configure SQL before use. {.wp-block-heading}

Before using MariaDB run secure installation command to run basic configuration. Use the following command to do so;

<pre class="wp-block-code"><code>$ sudo mysql_secure_installation</code></pre>

Follow all the prompts to run a secure installation.

<pre class="wp-block-code"><code>Output
NOTE: RUNNING ALL PARTS OF THIS SCRIPT IS RECOMMENDED FOR ALL MariaDB
      SERVERS IN PRODUCTION USE!  PLEASE READ EACH STEP CAREFULLY!

In order to log into MariaDB to secure it, we'll need the current
password for the root user. If you've just installed MariaDB, and
haven't set the root password yet, you should just press enter here.

Enter current password for root (enter for none): 
OK, successfully used password, moving on...

Setting the root password or using the unix_socket ensures that nobody
can log into the MariaDB root user without the proper authorisation.

You already have your root account protected, so you can safely answer 'n'.

Switch to unix_socket authentication &#91;Y/n] n
 ... skipping.

You already have your root account protected, so you can safely answer 'n'.

Change the root password? &#91;Y/n] n
 ... skipping.

By default, a MariaDB installation has an anonymous user, allowing anyone
to log into MariaDB without having to have a user account created for
them.  This is intended only for testing, and to make the installation
go a bit smoother.  You should remove them before moving into a
production environment.

Remove anonymous users? &#91;Y/n] y
 ... Success!

Normally, root should only be allowed to connect from 'localhost'.  This
ensures that someone cannot guess at the root password from the network.

Disallow root login remotely? &#91;Y/n] y
 ... Success!

By default, MariaDB comes with a database named 'test' that anyone can
access.  This is also intended only for testing, and should be removed
before moving into a production environment.

Remove test database and access to it? &#91;Y/n] y
 - Dropping test database...
 ... Success!
 - Removing privileges on test database...
 ... Success!

Reloading the privilege tables will ensure that all changes made so far
will take effect immediately.

Reload privilege tables now? &#91;Y/n] y
 ... Success!

Cleaning up...

All done!  If you've completed all of the above steps, your MariaDB
installation should now be secure.

Thanks for using MariaDB!</code></pre>

In order to run SQL use the following command;

<pre class="wp-block-code"><code>$ sudo mysql -u root -p</code></pre>

If you didn&#8217;t set the password use the above command.

<pre class="wp-block-code"><code>Output
Enter password: 
Welcome to the MariaDB monitor.  Commands end with ; or \g.
Your MariaDB connection id is 9
Server version: 10.7.1-MariaDB MariaDB Server

Copyright (c) 2000, 2018, Oracle, MariaDB Corporation Ab and others.

Type 'help;' or '\h' for help. Type '\c' to clear the current input statement.

MariaDB &#91;(none)]> </code></pre>

## Conclusion {.wp-block-heading}

We have successfully installed and configured MariaDB 10.7 on our Fedora 35 distribution. Continue exploring different SQL commands to learn more. In case of issues contact us or consult [MariaDB documentation][4].

 [1]: https://mariadb.org/
 [2]: https://galeracluster.com/library/whats-new.html
 [3]: https://nextgentips.com/2021/10/15/how-to-install-mariadb-10-7-on-ubuntu-21-04/
 [4]: https://mariadb.org/documentation/