---
title: How to install and configure Memcached on Fedora 35
author: Kipkoech Sang
type: post
date: 2021-11-16T10:20:10+00:00
url: /2021/11/16/how-to-install-and-configure-memcached-on-fedora-35/
ocean_gallery_link_images:
  - off
ocean_disable_margins:
  - enable
ocean_display_top_bar:
  - default
ocean_display_header:
  - default
ocean_disable_title:
  - default
ocean_disable_heading:
  - default
ocean_disable_breadcrumbs:
  - default
ocean_display_footer_widgets:
  - default
ocean_display_footer_bottom:
  - default
ocean_link_format_target:
  - self
ocean_quote_format_link:
  - post
rank_math_news_sitemap_robots:
  - index
rank_math_robots:
  - 'a:1:{i:0;s:5:"index";}'
rank_math_analytic_object_id:
  - 100
rank_math_internal_links_processed:
  - 1
categories:
  - Linux

---
In this tutorial guide I will be guiding you through installation and configuration of Memcached on Fedora 35.

Memcached is a general-purpose distributed memory caching system. It is used to speed up dynamic databases-driven websites by caching data and objects in the RAM to reduce the number of times an external data source must be read.

Memcached has an API with a very large hash table distributed distributed across multiple machines. when a table is full, subsequent inserts cause older data to be purged in least recently used order. 

Memcached is simple yet powerful. Its simple design provides quick deployment, ease of development and solves many problems facing large data caches.

## Related Articles  {.wp-block-heading}

[How to Flush DNS cache on Ubuntu 20.04][1]

## Prerequisites  {.wp-block-heading}

  * Root access to the server or sudo user
  * Access to internet
  * Fedora 35 server

## Table of Contents {.wp-block-heading}

  1. Update the system repositories 
  2. Install memcached 
  3. Configure memcached 
  4. install memcached PHP extension 
  5. Conclusion

## 1. Update Fedora system repositories  {.wp-block-heading}

To begin our installation we need to make sure all the repositories are up to date. 

<pre class="wp-block-code"><code>$ sudo dnf update</code></pre>

After the updates is complete then we can install Memcached.

## 2. Install Memcached  {.wp-block-heading}

To begin our installation, type the following command into the terminal to run the installation.

<pre class="wp-block-code"><code>$ sudo dnf install memcached -y</code></pre>

This will run the installation of memcached in your system. Wait for the installation to finish then move on to configuration. It will install dependencies in the process.

## 3. Configure Memcached  {.wp-block-heading}

We need to make some configuration in memcached. The default configuration is found in the /etc/ sysconfig/memcached.

<pre class="wp-block-code"><code>$ sudo vim /etc/sysconfig/memcached</code></pre>

The output should look as follows 

<pre class="wp-block-code"><code>PORT="11211"
USER="memcached"
MAXCONN="1024"
CACHESIZE="64"
OPTIONS="-l 127.0.0.1,::1"</code></pre>

Change the following to suite your needs. After you have completed making any changes save the file and exit then restart your memcached.

<pre class="wp-block-code"><code>$ sudo systemctl restart memcached </code></pre>

As the best practice enable Memcached to start on reboot, use the following command;

<pre class="wp-block-code"><code>$ sudo systemctl enable memcached</code></pre>

To view the status of Memcached do the following in your terminal.

<pre class="wp-block-code"><code>$ sudo systemctl status memcached </code></pre>

Your output should look like this 

<pre class="wp-block-code"><code># sudo systemctl status memcached 
● memcached.service - memcached daemon
     Loaded: loaded (/usr/lib/systemd/system/memcached.service; enabled; vendor preset: disabled)
     Active: active (running) since Tue 2021-11-16 09:02:47 UTC; 3min 33s ago
   Main PID: 19179 (memcached)
      Tasks: 10 (limit: 2319)
     Memory: 1.6M
        CPU: 46ms
     CGroup: /system.slice/memcached.service
             └─19179 /usr/bin/memcached -p 11211 -u memcached -m 1024 -c 1024 -l 127.0.0.1,::1

Nov 16 09:02:47 fedora systemd&#91;1]: Started memcached daemon.</code></pre>

You can also restart memcached with the following command;

<pre class="wp-block-code"><code>$ sudo /etc/init.d/memcached restart</code></pre>

Restarting Memcached will drop all sessions stored in an instance. 

## 4. Install Memcached PHP Extension {.wp-block-heading}

Install the following PHP extension php-pecl-memcached as shown below;

<pre class="wp-block-code"><code>$ sudo dnf search php memcached</code></pre>

The output will be as follows:

<pre class="wp-block-code"><code>Last metadata expiration check: 0:40:53 ago on Tue 16 Nov 2021 09:08:30 AM UTC.
==================================== Name & Summary Matched: memcached, php =====================================
php-pecl-memcached.x86_64 : Extension to work with the Memcached caching daemon
php-pecl-memcache.x86_64 : Extension to work with the Memcached caching daemon</code></pre>

So let&#8217;s begin the installation with the following command

<pre class="wp-block-code"><code>$ sudo dnf install php-pecl-memcached

Installed:
  fastlz-0.1.0-0.16.20070619svnrev12.fc35.x86_64             libmemcached-awesome-1.1.1-1.fc35.x86_64            
  php-common-8.0.12-2.fc35.x86_64                            php-pecl-igbinary-3.2.5-1.fc35.x86_64               
  php-pecl-memcached-3.1.5-7.fc35.x86_64                     php-pecl-msgpack-2.1.2-4.fc35.x86_64                
</code></pre>

Now you can restart memcached and Apache for the changes to take effect.

<pre class="wp-block-code"><code>$ sudo systemctl restart memcached 
$ sudo systemctl restart httpd</code></pre>

Finally you can check the php.ini configuration file to confirm everything.

<pre class="wp-block-code"><code>$ php -i | grep memcached</code></pre>

The output should be as follows;

<pre class="wp-block-code"><code>/etc/php.d/50-memcached.ini
memcached
memcached support => enabled
libmemcached version => 1.1.1
memcached.compression_factor => 1.3 => 1.3
memcached.compression_threshold => 2000 => 2000
memcached.compression_type => fastlz => fastlz
memcached.default_binary_protocol => Off => Off
memcached.default_connect_timeout => 0 => 0
memcached.default_consistent_hash => Off => Off
memcached.serializer => igbinary => igbinary
memcached.sess_binary_protocol => On => On
memcached.sess_connect_timeout => 0 => 0
memcached.sess_consistent_hash => On => On
memcached.sess_consistent_hash_type => ketama => ketama
memcached.sess_lock_expire => 0 => 0
memcached.sess_lock_max_wait => not set => not set
memcached.sess_lock_retries => 5 => 5
memcached.sess_lock_wait => not set => not set
memcached.sess_lock_wait_max => 150 => 150
memcached.sess_lock_wait_min => 150 => 150
memcached.sess_locking => On => On
memcached.sess_number_of_replicas => 0 => 0
memcached.sess_persistent => Off => Off
memcached.sess_prefix => memc.sess.key. => memc.sess.key.
memcached.sess_randomize_replica_read => Off => Off
memcached.sess_remove_failed_servers => Off => Off
memcached.sess_sasl_password => no value => no value
memcached.sess_sasl_username => no value => no value
memcached.sess_server_failure_limit => 0 => 0
memcached.store_retry_count => 2 => 2
Registered save handlers => files user memcached </code></pre>

## Conclusion {.wp-block-heading}

We have learn how to install Memcached in our Fedora server. If in case you encounter problem please consult the [documentation][2].

 [1]: https://nextgentips.com/2021/10/03/how-to-flush-dns-cache-on-ubuntu-20-04/
 [2]: https://memcached.org/