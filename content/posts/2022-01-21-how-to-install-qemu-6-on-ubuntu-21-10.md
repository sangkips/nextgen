---
title: How to install QEMU 6 on Ubuntu 21.10
author: Kipkoech Sang
type: post
date: 2022-01-21T08:08:02+00:00
url: /2022/01/21/how-to-install-qemu-6-on-ubuntu-21-10/
rank_math_news_sitemap_robots:
  - index
rank_math_robots:
  - 'a:1:{i:0;s:5:"index";}'
rank_math_analytic_object_id:
  - 27
rank_math_internal_links_processed:
  - 1
categories:
  - Linux

---
In this tutorial, we are going to learn how to install and configure QEMU 6 on Ubuntu 21.10.

QEMU is a free and open-source hypervisor, it emulates the machine’s processor through dynamic binary translation and provides a set of different hardware and device models for the machine, enabling it to run a variety of guest operating systems.

QEMU is capable of emulating a complete machine in software without the need for hardware virtualization support. It is also capable of providing userspace API virtualization for Linux and BSD kernel services. It is commonly invoked indirectly via libvirt library when using open source applications such as oVirt, OpenStack, and virt-manager.

## Related Articles  {.wp-block-heading}

  * [How to install and configure QEMU 5 on Ubuntu 20.04][1]

## Install QEMU 6 on Ubuntu 21.10 {.wp-block-heading}

### 1. Run system updates {.wp-block-heading}

To begin with, we need to update our repositories in order to make them up to date, Use the following command on your terminal.

<pre class="wp-block-syntaxhighlighter-code">$ sudo apt update && apt upgrade -y</pre>

When Updates are complete, now we can install QEMU.

### 2. Install QEMU 6 Dependencies  {.wp-block-heading}

QEMU is available from Ubuntu 21.10 repositories so we only need to install its dependencies with the following command.

<pre class="wp-block-code"><code>$ sudo apt install git libglib2.0-dev libfdt-dev libpixman-1-dev zlib1g-dev</code></pre>

This command will install all the dependencies needed by QEMU.

Packages to be installed include the following 

<pre class="wp-block-code"><code>The following additional packages will be installed:
  bzip2 fontconfig-config fonts-dejavu-core libblkid-dev libc-dev-bin libc-devtools libc6-dev libcrypt-dev libdeflate0
  libdpkg-perl libfdt1 libffi-dev libfile-fcntllock-perl libfontconfig1 libgd3 libglib2.0-dev-bin libjbig0 libjpeg-turbo8
  libjpeg8 libmount-dev libnsl-dev libpcre16-3 libpcre2-16-0 libpcre2-32-0 libpcre2-dev libpcre2-posix3 libpcre3-dev
  libpcre32-3 libpcrecpp0v5 libpixman-1-0 libselinux1-dev libsepol1-dev libtiff5 libtirpc-dev libwebp6 libxpm4 linux-libc-dev
  manpages-dev pkg-config rpcsvc-proto uuid-dev
Suggested packages:
  bzip2-doc glibc-doc debian-keyring gcc | c-compiler bzr libgd-tools libgirepository1.0-dev libglib2.0-doc
  libgdk-pixbuf2.0-bin | libgdk-pixbuf2.0-dev libxml2-utils dpkg-dev
The following NEW packages will be installed:
  bzip2 fontconfig-config fonts-dejavu-core libblkid-dev libc-dev-bin libc-devtools libc6-dev libcrypt-dev libdeflate0
  libdpkg-perl libfdt-dev libfdt1 libffi-dev libfile-fcntllock-perl libfontconfig1 libgd3 libglib2.0-dev libglib2.0-dev-bin
  libjbig0 libjpeg-turbo8 libjpeg8 libmount-dev libnsl-dev libpcre16-3 libpcre2-16-0 libpcre2-32-0 libpcre2-dev
  libpcre2-posix3 libpcre3-dev libpcre32-3 libpcrecpp0v5 libpixman-1-0 libpixman-1-dev libselinux1-dev libsepol1-dev libtiff5
  libtirpc-dev libwebp6 libxpm4 linux-libc-dev manpages-dev pkg-config rpcsvc-proto uuid-dev zlib1g-dev
0 upgraded, 45 newly installed, 0 to remove and 0 not upgraded.</code></pre>

### 3. Install QEMU 6. on Ubuntu 21.10 {.wp-block-heading}

To install QEMU we will use the following command 

<pre class="wp-block-code"><code>$ sudo apt install qemu-system-x86</code></pre>

To check if QEMU have been installed successfully, run the following command to see the version.

<pre class="wp-block-code"><code># check version
$ sudo apt show qemu-system-x86</code></pre>

You will see the following output

<pre class="wp-block-code"><code># output
Package: qemu-system-x86
&lt;strong>Version: 1:6.0+dfsg-2expubuntu1.1
Priority: optional
Section: misc
Source: qemu
Origin: Ubuntu&lt;/strong>
Maintainer: Ubuntu Developers &lt;ubuntu-devel-discuss@lists.ubuntu.com>
Original-Maintainer: Debian QEMU Team &lt;pkg-qemu-devel@lists.alioth.debian.org>
Bugs: https://bugs.launchpad.net/ubuntu/+filebug
Installed-Size: 37.5 MB
Provides: qemu-kvm, qemu-system-i386, qemu-system-x86-64, qemu-system-x86-microvm
Depends: libaio1 (>= 0.3.93), libasound2 (>= 1.0.16), libbrlapi0.8 (>= 6.3+dfsg), libc6 (>= 2.34), libcacard0 (>= 2.2), libepoxy0 (>= 1.0), libfdt1 (>= 1.6.0), libgbm1 (>= 7.11~1), libgcc-s1 (>= 4.7), libglib2.0-0 (>= 2.39.4), libgnutls30 (>= 3.7.0), libibverbs1 (>= 28), libjpeg8 (>= 8c), libncursesw6 (>= 6), libnettle8, libnuma1 (>= 2.0.11), libpixman-1-0 (>= 0.19.6), libpmem1 (>= 1.4), libpng16-16 (>= 1.6.2-1), librdmacm1 (>= 1.0.15), libsasl2-2 (>= 2.1.27+dfsg), libseccomp2 (>= 2.1.0), libslirp0 (>= 4.0.0), libspice-server1 (>= 0.14.2), libtinfo6 (>= 6), libudev1 (>= 183), liburing1 (>= 0.7), libusb-1.0-0 (>= 2:1.0.23~), libusbredirparser1 (>= 0.6), libvirglrenderer1 (>= 0.8.0), zlib1g (>= 1:1.2.0), qemu-system-common (>> 1:6.0+dfsg-2expubuntu1.1~), qemu-system-data (>> 1:6.0+dfsg-2expubuntu1.1~), ipxe-qemu-256k-compat-efi-roms, seabios (>= 1.10.2-1~), ipxe-qemu (>= 1.0.0+git-20131111.c3d1e78-1~)
Recommends: qemu-system-gui (= 1:6.0+dfsg-2expubuntu1.1), qemu-utils, ovmf, cpu-checker
Suggests: samba, vde2, qemu-block-extra (= 1:6.0+dfsg-2expubuntu1.1), sgabios
Breaks: qemu-kvm, qemu-system-x86-microvm (&lt;&lt; 1:5.0-5ubuntu1~)
Replaces: qemu-kvm, qemu-system-x86-microvm (&lt;&lt; 1:5.0-5ubuntu1~)
Homepage: http://www.qemu.org/
Task: ubuntu-server-raspi, ubuntu-desktop-raspi, ubuntu-budgie-desktop-raspi
Download-Size: 9869 kB
APT-Manual-Installed: yes
APT-Sources: http://mirrors.digitalocean.com/ubuntu impish-updates/main amd64 Packages
Description: QEMU full system emulation binaries (x86)
 QEMU is a fast processor emulator: currently the package supports
 i386 and x86-64 emulation. By using dynamic translation it achieves
 reasonable speed while being easy to port on new host CPUs.
 .
 This package provides the full system emulation binaries to emulate
 the following x86 hardware:  i386 x86_64.
 .
 In system emulation mode QEMU emulates a full system, including a processor</code></pre>

## 4. Uninstall QEMU 6 {.wp-block-heading}

If you want to uninstall Qemu completely from your system run the following command:

<pre class="wp-block-code"><code>$ sudo apt purge "qemu*"
$ sudo apt autoremove</code></pre>

See the following output for packages to be removed

<pre class="wp-block-code"><code>The following packages will be REMOVED:
  qemu-block-extra* qemu-system-common* qemu-system-data* qemu-system-gui* qemu-system-x86* qemu-utils*
0 upgraded, 0 newly installed, 6 to remove and 0 not upgraded.
After this operation, 60.7 MB disk space will be freed.
Do you want to continue? &#91;Y/n] y
(Reading database ... 112471 files and directories currently installed.)
Removing qemu-utils (1:6.0+dfsg-2expubuntu1.1) ...
Removing qemu-system-gui:amd64 (1:6.0+dfsg-2expubuntu1.1) ...
Removing qemu-system-common (1:6.0+dfsg-2expubuntu1.1) ...
Removing qemu-block-extra (1:6.0+dfsg-2expubuntu1.1) ...
Removing qemu-system-x86 (1:6.0+dfsg-2expubuntu1.1) ...
Removing qemu-system-data (1:6.0+dfsg-2expubuntu1.1) ...
Processing triggers for man-db (2.9.4-2) ...
Processing triggers for hicolor-icon-theme (0.17-2) ...
(Reading database ... 112197 files and directories currently installed.)
Purging configuration files for qemu-system-common (1:6.0+dfsg-2expubuntu1.1) ...
Purging configuration files for qemu-block-extra (1:6.0+dfsg-2expubuntu1.1) ...</code></pre>

## Conclusion {.wp-block-heading}

We have successfully installed QEMU 6 on Ubuntu 21.10 system. If you face any difficulty drop a comment or consult the [documentation][2].

 [1]: https://nextgentips.com/2021/12/17/how-to-install-and-configure-qemu-6-2-on-ubuntu-20-04/
 [2]: https://ubuntu.pkgs.org/21.10/ubuntu-main-amd64/qemu-system-x86_6.0+dfsg-2expubuntu1_amd64.deb.html "documentation"