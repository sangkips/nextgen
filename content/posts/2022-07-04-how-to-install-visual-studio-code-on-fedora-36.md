---
title: How to install Visual Studio Code on Fedora 36
author: Kipkoech Sang
type: post
date: 2022-07-04T08:52:58+00:00
url: /2022/07/04/how-to-install-visual-studio-code-on-fedora-36/
categories:
  - Linux

---
In this article we are going to learn how to install Visual Studio code also know as (VScode) on Fedora 36. Visual Studio Code is a lightweight source code editor which runs on desktops and is available to all operating systems out there. It comes with built in Javascript, Node.js and Typescript. One can do programming for almost languages with ease with Visual Studio Code. The languages supported are like, Go, PHP, C++, C#, Java, Python and also .NET.

## Why do we like Visual Studio Code? {.wp-block-heading}

The reason VS code is such a nice tool is because of the following reasons:

  * It is available on all operating systems so it does not limit one on where to run the VS code, you can easily hit the ground running.
  * Vscode has a rich build-in developer tooling for example IntelliSense code completion and debugging which become handy when you don not know the complete code snippet. IT acts a guide and also lessen time to code.
  * VScode can be customized to suit your needs. You can customize every feature the way you wish and also you can add third party extensions easily.
  * VScode is an open source project. So it means you can contribute to the project development, also it means that you have a bigger community where you can ask questions whenever you are faced with problem.
  * VScode is built for the web. It includes great tools for web development such React JSx, JSON, SCSS,CSS, HTML and Less.

### Install Visual Studio code on Fedora 36 {.wp-block-heading}

### 1. Update system repositories  {.wp-block-heading}

In order not to run into any errors during your installation, you need to update your system repositories in order to make them up to date.

<pre class="wp-block-code"><code>$ sudo dnf update</code></pre>

### 2. Add key to the repository {.wp-block-heading}

Currently the Visual studio code ships 64 bit version using the yum repository. So lets import the key to sign our repos using the following command.

<pre class="wp-block-code"><code>$ sudo rpm --import https://packages.microsoft.com/keys/microsoft.asc</code></pre>

After you have imported the key, add Visual studio code to the yum repositories.

<pre class="wp-block-code"><code>$ sudo sh -c 'echo -e "&#91;code]\nname=Visual Studio Code\nbaseurl=https://packages.microsoft.com/yumrepos/vscode\nenabled=1\ngpgcheck=1\ngpgkey=https://packages.microsoft.com/keys/microsoft.asc" > /etc/yum.repos.d/vscode.repo'</code></pre>

The next thing is to update your system repositories for the changes to be effected.

<pre class="wp-block-code"><code>$ sudo dnf check-update 
Visual Studio Code                               38 MB/s |  26 MB     00:00    
Last metadata expiration check: 0:00:06 ago on Mon Jul  4 07:50:47 2022.</code></pre>

### 3. Install Visual Studio code on Fedora 36 {.wp-block-heading}

To install Visual studio code run the following command on your terminal.

<pre class="wp-block-code"><code>$ sudo dnf install code </code></pre>

You should get something like this.

<pre class="wp-block-code"><code># output
Installing:
 &lt;strong>code &lt;/strong>                   x86_64 1.68.1-1655263151.el7             code    112 M
Installing dependencies:
 adwaita-cursor-theme    noarch 42.0-1.fc36                       fedora  622 k
 adwaita-icon-theme      noarch 42.0-1.fc36                       fedora  4.2 M
 alsa-lib                x86_64 1.2.7.1-1.fc36                    updates 502 k
 at-spi2-atk             x86_64 2.38.0-4.fc36                     fedora   86 k
 at-spi2-core            x86_64 2.44.1-1.fc36                     updates 178 k
 atk                     x86_64 2.38.0-1.fc36                     fedora  272 k
 avahi-glib              x86_64 0.8-15.fc36                       fedora   15 k
 cairo                   x86_64 1.17.6-1.fc36                     fedora  675 k
 cairo-gobject           x86_64 1.17.6-1.fc36                     fedora   18 k
 cdparanoia-libs         x86_64 10.2-39.fc36                      fedora   54 k
 colord-libs             x86_64 1.4.6-1.fc36                      fedora  233 k
 cups-libs               x86_64 1:2.4.2-1.fc36                    updates 266 k
 exempi                  x86_64 2.6.0-0.2.20211007gite23c213.fc36 fedora  540 k
 exiv2-libs              x86_64 0.27.5-2.fc36                     fedora  770 k</code></pre>

### 4. Set Visual studio code as default editor {.wp-block-heading}

if you want Visual studio code as your default code editor, you can do so with the help of the following command.

<pre class="wp-block-code"><code>$ xdg-mime default code.desktop text/plain</code></pre>

To start using Visual studio code type code on your terminal.

<pre class="wp-block-code"><code>$ code </code></pre>

### 5. Uninstall Visual Studio code  {.wp-block-heading}

To remove visual studio code from your system, use the following command.

<pre class="wp-block-code"><code>sudo dnf remove code </code></pre>