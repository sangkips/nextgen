---
title: How to install MariaDB 10.7 on Ubuntu 21.04
author: Kipkoech Sang
type: post
date: 2021-10-15T17:59:13+00:00
url: /2021/10/15/how-to-install-mariadb-10-7-on-ubuntu-21-04/
site-sidebar-layout:
  - default
site-content-layout:
  - default
theme-transparent-header-meta:
  - default
rank_math_news_sitemap_robots:
  - index
rank_math_robots:
  - 'a:1:{i:0;s:5:"index";}'
rank_math_analytic_object_id:
  - 135
rank_math_internal_links_processed:
  - 1
categories:
  - Database

---
In this guide we are going to install MariaDB 10.7 development version, but first what is MariaDB?

MariaDB Server is one of the most popular open source relational databases. It’s made by the original developers of MySQL and guaranteed to stay open source. It is part of most cloud offerings and the default in most Linux distributions.

It is built upon the values of performance, stability, and openness, and MariaDB Foundation ensures contributions will be accepted on technical merit. Recent new functionality includes advanced clustering with [Galera Cluster 4][1], compatibility features with Oracle Database and Temporal Data Tables, allowing one to query the data as it stood at any point in the past.

You need to have Ubuntu 20.04 server with non root administrative privileges and a firewall configured with UFW. Check out this for [initial server setup guide for Ubuntu 20.04.][2]

## Installing MariaDB {.wp-block-heading}

Begin by updating your apt repository with the following command on your terminal

<pre class="wp-block-code"><code>$ sudo apt update</code></pre>

After updates are complete you can then install MariaDB server

<pre class="wp-block-code"><code>$ sudo apt install mariadb-server</code></pre>

The installation process will not ask you to set password and this leaves Mariadb insecure. We will use a script that mariadb-server package provides strict access to the server and remove unused accounts. Read more about security script [here][3].

## Initial Configuration for MariaDB 10.7 {.wp-block-heading}

Let us run the security script below:

<pre class="wp-block-code"><code>$ sudo mysql_secure_installation</code></pre>

## Testing MariaDB  {.wp-block-heading}

Check the status of MariaDB with the following command:

<pre class="wp-block-code"><code>$ sudo systemctl status mariadb</code></pre>

Now we can run few database operations to check if its working 

Show the user list

<pre class="wp-block-code"><code># select user,host,password from mysql.user;</code></pre>

### Create test database {.wp-block-heading}

<pre class="wp-block-code"><code># create database test_database;</code></pre>

## Conclusion {.wp-block-heading}

We have learned how to install MariaDB on Ubuntu 21.04 server and secured it using the `mysql_secure_installation` script. You can now practice running SQL queries to learn more.

 [1]: https://galeracluster.com/library/whats-new.html
 [2]: https://nextgentips.com/2021/10/10/initial-server-setup-for-ubuntu-20-04/
 [3]: https://mariadb.com/kb/en/mysql_secure_installation/