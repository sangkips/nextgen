---
title: How to solve “Failed to connect to FTP server” in WordPress.
author: Kipkoech Sang
type: post
date: 2022-03-25T10:04:05+00:00
url: /2022/03/25/how-to-solve-failed-to-connect-to-ftp-server-in-wordpress/
rank_math_seo_score:
  - 4
rank_math_focus_keyword:
  - failed to connect to ftp server wordpress
rank_math_internal_links_processed:
  - 1
rank_math_analytic_object_id:
  - 160
post_view:
  - 3
is_post_view:
  - 'a:1:{i:0;s:30:"105.160.47.238, 13.248.101.194";}'
rank_math_primary_category:
  - 6
categories:
  - Linux

---
In this tutorial, I will show you how to solve the error message **&#8220;Failed to connect to FTP server&#8221;** on the WordPress site. Do not panic because this is a small issue you can sort by giving explicit permissions.

This error does occur due to permission issues with the WordPress server. The web server does not grant permission explicitly, so the administrator must allow and grant permission so that communication can be established. 

The following error can persist for a long time if you do not know where to start from.

When you use something like Nginx or Apache while doing WordPress install, always ensure you give Nginx or Apache to own the content by granting full permission.

<pre class="wp-block-code"><code>$ sudo chown -R nginx.nginx /var/www/nextgentips</code></pre>

**Var/www/** is the folder where your content resides 

**n**extgentips is the name of my project

After you have given the above permission restart your Nginx/Apache2 for the changes to take effect.

<pre class="wp-block-code"><code>$ sudo systemctl restart nginx</code></pre>

For Apache2, you can do the following 

<pre class="wp-block-code"><code>$ sudo systemctl restart httpd</code></pre>

## Conclusion {#conclusion.wp-block-heading}

You now know how to solve FTP connection errors in WordPress. Apply those securities and you will not get the same error again in WordPress