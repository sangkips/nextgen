---
title: How to Deploy MongoDB with Docker
author: Kipkoech Sang
type: post
date: 2021-10-26T15:01:09+00:00
url: /2021/10/26/how-to-deploy-mongodb-with-docker/
ocean_gallery_link_images:
  - off
ocean_disable_margins:
  - enable
ocean_display_top_bar:
  - default
ocean_display_header:
  - default
ocean_disable_title:
  - default
ocean_disable_heading:
  - default
ocean_disable_breadcrumbs:
  - default
ocean_display_footer_widgets:
  - default
ocean_display_footer_bottom:
  - default
ocean_link_format_target:
  - self
ocean_quote_format_link:
  - post
entry_views:
  - 1
view_ip:
  - 'a:1:{i:0;s:15:"136.243.148.249";}'
rank_math_news_sitemap_robots:
  - index
rank_math_robots:
  - 'a:1:{i:0;s:5:"index";}'
rank_math_analytic_object_id:
  - 122
rank_math_internal_links_processed:
  - 1
categories:
  - Linux

---
In this article we are going to explore how to deploy MongoDB with Docker. What is MongoDB? MongoDB is an open source document database built on an horizontal scale out architecture that uses a flexible schema for storing data. 

Docker is a set of platform as a service products that uses OS level virtualization to deliver software service in packages called containers.

Related content:

  * [How to install MongoDB 5 on Ubuntu 21.04][1]
  * [How to install Docker on Arch Linux][2]

## Prerequisites  {.wp-block-heading}

  * A root or normal user with sudo privileges 
  * Running Docker Instance 
  * Internet connectivity

## 1. Install Docker instance  {.wp-block-heading}

First of all we need to make sure that the docker is up and running on our machine

<pre class="wp-block-code"><code>$ sudo apt update</code></pre>

When update is complete do run the following command to install docker.

<pre class="wp-block-code"><code>$ sudo apt install docker.io</code></pre>

## 2. Enable Docker Instance  {.wp-block-heading}

After download is complete it is now time to enable docker so that it can run on boot 

<pre class="wp-block-code"><code>$ sudo systemctl enable docker.service
$ sudo systemctl enable containerd.service</code></pre>

Most Linux distros use systemd to manage which services start on boot. But on Debian and Ubuntu, the Docker service is configured to start automatically.

we can check the status Docker service with the following command:

<pre class="wp-block-code"><code>$ sudo systemctl status docker</code></pre>

Sample output

<pre class="wp-block-code"><code># sudo systemctl status docker
● docker.service - Docker Application Container Engine
     Loaded: loaded (/lib/systemd/system/docker.service; enabled; vendor preset: enabled)
     Active: active (running) since Tue 2021-10-26 13:45:06 UTC; 12min ago
TriggeredBy: ● docker.socket
       Docs: https://docs.docker.com
   Main PID: 5066 (dockerd)
      Tasks: 8
     Memory: 41.1M
        CPU: 352ms
     CGroup: /system.slice/docker.service
             └─5066 /usr/bin/dockerd -H fd:// --containerd=/run/containerd/containerd.sock
....</code></pre>

## 3. Download official Docker image for MongoDB database {.wp-block-heading}

Most of the images are created on top a a base image from the [Docker Hub][3] registry. Docker hub contains many pre-built images that you can **pull** and try without needing to define and configure your own. use the following command to pull images you require:

<pre class="wp-block-code"><code>$ sudo docker pull mongo</code></pre>

Sample output

<pre class="wp-block-code"><code># sudo docker pull mongo
Using default tag: latest
latest: Pulling from library/mongo
7b1a6ab2e44d: Pull complete 
90eb44ebc60b: Pull complete 
5085b59f2efb: Pull complete 
c7499923d022: Pull complete 
019496b6c44a: Pull complete 
c0df4f407f69: Pull complete 
351daa315b6c: Pull complete 
557b07ecd9d7: Pull complete 
a2dff157a5e3: Pull complete 
07d83e88231b: Pull complete 
Digest: sha256:07212fb304ea36b8c5a9e5694527f16deeb0b99f87fc60162dc15ab260bf8a2a
Status: Downloaded newer image for mongo:latest
docker.io/library/mongo:latest</code></pre>

## 4. Check Docker Images {.wp-block-heading}

Use **docker images** command to check images in your container. 

<pre class="wp-block-code"><code>$ sudo docker images</code></pre>

Sample ouput

<pre class="wp-block-code"><code># sudo docker images 
REPOSITORY   TAG       IMAGE ID       CREATED       SIZE
mongo        latest    fefd78e9381a   10 days ago   699MB</code></pre>

## 5. Deploy MongoDB container  {.wp-block-heading}

First create a /mongodb directory on the host system. MongoDB stores data in **/data/db** directory within a Docker container. Use the following command to create the directory:

<pre class="wp-block-code"><code>$ sudo mkdir -p /mongodata</code></pre>

Now you can start Docker container with the **docker run**. **Docker run** command first creates a writable container layer over specified image and starts it using the specified command. 

<pre class="wp-block-code"><code>$ sudo docker run -it -v mongodata:/data/db --name mongodb -d mongo</code></pre>

  * **-it**: It provides an interactive shell to the Docker container 
  * **-v:** Attaches the **/mongodata** host volume to the **/data/db** container volume
  * **-d**: Starts the container as a background process
  * **&#8211;name:** name of the container 

Now if the MongoDB server is running, you can check the status with the following command:

<pre class="wp-block-code"><code>$ sudo docker ps</code></pre>

Sample output

<pre class="wp-block-code"><code># sudo docker ps
CONTAINER ID   IMAGE     COMMAND                  CREATED          STATUS          PORTS       NAMES
804b386fac22   mongo     "docker-entrypoint.s…"   10 minutes ago   Up 10 minutes   27017/tcp   mongodb</code></pre>

## Conclusion  {.wp-block-heading}

Congratulations! You have deployed MongoDb with Docker successfully. We have learned few commands used in managing Docker. You can read more on [Docker documentation][4]

 [1]: https://nextgentips.com/2021/10/26/how-to-install-mongodb-5-on-ubuntu-21-04/
 [2]: https://nextgentips.com/2021/09/30/how-to-install-docker-on-arch-linux/
 [3]: https://hub.docker.com/
 [4]: https://docs.docker.com/desktop/