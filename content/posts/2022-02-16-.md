---
title: How to install LibreNMS on Debian 11
author: Kipkoech Sang
type: post
date: -001-11-30T00:00:00+00:00
draft: true
url: /?p=1075
rank_math_news_sitemap_robots:
  - index
rank_math_robots:
  - 'a:1:{i:0;s:5:"index";}'
categories:
  - Uncategorized

---
In this article, we are going to learn how to install the LibreNMS network monitoring tool on Debian 11.

LibreNMS is an auto-discovering PHP/MYSQL/SNMP-based network monitoring tool. It has support for Linux, FreeBSD, CISCO, Brocade, etc. 

LibreNMS provides a tone of useful features which include:

## Features of LibreNMS {#features-of-librenms.wp-block-heading}

  * It has high level alerting system which gives notification via email, slack etc.
  * It has good biling system which generate bandwidth bills for ports on your system based on its usage.
  * LibreNMS has autodiscovery which discovers entire network using OSPF, CDP, BGP, ARP, LLDP, FDP and SNMP.
  * Has a full API to manage, graph and retrieve data from your install.
  * Has automatic updates feature which lets you know of bug fixes, new features added etc
  * It provides horizontal scalling architecture which helps to grow with your network size.
  * It has both android and iphone apps which lets you manage your network using your phone. 
  * LibreNMS has integration support for <a href="http://nfsen.sourceforge.net/" target="_blank" rel="noreferrer noopener">NfSen</a> <a href="https://collectd.org/" target="_blank" rel="noreferrer noopener">collectd</a> <a href="http://oss.oetiker.ch/smokeping/" target="_blank" rel="noreferrer noopener">SmokePing</a> <a href="http://www.shrubbery.net/rancid/" target="_blank" rel="noreferrer noopener">RANCID</a> <a href="https://github.com/ytti/oxidized/" target="_blank" rel="noreferrer noopener">Oxidized</a>
  * It has multiple authentication methods such as MYSQL, HTTP, LDAP, Radius, Active directory. 

## Prerequisites  {#prerequisites.wp-block-heading}

  * Have Debian 11 server up and running 
  * Make sure you are root or have sudo privileges. check out this article [How to setup Ubuntu 20.04 Server for the first time][1]
  * Have PHP 7.3 installed and above. Check this article [How to install PHP 8.1 on Ubuntu 21.10][2]
  * Have NGINX running or Apache if you like 
  * Have MariaDB up and running 

## Install LibreNMS on Debian 11 {#install-librenms-on-debian-11.wp-block-heading}

### 1. Run system updates  {#1-run-system-updates.wp-block-heading}

Running system updates will enable us to avoid running into errors while installing any package. This is because updates make repositories up to date.

<pre class="wp-block-code"><code>$ sudo apt update && apt upgrade -y</code></pre>

### 2. Install and configure MariaDB  {#2-install-and-configure-mariadb.wp-block-heading}

MariaDB is required by librenms as a database. To install it use the following command.

<pre class="wp-block-code"><code>$ sudo apt install mariadb-server -y</code></pre>

Run secure installation

<pre class="wp-block-code"><code>$ mysql_secure_installation</code></pre>

### 3. Configure MariaDB {#3-configure-mariadb.wp-block-heading}

Remember we installed MariaDB on the second step above and now we need to configure it. Edit the following **/etc/mysql/mariadb.conf.d/50-server.cnf** file 

<pre class="wp-block-code"><code>$ sudo vi /etc/mysql/mariadb.conf.d/50-server.cnf</code></pre>

On **mysqld** section add the following 

<pre class="wp-block-code"><code># mysqld section
innodb_file_per_table=1
lower_case_table_names=0</code></pre>

After we need to enable and restart MariaDB.

<pre class="wp-block-code"><code># enable and restart 
$ sudo systemctl enable mariadb
$ sudo systemctl restart mariadb
$ sudo systemctl status mariadb</code></pre>

<pre class="wp-block-code"><code># status
● mariadb.service - MariaDB 10.5.12 database server
     Loaded: loaded (/lib/systemd/system/mariadb.service; enabled; vendor preset: enabled)
     Active: active (running) since Wed 2022-02-16 17:52:39 UTC; 12s ago
       Docs: man:mariadbd(8)
             https:&#47;&#47;mariadb.com/kb/en/library/systemd/
    Process: 3120 ExecStartPre=/usr/bin/install -m 755 -o mysql -g root -d /var/run/mysqld (code=exited, status=0/SUCCESS)
    Process: 3121 ExecStartPre=/bin/sh -c systemctl unset-environment _WSREP_START_POSITION (code=exited, status=0/SUCCESS)
    Process: 3123 ExecStartPre=/bin/sh -c &#91; ! -e /usr/bin/galera_recovery ] && VAR= ||   VAR=`cd /usr/bin/..; /usr/bin/galera_>
    Process: 3188 ExecStartPost=/bin/sh -c systemctl unset-environment _WSREP_START_POSITION (code=exited, status=0/SUCCESS)
    Process: 3190 ExecStartPost=/etc/mysql/debian-start (code=exited, status=0/SUCCESS)
   Main PID: 3176 (mariadbd)
     Status: "Taking your SQL requests now..."
      Tasks: 12 (limit: 1132)
     Memory: 65.5M
        CPU: 361ms
     CGroup: /system.slice/mariadb.service
             └─3176 /usr/sbin/mariadbd</code></pre>

**Login to the MariaDB database**.

<pre class="wp-block-code"><code>$ sudo mysql -u root -p
Enter password: 
Welcome to the MariaDB monitor.  Commands end with ; or \g.
Your MariaDB connection id is 30
Server version: 10.5.12-MariaDB-0+deb11u1 Debian 11

Copyright (c) 2000, 2018, Oracle, MariaDB Corporation Ab and others.

Type 'help;' or '\h' for help. Type '\c' to clear the current input statement.

MariaDB &#91;(none)]> </code></pre>

Add this to the database 

<pre class="wp-block-code"><code># On mariadb database
CREATE DATABASE librenms CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;
CREATE USER 'librenms'@'localhost' IDENTIFIED BY '$passwd';
GRANT ALL PRIVILEGES ON librenms.* TO 'librenms'@'localhost';
FLUSH PRIVILEGES;
exit</code></pre>

What you will get will look like this;

<pre class="wp-block-code"><code>Welcome to the MariaDB monitor.  Commands end with ; or \g.
Your MariaDB connection id is 130
Server version: 10.5.12-MariaDB-0+deb11u1 Debian 11

Copyright (c) 2000, 2018, Oracle, MariaDB Corporation Ab and others.

Type 'help;' or '\h' for help. Type '\c' to clear the current input statement.

MariaDB &#91;(none)]&gt; CREATE DATABASE librenms CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;
Query OK, 1 row affected (0.001 sec)

MariaDB &#91;(none)]&gt; CREATE USER 'librenms'@'localhost' IDENTIFIED BY '$passwd';
Query OK, 0 rows affected (0.005 sec)

MariaDB &#91;(none)]&gt; GRANT ALL PRIVILEGES ON librenms.* TO 'librenms'@'localhost';
Query OK, 0 rows affected (0.001 sec)

MariaDB &#91;(none)]&gt; FLUSH PRIVILEGES;
Query OK, 0 rows affected (0.001 sec)

MariaDB &#91;(none)]&gt; show databases;
+--------------------+
| Database           |
+--------------------+
| information_schema |
| &lt;strong>librenms&lt;/strong>           |
| mysql              |
| performance_schema |
+--------------------+
4 rows in set (0.001 sec)

MariaDB &#91;(none)]&gt; exit;</code></pre>

### 4. install LibreNMS dependencies (PHP, Nginx) {#4-install-librenms-dependencies-php-nginx.wp-block-heading}

LibreNMS same with other applications depends on some other applications to perform optimally, we need to install those dependencies in our system before we can install LibreNMS.

<pre class="wp-block-code"><code>$ sudo apt install nginx-full nmap php-cli php-curl php-fpm php-gd php-json php-mbstring php-mysql php-snmp php-xml php-zip python3-dotenv python3-pip python3-pymysql python3-redis python3-setuptools python3-systemd rrdtool snmp snmpd whois acl curl composer fping git graphviz imagemagick mtr-tiny</code></pre>

**Create librenms user** 

<pre class="wp-block-code"><code>$ useradd librenms -d /opt/librenms -M -r -s /bin/bash</code></pre>

Add librenms to www-data group

<pre class="wp-block-code"><code>$ usermod -aG librenms www-data</code></pre>

PHP files need to have a timezone to be used by the php.ini script. To set the timezone follow the following:

<pre class="wp-block-code"><code>$ sudo vi /etc/php/7.4/cli/php.ini</code></pre>

<pre class="wp-block-code"><code>$ sudo vi /etc/php/7.4/fpm/php.ini</code></pre>

Add time.zone=Africa/Nairobi

Configure timezone 

<pre class="wp-block-code"><code>$ sudo timedatectl set-timezone Africa/Nairobi</code></pre>

### 5.Clone LibreNMS from github {#5-clone-librenms-from-github.wp-block-heading}

Let&#8217;s clone librenms from the git repository

<pre class="wp-block-code"><code>$ cd /opt/
$ git clone https://github.com/librenms/librenms.git</code></pre>

You will see the following output.

<pre class="wp-block-code"><code># output
Cloning into 'librenms'...
remote: Enumerating objects: 191353, done.
remote: Counting objects: 100% (753/753), done.
remote: Compressing objects: 100% (502/502), done.
remote: Total 191353 (delta 385), reused 471 (delta 247), pack-reused 190600
Receiving objects: 100% (191353/191353), 201.12 MiB | 12.18 MiB/s, done.
Resolving deltas: 100% (136686/136686), done.
Updating files: 100% (12626/12626), done.</code></pre>

After cloning is complete we need to set the repository permissions.

<pre class="wp-block-code"><code>$ chown -R librenms:librenms /opt/librenms
$ chmod 771 /opt/librenms
$ setfacl -d -m g::rwx /opt/librenms/rrd /opt/librenms/logs /opt/librenms/bootstrap/cache/ /opt/librenms/storage/
$ setfacl -R -m g::rwx /opt/librenms/rrd /opt/librenms/logs /opt/librenms/bootstrap/cache/ /opt/librenms/storage/</code></pre>

### 6. Install PHP dependencies  {#6-install-php-dependencies.wp-block-heading}

We need also to install PHP dependencies onto our system. We will use the following script to do so. We need to use the librenms user we created earlier.

<pre class="wp-block-code"><code>$ sudo su - librenms
$ ./scripts/composer_wrapper.php install --no-dev
$ exit</code></pre>

You will see the following output.

<pre class="wp-block-code"><code>&gt; LibreNMS\ComposerHelper::preInstall
Installing dependencies from lock file
Verifying lock file contents can be installed on current platform.
Package operations: 106 installs, 0 updates, 0 removals
  - Downloading amenadiel/jpgraph (3.6.14)
  - Downloading clue/socket-raw (v1.5.0)
  - Downloading dapphp/radius (2.5.6)
  - Downloading doctrine/event-manager (1.1.1)
  - Downloading doctrine/deprecations (v0.5.3)
  - Downloading doctrine/cache (2.1.1)
  - Downloading doctrine/dbal (2.13.7)
  - Downloading doctrine/inflector (2.0.4)
  - Downloading doctrine/lexer (1.2.2)
  - Downloading symfony/polyfill-ctype (v1.24.0)
  - Downloading webmozart/assert (1.10.0)
  - Downloading dragonmantank/cron-expression (v3.3.1)
  - Downloading easybook/geshi (v1.0.8.19)
  - Downloading fgrosse/phpasn1 (v2.4.0)
......</code></pre>

Also, PHP files need to have a timezone to be used by the php.ini script. To set the timezone follow the following:

<pre class="wp-block-code"><code>$ sudo vi /etc/php/7.4/cli/php.ini</code></pre>

Search for cgi.fi_pathinfo parameter and uncomment and add 0

<pre class="wp-block-code"><code>$ cgi.fix_pathinfo=0</code></pre>

Edit the following again

<pre class="wp-block-code"><code>$ sudo vi /etc/php/7.4/fpm/php.ini</code></pre>

Add time.zone=Africa/Nairobi

Configure timezone 

<pre class="wp-block-code"><code>$ sudo timedatectl set-timezone Africa/Nairobi</code></pre>

## Configure PHP-FPM {#configure-php-fpm.wp-block-heading}

Create another separate configuration file for PHP-FPM with the following command;

<pre class="wp-block-code"><code>$ cp /etc/php/7.4/fpm/pool.d/www.conf /etc/php/7.4/fpm/pool.d/librenms.conf</code></pre>

<pre class="wp-block-code"><code>$ sudo vi /etc/php/7.4/fpm/pool.d/librenms.conf</code></pre>

We need to change **www** to **librenms**

<pre class="wp-block-code"><code>&#91;librenms]
user=librenms
group=librenms</code></pre>

Change listen to a unique name 

<pre class="wp-block-code"><code>listen = /run/php-fpm-librenms.sock</code></pre>

Resart php-fpm

<pre class="wp-block-code"><code>$ systemctl restart php7.4-fpm</code></pre>

### 7. Configure Nginx web server {#7-configure-nginx-web-server.wp-block-heading}

We need to create a librenms configuration file within Nginx to make its web interface accessible. 

<pre class="wp-block-code"><code>$ sudo vi /etc/nginx/sites-enabled/librenms.vhost</code></pre>

Add the following 

<pre class="wp-block-code"><code>server {
 listen      80;
 server_name ip address;
 root        /opt/librenms/html;
 index       index.php;

 charset utf-8;
 gzip on;
 gzip_types text/css application/javascript text/javascript application/x-javascript image/svg+xml text/plain text/xsd text/xsl text/xml image/x-icon;
 location / {
  try_files $uri $uri/ /index.php?$query_string;
 }
 location ~ &#91;^/]\.php(/|$) {
  fastcgi_pass unix:/run/php-fpm-librenms.sock;
  fastcgi_split_path_info ^(.+\.php)(/.+)$;
  include fastcgi.conf;
 }
 location ~ /\.(?!well-known).* {
  deny all;
 }
}
</code></pre>

Save and exit.

Remove sites-enabled 

<pre class="wp-block-code"><code># remove sites-enabled
$ rm /etc/nginx/sites-enabled/default
# reload nginx and restart
$ systemctl reload nginx
$ systemctl restart php7.4-fpm</code></pre>

PHP7.4-fpm status

<pre class="wp-block-code"><code>● php7.4-fpm.service - The PHP 7.4 FastCGI Process Manager
     Loaded: loaded (/lib/systemd/system/php7.4-fpm.service; enabled; vendor preset: enabled)
     Active: active (running) since Sat 2022-01-29 10:11:29 EAT; 5min ago
       Docs: man:php-fpm7.4(8)
    Process: 33222 ExecStartPost=/usr/lib/php/php-fpm-socket-helper install /run/php/php-fpm.sock /etc/php/7.4/fpm/pool.d/www.&gt;
   Main PID: 33219 (php-fpm7.4)
     Status: "Processes active: 0, idle: 2, Requests: 0, slow: 0, Traffic: 0req/sec"
      Tasks: 3 (limit: 1132)
     Memory: 12.0M
        CPU: 67ms
     CGroup: /system.slice/php7.4-fpm.service
             ├─33219 php-fpm: master process (/etc/php/7.4/fpm/php-fpm.conf)
             ├─33220 php-fpm: pool www
             └─33221 php-fpm: pool www

Jan 29 10:11:29 debian systemd&#91;1]: Starting The PHP 7.4 FastCGI Process Manager...
Jan 29 10:11:29 debian systemd&#91;1]: Started The PHP 7.4 FastCGI Process Manager.</code></pre>

### 7. Configure SNMP {#7-configure-snmp.wp-block-heading}

Setup SNMP file with the following by copying it to conf file

<pre class="wp-block-code"><code>$ sudo cp /opt/librenms/snmpd.conf.example /etc/snmp/snmpd.conf</code></pre>

<pre class="wp-block-code"><code>$ sudo vi /etc/snmp/snmpd.conf</code></pre>

Edit this text **RANDOMSTRINGGOESHERE** and add the community string of your choice. 

<pre class="wp-block-code"><code>Remove &lt;strong>RANDOMSTRINGGOESHERE&lt;/strong> and add any name </code></pre>

Pull the following file give it permission and lastly restart SNMP.

<pre class="wp-block-code"><code>$ curl -o /usr/bin/distro https://raw.githubusercontent.com/librenms/librenms-agent/master/snmp/distro
# make it executable
$ chmod +x /usr/bin/distro
# Enable snmp
$ systemctl enable snmpd
# restart snmp
$ systemctl restart snmpd</code></pre>

### 8. Copy logrotate config {#8-copy-logrotate-config.wp-block-heading}

LibreNMS keeps logs in **/opt/librenms/logs**. Over time these logs keep growing and need to be rotated out. Now to rotate log files we need to use a logrotate file. 

<pre class="wp-block-code"><code>$ sudo cp /opt/librenms/misc/librenms.logrotate /etc/logrotate.d/librenms</code></pre>

### 9. Web installer  {#9-web-installer.wp-block-heading}

Finally to launch web installer head over to **http://<ip_address>/install**

<pre class="wp-block-code"><code>$ http://IP Address/install</code></pre>

The web installer will prompt you to create a **config.php file** shown on the screen. Make sure you set permission after you have created config.php with the following:

<pre class="wp-block-code"><code>$ chown librenms:librenms /opt/librenms/config.php</code></pre>

 [1]: https://nextgentips.com/2021/10/10/initial-server-setup-for-ubuntu-20-04/
 [2]: https://nextgentips.com/2021/12/26/how-to-install-php-8-1-on-ubuntu-21-10/