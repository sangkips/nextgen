---
title: How to upgrade Linux Kernel on Ubuntu 20.04 to 5.16 Release
author: Kipkoech Sang
type: post
date: 2021-12-14T10:25:05+00:00
url: /2021/12/14/how-to-upgrade-linux-kernel-on-ubuntu-20-04-to-5-16-release/
entry_views:
  - 2
view_ip:
  - 'a:2:{i:0;s:36:"2804:18:846:79db:2c3f:a71c:2f93:4a4c";i:1;s:14:"54.242.182.186";}'
rank_math_primary_category:
  - 6
rank_math_description:
  - Linux Kernel is a free and open-source, monolithic, modular, multitasking Unix-like operating system. It is the main component of a Linux operating system.
rank_math_focus_keyword:
  - linux kernel
rank_math_news_sitemap_robots:
  - index
rank_math_robots:
  - 'a:1:{i:0;s:5:"index";}'
rank_math_analytic_object_id:
  - 64
rank_math_internal_links_processed:
  - 1
categories:
  - Linux

---
In this tutorial, we are going to learn how to upgrade Linux Kernel to 5.16 on Ubuntu 20.04.

[Linux Kernel][1] is a free and open-source, monolithic, modular, multitasking Unix-like operating system. It is the main component of a Linux operating system and is the core interface between the computer’s hardware and its processes. It makes communication possible between computer hardware and processes running on it and it manages resources effectively.

Linux 5.16 mainline was released recently by&nbsp;**Linux Torvalds**&nbsp;with better new features to try out. The mainline tree is maintained by Linus Torvalds and It is where all new features are added and releases always come from.

Notable updates on this release are:

  * Intel Advanced Matrix Extension support is now stable with this release.
  * This kernel now supports Rasperry-Pi compute modules
  * Raptor Lake p[rocessor support is included in this release.
  * It now bring support for Rockchips
  * It has added Apple magic keyboard

## Related Content {#related-content.wp-block-heading}

  * [How to upgrade Ubuntu 20.04 Linux Kernel from 5.4 to 5.15][2]
  * [How to upgrade Linux Kernel on Rocky Linux/AlmaLinux/CentOS 8][3]
  * [How to upgrade Linux Kernel 5.13 to 5.15 on Ubuntu 21.10][4]

## Table of Contents {#table-of-contents.wp-block-heading}

  1. Run updates for your system
  2. Check the current version of Linux kernel you are running
  3. Download Linux kernel headers from Ubuntu Mainline
  4. Download Linux Kernel image
  5. Download modules required to build the kernel
  6. Install new kernel
  7. Reboot the system
  8. Conclusion

## Upgrade Linux Kernel to 5.16 release {#upgrade-linux-kernel-to-5-16-release.wp-block-heading}

## 1. Run system update {#1-run-system-update.wp-block-heading}

The first thing to do is to run system updates on our Ubuntu 20.04 server. Use the following command on your terminal.

<pre class="wp-block-code"><code>$ sudo apt update && apt upgrade -y</code></pre>

When upgrades and updates are complete, we can now begin to download headers, modules, and images.

## 2. Download Linux kernel Headers. {#2-download-linux-kernel-headers.wp-block-heading}

Linux kernel headers is a package providing the Linux kernel headers. These are part of the Kernel even though shipped separately. The headers act as an interface between internal kernel components and also between userspace and the kernel.

To download this package header, head over to the Ubuntu PPA mainline repository and make downloads for your amd64 system. We are going to download the following header files.

<pre class="wp-block-code"><code>$ wget -c https://kernel.ubuntu.com/~kernel-ppa/mainline/v5.16-rc5/amd64/linux-headers-5.16.0-051600rc5_5.16.0-051600rc5.202112121931_all.deb</code></pre>

You will see the following output from your terminal.

<pre class="wp-block-code"><code>output
--2021-12-14 08:44:29--  https://kernel.ubuntu.com/~kernel-ppa/mainline/v5.16-rc5/amd64/linux-headers-5.16.0-051600rc5_5.16.0-051600rc5.202112121931_all.deb
Resolving kernel.ubuntu.com (kernel.ubuntu.com)... 91.189.94.216
Connecting to kernel.ubuntu.com (kernel.ubuntu.com)|91.189.94.216|:443... connected.
HTTP request sent, awaiting response... 200 OK
Length: 12284980 (12M) &#91;application/x-debian-package]
Saving to: ‘linux-headers-5.16.0-051600rc5_5.16.0-051600rc5.202112121931_all.deb’

&lt;strong>linux-headers-5.16.0-051600r 100%&lt;/strong>&#91;===========================================&gt;]  11.72M  60.9MB/s    in 0.2s    

2021-12-14 08:44:30 (60.9 MB/s) - &lt;strong>‘linux-headers-5.16.0-051600rc5_5.16.0-051600rc5.202112121931_all.deb’ saved &#91;12284980/12284980]&lt;/strong></code></pre>

Another header file to download is this one.

<pre class="wp-block-code"><code>$ wget -c https://kernel.ubuntu.com/~kernel-ppa/mainline/v5.16-rc5/amd64/linux-headers-5.16.0-051600rc5-generic_5.16.0-051600rc5.202112121931_amd64.deb</code></pre>

Sample output will look like this:

<pre class="wp-block-code"><code>output
--2021-12-14 08:45:17--  https://kernel.ubuntu.com/~kernel-ppa/mainline/v5.16-rc5/amd64/linux-headers-5.16.0-051600rc5-generic_5.16.0-051600rc5.202112121931_amd64.deb
Resolving kernel.ubuntu.com (kernel.ubuntu.com)... 91.189.94.216
Connecting to kernel.ubuntu.com (kernel.ubuntu.com)|91.189.94.216|:443... connected.
HTTP request sent, awaiting response... 200 OK
Length: 2908436 (2.8M) &#91;application/x-debian-package]
Saving to: ‘linux-headers-5.16.0-051600rc5-generic_5.16.0-051600rc5.202112121931_amd64.deb’

&lt;strong>linux-headers-5.16.0-051600r 100%&lt;/strong>&#91;===========================================&gt;]   2.77M  --.-KB/s    in 0.1s    

2021-12-14 08:45:17 (27.9 MB/s) - &lt;strong>‘linux-headers-5.16.0-051600rc5-generic_5.16.0-051600rc5.202112121931_amd64.deb’ saved &#91;2908436/2908436]&lt;/strong></code></pre>

Note the difference between those headers

## 2. Download Linux kernel Modules  {#2-download-linux-kernel-modules.wp-block-heading}

Linux kernel are pieces of code that can be loaded and unloaded into the kernel upon demand. They extend the functionality of the kernel without the need to reboot the system. A module can be configured as built-in or loadable. To dynamically load or remove a module, it has to be configured as a loadable module in the kernel configuration.

To download the Linux Kernel module run the following command on your terminal.

<pre class="wp-block-code"><code>$ wget -c https://kernel.ubuntu.com/~kernel-ppa/mainline/v5.16-rc5/amd64/linux-modules-5.16.0-051600rc5-generic_5.16.0-051600rc5.202112121931_amd64.deb</code></pre>

Next is to download the image.

## 3. Download Linux Kernel Image {#3-download-linux-kernel-image.wp-block-heading}

Linux kernel image is a snapshot of the Linux kernel that is able to run by itself after giving the control to it. For example, whenever you need to boot the system up, it will bootload an image from the hard disk.

To download the Linux Kernel image 5.16 run the following command on your terminal.

<pre class="wp-block-code"><code>$ wget -c https://kernel.ubuntu.com/~kernel-ppa/mainline/v5.16-rc5/amd64/linux-image-unsigned-5.16.0-051600rc5-generic_5.16.0-051600rc5.202112121931_amd64.deb</code></pre>

Make sure you are seeing the image from the downloads.

Sample output will look like this:

<pre class="wp-block-code"><code>Output
linux-image-unsigned-5.16.0- 100%&#91;===========================================&gt;]   9.85M  55.5MB/s    in 0.2s    

2021-12-14 08:47:29 (55.5 MB/s) - ‘linux-image-unsigned-5.16.0-051600rc5-generic_5.16.0-051600rc5.202112121931_amd64.deb’ saved &#91;10329384/10329384]</code></pre>

Now that we have finished downloading images, modules, and headers it is now time to install them but first, let&#8217;s confirm our downloads.

<pre class="wp-block-code"><code>$ ls
linux-headers-5.16.0-051600rc5-generic_5.16.0-051600rc5.202112121931_amd64.deb
linux-headers-5.16.0-051600rc5_5.16.0-051600rc5.202112121931_all.deb
linux-image-unsigned-5.16.0-051600rc5-generic_5.16.0-051600rc5.202112121931_amd64.deb
linux-modules-5.16.0-051600rc5-generic_5.16.0-051600rc5.202112121931_amd64.deb</code></pre>

## 4. Install Linux Kernel on Ubuntu 20.04 {#4-install-linux-kernel-on-ubuntu-20-04.wp-block-heading}

To install Linux Kernel 5.16, let&#8217;s run the following command;

<pre class="wp-block-code"><code>$ sudo dpkg -i *.deb</code></pre>

Wait for the process to complete before restarting the system.

After you have restarted the system, check the Linux Kernel release installed.

<pre class="wp-block-code"><code>$ uname -r</code></pre>

the output you will now get is like this;

<pre class="wp-block-code"><code>5.16.0-051600rc5-generic</code></pre>

## Conclusion {#conclusion.wp-block-heading}

As you can see we have successfully upgraded from 5.4.0-88-generic to 5.16.0-051600rc5-generic, the latter being the latest release.

## Best Books to learn Linux {#best-books-to-learn-linux.wp-block-heading}

  * <a href="https://www.amazon.com/gp/product/1718500408/ref=as_li_tl?ie=UTF8&camp=1789&creative=9325&creativeASIN=1718500408&linkCode=as2&tag=nextgentips-20&linkId=dc6663306e272a2782727233a0b2fb07" target="_blank" rel="noreferrer noopener">How Linux Works, 3rd Edition: What Every Superuser Should Know</a>
  * <a href="https://www.amazon.com/gp/product/0596005652/ref=as_li_tl?ie=UTF8&camp=1789&creative=9325&creativeASIN=0596005652&linkCode=as2&tag=nextgentips-20&linkId=2aee20dc4f8d3d1132086749654a8872" target="_blank" rel="noreferrer noopener">Understanding the Linux Kernel, Third Edition</a>

 [1]: https://www.kernel.org/
 [2]: https://nextgentips.com/2021/11/21/how-to-upgrade-ubuntu-20-04-linux-kernel-from-5-4-to-5-15/
 [3]: https://nextgentips.com/2021/11/14/how-to-upgrade-linux-kernel-on-rocky-linux-almalinux-centos-8/
 [4]: https://nextgentips.com/2021/11/01/how-to-upgrade-linux-kernel-5-13-to-5-15-on-ubuntu-21-10/