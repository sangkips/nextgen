---
title: How to install and configure Minikube on Ubuntu 21.10
author: Kipkoech Sang
type: post
date: 2021-12-24T09:10:13+00:00
url: /2021/12/24/how-to-install-and-configure-minikube-on-ubuntu-21-10/
rank_math_news_sitemap_robots:
  - index
rank_math_robots:
  - 'a:1:{i:0;s:5:"index";}'
rank_math_analytic_object_id:
  - 54
rank_math_internal_links_processed:
  - 1
categories:
  - Kubernetes

---
[Minikube][1] is a tool that makes it easy to run Kubernetes locally. Minikube runs a single-node Kubernetes cluster inside a VM on your local machine. Kubernetes quickly set up a local Kubernetes cluster on Linux, Windows, and macOS.

It is always advisable before diving into Kubernetes, you will need a minimal Kubernetes setup. Such a setup should spin up fast and integrate well with other tools.

Minikube is the best fit because of the following:

  * It runs on Windows, Linux and MacOS.
  * It supports the latest Kubernetes release
  * It supports multiple container runtimes i.e Containerd, KVM, Docker, Podman etc
  * It has supports for adbvanced features such as Load balancing, Featuregates and filesystems mounts.
  * It has support for Addons. Addons being a marketplace for developers to share configurations for running services on Minikube.
  * It supports CI environments

## Prerequisites {.wp-block-heading}

To run Kubernetes effectively you need to allocate the following to Minikube on your system.

  * 2 CPUs or more
  * 2 GB of memory
  * 20 GB or more of free disk space
  * Reliable internet connections.
  * Conatiner or virtual machine manager such as Docker, KVM, Podman, Virtual Box etc.

## Related Articles {.wp-block-heading}

  * [How to install and use Minikube on Fedora 35][2]
  * [How to install Oracle VirtualBox 6.1 on Ubuntu 21.04][3]
  * [How to install Podman 3 on Debian 11][4]
  * [How to install Docker on Arch Linux][5]
  * 

## Install Minikube on Ubuntu 21.10 {.wp-block-heading}

Let’s now dive into installing and playing with Kubernetes. I am going to be using Docker for running Kubernetes, You can spin up using Podman, Virtualbox, or KVM whichever you understand well.

## 1. Run system updates  {.wp-block-heading}

To make our repositories up to date run total system updates with the following command;

<pre class="wp-block-code"><code>$ sudo apt update && apt upgrade -y</code></pre>

## 2. Install Docker on Ubuntu 21.10 {.wp-block-heading}

Docker container acts as a base for installing Minikube. Kubernetes depends on docker to run. So use the following command to install docker if you have not already installed it. For complete docker install check this out. 

  * [How to install Docker-ce on Ubuntu 21.10][6]

<pre class="wp-block-code"><code>$ sudo apt install docker.io</code></pre>

Once installed, add your local user to it because Minikube doesn&#8217;t run as a root.

Lets add new user with the following command.

<pre class="wp-block-code"><code>$ useradd --create-home nextgentips</code></pre>

<pre class="wp-block-code"><code>$ sudo usermod -aG docker nextgentips</code></pre>

## 3. Download Minikube  {.wp-block-heading}

We are going to download the Minikube binary from the Minikube Library.

<pre class="wp-block-code"><code>$ curl -LO https://storage.googleapis.com/minikube/releases/latest/minikube-linux-amd64
sudo install minikube-linux-amd64 /usr/local/bin/minikube</code></pre>

Now that we have installed Minikube, you can check the version of it with the following command.

<pre class="wp-block-code"><code>$ minikube version
minikube version: v1.24.0
commit: 76b94fb3c4e8ac5062daf70d60cf03ddcc0a741b</code></pre>

You now need to install kubectl

## 4. Install Kubectl {.wp-block-heading}

Kubectl is a command-line tool for controlling Kubernetes clusters. To install kubectl run the following command.

<pre class="wp-block-code"><code>curl -LO https://storage.googleapis.com/kubernetes-release/release/`curl -s https://storage.googleapis.com/kubernetes-release/release/stable.txt`/bin/linux/amd64/kubectl</code></pre>

make sure you make it executable with the following command.

<pre class="wp-block-code"><code>$ chmod +x ./kubectl</code></pre>

Then you will have to move it in your $PATH

<pre class="wp-block-code"><code>sudo mv ./kubectl /usr/local/bin/kubectl</code></pre>

## 5. Start Minikube on Ubuntu 21.10 {.wp-block-heading}

To start minikube run **minikube start** command.

<pre class="wp-block-code"><code>$ minikube start</code></pre>

You will get the following output.

<pre class="wp-block-code"><code>Output
minikube v1.24.0 on Ubuntu 21.10 (kvm/amd64)
* Automatically selected the docker driver

X The requested memory allocation of 1977MiB does not leave room for system overhead (total system memory: 1977MiB). You may face stability issues.
* Suggestion: Start minikube with less memory allocated: 'minikube start --memory=1977mb'

* Starting control plane node minikube in cluster minikube
* Pulling base image ...
* Downloading Kubernetes v1.22.3 preload ...
    > preloaded-images-k8s-v13-v1...: 501.73 MiB / 501.73 MiB  100.00% 154.31 M
    > gcr.io/k8s-minikube/kicbase: 355.78 MiB / 355.78 MiB  100.00% 19.26 MiB p
* Creating docker container (CPUs=2, Memory=1977MB) ...
* Preparing Kubernetes v1.22.3 on Docker 20.10.8 ...
  - Generating certificates and keys ...
  - Booting up control plane ...
  - Configuring RBAC rules ...
* Verifying Kubernetes components...
  - Using image gcr.io/k8s-minikube/storage-provisioner:v5
* Enabled addons: default-storageclass, storage-provisioner
* kubectl not found. If you need it, try: 'minikube kubectl -- get pods -A'
* Done! kubectl is now configured to use "minikube" cluster and "default" namespace by default</code></pre>

Check basic Minikube commands 

Check Minikube cluster-info

<pre class="wp-block-code"><code>$ kubectl cluster-info</code></pre>

<pre class="wp-block-code"><code>Output
Kubernetes control plane is running at https://192.168.49.2:8443
CoreDNS is running at https://192.168.49.2:8443/api/v1/namespaces/kube-system/services/kube-dns:dns/proxy

To further debug and diagnose cluster problems, use 'kubectl cluster-info dump'.</code></pre>

## Conclusion {.wp-block-heading}

We have successfully installed Minikube on Ubuntu 21.10. For more information check [Minikube documentation][7].

 [1]: https://minikube.sigs.k8s.io/docs/
 [2]: https://nextgentips.com/2021/12/18/how-to-install-and-configure-minikube-on-fedora-35/
 [3]: https://nextgentips.com/2021/10/29/how-to-install-oracle-virtualbox-6-1-on-ubuntu-21-04/
 [4]: https://nextgentips.com/2021/10/22/how-to-install-podman-3-on-debian-11/
 [5]: https://nextgentips.com/2021/09/30/how-to-install-docker-on-arch-linux/
 [6]: https://nextgentips.com/2021/11/27/how-to-install-docker-ce-on-ubuntu-21-10/
 [7]: https://minikube.sigs.k8s.io/docs/start/