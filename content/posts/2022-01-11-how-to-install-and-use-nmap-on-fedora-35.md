---
title: How to install and Use NMAP on Fedora 35
author: Kipkoech Sang
type: post
date: 2022-01-11T14:29:55+00:00
url: /2022/01/11/how-to-install-and-use-nmap-on-fedora-35/
entry_views:
  - 2
view_ip:
  - 'a:2:{i:0;s:39:"2a00:23c4:c383:c901:e680:7c16:cbc6:b594";i:1;s:13:"66.249.75.118";}'
rank_math_news_sitemap_robots:
  - index
rank_math_robots:
  - 'a:1:{i:0;s:5:"index";}'
rank_math_analytic_object_id:
  - 38
rank_math_internal_links_processed:
  - 1
categories:
  - Security

---
Welcome to today&#8217;s topic where we will be talking about how to install Nmap on Fedora 35.

Nmap (Network mapper) is a free and open-source software for network discovery and security auditing. It is also used for network inventory services, managing service upgrades, and monitoring hosts&#8217; downtime.

Nmap is designed for bigger networks but it can also work fine with standalone hosts. Nmap suite includes an advanced GUI and results viewer called [Zenmap][1], a flexible data transfer, redirection and a debugging tool called [Ncat][2], a utility for comparing scan results called [Ndiff][3], and a packet generation and response analysis tool called [Nping][4]. 

## Why use Nmap? {.wp-block-heading}

  * Nmap hs well writtend documentation for easy reference incase of difficulty.
  * Nmap supports a dozen advanced techniques out there 
  * Most operating systems supports Nmap
  * Its free therefore maintained by the community.

## What is Nmap used for? {.wp-block-heading}

  * It is used for port, OS and host scanning 
  * It is used for checking the number of live hosts
  * It is used for checking which ports are open
  * It is used for checking which IPs are connecting to your network
  * It provide real time information of the network 

## Installing Nmap on Fedora 35 {.wp-block-heading}

There are two ways to install Nmap:

  1. Installing Nmap from Binary RPMs
  2. Installing from the Yum repository
  3. Building and installing from the source 

## Installing Nmap from Binary RPMs  {.wp-block-heading}

## 1. Run system updates  {.wp-block-heading}

Before we can run the installation, we will first be required to update our repositories by running the following command on the terminal.

<pre class="wp-block-code"><code>$ sudo dnf update -y</code></pre>

## 2. Install Nmap on Fedora 35 {.wp-block-heading}

Now that we have completed system updates, it&#8217;s now time we install Nmap. To install Nmap run the following command

<pre class="wp-block-code"><code>$ rpm -vhU https://nmap.org/dist/nmap-7.92-1.x86_64.rpm</code></pre>

Output from the following will look like this 

<pre class="wp-block-code"><code>Output
Retrieving https://nmap.org/dist/nmap-7.92-1.x86_64.rpm
Verifying...                          ################################# &#91;100%]
Preparing...                          ################################# &#91;100%]
Updating / installing...
   &lt;strong>1:nmap-2:7.92-1                    ################################# &#91;100%]&lt;/strong></code></pre>

To install Zenmap GUI run the following command.

<pre class="wp-block-code"><code>$ rpm -vhU https://nmap.org/dist/zenmap-7.92-1.noarch.rpm</code></pre>

To install Ncat run the following command 

<pre class="wp-block-code"><code>$ rpm -vhU https://nmap.org/dist/ncat-7.92-1.x86_64.rpm</code></pre>

To install Nping run the following command

<pre class="wp-block-code"><code>$ rpm -vhU https://nmap.org/dist/ncat-7.92-1.x86_64.rpm</code></pre>

Note that Ncat, Nping commands above can be used only if you find that after installing Nmap you cant see the Ncat, Nping otherwise it comes preinstalled with Nmap.

## Removing Nmap  {.wp-block-heading}

To remove Nmap run the following command;

<pre class="wp-block-code"><code>rpm -e nmap zenmap</code></pre>

This command will remove Nmap and zenmap from the repository.

## Installing Nmap from the Yum repository {.wp-block-heading}

The easiest way you can install Nmap is via yum repository, even though this install can&#8217;t install the latest releases.

Yum takes care of the installation by contacting the necessary package of your architecture and taking care of the installation for you.

To install from the yum repository, run the following command;

<pre class="wp-block-code"><code>$ sudo dnf install nmap</code></pre>

Sample output from the above command looks like this

<pre class="wp-block-code"><code>Output
Last metadata expiration check: 0:00:18 ago on Tue 11 Jan 2022 01:49:12 PM UTC.
Dependencies resolved.
=================================================================================================================
 Package                Architecture             Version                          Repository                Size
=================================================================================================================
Installing:
 nmap                   x86_64                   3:7.91-8.fc35                    fedora                   5.4 M

Transaction Summary
=================================================================================================================
Install  1 Package

Total download size: 5.4 M
Installed size: 24 M
Is this ok &#91;y/N]: y
Last metadata expiration check: 0:00:18 ago on Tue 11 Jan 2022 01:49:12 PM UTC.
Dependencies resolved.
=================================================================================================================
 Package                Architecture             Version                          Repository                Size
=================================================================================================================
Installing:
 nmap                   x86_64                   3:7.91-8.fc35                    fedora                   5.4 M

Transaction Summary
=================================================================================================================
Install  1 Package

Total download size: 5.4 M
Installed size: 24 M
Is this ok &#91;y/N]: y
Downloading Packages:
nmap-7.91-8.fc35.x86_64.rpm                                                       14 MB/s | 5.4 MB     00:00    
-----------------------------------------------------------------------------------------------------------------
Total                                                                            3.5 MB/s | 5.4 MB     00:01     
Running transaction check
Transaction check succeeded.
Running transaction test
Transaction test succeeded.
Running transaction
  Preparing        :                                                                                         1/1 
  Installing       : nmap-3:7.91-8.fc35.x86_64                                                               1/1 
  Running scriptlet: nmap-3:7.91-8.fc35.x86_64                                                               1/1 
  Verifying        : nmap-3:7.91-8.fc35.x86_64                                                               1/1 

Installed:
  nmap-3:7.91-8.fc35.x86_64   </code></pre>

You can install both Nmap and Zenmap from just one command if you require to use the GUI also like this 

<pre class="wp-block-code"><code>$ sudo dnf install nmap zenmap</code></pre>

You can check the version of Nmap with the following 

<pre class="wp-block-code"><code>$ nmap --version
Nmap version 7.91 ( https://nmap.org )
Platform: x86_64-redhat-linux-gnu
Compiled with: nmap-liblua-5.3.5 openssl-1.1.1k libssh2-1.9.0 libz-1.2.11 libpcre-8.45 libpcap-1.10.1 nmap-libdnet-1.12 ipv6
Compiled without:
Available nsock engines: epoll poll select</code></pre>

## How to use Nmap {.wp-block-heading}

Whenever you want to use Nmap you can use the command **nmap -h**

<pre class="wp-block-code"><code>$ nmap -h</code></pre>

Nmap usage is like this 

<pre class="wp-block-code"><code>Usage: nmap &#91;Scan Type(s)] &#91;Options] {target specification}</code></pre>

Target specifications can be done by passing the hostname or the IP address

For example, to scan a network you should do the following:

<pre class="wp-block-code"><code>$ nmap www.hostname.com</code></pre>

You will get the following output from running your own host

<pre class="wp-block-code"><code>Output
Starting Nmap 7.91 ( https://nmap.org ) at 2022-01-11 14:17 UTC
Nmap scan report for www.nextgentips.com (104.21.76.248)
Host is up (0.0022s latency).
Other addresses for www.nextgentips.com (not scanned): 172.67.202.129 2606:4700:3034::6815:4cf8 2606:4700:3032::ac43:ca81
Not shown: 996 filtered ports
PORT     STATE SERVICE
80/tcp   open  http
443/tcp  open  https
8080/tcp open  http-proxy
8443/tcp open  https-alt

Nmap done: 1 IP address (1 host up) scanned in 4.64 seconds</code></pre>

You can also scan by specifying the IP Address

<pre class="wp-block-code"><code>$ nmap 167.71.133.223</code></pre>

<pre class="wp-block-code"><code>Output
Starting Nmap 7.91 ( https://nmap.org ) at 2022-01-11 14:22 UTC
Nmap scan report for 167.71.133.223
Host is up (0.0000040s latency).
Not shown: 999 closed ports
PORT   STATE SERVICE
22/tcp open  ssh

Nmap done: 1 IP address (1 host up) scanned in 0.22 seconds</code></pre>

## Conclusion {.wp-block-heading}

We have successfully installed Nmap on Fedora 35. You can explore more about Nmap because it has wide usage. Check out its help page to learn more.

 [1]: https://nmap.org/zenmap/
 [2]: https://nmap.org/ncat/
 [3]: https://nmap.org/ndiff/
 [4]: https://nmap.org/nping/