---
title: How to install MongoDB 5 on Ubuntu 21.04
author: Kipkoech Sang
type: post
date: 2021-10-26T12:47:58+00:00
url: /2021/10/26/how-to-install-mongodb-5-on-ubuntu-21-04/
ocean_gallery_link_images:
  - off
ocean_disable_margins:
  - enable
ocean_display_top_bar:
  - default
ocean_display_header:
  - default
ocean_disable_title:
  - default
ocean_disable_heading:
  - default
ocean_disable_breadcrumbs:
  - default
ocean_display_footer_widgets:
  - default
ocean_display_footer_bottom:
  - default
ocean_link_format_target:
  - self
ocean_quote_format_link:
  - post
entry_views:
  - 1
view_ip:
  - 'a:1:{i:0;s:14:"54.242.182.186";}'
rank_math_news_sitemap_robots:
  - index
rank_math_robots:
  - 'a:1:{i:0;s:5:"index";}'
rank_math_analytic_object_id:
  - 123
rank_math_internal_links_processed:
  - 1
categories:
  - Database

---
In this tutorial guide we are going to learn how to install Mongodb on Ubuntu 21.04. Mongodb is an open source NoSQL database that provides high throughput for data driven applications. Unlike relational databases such as MySQL, Oracle and SQL server which store data in tables according to a rigid schema, Mongodb stores data in documents with flexible schema.

## Why do we need MongoDB? {.wp-block-heading}

  * MongoDB makes it easy for developers to store structured or unstructured data, it uses JSON-like format to store documents. This format directly maps to native objects in most programming languages, making natural database for most developers.
  * MongoDB is built on a scale-out architecture making popular for developers developing scalable applications with evolving data schemas.
  * MongoDB is available in every major public cloud provider such as Azure, AWS, GCP making it easy for developers to deploy to any cloud provider of choice.
  * MongoDB supports rapid iterative development where developers collaborate with larger teams.
  * In MongoDB records are stored as documents in compressed [BSON][1] files.

MongoDB documents can be retrieved in JSON formats which has many benefits such as:

  * it is human readable, makes it easy to be read
  * It is a natural form to store data
  * You can nest JSON to to store complex data objects.
  * Documents maps to objects in most popular programming languages.
  * Structured and unstructured information can be stored in the same document 
  * JSON has a flexible and dynamic schema, so adding fields or leaving a field out is not a problem.

## Prerequisites  {.wp-block-heading}

  * Ubuntu 21.04 server having non-root privileges. Check out this article for more: [how to setup Ubuntu 20.04 Server for the first time][2]

## Installing MongoDB {.wp-block-heading}

We are going to install MongoDB 5.0 in our system. For it to reflect the latest version, you must include MongoDB&#8217;s dedicated package repository to your APT sources.

First you have to import public GPG key to our repository by running the following command.

<pre class="wp-block-code"><code>$ wget -qO - https://www.mongodb.org/static/pgp/server-5.0.asc | sudo apt-key add -</code></pre>

Let us look at the contents we used above 

  * **wget** which is a free software for retrieving files using HTTP, HTTPS, FTP and FTPS, the most widely used Internet protocols.
  * -q0 means quite to standard output silently. 

We can check if our public key was added successfully with the following command:

<pre class="wp-block-code"><code>$ apt-key list</code></pre>

The following output is given

<pre class="wp-block-code"><code>$ # apt-key list
Warning: apt-key is deprecated. Manage keyring files in trusted.gpg.d instead (see apt-key(8)).
/etc/apt/trusted.gpg
--------------------
pub   rsa4096 2021-02-16 &#91;SC] &#91;expires: 2026-02-15]
      F567 9A22 2C64 7C87 527C  2F8C B00A 0BD1 E2C6 3C11
uid           &#91; unknown] MongoDB 5.0 Release Signing Key &lt;packaging@mongodb.com&gt;

/etc/apt/trusted.gpg.d/ubuntu-keyring-2012-cdimage.gpg
------------------------------------------------------
pub   rsa4096 2012-05-11 &#91;SC]
      8439 38DF 228D 22F7 B374  2BC0 D94A A3F0 EFE2 1092
uid           &#91; unknown] Ubuntu CD Image Automatic Signing Key (2012) &lt;cdimage@ubuntu.com&gt;

/etc/apt/trusted.gpg.d/ubuntu-keyring-2018-archive.gpg
------------------------------------------------------
pub   rsa4096 2018-09-17 &#91;SC]
      F6EC B376 2474 EDA9 D21B  7022 8719 20D1 991B C93C
uid           &#91; unknown] Ubuntu Archive Automatic Signing Key (2018) &lt;ftpmaster@ubuntu.com&gt;</code></pre>

Run the following command to create a file **sources.list.d** directory named **mongodb-org-5.0.list**.

<pre class="wp-block-code"><code>$ echo "deb &#91; arch=amd64,arm64 ] https://repo.mongodb.org/apt/ubuntu focal/mongodb-org/5.0 multiverse" | sudo tee /etc/apt/sources.list.d/mongodb-org-5.0.list
</code></pre>

Output will look like this:

<pre class="wp-block-code"><code>$ deb &#91; arch=amd64,arm64 ] https://repo.mongodb.org/apt/ubuntu focal/mongodb-org/5.0 multiverse</code></pre>

You can now update the system to reflect on the changes 

<pre class="wp-block-code"><code>$ sudo apt update</code></pre>

After the update is complete, you can now install mongoDB with the following command:

<pre class="wp-block-code"><code>$ sudo apt install mongodb-org</code></pre>

Sample output will look like this:

<pre class="wp-block-code"><code># sudo apt install mongodb-org
Reading package lists... Done
Building dependency tree... Done
Reading state information... Done
The following additional packages will be installed:
  mongodb-database-tools mongodb-mongosh mongodb-org-database mongodb-org-database-tools-extra
  mongodb-org-mongos mongodb-org-server mongodb-org-shell mongodb-org-tools
The following NEW packages will be installed:
  mongodb-database-tools mongodb-mongosh mongodb-org mongodb-org-database mongodb-org-database-tools-extra
  mongodb-org-mongos mongodb-org-server mongodb-org-shell mongodb-org-tools
0 upgraded, 9 newly installed, 0 to remove and 0 not upgraded.
Need to get 147 MB of archives.
After this operation, 464 MB of additional disk space will be used.
Do you want to continue? &#91;Y/n] y
....

Adding group `mongodb' (GID 120) ...
Done.
Adding user `mongodb' to group `mongodb' ...
Adding user mongodb to group mongodb
Done.
Setting up mongodb-org-shell (5.0.3) ...
Setting up mongodb-database-tools (100.5.1) ...
Setting up mongodb-org-mongos (5.0.3) ...
Setting up mongodb-org-database-tools-extra (5.0.3) ...
Setting up mongodb-org-database (5.0.3) ...
Setting up mongodb-org-tools (5.0.3) ...
Setting up mongodb-org (5.0.3) ...</code></pre>

## Start MongoDB {.wp-block-heading}

We have completed the installation of MongoDB what is remaining is to start the service and enable it to start on boot. Use the following command to start the service. 

<pre class="wp-block-code"><code>$ sudo systemctl start mongod.service</code></pre>

You can check the status of the mongoDB if running with the following command:

<pre class="wp-block-code"><code>$ sudo systemctl status mongod</code></pre>

SAmple output will look like this:

<pre class="wp-block-code"><code># sudo systemctl status mongod
● mongod.service - MongoDB Database Server
     Loaded: loaded (/lib/systemd/system/mongod.service; disabled; vendor preset: enabled)
     Active: active (running) since Tue 2021-10-26 12:28:31 UTC; 7min ago
       Docs: https://docs.mongodb.org/manual
   Main PID: 3750 (mongod)
     Memory: 62.3M
        CPU: 2.377s
     CGroup: /system.slice/mongod.service
             └─3750 /usr/bin/mongod --config /etc/mongod.conf

Oct 26 12:28:31 ubuntu-21 systemd&#91;1]: Started MongoDB Database Server.</code></pre>

Make sure to see as active and running else the service is not running 

Now we can enableMongoDB to start on reboot. Use the following command:

<pre class="wp-block-code"><code>$ sudo systemctl enable mongod</code></pre>

Output will look like this:

<pre class="wp-block-code"><code># sudo systemctl enable mongod
Created symlink /etc/systemd/system/multi-user.target.wants/mongod.service → /lib/systemd/system/mongod.service.</code></pre>

That is all for MongoDB.

## Conclusion {.wp-block-heading}

We have accomplished the task of installing MongoDB in our Ubuntu 21.04 server, You have also enable the MongoDB to start on reboot which eliminates manual operation of enabling it every time.

 [1]: https://www.mongodb.com/json-and-bson
 [2]: https://nextgentips.com/2021/10/10/initial-server-setup-for-ubuntu-20-04/