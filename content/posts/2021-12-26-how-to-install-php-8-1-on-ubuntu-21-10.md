---
title: How to install PHP 8.1 on Ubuntu 21.10
author: Kipkoech Sang
type: post
date: 2021-12-26T09:31:44+00:00
url: /2021/12/26/how-to-install-php-8-1-on-ubuntu-21-10/
entry_views:
  - 1
view_ip:
  - 'a:1:{i:0;s:12:"200.8.204.33";}'
rank_math_news_sitemap_robots:
  - index
rank_math_robots:
  - 'a:1:{i:0;s:5:"index";}'
rank_math_analytic_object_id:
  - 52
rank_math_internal_links_processed:
  - 1
categories:
  - Database

---
In this tutorial, I am going to show you how to install the current stable release version of PHP 8.1 on Ubuntu 21.10.

[PHP][1] is a general-purpose scripting language suitable for web development. Most websites out there are programmed using PHP language because it is:

  * **Fast** &#8211; PHP is fast because it runs on its own memory space. Also PHP has Zend engine that parses PHP code and turns it into opcodes which are then interpreted.
  * **Flexible** &#8211; PHP is flexible because it allows to almost all databases. MySQL being the de-facto database to connect to because it is open source.
  * Free and open source
  * **PHP is forgiving** &#8211; Why do we say PHP is forgiving? It is forgiving meaning its learning it is not so hard therefore suitable to almost all programmers starting out.
  * PHP supports major communication protocols i.e LDAP, IMAP, POP3.

## Prerequisites for Installing PHP 8.1  {.wp-block-heading}

  * Before you can use PHP you must have **web server installed**. Apache web server is the most widely used with PHP.
  * **Database** must be there for PHP to run. It works well on all databases such Oracle, MySQL, PostgreSQL.
  * **PHP parser** must be there. What is PHP parser if you can ask? PHP Parser is a library that takes a source code written in PHP, passes it through a lexical analyzer and creates its respective syntax tree.

PHP 8.1 is a major update release version. It contains many new features and optimizations e.g

  * It introduced Enumerations feature. Enumerations or Enum feature allow a developer to define a custom type that is limited to one of the discrete number of possible values. This is helpful while defining domain model.
  * Improvement of RFC readonly property version 2. This prevents modification of the property after initialization.
  * Introduction of RFC fibers call action. This seeks to eliminate synchronous and asychronous function call by allowing functions to become interruptible without affecting the entire fuction.
  * Introduction of a non return type. This indicates that a function either exits, throws an exception or doesn&#8217;t terminate.
  * Introduction of first class collable actions. 
  * Introduction of [fsync][2] and [fdatasync][3] functions 
  * Introduction of [array\_is\_list][4] functions
  * Introduction of explicit octal numeral notation

## Related Articles  {.wp-block-heading}

  * [How to install PHP 8.1 on CentOS 8/RHEL 8][5]
  * [How to install PHP 8.1 on Ubuntu 21.04][6]
  * [How to Install PHP 8 on Ubuntu 21.04][7]

## Installing PHP 8.1 on Ubuntu 21.10 {.wp-block-heading}

## 1. Run System updates  {.wp-block-heading}

Before we can begin the installation, we need to make our repositories up to date by running complete system updates.

<pre class="wp-block-code"><code>$ sudo apt update && apt upgrade -y</code></pre>

## 2. Add Ondrej sury PPA repository {.wp-block-heading}

To run PHP 8.1 on Ubuntu 21.10, we need to use Add Ondrej sury PPA into our system. This is the maintainer of our PHP repository at the moment. This PPA is not currently checked so installing from this is not guaranteed 100% results. 

To add this PPA use the following command on our terminal.

<pre class="wp-block-code"><code>$ sudo add-apt-repository ppa:ondrej/php</code></pre>

You will get the following sample output.

<pre class="wp-block-code"><code>Output
PPA publishes dbgsym, you may need to include 'main/debug' component
Repository: 'deb http://ppa.launchpad.net/ondrej/php/ubuntu/ impish main'
Description:
Co-installable PHP versions: PHP 5.6, PHP 7.x and most requested extensions are included. Only Supported Versions of PHP (http://php.net/supported-versions.php) for Supported Ubuntu Releases (https://wiki.ubuntu.com/Releases) are provided. Don't ask for end-of-life PHP versions or Ubuntu release, they won't be provided.

Debian oldstable and stable packages are provided as well: https://deb.sury.org/#debian-dpa

You can get more information about the packages at https://deb.sury.org

IMPORTANT: The &lt;foo>-backports is now required on older Ubuntu releases.

BUGS&FEATURES: This PPA now has a issue tracker:
https:&#47;&#47;deb.sury.org/#bug-reporting

CAVEATS:
1. If you are using php-gearman, you need to add ppa:ondrej/pkg-gearman
2. If you are using apache2, you are advised to add ppa:ondrej/apache2
3. If you are using nginx, you are advised to add ppa:ondrej/nginx-mainline
   or ppa:ondrej/nginx

PLEASE READ: If you like my work and want to give me a little motivation, please consider donating regularly: https://donate.sury.org/</code></pre>

You need to run system updates again to update our repository to the current state.

<pre class="wp-block-code"><code>$ sudo apt update
&lt;strong>Hit:1 http://ppa.launchpad.net/ondrej/php/ubuntu impish InRelease&lt;/strong>
Hit:2 http://mirrors.digitalocean.com/ubuntu impish InRelease                                                   
Hit:3 https://repos-droplet.digitalocean.com/apt/droplet-agent main InRelease  </code></pre>

## 3. Install PHP 8.1 on Ubuntu 21.10 {.wp-block-heading}

Having added the PPA into our system, we now need to install PHP 8.1. The installation will be easy because we have added what is needed to run the installer effectively.

<pre class="wp-block-code"><code>$ sudo apt install php8.1</code></pre>

You will get the following sample output.

<pre class="wp-block-code"><code>Output
The following additional packages will be installed:
  apache2 apache2-bin apache2-data apache2-utils bzip2 libapache2-mod-php8.1 libapr1 libaprutil1
  libaprutil1-dbd-sqlite3 libaprutil1-ldap libjansson4 liblua5.3-0 libpcre2-8-0 libsodium23 mailcap
  mime-support php-common php8.1-cli php8.1-common php8.1-opcache php8.1-readline ssl-cert
Suggested packages:
  apache2-doc apache2-suexec-pristine | apache2-suexec-custom www-browser bzip2-doc php-pear
The following NEW packages will be installed:
  apache2 apache2-bin apache2-data apache2-utils bzip2 libapache2-mod-php8.1 libapr1 libaprutil1
  libaprutil1-dbd-sqlite3 libaprutil1-ldap libjansson4 liblua5.3-0 libsodium23 mailcap mime-support php-common
  php8.1 php8.1-cli php8.1-common php8.1-opcache php8.1-readline ssl-cert
The following packages will be upgraded:
  libpcre2-8-0
1 upgraded, 22 newly installed, 0 to remove and 0 not upgraded.
Need to get 7639 kB of archives.
After this operation, 30.2 MB of additional disk space will be used.
Do you want to continue? &#91;Y/n] </code></pre>

Press Y to allow installation to continue.

Having successfully installed PHP 8.1, we can confirm our installed version with the following command.

<pre class="wp-block-code"><code>$ php --version
PHP 8.1.0 (cli) (built: Nov 25 2021 20:22:41) (NTS)
Copyright (c) The PHP Group
Zend Engine v4.1.0, Copyright (c) Zend Technologies
    with Zend OPcache v8.1.0, Copyright (c), by Zend Technologies</code></pre>

To list the available PHP modules use the following command;

<pre class="wp-block-code"><code>$ php --modules
&#91;PHP Modules]
calendar
Core
ctype
date
exif
FFI
fileinfo
filter
ftp
gettext
hash
iconv
json
libxml
openssl
pcntl
pcre
PDO
Phar
posix
readline
Reflection
session
shmop
sockets
sodium
SPL
standard
sysvmsg
sysvsem
sysvshm
tokenizer
Zend OPcache
zlib

&#91;Zend Modules]
Zend OPcache</code></pre>

## Conclusion {.wp-block-heading}

We have learned how to install PHP 8.1 on Ubuntu 21.10, now you can proceed to write your programs using this awesome language. Consult [PHP documentation][8] in case you experience any difficulty.

 [1]: https://www.php.net/
 [2]: https://www.php.net/manual/en/function.fsync.php
 [3]: https://www.php.net/manual/en/function.fdatasync.php
 [4]: https://www.php.net/manual/en/function.array-is-list.php
 [5]: https://nextgentips.com/2021/12/04/how-to-install-php-8-1-on-centos-8-rhel-8/
 [6]: https://nextgentips.com/2021/12/03/how-to-install-php-8-1-on-ubuntu-21-04/
 [7]: https://nextgentips.com/2021/10/19/how-to-install-php-8-on-ubuntu-21-04/
 [8]: https://www.php.net/docs.php