---
title: How to install Sublime Text 4 Editor on Ubuntu 22.04
author: Kipkoech Sang
type: post
date: 2022-07-01T08:51:00+00:00
url: /2022/07/01/how-to-install-sublime-text-4-editor-on-ubuntu-22-04/
categories:
  - Programming

---
In this tutorial, we are going to learn how to install the Sublime Text 4 code editor on Ubuntu 22.04.

A sublime Text editor is a shareware cross-platform source code editor, it supports almost all the programming languages. 

## What&#8217;s new with Sublime Text 4? {.wp-block-heading}

Sublime Text 4 comes with new features such as:

  * GPU rendering. It can now utilize GPU on Linux, Mac, and windows giving fluid UI up to 8K resolutions.
  * Tab multi-select. File tabs have been enhanced to make split-view effortless with support throughout the interface and built-in commands.
  * Context-aware auto-complete. It has been rewritten to provide smart completion based on existing code.
  * Updated Python API. It has updated Python to 3.8 while keeping backward compatibility.
  * Superpowered syntax definition. It has improved the syntax highlighting engine a lot e.g inclusion of lazy embeds and syntax inheritance.
  * Typescript, JSX, and TSX support. You are now in a position to utilize all the smart syntax-based features of sublime Text.

## Install Sublime Text 4 on Ubuntu 22.04 {.wp-block-heading}

### 1. Update system repositories  {.wp-block-heading}

Before we can begin the installation of Sublime Text 4 on Ubuntu 22.04, we need to update our system repositories in order to make them up to date.

<pre class="wp-block-code"><code>sudo apt update && apt upgrade -y</code></pre>

### 2. Install Sublime Text 4 dependencies  {.wp-block-heading}

Sublime Text 4 requires dependencies in order to run effectively such as HTTPs, so we are required to install this if not already installed on your system.

<pre class="wp-block-code"><code>sudo apt install dirmngr gnupg apt-transport-https ca-certificates software-properties-common</code></pre>

### 3. Install Sublime Text 4 on Ubuntu 22.04 {.wp-block-heading}

To begin installing Sublime Text, start by installing GPG keys on your system.

<pre class="wp-block-code"><code>wget -qO - https://download.sublimetext.com/sublimehq-pub.gpg | gpg --dearmor | sudo tee /etc/apt/trusted.gpg.d/sublimehq-archive.gpg</code></pre>

When you have successfully added the GPG key add Sublime Text to the apt repository with the following command.

<pre class="wp-block-code"><code>echo "deb https://download.sublimetext.com/ apt/stable/" | sudo tee /etc/apt/sources.list.d/sublime-text.list</code></pre>

Then the next thing is to update the repositories for the changes to take effect and then run the installation afterward.

<pre class="wp-block-code"><code>$ sudo apt update
$ sudo apt install sublime-text</code></pre>

You should be in a position to see the following output

<pre class="wp-block-code"><code>#output
The following NEW packages will be installed:
  &lt;strong>sublime-text&lt;/strong>
0 upgraded, 1 newly installed, 0 to remove and 184 not upgraded.
Need to get 16.4 MB of archives.
After this operation, 50.6 MB of additional disk space will be used.
Get:1 https://download.sublimetext.com apt/stable/ sublime-text 4126 &#91;16.4 MB]
Fetched 16.4 MB in 24s (685 kB/s)                                              
Selecting previously unselected package sublime-text.
(Reading database ... 260078 files and directories currently installed.)
Preparing to unpack .../sublime-text_4126_amd64.deb ...
Unpacking sublime-text (4126) ...
Setting up sublime-text (4126) ...
Processing triggers for mailcap (3.70+nmu1ubuntu1) ...
Processing triggers for desktop-file-utils (0.26-1ubuntu3) ...
Processing triggers for hicolor-icon-theme (0.17-2) ...
Processing triggers for gnome-menus (3.36.0-1ubuntu3) ...</code></pre>

And you have successfully installed Sublime Text on your ubuntu 22.04.

### 4. Start using Sublime Text {.wp-block-heading}

To start using Sublime Text, you can open via the **terminal or via the activities in** the applications section.

Open Sublime Text on the terminal

<pre class="wp-block-code"><code>$ subl</code></pre>

Open via the activities menu.

<pre class="wp-block-code"><code>show applications &gt;&gt; Search for sublime &gt;&gt; Click on sublime icon</code></pre>

To start using Sublime you can import your existing project or you can create a new project.<figure class="wp-block-image size-full">

<img decoding="async" loading="lazy" width="756" height="647" src="https://i0.wp.com/nextgentips.com/wp-content/uploads/2022/07/Screenshot-from-2022-07-01-11-46-33.png?resize=756%2C647&#038;ssl=1" alt="Sublime Text 4 interface" class="wp-image-1424" srcset="https://i0.wp.com/nextgentips.com/wp-content/uploads/2022/07/Screenshot-from-2022-07-01-11-46-33.png?w=756&ssl=1 756w, https://i0.wp.com/nextgentips.com/wp-content/uploads/2022/07/Screenshot-from-2022-07-01-11-46-33.png?resize=300%2C257&ssl=1 300w" sizes="(max-width: 756px) 100vw, 756px" data-recalc-dims="1" /> <figcaption>Sublime Text 4 interface</figcaption></figure> 

To check the version of Sublime Text go to help then about, you will find your number there.

<pre class="wp-block-code"><code>help >> about sublime text</code></pre><figure class="wp-block-image size-full">

<img decoding="async" loading="lazy" width="330" height="198" src="https://i0.wp.com/nextgentips.com/wp-content/uploads/2022/07/Screenshot-from-2022-07-01-11-58-19.png?resize=330%2C198&#038;ssl=1" alt="Sublime Text version" class="wp-image-1427" srcset="https://i0.wp.com/nextgentips.com/wp-content/uploads/2022/07/Screenshot-from-2022-07-01-11-58-19.png?w=330&ssl=1 330w, https://i0.wp.com/nextgentips.com/wp-content/uploads/2022/07/Screenshot-from-2022-07-01-11-58-19.png?resize=300%2C180&ssl=1 300w" sizes="(max-width: 330px) 100vw, 330px" data-recalc-dims="1" /> <figcaption>Sublime Text version</figcaption></figure> 

The build number is your current version (Build 4126)

## Conclusion {.wp-block-heading}

Congratulations, you have successfully installed the Sublime Text 4 editor on Ubuntu 22.04. For more information, you can head over to <a href="https://www.sublimetext.com/blog/articles/sublime-text-4" target="_blank" rel="noreferrer noopener">Sublime Text documentation</a>.