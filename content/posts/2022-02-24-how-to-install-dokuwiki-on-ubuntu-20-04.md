---
title: How to install DokuWiki on Ubuntu 20.04
author: Kipkoech Sang
type: post
date: 2022-02-24T18:17:08+00:00
url: /2022/02/24/how-to-install-dokuwiki-on-ubuntu-20-04/
rank_math_primary_category:
  - 6
rank_math_description:
  - "DokuWiki is a simple to use and highly versatile Open Source wiki software that doesn't require a database."
rank_math_focus_keyword:
  - install DokuWiki
rank_math_news_sitemap_robots:
  - index
rank_math_robots:
  - 'a:1:{i:0;s:5:"index";}'
rank_math_analytic_object_id:
  - 3
rank_math_internal_links_processed:
  - 1
categories:
  - Linux

---
In this tutorial we are going to learn how to install DokuWiki on Ubuntu 20.04.

DokuWiki is easy to use and simple open-source wiki software it that doesn&#8217;t require a database. Something that doesn&#8217;t require any storage space is something incredible. Lets highlight it more.

**The reason why we need this wiki software is that,** **it is easy to install and use, it doesn&#8217;t require specialized system requirements such as storage space, RAM, it has high language base support, its open-source and also device independent.** 

We have other alternatives apart from DokuWiki such as: **confluence, clickup, notion, xwiki, bloomfire, yext, guru** and many more.

## Installing DokuWiki on Ubuntu 20.04 {.wp-block-heading}

Installing DokuWiki on Debian based system is easy because it is found on the Debian repository, but it is not the best way to do install. The reasons being:

  * Packages found might be outdated
  * The packages replaces some of the Dokuwiki&#8217;s internal libraries which again can cause incompatibilities. 
  * Debian uses different directory structure, which can cause issues with some plugins 

### Prerequisites {.wp-block-heading}

  * Apache webserver
  * Ubuntu 20.04 
  * basic understanding of the command line

### 1. Update system repositories. {.wp-block-heading}

The best way to start installing any software is to run system updates in order to make repositories up to date hence avoid running into some errors during installation process.

<pre class="wp-block-code"><code>$ sudo apt update && apt upgrade -y</code></pre>

When updates and upgrades are complete, we can move forward and install apache2 

### 2. Install Apache2  {.wp-block-heading}

The reason why we require apache is to provide a secure, efficient and extensible server that provides HTTP services in sync with the current HTTP standards. 

To install Apache check this article: <a href="https://nextgentips.com/2021/10/11/how-to-install-apache-mysql-php-on-ubuntu-20-04/" target="_blank" rel="noreferrer noopener">How to Install Apache, MYSQL, PHP on Ubuntu 20.04</a>.

<pre class="wp-block-code"><code>$ sudo apt install apache2 -y</code></pre>

Enable and start Apache service on start.

<pre class="wp-block-code"><code># start
$ sudo systemctl start apache2
# enable
$ sudo systemctl enable apache2
# status
$ sudo systemctl status apache2</code></pre>

<pre class="wp-block-code"><code># status
● apache2.service - The Apache HTTP Server
     Loaded: loaded (/lib/systemd/system/apache2.service; enabled; vendor preset: enabled)
     Active: active (running) since Thu 2022-02-24 17:45:58 UTC; 2min 13s ago
       Docs: https://httpd.apache.org/docs/2.4/
   Main PID: 15564 (apache2)
      Tasks: 55 (limit: 1132)
     Memory: 5.2M
     CGroup: /system.slice/apache2.service
             ├─15564 /usr/sbin/apache2 -k start
             ├─15566 /usr/sbin/apache2 -k start
             └─15567 /usr/sbin/apache2 -k start</code></pre>

The next thing is to adjust the firewall settings to accept Http and Https traffic. In order to allow apache full control run the following command:

<pre class="wp-block-code"><code>$ sudo ufw allow "Apache Full"</code></pre>

### 3. Install DokuWiki dependencies.  {.wp-block-heading}

In order to install DokuWiki, we need to install some dependencies such as apache-mod-php. To install those dependencies run the following command.

<pre class="wp-block-code"><code>$ sudo apt install php libapache2-mod-php php-xml</code></pre>

The next thing is to restart apache server for the changes to take effect.

<pre class="wp-block-code"><code>$ sudo systemctl restart apache2</code></pre>

### 4. Download latest release of DokuWiki. {.wp-block-heading}

We are going to download and extract dokuwiki from archive. First lets download using wget command.

<pre class="wp-block-code"><code># cd into www directory
$ cd /var/www
# download dokuwiki
$ sudo wget https://download.dokuwiki.org/src/dokuwiki/dokuwiki-stable.tgz</code></pre>

After the download is complete, extract it from the archives 

<pre class="wp-block-code"><code>$ sudo tar xvf dokuwiki-stable.tgz</code></pre>

Then we need to move the extracted archive to dokuwiki directory.

<pre class="wp-block-code"><code>$ sudo mv dokuwiki-*/ dokuwiki</code></pre>

Also we need to change read and write permission to allow it to run whenever we called it.

<pre class="wp-block-code"><code>$ sudo chown -R www-data:www-data /var/www/dokuwiki</code></pre>

The next thing is to change the document root in Apache to point to /var/www/dokuwiki

<pre class="wp-block-code"><code>$ sudo vim /etc/apache2/sites-enabled/000*.conf</code></pre>

We need to replace **DocumentRoot /var/www/html** with **DocumentRoot /var/www/dokuwiki**.

NB: If you want to host a DokuWiki server on localhost for testing purposes do NOT change the content of `<strong>/etc/apache2/sites-enabled/000*.conf</strong>`, instead create a new file in `<strong>/etc/apache2/sites-available</strong>` with the following code. Because mine is testing purpose I will go this route.

<pre class="wp-block-code"><code>$ sudo touch apache2-dokuwiki.conf</code></pre>

Input the following content onto the file 

<pre class="wp-block-code"><code>&lt;VirtualHost 127.0.0.1>
        DocumentRoot /var/www/dokuwiki
 	ServerName localhost
&lt;/VirtualHost></code></pre>

Then we need to enable the new project by placing it into  **/etc/apache2/sites-enabled**

<pre class="wp-block-code"><code>$  sudo a2ensite apache2-dokuwiki</code></pre>

Reload apache2 again

<pre class="wp-block-code"><code>$ sudo systemctl reload apache2</code></pre>

Lastly is to change allowoverides settings on Apache 2 to use **.htaccess** files for security

<pre class="wp-block-code"><code>$ sudo vim /etc/apache2/apache2.conf</code></pre>

In the directory **/var/www/** replace **Allowoveride None** with **Allowoveride All** and then restart Apache2

<pre class="wp-block-code"><code>$ sudo systemctl restart apache2</code></pre>

When all those is over, visit http://<your\_IP\_Address>/install.php to run your configurations.

## Conclusion {.wp-block-heading}

Thank you for staying with me during this tutorial, I hope you have learned something new. Incase you have any difficulty don&#8217;t hesitate to contact us or see the <a href="https://www.dokuwiki.org/dokuwiki" target="_blank" rel="noreferrer noopener">DokuWiki documentations</a> for further reading.