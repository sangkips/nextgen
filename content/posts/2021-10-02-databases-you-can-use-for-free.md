---
title: Databases you can use for free
author: Kipkoech Sang
type: post
date: 2021-10-02T11:19:04+00:00
url: /2021/10/02/databases-you-can-use-for-free/
site-sidebar-layout:
  - default
site-content-layout:
  - default
theme-transparent-header-meta:
  - default
ocean_gallery_link_images:
  - off
ocean_disable_margins:
  - enable
ocean_display_top_bar:
  - default
ocean_display_header:
  - default
ocean_disable_title:
  - default
ocean_disable_heading:
  - default
ocean_disable_breadcrumbs:
  - default
ocean_display_footer_widgets:
  - default
ocean_display_footer_bottom:
  - default
ocean_link_format_target:
  - self
ocean_quote_format_link:
  - post
rank_math_primary_category:
  - 7
rank_math_news_sitemap_robots:
  - index
rank_math_robots:
  - 'a:1:{i:0;s:5:"index";}'
rank_math_analytic_object_id:
  - 151
rank_math_internal_links_processed:
  - 1
categories:
  - Database

---
In this article I will walk you through databases management systems that you can use for free. It provides needed flexibility and strength to enable you run your databases smoothly.

## RethinkDB {.wp-block-heading}

RethinkDB is an open-source, scalable JSON database built from the ground up for the realtime web. It inverts the traditional database architecture by exposing an exciting new access model, instead of polling for changes, the developer can tell RethinkDB to continuously push updated query results to applications in realtime. RethinkDB’s realtime push architecture dramatically reduces the time and effort necessary to build scalable realtime apps. It is a great option when you need real time feeds to your data.

## CockroachDB {.wp-block-heading}

CockroachDB is the most evolved database born in the cloud architected to scale and survive. CockroachDB is in production across thousands of modern cloud applications and services such as general purpose databases, transactional databases.

Capabilities of CockroachDB

  * It scales first. It automatically distributes data and workload demand therefore breaks from manual sharding and complex workarounds.
  * It survive any failure. cockroach is designed to handle unpredictability and survive machine, datacenter, and region failures.
  * It ensures transactional consistency. CockroachDB provides guaranteed ACID compliant transactions. 

## HarperDB {.wp-block-heading}

HarperDB is a&nbsp;geo-distributed database&nbsp;with hybrid SQL & NoSQL functionality in one powerful tool, accessed via a REST API.

## Features of HarperDB {.wp-block-heading}

  * **&nbsp;**IT runs anywhere**&nbsp;**presenting a single interface across all deployment architectures.
  * It has hybrid cloud&nbsp;capability that can run on public cloud, edge data centers, on-premise, and/or in the field.&nbsp;
  * It provides&nbsp;low latency&nbsp;edge data replication, effectively the evolution of CDN, but for your data.
  * It provides real-time&nbsp;data synchronization between nodes
  * A&nbsp;single node&nbsp;of HarperDB can handle&nbsp;100,000 requests per second**&nbsp;**and it can&nbsp;globally replicate data&nbsp;at the speed of the Internet

## CouchDB {.wp-block-heading}

CouchDB is a seamless multi-master sync that scales from big data to mobile with an intuitive HTTP/JSON API and designed for reliability.

Apache CouchDB lets you access your data where you need it. The&nbsp;Couch Replication Protocol&nbsp;is implemented in a variety of projects and products that span every imaginable computing environment from&nbsp;globally distributed server-clusters, over&nbsp;mobile phones&nbsp;to&nbsp;web browsers.

The Couch Replication Protocol&nbsp;lets your data flow seamlessly between server clusters to mobile phones and web browsers, enabling a compelling&nbsp;offline-first&nbsp;user experience while maintaining high performance and strong reliability. CouchDB comes with a&nbsp;developer friendly&nbsp;query language, and optionally MapReduce for simple, efficient, and comprehensive data retrieval.

## FaunaDB {.wp-block-heading}

FaunaDB is a flexible, developer-friendly, transactional database delivered as a secure and scalable cloud API with native GraphQL. Never again worry about database provisioning, scaling, sharding, replication, or correctness.

Fauna provides serverless, multi-region, transactional database instances that are accessible via a cloud API. Easy to adopt and highly productive to use, Fauna gives you the data safety and reliability you need, without the operational pain typically inflicted by databases.

Fauna is a unique indexed document system that supports relations, documents and graphs for unmatched modeling flexibility.

Fauna gives you the data safety, security, and scalability you need to build a new business or modernize existing applications.

Fauna is an API, not a DBaaS or a cluster that you must manage. It is provisioning-free, configuration-free and available instantly as a serverless utility.

## Apache Cassandra Database {.wp-block-heading}

Apache Cassandra is an open-source, free, NoSQL database used to store large data.

NoSQL databases are light weight, open source, non-relational and largely distributed. It has the following strengths:

  * It has horizontal scalability. Cassandra allows for seamless addition of nodes. a node represent an instance of Cassandra. This nodes do communicate with each other through a protocol called [gossip][1]. Gossip is a peer-to-peer communication protocol in which nodes periodically exchanges communication about themselves and other nodes they know about.
  * It contain distributed architecture. Distributed means that Cassandra can run on multiple machines while appearing to users as a unified whole. Cassandra databases easily scale when an application is under stress, this prevents data loss from any given datacenter&#8217;s hardware failure. 
  * It has flexible approach to schema definition. A schema once defined its columns for a table while inserting data in every row, all columns must at least filled with null value but for Cassandra column families are defined the columns are not. You can add any column to any column family at any time.

Apache Cassandra is suitable for high volume data types. With the advent of Big data, Cassandra have become the solution to issues which SQL databases was not able to solve.

## PostgreSQL Database {.wp-block-heading}

PostgreSQL is world&#8217;s most advanced open source relational database. It has experienced running up to 30 years that is why it has earned strong reputation for its reliability, robustness and its strong performance.

PostgreSQL 14 brings a variety of features that help developers and administrators deploy there data backed applications. PostgreSQL 14 continues to build on complex data types include access to convenient JSON files and support for non contiguous ranges of data. PostgreSQL 14 its main agenda was to provide high performance and distributed data workloads. It advances in data concurrency, high write workloads, query parallelism and logical replication.

## New features contained in PostgreSQL 14 {.wp-block-heading}

  * The SQL standard **search** and **cycle** for options for common table expressions have been implemented. When computing a tree traversal using a recursive query, you might want to order the results in either depth-first or breadth-first order.B-tree index updates are managed more effectively, reducing index plot.**libpq** now has the ability to pipeline multiple queries, which can boost throughput over high latency connections. libpq is a set of library functions that allow client programs to pass queries to the PostgreSQL backend server and to receive the results of these queries.Extended statistics can now be collected on expressions, allowing better planning for complex results.Improvement have been made for parallel queries, heavily concurrent workloads, partitioned tables, logical replication and vacuuming. VACUUM reclaims storage occupied by dead tuples. In normal PostgreSQL operation, tuples that are deleted or obsoleted by an update are not physically removed from their table; they remain present until a VACUUM is done. Therefore it&#8217;s necessary to do VACUUM periodically, especially on frequently-updated tables.VACUUM has become more aggressive, it skips inessential cleanup if the database starts to approach a transaction ID wraparound condition.

## Related content {.wp-block-heading}

  * [How to Install PostgreSQL 14 on Rocky Linux 8][2]
  * [How to Install Apache Cassandra 4 on Ubuntu 21.10][3]
  * [How to install MongoDB 5 on Ubuntu 21.04][4]
  * [How to install Apache CouchDB 3.2 on Ubuntu 20.04][5]
  * [How to install MariaDB 10.7 on Ubuntu 21.04][6]
  * [How to install PostgreSQL 14 on Ubuntu 20.04][7]

 [1]: https://docs.datastax.com/en/cassandra-oss/3.x/cassandra/architecture/archGossipAbout.html
 [2]: https://nextgentips.com/2021/10/28/how-to-install-postgresql-14-on-rocky-linux-8/
 [3]: https://nextgentips.com/2021/10/28/how-to-install-apache-cassandra-4-on-ubuntu-21-10/
 [4]: https://nextgentips.com/2021/10/26/how-to-install-mongodb-5-on-ubuntu-21-04/
 [5]: https://nextgentips.com/2021/10/17/how-to-install-apache-couchdb-3-2-on-ubuntu-20-04/
 [6]: https://nextgentips.com/2021/10/15/how-to-install-mariadb-10-7-on-ubuntu-21-04/
 [7]: https://nextgentips.com/2021/10/09/how-to-install-postgresql-on-ubuntu-20-04/