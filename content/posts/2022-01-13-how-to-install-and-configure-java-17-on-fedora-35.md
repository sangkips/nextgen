---
title: How to install and Configure Java 17 on Fedora 35
author: Kipkoech Sang
type: post
date: 2022-01-13T19:02:27+00:00
url: /2022/01/13/how-to-install-and-configure-java-17-on-fedora-35/
rank_math_news_sitemap_robots:
  - index
rank_math_robots:
  - 'a:1:{i:0;s:5:"index";}'
rank_math_analytic_object_id:
  - 34
rank_math_internal_links_processed:
  - 1
categories:
  - Programming

---
In today&#8217;s guide, we are going to learn how we can install Java SE 17 on Fedora 35. Java is widely used in programs like Cassandra, Graylog, Wine, etc.

Java delivers thousands of performance, stability, and security updates that is the reason why java is widely used and has a larger community base worldwide.

## Java 17 new features {.wp-block-heading}

Java 17 includes new language enhancement, updates to the libraries, updates to the new Apple computers, removals, and deprecations of legacy features. Updates include the following:

  * Introduction of sealed classes. That is, it restrict which other classes or interfaces may extend or implement them.
  * It introduces restore always-strict floating-point semantics
  * Introduction of enhanced pseudo random number generator
  * Introduction of new MacOS rendering pipeline using the Apple metal API

It also deprecated some old features such as:

  * Deprecated the java Applet API
  * It removed Remote Method Invocation activation
  * Removes the experimental Ahead Of Time and Just In Time compiler
  * Deprecated the Security Manager which has been the conerstone of securing client side java code.

## Prerequisites {.wp-block-heading}

  * Have a root user account while running the installation
  * Have Fedora 35 

## Related Articles {.wp-block-heading}

  * [How to install Oracle Java SE 17 on Ubuntu 20.04][1]

## Installation Java SE 17 on Fedora 35 {.wp-block-heading}

## 1. Run sytem updates  {.wp-block-heading}

Before you begin any install make sure you run system updates to make your repositories up to date.

<pre class="wp-block-code"><code>$ sudo dnf update -y</code></pre>

## 2. Installing java 17 {.wp-block-heading}

Installing java 17 we need to get the installation package from the Oracle download pages. Use the following link to make the download easier and to get the latest version.

<pre class="wp-block-code"><code>$ wget https://download.java.net/java/GA/jdk17.0.1/2a2082e5a09d4267845be086888add4f/12/GPL/openjdk-17.0.1_linux-x64_bin.tar.gz</code></pre>

If you get an error that wget is not installed, you can run the installation with the following command 

<pre class="wp-block-code"><code>$ sudo dnf install wget -y</code></pre>

Run the installer again this time it will go through successfully.

Check the downloaded file before extracting it 

<pre class="wp-block-code"><code>$ ls 
openjdk-17.0.1_linux-x64_bin.tar.gz</code></pre>

Move ahead and extract the contents into your home directory.

<pre class="wp-block-code"><code>$ tar -xzf openjdk-17.0.1_linux-x64_bin.tar.gz</code></pre>

We need to move the contents to the /opt directory 

<pre class="wp-block-code"><code>$ sudo mv jdk-17.0.1/ /opt</code></pre>

You can do an ls again to verify that indeed you have moved jdk17 to the opt directory.

Let&#8217;s now set our java home directory so that every time you run java application, it knows where to run from. To do that we can do the following;

Open the ~/.bashrc file and add the following

<pre class="wp-block-code"><code>$ sudo nano ~/.bashrc</code></pre>

Add the following contents to the bashrc file

<pre class="wp-block-code"><code>export JAVA_HOME=/opt/jdk-17.0.1
export PATH=$PATH:$JAVA_HOME/bin</code></pre>

Save and exit the nano.

Run source ~/.bashrc so that changes to bashrc file can take effect.

<pre class="wp-block-code"><code>$ source ~/.bashrc</code></pre>

Now we can check the version of the installed java with the following command;

<pre class="wp-block-code"><code>$ java --version
openjdk 17.0.1 2021-10-19
OpenJDK Runtime Environment (build 17.0.1+12-39)
OpenJDK 64-Bit Server VM (build 17.0.1+12-39, mixed mode, sharing)</code></pre>

## Test java 17 program {.wp-block-heading}

Let&#8217;s test if Java is working correctly. To do that we are going to run our first hello world program

<pre class="wp-block-code"><code>$ sudo nano hello.java</code></pre>

Add the following to your hello.java

<pre class="wp-block-code"><code>public class hello {
    public static void main(String&#91;] args) {
        System.out.println("Hello, World! from Nextgentips"); 
    }
}</code></pre>

You need to compile the java programs in order to show the output. To run the compilation run the following code

<pre class="wp-block-code"><code>$ javac hello.java</code></pre>

If you don’t run into any errors then it’s time to run your first java program.

<pre class="wp-block-code"><code>$ java hello
Hello, World! from Nextgentips</code></pre>

## Setting Java 17 environment {.wp-block-heading}

To ensure that you cant run into problems while running java programs, we need to set JAVA_HOME environment where java will be executed. If you don’t set this environment you will encounter errors in the future. To avoid that let&#8217;s set the environment.

Let&#8217;s check where we have our java 17 with the following command.

<pre class="wp-block-code"><code>$ which java 
/opt/jdk-17.0.1/bin/java</code></pre>

You will get the path like this

So let&#8217;s set this in our **/etc/environment**

<pre class="wp-block-code"><code>$ sudo nano /etc/environment </code></pre>

Add the following

<pre class="wp-block-code"><code>export JAVA_HOME=/opt/jdk-17.0.1/bin/java</code></pre>

Save and exit

To apply the changes using the following command

<pre class="wp-block-code"><code>$ source /etc/environment</code></pre>

To check if the environment path is correct use the following command

<pre class="wp-block-code"><code>$ echo $JAVA_HOME
/opt/jdk-17.0.1/bin/java</code></pre>

If you are getting empty results know that you have not applied the changes as required, go back and do **source /etc/environment**

From here now you will not be required to set the environment every time you install another java version.

## Conclusion {.wp-block-heading}

You have successfully installed Java 17 on Fedora 35. In case of difficulty consult the [documentation][2]

 [1]: https://nextgentips.com/2022/01/13/how-to-install-oracle-java-se-17-on-ubuntu-20-04/
 [2]: https://docs.oracle.com/cd/E19182-01/821-0917/inst_jdk_javahome_t/index.html