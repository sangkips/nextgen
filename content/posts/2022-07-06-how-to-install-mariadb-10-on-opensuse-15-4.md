---
title: How to install MariaDB 10 on OpenSuse 15.4
author: Kipkoech Sang
type: post
date: 2022-07-06T14:25:04+00:00
url: /2022/07/06/how-to-install-mariadb-10-on-opensuse-15-4/
categories:
  - Database

---
In this guide we are going to install MariaDB 10 on Opensuse, but first what is MariaDB?

MariaDB Server is one of the most popular open-source relational databases. It’s made by the original developers of MySQL and guaranteed to stay open source. It is part of most cloud offerings and the default in most Linux distributions.

It is built upon the values of performance, stability, and openness, and MariaDB Foundation ensures contributions will be accepted on technical merit. Recent&nbsp;**new functionality**&nbsp;includes&nbsp;**advanced clustering with&nbsp;[Galera Cluster 4][1]**,&nbsp;**compatibility features with Oracle Database**&nbsp;and&nbsp;**Temporal Data Tables**, allowing one to query the data as it stood at any point in the past.

## Install MariaDB on OpenSuse 15.4  {.wp-block-heading}

### 1. Update system repositories  {.wp-block-heading}

To begin the installation, lets first update the repositories in order to make them up to date.

<pre class="wp-block-code"><code>$ sudo zypper ref
$ sudo zypper update -y</code></pre>

### 2. Install MariaDB 10 on OpenSuse 15.4 {.wp-block-heading}

To install MariaDB, we are going to configure `zypper` so that we can use to make the installation. Use the following command to add MariaDB to zypper.

<pre class="wp-block-code"><code>sudo zypper addrepo --gpgcheck --refresh https://yum.mariadb.org/10.6/sles/15/x86_64 mariadb</code></pre>

Import Keys to sign the MariaDB on OpenSuse repositories.

<pre class="wp-block-code"><code>sudo rpm --import https://yum.mariadb.org/RPM-GPG-KEY-MariaDB
sudo zypper --gpg-auto-import-keys refresh</code></pre>

To install MariaDB use the following command.

<pre class="wp-block-code"><code>sudo zypper install MariaDB-server</code></pre>

To install commonly used packages use the following command.

<pre class="wp-block-code"><code>sudo zypper install galera MariaDB-shared MariaDB-backup MariaDB-common</code></pre>

### 3. Initial MariaDB configuration {#3-initial-mariadb-configuration.wp-block-heading}

The installation process will not ask you to set a password and this leaves MariaDB insecure. We will use a script that the MariaDB-server package provides strict access to the server and remove unused accounts. Read more about the security script&nbsp;[here][2].

To ensure that our database is secure we need to run the&nbsp;**mysql\_secure\_installation**&nbsp;command to remove all remote root login, remove anonymous users, etc.

<pre class="wp-block-code"><code>$ sudo mysql_secure_installation</code></pre>

Following the prompts to secure your MariaDB 

<pre class="wp-block-code"><code>NOTE: RUNNING ALL PARTS OF THIS SCRIPT IS RECOMMENDED FOR ALL MariaDB
      SERVERS IN PRODUCTION USE!  PLEASE READ EACH STEP CAREFULLY!

In order to log into MariaDB to secure it, we'll need the current
password for the root user.  If you've just installed MariaDB, and
you haven't set the root password yet, the password will be blank,
so you should just press enter here.

Enter current password for root (enter for none): 
ERROR 1045 (28000): Access denied for user 'root'@'localhost' (using password: YES)
Enter current password for root (enter for none): 
OK, successfully used password, moving on...

Setting the root password ensures that nobody can log into the MariaDB
root user without the proper authorisation.

Set root password? &#91;Y/n] y
New password: 
Re-enter new password: 
Password updated successfully!
Reloading privilege tables..
 ... Success!


By default, a MariaDB installation has an anonymous user, allowing anyone
to log into MariaDB without having to have a user account created for
them.  This is intended only for testing, and to make the installation
go a bit smoother.  You should remove them before moving into a
production environment.

Remove anonymous users? &#91;Y/n] y
 ... Success!

Normally, root should only be allowed to connect from 'localhost'.  This
ensures that someone cannot guess at the root password from the network.

Disallow root login remotely? &#91;Y/n] y
 ... Success!

By default, MariaDB comes with a database named 'test' that anyone can
access.  This is also intended only for testing, and should be removed
before moving into a production environment.

Remove test database and access to it? &#91;Y/n] y
 - Dropping test database...
 ... Success!
 - Removing privileges on test database...
 ... Success!

Reloading the privilege tables will ensure that all changes made so far
will take effect immediately.

Reload privilege tables now? &#91;Y/n] y
 ... Success!

Cleaning up...

All done!  If you've completed all of the above steps, your MariaDB
installation should now be secure.

Thanks for using MariaDB!
</code></pre>

Next is to&nbsp;**enable MariaDB database**

<pre class="wp-block-code"><code>$ sudo systemctl enable mariadb</code></pre>

**Start MariaDB database**

<pre class="wp-block-code"><code>$ sudo systemctl start mariadb</code></pre>

Check the&nbsp;**status of the MariaDB database**

<pre class="wp-block-code"><code>$ sudo systemctl status mariadb</code></pre>

You will see the following as the output.

<pre class="wp-block-code"><code>● mariadb.service - MariaDB 10.3.35 database server
     Loaded: loaded (/usr/lib/systemd/system/mariadb.service; enabled; vendor p&gt;
    Drop-In: /etc/systemd/system/mariadb.service.d
             └─migrated-from-my.cnf-settings.conf
     Active: active (running) since Wed 2022-07-06 13:58:41 UTC; 8s ago
       Docs: man:mysqld(8)
             https:&#47;&#47;mariadb.com/kb/en/library/systemd/
    Process: 7810 ExecStartPre=/bin/sh -c systemctl unset-environment _WSREP_ST&gt;
    Process: 7813 ExecStartPre=/bin/sh -c &#91; ! -e /usr/bin/galera_recovery ] && &gt;
    Process: 7936 ExecStartPost=/bin/sh -c systemctl unset-environment _WSREP_S&gt;
   Main PID: 7836 (mysqld)
     Status: "Taking your SQL requests now..."
      Tasks: 30 (limit: 1129)
     CGroup: /system.slice/mariadb.service
             └─7836 /usr/sbin/mysqld

Jul 06 13:58:41 localhost mysqld&#91;7836]: 2022-07-06 13:58:41 0 &#91;Note] InnoDB: 10&gt;
Jul 06 13:58:41 localhost mysqld&#91;7836]: 2022-07-06 13:58:41 0 &#91;Note] InnoDB: Lo&gt;
Jul 06 13:58:41 localhost mysqld&#91;7836]: 2022-07-06 13:58:41 0 &#91;Note] Plugin 'FE&gt;
Jul 06 13:58:41 localhost mysqld&#91;7836]: 2022-07-06 13:58:41 0 &#91;Note] InnoDB: Bu&gt;
Jul 06 13:58:41 localhost mysqld&#91;7836]: 2022-07-06 13:58:41 0 &#91;Note] Server soc&gt;
Jul 06 13:58:41 localhost mysqld&#91;7836]: 2022-07-06 13:58:41 0 &#91;Note] Reading of&gt;
lines 1-22
</code></pre>

To test MariaDB we can create a database. Login to MariaDB console with the following command.

<pre class="wp-block-code"><code>$ mysql -u root -p
Enter password: 
Welcome to the MariaDB monitor.  Commands end with ; or \g.
Your MariaDB connection id is 17
Server version: 10.3.35-MariaDB MariaDB Server

Copyright (c) 2000, 2018, Oracle, MariaDB Corporation Ab and others.

Type 'help;' or '\h' for help. Type '\c' to clear the current input statement.

MariaDB &#91;(none)]&gt; SELECT VERSION();
+-----------------+
| VERSION()       |
+-----------------+
| 10.3.35-MariaDB |
+-----------------+
1 row in set (0.000 sec)

MariaDB &#91;(none)]&gt; </code></pre>

## Conclusion {#conclusion.wp-block-heading}

We have learned how to install MariaDB on Ubuntu 21.04 server and secured it using the&nbsp;`mysql_secure_installation`&nbsp;script. You can now practice running SQL queries to learn more. I hope you enjoyed it and you have learned something new.

 [1]: https://galeracluster.com/library/whats-new.html
 [2]: https://mariadb.com/kb/en/mysql_secure_installation/