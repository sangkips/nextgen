---
title: How to Create, Move and Delete Files in Linux
author: Kipkoech Sang
type: post
date: 2021-10-13T06:32:30+00:00
url: /2021/10/13/creating-moving-and-deleting-files-in-linux/
site-sidebar-layout:
  - default
site-content-layout:
  - default
theme-transparent-header-meta:
  - default
rank_math_news_sitemap_robots:
  - index
rank_math_robots:
  - 'a:1:{i:0;s:5:"index";}'
rank_math_analytic_object_id:
  - 138
rank_math_internal_links_processed:
  - 1
categories:
  - Linux

---
In this article we are going to learn how to create, move and delete files in a Linux using command-line tools. We will be using the following commands: **mv, ls, cp, pwd, find, touch, rmdir, rm, echo, cat and mkdir**

The command-line interface is the most effective way to manage Linux file systems. 

## Creating Directory {.wp-block-heading}

A directory is a special kind of file used to organize files. A good way to think off files is like the file folders used to organize papers in a file cabinet.

The **mkdir** commands are used to create directories. 

<pre class="wp-block-code"><code>$ mkdir </code></pre>

To return to the home directory, use the cd command:

<pre class="wp-block-code"><code>$ cd</code></pre>

## Find command  {.wp-block-heading}

The **find** command is the handiest tool in Linux. It searches for files and directories in a directory hierarchy based on a user-given expression and can perform user-specified action on each matched file. It can be combined with other tools such as [grep][1].

### Find command syntax {.wp-block-heading}

<pre class="wp-block-code"><code>$ find &#91;options] &#91;path...] &#91;expression]
</code></pre>

  * The **options** attributes controls the treatment of symbolic link, debugging options and optimization method.
  * The **path&#8230;** attribute defines the starting directory or directories where find search the files.
  * The **expression** attribute is made up of options, search patterns and actions separated by operators.

When invoking the find command you need to have read permissions on that directory.

## Creating files  {.wp-block-heading}

An empty file can be created with the **touch** command. For example, 

<pre class="wp-block-code"><code># touch &lt;filename&gt;</code></pre>

<pre class="wp-block-code"><code># touch file1</code></pre>

## Cat command  {.wp-block-heading}

The **cat** command can be used to view the contents inside a file. Let&#8217;s take the example we created above.

<pre class="wp-block-code"><code># cat file1</code></pre>

It returns nothing because touch creates empty files. 

## Echo command {.wp-block-heading}

The **echo** command is used with > to create simple text files. For example, we can add content to our file1 created above with the following command:

<pre class="wp-block-code"><code># echo welcome &gt; file1</code></pre>

The `>` character instructs the shell to write the output of a command to the specified file instead of your terminal.

## Renaming Files {.wp-block-heading}

Files are moved and renamed with the mv command. For example, let us add some files to our example above.

<pre class="wp-block-code"><code>$ touch file3 file44

$ echo file3 &gt; file3

$ echo file44 &gt; file44</code></pre>

What if we made a mistake with naming files, instead of file4 we typed file44. We fix this with the **mv** command as follows:

<pre class="wp-block-code"><code>$ mv file44 file4</code></pre>

## Moving Files  {.wp-block-heading}

Files are moved from one directory to another with the **mv** command 

<pre class="wp-block-code"><code>$ mkdir dir1 dir2 dir3</code></pre>

## Deleting files and Directories  {.wp-block-heading}

We use t**he rm** command to delete files and directories.

The rmdir command deletes directories only.

rm will not delete a directory by default, we need to add **-r** for it to delete the directory.

<pre class="wp-block-code"><code>$ rm -r dir1</code></pre>

## Copying files and Directories  {.wp-block-heading}

The **cp** command is used to copy files and directories. 

<pre class="wp-block-code"><code># cp &#91;additional_option] source_file target_file</code></pre>

For example

<pre class="wp-block-code"><code>cp file.txt file1.txt</code></pre>

This Linux command creates a copy of the&nbsp;_file.txt&nbsp;_file and renames the new file to&nbsp;_**file1.txt**_.

By default, the `<strong>cp</strong>`** **command runs in the same directory you are working in. However, the same file cannot exist twice in the same directory. You’ll need to change the name of the target file to copy in the same location.

## Conclusion {.wp-block-heading}

From the above, we have learned how to create, move and delete files on a Linux system. Try to do a lot of practice to get full grasp of the content.

Check out these related articles 

  * [Finding your way on a Linux System part 2][2]
  * [Finding your way on a Linux System][3]

 [1]: https://www.gnu.org/software/grep/manual/grep.html
 [2]: https://nextgentips.com/2021/10/08/finding-your-way-on-a-linux-system-part-2/
 [3]: https://nextgentips.com/2021/10/06/finding-your-way-on-a-linux-system/