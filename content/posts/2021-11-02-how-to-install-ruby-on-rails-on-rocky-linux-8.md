---
title: How to install Ruby on Rails on Rocky Linux 8
author: Kipkoech Sang
type: post
date: 2021-11-02T16:04:19+00:00
url: /2021/11/02/how-to-install-ruby-on-rails-on-rocky-linux-8/
ocean_gallery_link_images:
  - off
ocean_disable_margins:
  - enable
ocean_display_top_bar:
  - default
ocean_display_header:
  - default
ocean_disable_title:
  - default
ocean_disable_heading:
  - default
ocean_disable_breadcrumbs:
  - default
ocean_display_footer_widgets:
  - default
ocean_display_footer_bottom:
  - default
ocean_link_format_target:
  - self
ocean_quote_format_link:
  - post
rank_math_news_sitemap_robots:
  - index
rank_math_robots:
  - 'a:1:{i:0;s:5:"index";}'
rank_math_analytic_object_id:
  - 113
rank_math_internal_links_processed:
  - 1
categories:
  - Linux

---
In this tutorial guide, we are going to install and configure Ruby on Rails on Rocky Linux 8. 

Ruby on Rails is a server-side web application framework written in Ruby under the MIT license. Rails is a model-view-controller framework providing default structures for databases, a web service, and web pages. Ruby on Rails is used by notable organizations such Airbnb, GitHub, Crunchbase, Bloomberg, etc

Ruby on Rails is open source web development framework suitable for building dynamic web pages. Rails stand out among other web development frameworks because of their elegance, power, and integrated approach to web applications.

We will install Ruby version 3.0.3 and Rails version 7.0.0

## Prerequisites  {.wp-block-heading}

  * Rocky Linux server running as root 
  * High speed internet connection.
  * gnupg2, bash, wget and curl install

## Install Ruby on rails using Ruby Version manager(RVM) {.wp-block-heading}

To use the RVM route, we need to install the RVM startup script first on our system

## Installing Ruby on Rails on Rocky Linux {.wp-block-heading}

## Step 1. Install required dependencies  {.wp-block-heading}

First, let us install the required dependencies in our system with the following command:

<pre class="wp-block-code"><code>$ sudo dnf update -y

$ sudo dnf install gnupg2 wget curl -y</code></pre>

<pre class="wp-block-code"><code># sudo dnf install gnupg2 wget curl -y
DigitalOcean Droplet Agent                                                        56 kB/s | 3.3 kB     00:00    
Package gnupg2-2.2.20-2.el8.x86_64 is already installed.
Package curl-7.61.1-18.el8_4.1.x86_64 is already installed.
Dependencies resolved.
=================================================================================================================
 Package               Architecture            Version                          Repository                  Size
=================================================================================================================
Installing:
 wget                  x86_64                  1.19.5-10.el8                    appstream                  733 k

Transaction Summary
=================================================================================================================
Install  1 Package

Total download size: 733 k
Installed size: 2.8 M
Downloading Packages:
wget-1.19.5-10.el8.x86_64.rpm                                                    985 kB/s | 733 kB     00:00    
-----------------------------------------------------------------------------------------------------------------
Total                                                                            906 kB/s | 733 kB     00:00     
Running transaction check
Transaction check succeeded.
Running transaction test
Transaction test succeeded.
Running transaction
  Preparing        :                                                                                         1/1 
  Installing       : wget-1.19.5-10.el8.x86_64                                                               1/1 
  Running scriptlet: wget-1.19.5-10.el8.x86_64                                                               1/1 
  Verifying        : wget-1.19.5-10.el8.x86_64                                                               1/1 

Installed:
  wget-1.19.5-10.el8.x86_64                                                                                      

Complete!</code></pre>

After the update is complete you can then install its dependencies. Bash is always installed by default on our Linux systems but to be sure that it is there we can run the following command for confirmation.

<pre class="wp-block-code"><code>$ echo "${BASH_VERSION}"
4.4.20(1)-release</code></pre>

## Step 2. Run RVM Script {.wp-block-heading}

We use **curl** to add the script to our system. Use the following command, I am going to download the stable version but you can install the development version also:

To install the development version of RVM, use the following command:

<pre class="wp-block-code"><code>$ \curl -sSL https://get.rvm.io | bash</code></pre>

<pre class="wp-block-code"><code>output
Downloading https://github.com/rvm/rvm/archive/master.tar.gz
Creating group 'rvm'
Installing RVM to /usr/local/rvm/
Installation of RVM in /usr/local/rvm/ is almost complete:

  * First you need to add all users that will be using rvm to 'rvm' group,
    and logout - login again, anyone using rvm will be operating with `umask u=rwx,g=rwx,o=rx`.

  * To start using RVM you need to run `source /etc/profile.d/rvm.sh`
    in all your open shell windows, in rare cases you need to reopen all shell windows.
  * Please do NOT forget to add your users to the rvm group.
     The installer no longer auto-adds root or users to the rvm group. Admins must do this.
     Also, please note that group memberships are ONLY evaluated at login time.
     This means that users must log out then back in before group membership takes effect!
Thanks for installing RVM 🙏
Please consider donating to our open collective to help us maintain RVM.

👉  Donate: https://opencollective.com/rvm/donate</code></pre>

Let&#8217;s analyze what is in the code above

  * **_Backslash \_**: It ensures that we are using regular curl command and not any other version.
  * **_curl_** : it is used to install RVM installation script from the project website
  * **_-s_** : it is used to indicate that utility should operate on silent mode 
  * **_S_**: this is to ensure that curl overrides all the errors that might occur during the downloading process
  * **_L_**: it instructs the utility to follow any redirects happening during installation process

## Step 3. Add Regular user to the group {.wp-block-heading}

In the script, we are requested to install regular users to the rvm group because the installer no longer auto-adds root or users to the rvm group. Use the following command to do so: 

<pre class="wp-block-code"><code>$ adduser nextgentips</code></pre>

Then you can add your created user to rvm group with the following command:

<pre class="wp-block-code"><code>$ usermod -aG rvm nextgentips</code></pre>

Check out this article on how to add users

  * [How to setup Ubuntu 20.04 Server for the first time][1]

## Step 4. Update System Variables  {.wp-block-heading}

To start using RVM we need to run **_source /usr/local/rvm/scripts/rvm_** in order to add it to /usr/local/rvm file. use the following command

<pre class="wp-block-code"><code>$ source /usr/local/rvm/scripts/rvm</code></pre>

We have to install package requirements with the following command 

Let&#8217;s reload rvm for the changes to take effect with the following command:

<pre class="wp-block-code"><code>$ rvm reload
RVM reloaded!</code></pre>

<pre class="wp-block-code"><code>$ rvm requirements</code></pre>

That command will install all the dependencies required 

<pre class="wp-block-code"><code>Output
Checking requirements for centos.
Installing requirements for centos.
Installing required packages: patch, autoconf, automake, bison, gcc-c++, libffi-devel, libtool, make, patch, readline-devel, ruby, sqlite-devel, zlib-devel, glibc-headers, glibc-devel, openssl-devel...........................|
Requirements installation successful.</code></pre>

After this we now need to check the versions of Ruby available then we install the latest version available.

<pre class="wp-block-code"><code>$ rvm list known</code></pre>

You will get the following output

<pre class="wp-block-code"><code>Output
# MRI Rubies
&#91;ruby-]1.8.6&#91;-p420]
&#91;ruby-]1.8.7&#91;-head] # security released on head
&#91;ruby-]1.9.1&#91;-p431]
&#91;ruby-]1.9.2&#91;-p330]
&#91;ruby-]1.9.3&#91;-p551]
&#91;ruby-]2.0.0&#91;-p648]
&#91;ruby-]2.1&#91;.10]
&#91;ruby-]2.2&#91;.10]
&#91;ruby-]2.3&#91;.8]
&#91;ruby-]2.4&#91;.10]
&#91;ruby-]2.5&#91;.9]
&#91;ruby-]2.6&#91;.9]
&#91;ruby-]2.7&#91;.5]
&lt;strong>&#91;ruby-]3&#91;.0.3]&lt;/strong>
&#91;ruby-]3.1&#91;.0-preview1]
ruby-head

# for forks use: rvm install ruby-head-&lt;name> --url https://github.com/github/ruby.git --branch 2.2

# JRuby
jruby-1.6&#91;.8]
jruby-1.7&#91;.27]
jruby-9.1&#91;.17.0]
jruby&#91;-9.2.20.0]
jruby&#91;-9.3.1.0]
jruby-head

# Rubinius
rbx-1&#91;.4.3]
rbx-2.3&#91;.0]
rbx-2.4&#91;.1]
rbx-2&#91;.5.8]
rbx-3&#91;.107]
rbx-4&#91;.20]
rbx-5&#91;.0]
rbx-head

# TruffleRuby
truffleruby&#91;-21.3.0]

# Opal
opal

# Minimalistic ruby implementation - ISO 30170:2012
mruby-1.0.0
mruby-1.1.0
mruby-1.2.0
mruby-1.3.0
mruby-1&#91;.4.1]
mruby-2.0.1
mruby-2&#91;.1.1]
mruby&#91;-head]

# Ruby Enterprise Edition
ree-1.8.6
ree&#91;-1.8.7]&#91;-2012.02]

# Topaz
topaz

# MagLev
maglev-1.0.0
maglev-1.1&#91;RC1]
maglev&#91;-1.2Alpha4]
maglev-head

# Mac OS X Snow Leopard Or Newer
macruby-0.10
macruby-0.11
macruby&#91;-0.12]
macruby-nightly
macruby-head

# IronRuby
ironruby&#91;-1.1.3]
ironruby-head</code></pre>

I will go ahead with the installation of Ruby 3.0.3 in this example.

<pre class="wp-block-code"><code>$ rvm install ruby 3.0.3</code></pre>

You will get the following as output.

<pre class="wp-block-code"><code>$ Output
Searching for binary rubies, this might take some time.
No binary rubies available for: centos/8/x86_64/ruby-3.0.3.
Continuing with compilation. Please read 'rvm help mount' to get more information on binary rubies.
Checking requirements for centos.
Requirements installation successful.
Installing Ruby from source to: /usr/local/rvm/rubies/ruby-3.0.3, this may take a while depending on your cpu(s)...
ruby-3.0.3 - #downloading ruby-3.0.3, this may take a while depending on your connection...
  % Total    % Received % Xferd  Average Speed   Time    Time     Time  Current
                                 Dload  Upload   Total   Spent    Left  Speed
100 19.3M  100 19.3M    0     0  31.8M      0 --:--:-- --:--:-- --:--:-- 31.8M
ruby-3.0.3 - #extracting ruby-3.0.3 to /usr/local/rvm/src/ruby-3.0.3.....
ruby-3.0.3 - #configuring.........................................................................
ruby-3.0.3 - #post-configuration..
ruby-3.0.3 - #compiling..........................................................................................
ruby-3.0.3 - #installing.....................
ruby-3.0.3 - #making binaries executable...
Installed rubygems 3.2.32 is newer than 3.0.9 provided with installed ruby, skipping installation, use --force to force installation.
ruby-3.0.3 - #gemset created /usr/local/rvm/gems/ruby-3.0.3@global
ruby-3.0.3 - #importing gemset /usr/local/rvm/gemsets/global.gems...............................................-
ruby-3.0.3 - #generating global wrappers........
ruby-3.0.3 - #gemset created /usr/local/rvm/gems/ruby-3.0.3
ruby-3.0.3 - #importing gemsetfile /usr/local/rvm/gemsets/default.gems evaluated to empty gem list
ruby-3.0.3 - #generating default wrappers........
ruby-3.0.3 - #adjusting #shebangs for (gem irb erb ri rdoc testrb rake).
Install of ruby-3.0.3 - #complete 
Ruby was built without documentation, to build it run: rvm docs generate-ri</code></pre>

Check the Ruby version with the following command:

<pre class="wp-block-code"><code>$ ruby --version
ruby 3.0.3p157 (2021-11-24 revision 3fb7d2cadc) &#91;x86_64-linux]</code></pre>

Also check the rvm version

<pre class="wp-block-code"><code>rvm version
rvm 1.29.12-next (master) by Michal Papis, Piotr Kuczynski, Wayne E. Seguin &#91;https://rvm.io]</code></pre>

## Install Rails using Gem {.wp-block-heading}

We can use the following to install rails on our system

<pre class="wp-block-code"><code>$ gem install rails</code></pre>

<pre class="wp-block-code"><code>Sample output
Done installing documentation for zeitwerk, thor, method_source, concurrent-ruby, tzinfo, i18n, activesupport, nokogiri, crass, loofah, rails-html-sanitizer, rails-dom-testing, rack, rack-test, erubi, builder, actionview, actionpack, railties, mini_mime, marcel, activemodel, activerecord, globalid, activejob, activestorage, actiontext, mail, actionmailer, actionmailbox, websocket-extensions, websocket-driver, nio4r, actioncable, rails after 65 seconds
35 gems installed</code></pre>

Lastly, check if rails have been installed with **rails &#8211;version** command

<pre class="wp-block-code"><code># rails --version
Rails 7.0.0</code></pre>

## Conclusion  {.wp-block-heading}

Congratulations you have installed Ruby on Rails on Rocky Linux. You can now go ahead and write your programs. Happy coding. If you are faced with any challenge consult [Ruby on Rails Documentation.][2]

 [1]: https://nextgentips.com/2021/10/10/initial-server-setup-for-ubuntu-20-04/
 [2]: https://rvm.io/rvm/install