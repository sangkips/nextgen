---
title: How to install GNS3 2.2.26 on Ubuntu 21.04
author: Kipkoech Sang
type: post
date: 2021-10-28T20:56:25+00:00
url: /2021/10/28/how-to-install-gns3-2-2-26-on-ubuntu-21-04/
ocean_gallery_link_images:
  - off
ocean_disable_margins:
  - enable
ocean_display_top_bar:
  - default
ocean_display_header:
  - default
ocean_disable_title:
  - default
ocean_disable_heading:
  - default
ocean_disable_breadcrumbs:
  - default
ocean_display_footer_widgets:
  - default
ocean_display_footer_bottom:
  - default
ocean_link_format_target:
  - self
ocean_quote_format_link:
  - post
rank_math_news_sitemap_robots:
  - index
rank_math_robots:
  - 'a:1:{i:0;s:5:"index";}'
rank_math_analytic_object_id:
  - 118
rank_math_internal_links_processed:
  - 1
categories:
  - Linux

---
In this guide you are going to learn how to install GNS3 2.2.26 on our Ubuntu 21.04 server. 

GNS3 is free and open source software used by many network engineers to emulate, configure, test and troubleshoot virtual networks. GNS3 allows one to run small topology consisting of few devices in your laptop, to those that have many devices hosted on multiple servers or even hosted on the cloud.

GNS3 help you prepare for certifications such as CISCO CCNA, CCNP, CCIE and also help while trying real deployments. GNS3 help network engineers to virtualize real hardware. At first it used to emulate CISCO devices with the help of Dynamips but now it has evolved until it can support other devices from multiple network providers i.e brocade, cumulus Linux switches, Docker instances etc.

## Advantages of Using GNS3 {.wp-block-heading}

  * It has no limitation on the number of devices supported. As long as your system has enough memory space, has enough CPU power you are free to use.
  * It supports multi vendor environments hence no limitation on what devices you can use.
  * Its open source and free to use.
  * You have wider support in case you meet any problem, reason being you have a wider and larger community ready to help.
  * It has native support for Linux hence no need for virtualization technologies.

## Installing GNS3 on Ubuntu 21.04 {.wp-block-heading}

## 1. Adding GNS3 PPA Repository {.wp-block-heading}

PPA allows you to upload Ubuntu source packages to be built and published as an apt repository by launchpad. Use the following command to add PPA:

<pre class="wp-block-code"><code>$ sudo add-apt-repository ppa:gns3/ppa</code></pre>

Press enter to continue 

Then you can update your system to reflect on the changes done with the following command:

<pre class="wp-block-code"><code>$ sudo apt update</code></pre>

## 2. Install GNS3 gui and server {.wp-block-heading}

Yo can now install GNS3 gui and server after update is complete with the following command:

<pre class="wp-block-code"><code>$ sudo apt install gns3-gui gns3-server</code></pre>

Required packages will be installed during the process.<figure class="wp-block-image size-full">

<img decoding="async" loading="lazy" width="810" height="322" src="https://i0.wp.com/nextgentips.com/wp-content/uploads/2021/10/Screenshot-from-2021-10-28-23-15-09.png?resize=810%2C322&#038;ssl=1" alt="" class="wp-image-445" srcset="https://i0.wp.com/nextgentips.com/wp-content/uploads/2021/10/Screenshot-from-2021-10-28-23-15-09.png?w=952&ssl=1 952w, https://i0.wp.com/nextgentips.com/wp-content/uploads/2021/10/Screenshot-from-2021-10-28-23-15-09.png?resize=300%2C119&ssl=1 300w, https://i0.wp.com/nextgentips.com/wp-content/uploads/2021/10/Screenshot-from-2021-10-28-23-15-09.png?resize=768%2C305&ssl=1 768w" sizes="(max-width: 810px) 100vw, 810px" data-recalc-dims="1" /> <figcaption>Ubridge configuration</figcaption></figure> 

Press yes and continue<figure class="wp-block-image size-full">

<img decoding="async" loading="lazy" width="810" height="322" src="https://i0.wp.com/nextgentips.com/wp-content/uploads/2021/10/Screenshot-from-2021-10-28-23-15-09-1.png?resize=810%2C322&#038;ssl=1" alt="" class="wp-image-446" srcset="https://i0.wp.com/nextgentips.com/wp-content/uploads/2021/10/Screenshot-from-2021-10-28-23-15-09-1.png?w=952&ssl=1 952w, https://i0.wp.com/nextgentips.com/wp-content/uploads/2021/10/Screenshot-from-2021-10-28-23-15-09-1.png?resize=300%2C119&ssl=1 300w, https://i0.wp.com/nextgentips.com/wp-content/uploads/2021/10/Screenshot-from-2021-10-28-23-15-09-1.png?resize=768%2C305&ssl=1 768w" sizes="(max-width: 810px) 100vw, 810px" data-recalc-dims="1" /> <figcaption>Wireshark capture</figcaption></figure> 

Do not allow non users to capture packets because it will raise security concerns.

## 3. Start GNS3 {.wp-block-heading}

On your terminal type **gns3** and it will power GNS3

<pre class="wp-block-code"><code>$ gns3</code></pre><figure class="wp-block-image size-full">

<img decoding="async" loading="lazy" width="810" height="456" src="https://i0.wp.com/nextgentips.com/wp-content/uploads/2021/10/Screenshot-from-2021-10-28-23-37-57.png?resize=810%2C456&#038;ssl=1" alt="" class="wp-image-447" srcset="https://i0.wp.com/nextgentips.com/wp-content/uploads/2021/10/Screenshot-from-2021-10-28-23-37-57.png?w=914&ssl=1 914w, https://i0.wp.com/nextgentips.com/wp-content/uploads/2021/10/Screenshot-from-2021-10-28-23-37-57.png?resize=300%2C169&ssl=1 300w, https://i0.wp.com/nextgentips.com/wp-content/uploads/2021/10/Screenshot-from-2021-10-28-23-37-57.png?resize=768%2C432&ssl=1 768w" sizes="(max-width: 810px) 100vw, 810px" data-recalc-dims="1" /> <figcaption>First instance of GNS3</figcaption></figure> 

Cnnfirm local server configuration<figure class="wp-block-image size-full">

<img decoding="async" loading="lazy" width="810" height="402" src="https://i0.wp.com/nextgentips.com/wp-content/uploads/2021/10/Screenshot-from-2021-10-28-23-40-56.png?resize=810%2C402&#038;ssl=1" alt="" class="wp-image-448" srcset="https://i0.wp.com/nextgentips.com/wp-content/uploads/2021/10/Screenshot-from-2021-10-28-23-40-56.png?w=925&ssl=1 925w, https://i0.wp.com/nextgentips.com/wp-content/uploads/2021/10/Screenshot-from-2021-10-28-23-40-56.png?resize=300%2C149&ssl=1 300w, https://i0.wp.com/nextgentips.com/wp-content/uploads/2021/10/Screenshot-from-2021-10-28-23-40-56.png?resize=768%2C381&ssl=1 768w" sizes="(max-width: 810px) 100vw, 810px" data-recalc-dims="1" /> </figure> 

Check if connection is successful if you get screenshot like the below figure you are now good to practice your CISCO practice exams <figure class="wp-block-image size-full">

<img decoding="async" loading="lazy" width="801" height="360" src="https://i0.wp.com/nextgentips.com/wp-content/uploads/2021/10/Screenshot-from-2021-10-28-23-44-40-1.png?resize=801%2C360&#038;ssl=1" alt="" class="wp-image-449" srcset="https://i0.wp.com/nextgentips.com/wp-content/uploads/2021/10/Screenshot-from-2021-10-28-23-44-40-1.png?w=801&ssl=1 801w, https://i0.wp.com/nextgentips.com/wp-content/uploads/2021/10/Screenshot-from-2021-10-28-23-44-40-1.png?resize=300%2C135&ssl=1 300w, https://i0.wp.com/nextgentips.com/wp-content/uploads/2021/10/Screenshot-from-2021-10-28-23-44-40-1.png?resize=768%2C345&ssl=1 768w" sizes="(max-width: 801px) 100vw, 801px" data-recalc-dims="1" /> </figure> 

## Conclusion {.wp-block-heading}

We have learned how to install GNS3 on Ubuntu 21.04, It is now high time you start practicing CISCO practice exams. If you encounter any challenge consult [GNS3 Documentation][1]

You may like this content also

  * [How to install Visual Studio Code on Ubuntu 21.04][2]

 [1]: https://docs.gns3.com/docs/
 [2]: https://nextgentips.com/2021/10/24/how-to-install-visual-studio-code-on-ubuntu-21-04/