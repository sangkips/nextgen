---
title: New version of GNS3 2.226
author: Kipkoech Sang
type: post
date: -001-11-30T00:00:00+00:00
draft: true
url: /?p=456
ocean_gallery_link_images:
  - off
ocean_disable_margins:
  - enable
ocean_display_top_bar:
  - default
ocean_display_header:
  - default
ocean_disable_title:
  - default
ocean_disable_heading:
  - default
ocean_disable_breadcrumbs:
  - default
ocean_display_footer_widgets:
  - default
ocean_display_footer_bottom:
  - default
ocean_link_format_target:
  - self
ocean_quote_format_link:
  - post
rank_math_news_sitemap_robots:
  - index
rank_math_robots:
  - 'a:1:{i:0;s:5:"index";}'
categories:
  - News

---
Here we will explore new features with the release of GNS3 2.2.26 release. 

GNS3 continues to play a bigger role in computer networking industry. This is because you can use it for free and its an open source project with large community base. Anyone interested in contribute to GNS3 development can do so. 

With the release of GNS3 2.2.26, it fixes bugs which comes with the earlier versions. Both server and GUI had advance features added to solve user problems.

## GNS3 2.2.26 New features  {.wp-block-heading}

### Server Updates {.wp-block-heading}

The new server updates made include the following 

  * It releases web UI 2.2.26 
  * It sorts symbols by theme 
  * It fixes memory percentage left warning 
  * It updated affinity symbols 

### GUI updates  {.wp-block-heading}

The following updates was introduced:

  * It upgraded embedded Python to version 3.7 in windows package.
  * It upgrades Visual C++ redistributable for for Visual Studio 2019 for windows package.
  * It provides fixes for SSL support in windows package.
  * It fixes custom symbols which can&#8217;t be unfolded after using filter field.
  *