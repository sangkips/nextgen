---
title: How to install OpenSSL 3 on Ubuntu 20.04
author: Kipkoech Sang
type: post
date: 2022-03-23T18:31:47+00:00
url: /2022/03/23/how-to-install-openssl-3-on-ubuntu-20-04/
rank_math_ca_keyword:
  - 'a:2:{s:7:"keyword";s:9:"openssl 3";s:7:"country";s:3:"all";}'
rank_math_seo_score:
  - 73
rank_math_focus_keyword:
  - openssl 3
rank_math_primary_category:
  - 6
rank_math_internal_links_processed:
  - 1
rank_math_analytic_object_id:
  - 158
categories:
  - Linux

---
OpenSSL 3 contains an open-source implementation of the SSL and TLS protocols. OpenSSL implements basic cryptographic function. The OpenSSL toolkit includes **libssl, libcrypto and openssl** which is the OpenSSL command-line tool, a swiss army knife for cryptographic tasks, testing, and analyzing. It can be used for:

  * Creation of key parameters.
  * Creation of x.509 certificates, CSRs and CRLs
  * Encryption and decryption
  * Calculation of message digests 
  * SSL/TLS client and server tests
  * Handling of S/MIME signed or encrypted mail.

In this tutorial, I will show you how to install OpenSSL on Ubuntu 20.04. OpenSSL 3 is the latest long-term release for OpenSSL. OpenSSL is a software application that is used to secure communication between two different mediums in a computer network. It is used by the majority of web servers.

## Install OpenSSL 3 on Ubuntu 20.04 {.wp-block-heading}

### 1. Update System Repositories  {.wp-block-heading}

To start installing OpenSSL 3 on Ubuntu 20.04, we need to update our system repositories in order to make them up to date. So to start with use the following command.

<pre class="wp-block-code"><code>sudo apt update && apt upgrade -y</code></pre>

When upgrades and updates are complete, proceed to install dependencies.

### 2. Install OpenSSL 3 dependencies  {.wp-block-heading}

We need to install the following dependencies so that when we install OpenSSL, we wouldn&#8217;t run into errors early on. The following dependencies will be installed with the following command.

<pre class="wp-block-code"><code>sudo apt install build-essential checkinstall zlib1g-dev -y</code></pre>

### 3. Download OpenSSL 3 {.wp-block-heading}

We need to head over to the <a href="https://www.openssl.org/source/" target="_blank" rel="noreferrer noopener">OpenSSL download page</a> to get the download link from there. We are going to use wget to make the download. First, make sure you are in this directory **/usr/local/src/**. So let&#8217;s cd into this directory then we proceed with the download.

<pre class="wp-block-code"><code>cd /usr/local/src/
wget https://www.openssl.org/source/openssl-3.0.2.tar.gz</code></pre>

When the download is complete, proceed to extract the archive contents to your system.

<pre class="wp-block-code"><code>sudo tar -xvf openssl-3.0.2.tar.gz</code></pre>

Before we can go ahead with the installation, cd into OpenSSL 3 you have extracted. For safety reasons lets ls to see the contents inside.

<pre class="wp-block-code"><code>ls
ACKNOWLEDGEMENTS.md  HACKING.md        NOTES-PERL.md      README-PROVIDERS.md  build.info        e_os.h    providers
AUTHORS.md           INSTALL.md        NOTES-UNIX.md      README.md            config            engines   ssl
CHANGES.md           LICENSE.txt       NOTES-VALGRIND.md  SUPPORT.md           config.com        external  test
CONTRIBUTING.md      NEWS.md           NOTES-VMS.md       VERSION.dat          configdata.pm.in  fuzz      tools
Configurations       NOTES-ANDROID.md  NOTES-WINDOWS.md   VMS                  crypto            include   util
Configure            NOTES-DJGPP.md    README-ENGINES.md  apps                 demos             ms        wycheproof
FAQ.md               NOTES-NONSTOP.md  README-FIPS.md     appveyor.yml         doc               os-dep</code></pre>

### 4. Install OpenSSL 3 on Ubuntu 20.04 {.wp-block-heading}

We are going to use the compilation method to install OpenSSL 3 for now. The default version of openssl 3 for now in my system is shown below.

<pre class="wp-block-code"><code>openssl version -a
&lt;strong>OpenSSL 1.1.1f  31 Mar 2020&lt;/strong>
built on: Wed Mar  9 12:12:45 2022 UTC
platform: debian-amd64
options:  bn(64,64) rc4(16x,int) des(int) blowfish(ptr) 
compiler: gcc -fPIC -pthread -m64 -Wa,--noexecstack -Wall -Wa,--noexecstack -g -O2 -fdebug-prefix-map=/build/openssl-2iuOVN/openssl-1.1.1f=. -fstack-protector-strong -Wformat -Werror=format-security -DOPENSSL_TLS_SECURITY_LEVEL=2 -DOPENSSL_USE_NODELETE -DL_ENDIAN -DOPENSSL_PIC -DOPENSSL_CPUID_OBJ -DOPENSSL_IA32_SSE2 -DOPENSSL_BN_ASM_MONT -DOPENSSL_BN_ASM_MONT5 -DOPENSSL_BN_ASM_GF2m -DSHA1_ASM -DSHA256_ASM -DSHA512_ASM -DKECCAK1600_ASM -DRC4_ASM -DMD5_ASM -DAESNI_ASM -DVPAES_ASM -DGHASH_ASM -DECP_NISTZ256_ASM -DX25519_ASM -DPOLY1305_ASM -DNDEBUG -Wdate-time -D_FORTIFY_SOURCE=2
OPENSSLDIR: "/usr/lib/ssl"
ENGINESDIR: "/usr/lib/x86_64-linux-gnu/engines-1.1"
Seeding source: os-specific</code></pre>

So to compile from the source run the following command.

<pre class="wp-block-code"><code>./config --prefix=/usr/local/ssl --openssldir=/usr/local/ssl shared zlib
make
make test</code></pre>

<pre class="wp-block-code"><code>#sample output
Configuring OpenSSL version 3.0.2 for target linux-x86_64
Using os-specific seed configuration
Creating configdata.pm
Running configdata.pm
Creating Makefile.in
Creating Makefile

**********************************************************************
***                                                                ***
***   OpenSSL has been successfully configured                     ***
***                                                                ***
***   If you encounter a problem while building, please open an    ***
***   issue on GitHub &lt;https://github.com/openssl/openssl/issues>  ***
***   and include the output from the following command:           ***
***                                                                ***
***       perl configdata.pm --dump                                ***
***                                                                ***
***   (If you are new to OpenSSL, you might want to consult the    ***
***   'Troubleshooting' section in the INSTALL.md file first)      ***
***                                                                ***
**********************************************************************</code></pre>

Please wait for the compiler to finish before proceeding with the **make install** command.

<pre class="wp-block-code"><code>make install</code></pre>

If you are successful with the installation, proceed to configure link libraries. The new OpenSSL loads files from **/usr/local/ssl/lib** directory, so lets cd into /etc/ld.so.conf.d/ and add the following .conf file, use your favourite text editor.

<pre class="wp-block-code"><code>cd /etc/ld.so.conf.d/
sudo vi openssl-3.0.2.conf</code></pre>

Add this into the file **/usr/local/ssl/lib** and reload the dynamic link with this command.

<pre class="wp-block-code"><code>sudo ldconfig -v</code></pre>

### 5. Configure openssl 3 Binary {.wp-block-heading}

We now need to replace OpenSSL binary found in **/usr/bin/openssl** or **/bin/openssl** with the new version in **/usr/local/ssl/bin/openssl**. First backup the binary files.

<pre class="wp-block-code"><code>mv /usr/bin/c_rehash /usr/bin/c_rehash.BEKUP
mv /usr/bin/openssl /usr/bin/openssl.BEKUP</code></pre>

Then we need to edit /etc/environment with your favorite editor.

<pre class="wp-block-code"><code>sudo vi /etc/environment</code></pre>

Add this **/usr/local/ssl/bin** to the end of the PATH

<pre class="wp-block-code"><code>PATH="/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/usr/games:/usr/local/games:/usr/local/ssl/bin"</code></pre>

Save and exit.

Then we need to reload the environment variable.

<pre class="wp-block-code"><code>source /etc/environment
echo $PATH
/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/usr/games:/usr/local/games:/usr/local/ssl/bin</code></pre>

## Conclusion {.wp-block-heading}

We have successfully installed OpenSSL 3 on Ubuntu 20.04. In case you face any challenges please consult the <a href="https://wiki.openssl.org/index.php/Compilation_and_Installation" target="_blank" rel="noreferrer noopener">OpenSSL documentation wiki</a>.