---
title: Finding your way on a Linux System Part 1
author: Kipkoech Sang
type: post
date: 2021-10-06T13:38:04+00:00
url: /2021/10/06/finding-your-way-on-a-linux-system/
site-sidebar-layout:
  - default
site-content-layout:
  - default
theme-transparent-header-meta:
  - default
rank_math_primary_category:
  - 6
rank_math_news_sitemap_robots:
  - index
rank_math_robots:
  - 'a:1:{i:0;s:5:"index";}'
rank_math_analytic_object_id:
  - 145
rank_math_internal_links_processed:
  - 1
categories:
  - Linux

---
Command-line is used basically in Linux. This is the communication means between the user and the system. It&#8217;s a very complex tool. Each command has its own key unique option, therefore documentation is key when working with a Linux system. 

There are many methods to get help from the Linux command lines such as man, help, and **info** but are only a few we will focus on here.

## Built-in Help {.wp-block-heading}

When started with the &#8211;help parameter, most commands display some brief instructions about their usage. Instructions from &#8211;help parameter is rather brief as compared to others.

<pre class="wp-block-code"><code>$help</code></pre>

<pre class="wp-block-code"><code>GNU bash, version 5.0.17(1)-release (x86_64-pc-linux-gnu)
These shell commands are defined internally.  Type `help' to see this list.
Type `help name' to find out more about the function `name'.
Use `info bash' to find out more about the shell in general.
Use `man -k' or `info' to find out more about commands not in this list.
</code></pre>

## Man pages {.wp-block-heading}

Most commands provide a manual page or a &#8216;man page&#8217;. This documentation comes with the software and can be accessed with the **man** command. For example, **man mkdir**

<pre class="wp-block-code"><code>$ man mkdir</code></pre>

This command opens the man page for Mkdir. You can navigate through using up and down arrow keys. To exit the man page press q for quiet.

<pre class="wp-block-code"><code>MKDIR(1)                         User Commands                        MKDIR(1)

NAME
       mkdir - make directories

SYNOPSIS
       mkdir &#91;OPTION]... DIRECTORY...

DESCRIPTION
       Create the DIRECTORY(ies), if they do not already exist.

       Mandatory  arguments  to  long  options are mandatory for short options
       too.

       -m, --mode=MODE
              set file mode (as in chmod), not a=rwx - umask

       -p, --parents
              no error if existing, make parent directories as needed

       -v, --verbose
              print a message for each created directory

</code></pre>

## Info pages  {.wp-block-heading}

The **info** pages are usually more detailed than the man pages and are formatted in hypertext, similar to web pages on the internet.

<pre class="wp-block-code"><code>$ info mkdir
Next: mkfifo invocation,  Prev: ln invocation,  Up: Special file types

12.3 ‘mkdir’: Make directories
==============================

‘mkdir’ creates directories with the specified names.  Synopsis:

     mkdir &#91;OPTION]... NAME...

   ‘mkdir’ creates each directory NAME in the order given.  It reports
an error if NAME already exists, unless the ‘-p’ option is given and
NAME is a directory.

   The program accepts the following options.  Also see *note Common
options::.

‘-m MODE’
‘--mode=MODE’
     Set the file permission bits of created directories to MODE, which
     uses the same syntax as in ‘chmod’ and uses ‘a=rwx’ (read, write
     and execute allowed for everyone) for the point of the departure.
     *Note File permissions::.
-----Info: (coreutils)mkdir invocation, 65 lines --Top--------------------------
Welcome to Info version 6.7.  Type H for help, h for tutorial.
</code></pre>

## Locating Files  {.wp-block-heading}

## The locate command  {.wp-block-heading}

The **locate** command searches within the database and then outputs every name that matches a given string. 

<pre class="wp-block-code"><code>$ locate note</code></pre>

## The find command  {.wp-block-heading}

**The find** command is used to search for files in the directories. **Find** searches a directory tree recursively. It does not maintain the database as **locate** command. Find requires the path to search.

Next article we will explore [directories and listing of files][1] in Linux

 [1]: https://nextgentips.com/2021/10/08/finding-your-way-on-a-linux-system-part-2/