---
title: How to install and use Nmap on Ubuntu 20.04
author: Kipkoech Sang
type: post
date: 2022-01-11T18:59:05+00:00
url: /2022/01/11/how-to-install-and-use-nmap-on-ubuntu-20-04/
entry_views:
  - 8
view_ip:
  - 'a:8:{i:0;s:14:"105.161.84.104";i:1;s:13:"66.249.87.180";i:2;s:13:"66.249.75.102";i:3;s:10:"77.88.5.46";i:4;s:39:"2a00:23c4:c383:c901:e680:7c16:cbc6:b594";i:5;s:13:"207.46.13.189";i:6;s:14:"54.242.182.186";i:7;s:13:"223.18.54.241";}'
rank_math_news_sitemap_robots:
  - index
rank_math_robots:
  - 'a:1:{i:0;s:5:"index";}'
rank_math_analytic_object_id:
  - 37
rank_math_internal_links_processed:
  - 1
categories:
  - Security

---
Welcome to today’s topic where we will be talking about how to install Nmap on Fedora 35.

Nmap (Network mapper) is a free and open-source software for network discovery and security auditing. It is also used for network inventory services, managing service upgrades, and monitoring hosts’ downtime.

Nmap is designed for bigger networks but it can also work fine with standalone hosts. Nmap suite includes an advanced GUI and results viewer called&nbsp;[Zenmap][1], a flexible data transfer, redirection and a debugging tool called&nbsp;[Ncat][2], a utility for comparing scan results called&nbsp;[Ndiff][3], and a packet generation and response analysis tool called&nbsp;[Nping][4].

## Why use Nmap? {.wp-block-heading}

  * Nmap hs well writtend documentation for easy reference incase of difficulty.
  * Nmap supports a dozen advanced techniques out there
  * Most operating systems supports Nmap
  * Its free therefore maintained by the community.

## What is Nmap used for? {.wp-block-heading}

  * It is used for port, OS and host scanning
  * It is used for checking the number of live hosts
  * It is used for checking which ports are open
  * It is used for checking which IPs are connecting to your network
  * It provide real time information of the network

## Installing Nmap on Fedora 35 {.wp-block-heading}

There are two ways to install Nmap:

  1. Installing Nmap from Binary RPMs
  2. Installing from the Yum repository
  3. Building and installing from the source

## Related Articles  {.wp-block-heading}

  * [How to install and Use NMAP on Fedora 35][5]

## Installing Nmap on Ubuntu 20.04 {.wp-block-heading}

## 1. Run system updates  {.wp-block-heading}

Before we can begin our installation let&#8217;s run system updates in order to make it up to date

<pre class="wp-block-code"><code>$ sudo apt update && apt upgrade -y</code></pre>

## 2. Install Nmap  {.wp-block-heading}

To install Nmap run the following command 

<pre class="wp-block-code"><code>$ sudo apt install nmap -y</code></pre>

You will get the following as an output

<pre class="wp-block-code"><code>Output
The following packages were automatically installed and are no longer required:
  libfprint-2-tod1 libgtkglext1 libllvm9 libminizip1 libpangox-1.0-0 shim
Use 'sudo apt autoremove' to remove them.
The following additional packages will be installed:
  liblinear4 lua-lpeg nmap-common
Suggested packages:
  liblinear-tools liblinear-dev ncat ndiff zenmap
The following NEW packages will be installed:
  liblinear4 lua-lpeg nmap nmap-common
0 upgraded, 4 newly installed, 0 to remove and 137 not upgraded.
Need to get 5,412 kB of archives.
After this operation, 25.8 MB of additional disk space will be used.
Do you want to continue? &#91;Y/n]</code></pre>

Press Y to allow installation to continue 

You can check the Nmap version installed on Ubuntu 20.04

<pre class="wp-block-code"><code>$ nmap --version
Nmap version 7.80 ( https://nmap.org )
Platform: x86_64-pc-linux-gnu
Compiled with: liblua-5.3.3 openssl-1.1.1d nmap-libssh2-1.8.2 libz-1.2.11 libpcre-8.44 libpcap-1.9.1 nmap-libdnet-1.12 ipv6
Compiled without:
Available nsock engines: epoll poll select</code></pre>

## How to use Nmap on Ubuntu 20.04 {.wp-block-heading}

Whenever you want to use Nmap you can use the command **nmap -h**

<pre class="wp-block-code"><code>$ nmap -h</code></pre>

Nmap usage is like this

<pre class="wp-block-code"><code>Usage: nmap &#91;Scan Type(s)] &#91;Options] {target specification}</code></pre>

Target specifications can be done by passing the hostname or the IP address

For example, to scan a network you should do the following:

<pre class="wp-block-code"><code>$ nmap www.hostname.com</code></pre>

You will get the following as output

<pre class="wp-block-code"><code>Output
Starting Nmap 7.80 ( https://nmap.org ) at 2022-01-11 21:34 EAT
Nmap scan report for www.nextgentips.com (172.67.202.129)
Host is up (0.026s latency).
Other addresses for www.nextgentips.com (not scanned): 104.21.76.248 2606:4700:3034::6815:4cf8 2606:4700:3032::ac43:ca81

PORT      STATE SERVICE
1/tcp     open  tcpmux
3/tcp     open  compressnet
4/tcp     open  unknown
6/tcp     open  unknown
7/tcp     open  echo
9/tcp     open  discard
13/tcp    open  daytime
......
Nmap done: 1 IP address (1 host up) scanned in 35.00 seconds</code></pre>

To scan multiple IP addresses you can do the following 

<pre class="wp-block-code"><code>$ nmap Ip address1 Ip address2 Ip adress3</code></pre>

## Install nmap using snap {.wp-block-heading}

You can also install Nmap using the snap package. Snap is the best method because snap packages don&#8217;t require you to update manually because it does their updates on the background whenever a release is made.

<pre class="wp-block-code"><code>$ sudo snap install nmap</code></pre>

## Remove Nmap from Ubuntu 20.04 {.wp-block-heading}

To uninstall Nmap from your system use the following command 

<pre class="wp-block-code"><code>$ sudo apt remove nmap
Output
The following packages will be REMOVED:
  &lt;strong>nmap&lt;/strong>
0 upgraded, 0 newly installed, 1 to remove and 137 not upgraded.
After this operation, 4,499 kB disk space will be freed.
Do you want to continue? &#91;Y/n] y</code></pre>

## Conclusion {.wp-block-heading}

Congratulations, you have successfully learned how to install and use Nmap on Ubuntu 20.04. Continue learning more to gain more experience with it.

 [1]: https://nmap.org/zenmap/
 [2]: https://nmap.org/ncat/
 [3]: https://nmap.org/ndiff/
 [4]: https://nmap.org/nping/
 [5]: https://nextgentips.com/2022/01/11/how-to-install-and-use-nmap-on-fedora-35/