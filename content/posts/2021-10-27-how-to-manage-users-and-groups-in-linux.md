---
title: How to Manage Users and Groups in Linux
author: Kipkoech Sang
type: post
date: 2021-10-27T12:04:57+00:00
url: /2021/10/27/how-to-manage-users-and-groups-in-linux/
ocean_gallery_link_images:
  - off
ocean_disable_margins:
  - enable
ocean_display_top_bar:
  - default
ocean_display_header:
  - default
ocean_disable_title:
  - default
ocean_disable_heading:
  - default
ocean_disable_breadcrumbs:
  - default
ocean_display_footer_widgets:
  - default
ocean_display_footer_bottom:
  - default
ocean_link_format_target:
  - self
ocean_quote_format_link:
  - post
rank_math_news_sitemap_robots:
  - index
rank_math_robots:
  - 'a:1:{i:0;s:5:"index";}'
rank_math_analytic_object_id:
  - 121
rank_math_internal_links_processed:
  - 1
categories:
  - Linux

---
In this guide we are going to learn about managing Users and Groups in a Linux environment. Most Linux system administrators are at pain to manage users and groups in a very large organization settings. This tutorial will help to unlock that pain.

**Users** are accounts that are used to login into a system. Each user is identified by unique identification number (UID). Users can be identified by level of access. There are two categories:

  * **Normal Users:** These are regular users who does not make any changes to the system. They have limited access, they don&#8217;t make any modifications.
  * **[Sudoers][1]/root/administrators.** These are users with all access rights. They can modify the system whenever they want. They have what we call sudoers privileges. Not every one can do the modification to the system but only a few in the organization. 

**Group** is a collection of users. The main purpose of groups is to define a set of privileges such as read, write and execute permission for a given resource that can be shared among the users in that group.

The main reason why we are talking about user and group management is because of security. Security is paramount in any organization setting. It is never a good idea to allow users to share credentials of the same account. Not all users have the same agenda. Some want to plan worms so that they can gain access to unauthorized sections. 

## Related content  {.wp-block-heading}

  * &nbsp;[How to setup Ubuntu 20.04 Server for the first time][2]

## Prerequisites  {.wp-block-heading}

  * Fresh installed server
  * Be sudo or root user 

## Commands used to manage Users and Group permissions  {.wp-block-heading}

The following commands are mostly used to manage users and groups. The concerned party for managing users and groups are the administrators, root users or users with sudo privileges. 

  * **adduser:** command used to add users to the system.
  * **userdel:** command used to delete users from the system.
  * **addgroup:** command used to add group to the system.
  * **delgroup:** command used to remove a group from the system.
  * **usermod:** command used to modify user account 
  * **chage:** command used to change user password expiry date
  * **sudo:** allows a system administrator to delegate authority to give certain users (or groups of users)

/**etc/passwd**: This is where configuration files for passwords management is stored.

**/etc/shadow**: This is where configuration files for encrypted passwords is stored.

**/etc/sudoers**: This is where configuration files for sudo is stored.

**/etc/group:** this is where configuration files for group users is stored.

Check if sudo is installed in your Linux flavor with the following command:

<pre class="wp-block-code"><code>$ which sudo</code></pre>

Sample output

<pre class="wp-block-code"><code>$ # which sudo
/usr/bin/sudo</code></pre>

If it returns an absolute path **/usr/bin/sudo** as show in the sample output, that means sudo is installed in your system else you can install with the following command:

<pre class="wp-block-code"><code>$ apt install sudo</code></pre>

## Adding a new regular User  {.wp-block-heading}

Regular users are added into the system so that they can operate daily operation. Use the following command to add new users: make sure you are in root to add new user or sudo if you already have sudo privileges.

<pre class="wp-block-code"><code>$ adduser nextgentips</code></pre>

<pre class="wp-block-code"><code>$ # adduser nextgentips
Adding user `nextgentips' ...
Adding new group `nextgentips' (1000) ...
Adding new user `nextgentips' (1000) with group `nextgentips' ...
Creating home directory `/home/nextgentips' ...
Copying files from `/etc/skel' ...
New password: 
Retype new password: 
passwd: password updated successfully
Changing the user information for nextgentips
Enter the new value, or press ENTER for the default
        Full Name &#91;]: 
        Room Number &#91;]: 
        Work Phone &#91;]: 
        Home Phone &#91;]: 
        Other &#91;]: 
Is the information correct? &#91;Y/n] y</code></pre>

Incase you are not prompted for the password, you can create new password with the following command:

<pre class="wp-block-code"><code>$ sudo passwd nextgentips</code></pre>

You will follow prompt to add password, full name room number like the above case. Accept that everything is right and press enter.

Connect to the created user using SSH like this:

<pre class="wp-block-code"><code>$ ssh nextgentips@&lt;your_IP_Address&gt;</code></pre>

Sample output

<pre class="wp-block-code"><code># ssh nextgentips@&lt;your_IP_Address
The authenticity of host '67.205.150.16 (67.205.150.16)' can't be established.
ECDSA key fingerprint is SHA256:pEHTcEIM4iEp66SuAJlbCCl7H9LJdQT0oZqc7qrUaNI.
Are you sure you want to continue connecting (yes/no/&#91;fingerprint])? yes
Warning: Permanently added '&lt;your_IP_Address' (ECDSA) to the list of known hosts.
nextgentips@&lt;your_IP_Address password: 
Welcome to Ubuntu 21.10 (GNU/Linux 5.13.0-20-generic x86_64)

 * Documentation:  https://help.ubuntu.com
 * Management:     https://landscape.canonical.com
 * Support:        https://ubuntu.com/advantage

  System information as of Wed Oct 27 10:24:50 UTC 2021

  System load:  0.0               Users logged in:       1
  Usage of /:   2.9% of 48.29GB   IPv4 address for eth0: &lt;your_IP_Address&gt;
  Memory usage: 9%                IPv4 address for eth0: 10.10.0.0
  Swap usage:   0%                IPv4 address for eth1: 10.211.0.0
  Processes:    99

3 updates can be applied immediately.
3 of these updates are standard security updates.
To see these additional updates run: apt list --upgradable



The programs included with the Ubuntu system are free software;
the exact distribution terms for each program are described in the
individual files in /usr/share/doc/*/copyright.

Ubuntu comes with ABSOLUTELY NO WARRANTY, to the extent permitted by
applicable law.</code></pre>

**Delete the user** with the following command:

<pre class="wp-block-code"><code>$ userdel -r username</code></pre>

**-r** will ensure that all the user files are removed completely.

## Adding user Group: {.wp-block-heading}

To create a new group, we can do it like the following:

<pre class="wp-block-code"><code>$ addgroup IT</code></pre>

### Deleting Group {.wp-block-heading}

We can delete the group with the following command:

<pre class="wp-block-code"><code>$ delgroup IT</code></pre>

## Adding Special Permissions to Users and Groups {.wp-block-heading}

Owners, users can be granted different type of access to read from, write and execute files.

## Commands used to grant permissions {.wp-block-heading}

  * **chown:** is used to change file owner 
  * **chmod:** is used to change file permission
  * **chgrp:** is used to change group ownership
  * **id:** is used to print user and group IDs

The owner is the user who created that file. Lets create a file and use it as an example:

<pre class="wp-block-code"><code>$ echo "This is awesome work" &gt; work</code></pre>

Then do list the file with the following command:

<pre class="wp-block-code"><code>$ ls -l</code></pre>

sample output

<pre class="wp-block-code"><code># ls -l work
-rw-r--r-- 1 root root   20 Oct 27 11:27 work</code></pre>

**-rw** group indicate that the user has both read and write permission.

**-r-** group indicate that the user has read permission only

To change permissions, use **chmod**.

The following are permissions you can add to change the behavior of the content:

  * **+r**&nbsp;adds read permission
  * **-r**&nbsp;removes read permission
  * **+w**&nbsp;adds write permission
  * **-w**&nbsp;removes write permission
  * **+x**&nbsp;adds execute permission
  * **-x**&nbsp;removes execute permission
  * **+rw**&nbsp;adds read and write permissions
  * **+rwx**&nbsp;adds read and write and execute permissions

Let us look at each with an example:

<pre class="wp-block-code"><code>$ chmod u+x work</code></pre>

<pre class="wp-block-code"><code># ls -l work
-rwxr--r-- 1 root root 20 Oct 27 11:27 work</code></pre>

As you can see the **w and x** had been added. 

Another way to set permissions is to us octal. Look at the following table to understand how octal works:<figure class="wp-block-table">

<table>
  <tr>
    <td>
      <strong>Permissions </strong>
    </td>
    
    <td>
      <strong>Binary</strong>
    </td>
    
    <td>
      <strong>Octal</strong>
    </td>
  </tr>
  
  <tr>
    <td>
      &#8211;
    </td>
    
    <td>
      000
    </td>
    
    <td>
    </td>
  </tr>
  
  <tr>
    <td>
      -x
    </td>
    
    <td>
      001
    </td>
    
    <td>
      1
    </td>
  </tr>
  
  <tr>
    <td>
      -w-
    </td>
    
    <td>
      010
    </td>
    
    <td>
      2
    </td>
  </tr>
  
  <tr>
    <td>
      -wx
    </td>
    
    <td>
      011
    </td>
    
    <td>
      3
    </td>
  </tr>
  
  <tr>
    <td>
      r-
    </td>
    
    <td>
      100
    </td>
    
    <td>
      4
    </td>
  </tr>
  
  <tr>
    <td>
      r-x
    </td>
    
    <td>
      101
    </td>
    
    <td>
      5
    </td>
  </tr>
  
  <tr>
    <td>
      rw-
    </td>
    
    <td>
      110
    </td>
    
    <td>
      6
    </td>
  </tr>
  
  <tr>
    <td>
      rwx
    </td>
    
    <td>
      111
    </td>
    
    <td>
      7
    </td>
  </tr>
</table><figcaption>Octal notation </figcaption></figure> 

Let us do it with an example

<pre class="wp-block-code"><code>$ chmod 456 work</code></pre>

<pre class="wp-block-code"><code># ls -l work
-r--r-xrw- 1 root root 20 Oct 27 11:27 work</code></pre>

<pre class="wp-block-code"><code>$ chmod 725 work</code></pre>

<pre class="wp-block-code"><code># ls -l work
-rwx-w-r-x 1 root root 20 Oct 27 11:27 work</code></pre>

## Conclusion {.wp-block-heading}

You have learned how to manage user and groups in an organization, try to do more practical work to know better.

 [1]: https://www.sudo.ws/man/1.8.15/sudoers.man.html
 [2]: https://nextgentips.com/2021/10/10/initial-server-setup-for-ubuntu-20-04/