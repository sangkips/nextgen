---
title: How to install Python 3.11 on Ubuntu 20.04
author: Kipkoech Sang
type: post
date: 2021-11-23T13:07:51+00:00
url: /2021/11/23/how-to-install-python-3-11-on-ubuntu-20-04/
rank_math_news_sitemap_robots:
  - index
rank_math_robots:
  - 'a:1:{i:0;s:5:"index";}'
rank_math_analytic_object_id:
  - 88
rank_math_internal_links_processed:
  - 1
categories:
  - Linux

---
In this tutorial guide, I will take you through the installation steps for the new Python 3.11 release on an Ubuntu 20.04 distro.

Python programming language is an interpreted high-level general-purpose programming language. Its design philosophy emphasizes code readability with its use of significant code indentation.

We can begin by installing a python 3 interpreter on our system. This is a program that reads Python programs and carries out their instructions, you need it before you can begin python programming.

## Python 3.11 new Features  {.wp-block-heading}

  * Introduction of&nbsp;**enhanced error locations in tracebacks**. Whenever you are printing tracebacks, the intepreter will now point to the exact expressions that caused the error instead of just the line.&nbsp;**Traceback**&nbsp;is a report containing the function calls made in your code at a specific point. When your program results in exception, Python will print the current traceback to help you know what went wrong. In Python it is best to read traceback from bottom up. This is very helpful if since the traceback is printed out and your terminal usually ends up at the bottom of the output, giving you the perfect place to start reading the traceback.
  * The introduction of column information for code objects. The information used by enhanced traceback feature is made available as a general API taht can be used to corelate byte code inmstaructions with source code. code objects are a low level detail of the CPython implementation.

## Related Articles  {.wp-block-heading}

  * [Python 3.11 new and deprecated Features][1]
  * [How to install Python 3.9 interpreter on Ubuntu 20.04][2]

## Prerequisites {.wp-block-heading}

  * Make sure you are running Ubuntu 20.04 distro
  * Have a user with sudo privileges 
  * Make sure you have basic understanding of terminal

## Table of Contents {.wp-block-heading}

  1. Update your Ubuntu 20.04 repositories 
  2. Install deadsnake PPAs 
  3. Install software dependencies
  4. Install Python 3.11
  5. Conclusion

## 1. Update Ubuntu 20.04 repositories {.wp-block-heading}

The first thing we need to do for our freshly installed system is to update repositories to reflect current changes going on within the system. Use the following command to run the updates and if necessary also run the system-wide upgrade.

<pre class="wp-block-code"><code>$ sudo apt update
$ sudo apt upgrade -y</code></pre>

After the updates and upgrades are complete, we can now add custom PPA to our Ubuntu repository.

## 2. Add custom PPA {.wp-block-heading}

PPA is also known as Personal package Archives allows one to upload Ubuntu source packages to be built and published as an apt repository by Launchpad.

Let&#8217;s add the following [deadsnakes][3] package into our system with the following command;

<pre class="wp-block-code"><code>$ sudo add-apt-repository ppa:deadsnakes/ppa</code></pre>

<pre class="wp-block-code"><code>Sample output
Third-Party Python Modules
==========================

Python modules in the official Ubuntu repositories are packaged to work with the Python interpreters from the official repositories. Accordingly, they generally won't work with the Python interpreters from this PPA. As an exception, pure-Python modules for Python 3 will work, but any compiled extension modules won't.

To install 3rd-party Python modules, you should use the common Python packaging tools.  For an introduction into the Python packaging ecosystem and its tools, refer to the Python Packaging User Guide:
https:&#47;&#47;packaging.python.org/installing/

Sources
=======
The package sources are available at:
https://github.com/deadsnakes/

Nightly Builds
==============

For nightly builds, see ppa:deadsnakes/nightly https://launchpad.net/~deadsnakes/+archive/ubuntu/nightly
 More info: https://launchpad.net/~deadsnakes/+archive/ubuntu/ppa
Press &#91;ENTER] to continue or Ctrl-c to cancel adding it.

Hit:1 http://security.ubuntu.com/ubuntu focal-security InRelease
Get:2 http://ppa.launchpad.net/deadsnakes/ppa/ubuntu focal InRelease &#91;18.1 kB]                              
Hit:3 https://repos-droplet.digitalocean.com/apt/droplet-agent main InRelease                               
Hit:4 http://mirrors.digitalocean.com/ubuntu focal InRelease                          
Hit:5 http://mirrors.digitalocean.com/ubuntu focal-updates InRelease
Hit:6 http://mirrors.digitalocean.com/ubuntu focal-backports InRelease
Get:7 http://ppa.launchpad.net/deadsnakes/ppa/ubuntu focal/main amd64 Packages &#91;22.5 kB]
Get:8 http://ppa.launchpad.net/deadsnakes/ppa/ubuntu focal/main Translation-en &#91;5168 B]
Fetched 45.7 kB in 1s (70.9 kB/s)                                         
Reading package lists... Done</code></pre>

## 3. Install Python 3.11 dependencies  {.wp-block-heading}

Pyuthon 3 depends on other packages to accomplish its installation such as software-properties-common. Let&#8217;s install this dependency package now;

<pre class="wp-block-code"><code>$ sudo apt install software-properties-common</code></pre>

Software-properties-common looks like it comes as a default with Ubuntu 20.04. So nothing new will be installed for now.

Run updates again to effect the changes done.

<pre class="wp-block-code"><code>Sample output
&lt;strong>Hit:1 http://ppa.launchpad.net/deadsnakes/ppa/ubuntu focal InRelease&lt;/strong>
Hit:2 http://mirrors.digitalocean.com/ubuntu focal InRelease                                                    
Hit:3 https://repos-droplet.digitalocean.com/apt/droplet-agent main InRelease                                   
Get:4 http://mirrors.digitalocean.com/ubuntu focal-updates InRelease &#91;114 kB]
Get:5 http://security.ubuntu.com/ubuntu focal-security InRelease &#91;114 kB]
Get:6 http://mirrors.digitalocean.com/ubuntu focal-backports InRelease &#91;101 kB]
Get:7 http://mirrors.digitalocean.com/ubuntu focal-updates/universe amd64 Packages &#91;876 kB]</code></pre>

From the above output, you can see that deadsnakes ppa has been added to the system.

## 4. Install Python 3.11 {.wp-block-heading}

We are now ready to install Python 3.11 into our system. Run the following command in your terminal to run the installation.

<pre class="wp-block-code"><code>$ sudo apt install python3.11</code></pre>

You will get the following from your terminal.

<pre class="wp-block-code"><code>Sample output
Reading package lists... Done
Building dependency tree       
Reading state information... Done
The following additional packages will be installed:
  libpython3.11-minimal libpython3.11-stdlib python3.11-minimal
Suggested packages:
  python3.11-venv binutils binfmt-support
The following NEW packages will be installed:
  libpython3.11-minimal libpython3.11-stdlib python3.11 python3.11-minimal
0 upgraded, 4 newly installed, 0 to remove and 0 not upgraded.
Need to get 4949 kB of archives.
After this operation, 20.3 MB of additional disk space will be used.
Do you want to continue? &#91;Y/n] 
...
Setting up libpython3.11-minimal:amd64 (3.11.0~a2-1+focal2) ...
Setting up python3.11-minimal (3.11.0~a2-1+focal2) ...
Setting up libpython3.11-stdlib:amd64 (3.11.0~a2-1+focal2) ...
Setting up python3.11 (3.11.0~a2-1+focal2) ...</code></pre>

Press yes to allow installation proceed.

Now we can check our Python version 

<pre class="wp-block-code"><code>$ python3.11 --version
Python 3.11.0a2</code></pre>

You can run the Python 3.11 shell like this

<pre class="wp-block-code"><code>$ python3.11
Python 3.11.0a2 (main, Nov 16 2021, 03:12:15) &#91;GCC 9.3.0] on linux
Type "help", "copyright", "credits" or "license" for more information.
>>> </code></pre>

Type exit() to get out of the shell.

## 5. Conclusion. {.wp-block-heading}

We have successfuly installed Python 3.11 on our Ubuntu distro. Incase of anything check [deadsnakes team ppa][3] repository

 [1]: https://nextgentips.com/2021/11/23/python-3-11-new-and-deprecated-features/
 [2]: https://nextgentips.com/2021/10/04/install-python-3-on-ubuntu-20-04/
 [3]: https://launchpad.net/~deadsnakes/+archive/ubuntu/ppa