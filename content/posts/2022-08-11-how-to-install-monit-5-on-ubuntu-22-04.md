---
title: How to install Monit 5 on Ubuntu 22.04
author: Kipkoech Sang
type: post
date: 2022-08-11T06:09:17+00:00
url: /2022/08/11/how-to-install-monit-5-on-ubuntu-22-04/
categories:
  - Monitoring

---
Monit is a small open source utility for monitoring and managing Unix systems. It performs automatic maintenance whenever there is downtime in your system. From this tutorial, you are going to learn how to install Monit on Ubuntu 22.04.

## Uses of Monit. {.wp-block-heading}

Monit can have many use cases because of its versatility. Let&#8217;s look at the following:

  * Monit can be used to monitor files and directories, Monit monitor this system for any changes such as file timestamp, security changes or file size changes, etc.
  * Monit acts proactively, whereby in case of an error in the system functionality, Monit can send an alert to inform that such a process wasn&#8217;t complete.
  * Monit can be used to test programs and scripts such as cron jobs do. You can monitor and check the scripts you want to use in any scenario.
  * We use Monit to monitor any daemon processes or any other process running on the localhost such as sshd, Nginx, or MySQL processes.
  * Monit can be used to monitor general system functionality such as CPU and RAM usage.
  * It can be used to monitor network connections on various servers because Monit has built-in tests for Internet protocols such as SMTP, and HTTP. 

## How to install Monit on Ubuntu 22.04 {.wp-block-heading}

### 1. Run system updates.  {.wp-block-heading}

Before we begin the installation, let&#8217;s start by updating our system repositories to make them up to date. 

<pre class="wp-block-code"><code>sudo apt update && apt upgrade -y</code></pre>

2. Install Monit from the packages.

Because Monit is available on the Ubuntu package repository, we are only going to use the following command to do the install `<mark style="background-color:#abb8c3" class="has-inline-color">apt install monit</mark>`.

<pre class="wp-block-code"><code>$ sudo apt install monit</code></pre>

You should be in a position to see the following output.

<pre class="wp-block-code"><code>Suggested packages:
  default-mta | postfix | mail-transport-agent sysvinit-core
The following NEW packages will be installed:
  monit
0 upgraded, 1 newly installed, 0 to remove and 304 not upgraded.
Need to get 399 kB of archives.
After this operation, 1,100 kB of additional disk space will be used.
Get:1 http://ke.archive.ubuntu.com/ubuntu jammy/universe amd64 monit amd64 1:5.31.0-1 &#91;399 kB]
Fetched 399 kB in 1s (497 kB/s)
Selecting previously unselected package monit.
(Reading database ... 330871 files and directories currently installed.)
Preparing to unpack .../monit_1%3a5.31.0-1_amd64.deb ...
Unpacking monit (1:5.31.0-1) ...
Setting up monit (1:5.31.0-1) ...
Processing triggers for man-db (2.10.2-1) ...</code></pre>

After the installation is complete, we can check the version with the following command `<mark style="background-color:#abb8c3" class="has-inline-color">monit --version</mark>`.

<pre class="wp-block-code"><code>$ monit --version
This is Monit version 5.31.0
Built with ssl, with ipv6, with compression, with pam and with large files
Copyright (C) 2001-2022 Tildeslash Ltd. All Rights Reserved.</code></pre>

### 3. Configure Monit service  {.wp-block-heading}

To configure Monit to start on boot we use the following command `<mark style="background-color:#abb8c3" class="has-inline-color">sudo systemctl enable monit</mark>`

<pre class="wp-block-code"><code>$ systemctl enable monit</code></pre>

To start Monit immediately we use the following command.

<pre class="wp-block-code"><code>$ sudo systemctl start monit </code></pre>

To check the status of Monit we use the following command.

<pre class="wp-block-code"><code>$ sudo systemctl status monit 
● monit.service - LSB: service and resource monitoring daemon
     Loaded: loaded (/etc/init.d/monit; generated)
     Active: active (running) since Thu 2022-08-11 08:47:50 EAT; 15min ago
       Docs: man:systemd-sysv-generator(8)
      Tasks: 1 (limit: 9355)
     Memory: 2.6M
        CPU: 141ms
     CGroup: /system.slice/monit.service
             └─16979 /usr/bin/monit -c /etc/monit/monitrc

Ago 11 08:47:50 zx-pc systemd&#91;1]: Starting LSB: service and resource monitoring>
Ago 11 08:47:50 zx-pc monit&#91;16966]:  * Starting daemon monitor monit
Ago 11 08:47:50 zx-pc monit&#91;16966]:    ...done.
Ago 11 08:47:50 zx-pc systemd&#91;1]: Started LSB: service and resource monitoring >
lines 1-14/14 (END)</code></pre>

## Conclusion {.wp-block-heading}

We have successfully installed and configured Monit on Ubuntu 22.04. For further reference check the <a href="https://mmonit.com/wiki/" target="_blank" rel="noreferrer noopener">Monit documentation</a>.