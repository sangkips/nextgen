---
title: How to install PostgreSQL 15 on RedHat Linux 8|Fedora 36
author: Kipkoech Sang
type: post
date: 2022-10-14T10:23:49+00:00
url: /2022/10/14/how-to-install-postgresql-on-redhat-linux-8fedora-36/
categories:
  - Database

---
In this article, we are going to learn how to install and get PostgreSQL 15 up and running on a RedHat Linux 8 and Fedora 36 server. PostgreSQL is a powerful, open-source object-relational database system that uses and extends the SQL language combined with many features that safely store and scale the most complicated data workloads.

### Notable Features introduced for PostgreSQL 15. {.wp-block-heading}

  * It removes both the long deprecated exclusive backup and support from Python 2 from PL/Python.
  * It revokes the CREATE permission from all users except for the database owner from the default.
  * It adds the new built-in extension, the pg_walinspect, that lets users inspect the contents of write-ahead log files directly from an SQL interface.
  * Server-level statistics are now collected in shared memory, eliminating the statistics collector process and periodically writing this data to disk.
  * It makes it possible to make an ICU Collation the default collation for a cluster or an individual database.
  * It introduces the new logging format `<mark style="background-color:#abb8c3" class="has-inline-color">jsonlog</mark>` which outputs log data using a defined json structure and allows PostgreSQL logs to be processed in structured logging systems. This gives database administrators more flexibility on how users can manage PostgreSQL configurations.
  * It provides more flexibility for managing logical replication. It introduces row filtering and column lists for publishers, letting users choose to replicate a subset of data from a table.
  * It added a feature to simplify conflict management, including the ability to skip replaying a conflicting transaction and to automatically disable a subscription if an error is detected.

### 1. Update your system packages  {.wp-block-heading}

Let&#8217;s begin by refreshing our system&#8217;s local index files using the following command.

<pre class="wp-block-code"><code>sudo dnf update -y</code></pre>

First check the default version of Apstream for PostgreSQL with the following command:

<pre class="wp-block-code"><code>$ sudo dnf module list postgresql
Red Hat Enterprise Linux 8 for x86_64 - AppStream from RHUI (RPMs)                                              14 kB/s | 4.5 kB     00:00    
Red Hat Enterprise Linux 8 for x86_64 - BaseOS from RHUI (RPMs)                                                 95 kB/s | 4.1 kB     00:00    
Red Hat Ansible Engine 2 for RHEL 8 (RPMs) from RHUI                                                            97 kB/s | 4.0 kB     00:00    
RHUI Client Configuration Server 8                                                                              46 kB/s | 2.0 kB     00:00    
Red Hat Enterprise Linux 8 for x86_64 - AppStream from RHUI (RPMs)
Name                         Stream                   Profiles                             Summary                                             
postgresql                   9.6                      client, server &#91;d]                   PostgreSQL server and client module                 
postgresql                   10 &#91;d]                   client, server &#91;d]                   PostgreSQL server and client module                 
postgresql                   12                       client, server &#91;d]                   PostgreSQL server and client module                 
postgresql                   13                       client, server &#91;d]                   PostgreSQL server and client module                 </code></pre>

### 2. Install PostgreSQL 15 on RedHat Linux 8 {.wp-block-heading}

In order to run PostgreSQL 15 on RedHat Linux 8, we are supposed to create the file repository configuration so that PostgreSQL will be available for installation.

<pre class="wp-block-code"><code>sudo dnf install -y https://download.postgresql.org/pub/repos/yum/reporpms/EL-8-x86_64/pgdg-redhat-repo-latest.noarch.rpm</code></pre>

Next, we need to disable the default PostgreSQL 10 from the system so that it can allow you to install the newer version.

<pre class="wp-block-code"><code>$ &lt;strong>sudo dnf -qy module disable postgresql&lt;/strong>

#output
Importing GPG key 0x442DF0F8:
 Userid     : "PostgreSQL RPM Building Project &lt;pgsql-pkg-yum@postgresql.org&gt;"
 Fingerprint: 68C9 E2B9 1A37 D136 FE74 D176 1F16 D2E1 442D F0F8
 From       : /etc/pki/rpm-gpg/RPM-GPG-KEY-PGDG
Importing GPG key 0x442DF0F8:
 Userid     : "PostgreSQL RPM Building Project &lt;pgsql-pkg-yum@postgresql.org&gt;"
 Fingerprint: 68C9 E2B9 1A37 D136 FE74 D176 1F16 D2E1 442D F0F8
 From       : /etc/pki/rpm-gpg/RPM-GPG-KEY-PGDG
Importing GPG key 0x442DF0F8:
 Userid     : "PostgreSQL RPM Building Project &lt;pgsql-pkg-yum@postgresql.org&gt;"
 Fingerprint: 68C9 E2B9 1A37 D136 FE74 D176 1F16 D2E1 442D F0F8
 From       : /etc/pki/rpm-gpg/RPM-GPG-KEY-PGDG
Importing GPG key 0x442DF0F8:
 Userid     : "PostgreSQL RPM Building Project &lt;pgsql-pkg-yum@postgresql.org&gt;"
 Fingerprint: 68C9 E2B9 1A37 D136 FE74 D176 1F16 D2E1 442D F0F8
 From       : /etc/pki/rpm-gpg/RPM-GPG-KEY-PGDG
Importing GPG key 0x442DF0F8:
 Userid     : "PostgreSQL RPM Building Project &lt;pgsql-pkg-yum@postgresql.org&gt;"
 Fingerprint: 68C9 E2B9 1A37 D136 FE74 D176 1F16 D2E1 442D F0F8
 From       : /etc/pki/rpm-gpg/RPM-GPG-KEY-PGDG
Importing GPG key 0x442DF0F8:
 Userid     : "PostgreSQL RPM Building Project &lt;pgsql-pkg-yum@postgresql.org&gt;"
 Fingerprint: 68C9 E2B9 1A37 D136 FE74 D176 1F16 D2E1 442D F0F8
 From       : /etc/pki/rpm-gpg/RPM-GPG-KEY-PGDG
Importing GPG key 0x442DF0F8:
 Userid     : "PostgreSQL RPM Building Project &lt;pgsql-pkg-yum@postgresql.org&gt;"
 Fingerprint: 68C9 E2B9 1A37 D136 FE74 D176 1F16 D2E1 442D F0F8
 From       : /etc/pki/rpm-gpg/RPM-GPG-KEY-PGDG</code></pre>

Now we can install PostgreSQL 

<pre class="wp-block-code"><code>sudo yum install -y postgresql15-server</code></pre>

You should be in a position to see the output below.

<pre class="wp-block-code"><code>Package Architecture Versio Repository Size
Installing:
 &lt;strong>postgresql15-server&lt;/strong>                  x86_64                  15.0-1PGDG.rhel8                  pgdg15                                   5.8 M
Installing dependencies:
 &lt;strong>libicu &lt;/strong>                              x86_64                  60.3-2.el8_1                      rhel-8-baseos-rhui-rpms                  8.8 M
 lz4 &lt;strong>                 &lt;/strong>                x86_64                  1.8.3-3.el8_4                     rhel-8-baseos-rhui-rpms                  104 k
 &lt;strong>postgresql15 &lt;/strong>                        x86_64                  15.0-1PGDG.rhel8                  pgdg15                                   1.6 M
 &lt;strong>postgresql15-libs&lt;/strong>                    x86_64                  15.0-1PGDG.rhel8                  pgdg15                                   287 k</code></pre>

### 3. Configure PostgreSQL {.wp-block-heading}

Enable PostgreSQL 15 service and initialize the database

We can initialize our database first with this command:

<pre class="wp-block-code"><code>$ sudo /usr/pgsql-15/bin/postgresql-15-setup initdb</code></pre>

To enable PostgreSQL to use the following command.

<pre class="wp-block-code"><code>sudo systemctl enable postgresql-15</code></pre>

We need to start PostgreSQL after we have enabled it. 

<pre class="wp-block-code"><code>sudo systemctl start postgresql-15</code></pre>

Lastly, let&#8217;s check the status of PostgreSQL

<pre class="wp-block-code"><code>sudo systemctl status postgresql-15</code></pre>

You should be in a position to see the following as the output.

<pre class="wp-block-code"><code>● postgresql-15.service - PostgreSQL 15 database server
   Loaded: loaded (/usr/lib/systemd/system/postgresql-15.service; enabled; vendor preset: disabled)
   Active: active (running) since Fri 2022-10-14 09:49:11 UTC; 1min 2s ago
     Docs: https://www.postgresql.org/docs/15/static/
  Process: 56300 ExecStartPre=/usr/pgsql-15/bin/postgresql-15-check-db-dir ${PGDATA} (code=exited, status=0/SUCCESS)
 Main PID: 56306 (postmaster)
    Tasks: 7 (limit: 4700)
   Memory: 21.3M
   CGroup: /system.slice/postgresql-15.service
           ├─56306 /usr/pgsql-15/bin/postmaster -D /var/lib/pgsql/15/data/
           ├─56307 postgres: logger 
           ├─56308 postgres: checkpointer 
           ├─56309 postgres: background writer 
           ├─56311 postgres: walwriter 
           ├─56312 postgres: autovacuum launcher 
           └─56313 postgres: logical replication launcher 

Oct 14 09:49:11 ip-172-31-92-33.ec2.internal systemd&#91;1]: Starting PostgreSQL 15 database server...
Oct 14 09:49:11 ip-172-31-92-33.ec2.internal postmaster&#91;56306]: 2022-10-14 09:49:11.188 UTC &#91;56306] LOG:  redirecting log output to logging co&gt;
Oct 14 09:49:11 ip-172-31-92-33.ec2.internal postmaster&#91;56306]: 2022-10-14 09:49:11.188 UTC &#91;56306] HINT:  Future log output will appear in di&gt;
Oct 14 09:49:11 ip-172-31-92-33.ec2.internal systemd&#91;1]: Started PostgreSQL 15 database server.</code></pre>

To use PostgreSQL we use the following command.

<pre class="wp-block-code"><code>$ sudo -u postgres psql
could not change directory to "/home/ec2-user": Permission denied
psql (15.0)
Type "help" for help.

postgres=# show server_version;
 server_version 
----------------
 &lt;strong>15.0&lt;/strong>
(1 row)

postgres=# </code></pre>

### 4. Uninstall PostgreSQL {.wp-block-heading}

Whenever you want to remove PostgreSQL from your system for any reason known to you, uninstall using the following procedure.

First, you need to list the PostgreSQL packages to enable you to determine what you need to remove.

<pre class="wp-block-code"><code>$ rpm -qa | grep postgres
postgresql15-libs-15.0-1PGDG.rhel8.x86_64
postgresql15-15.0-1PGDG.rhel8.x86_64
postgresql15-server-15.0-1PGDG.rhel8.x86_64</code></pre>

To remove all packages related to PostgreSQL, we use the wildcard operator like this 

<pre class="wp-block-code"><code>sudo dnf remove postgres\*</code></pre>

## Conclusion {.wp-block-heading}

Congratulations! You have installed PostgreSQL 15 on RedHat Linux 8, and you have also learned why PostgreSQL is important to large data sets. For more information always consult the&nbsp;[PostgreSQL documentation.][1]

Here is another related article <a href="https://nextgentips.com/2022/10/14/how-to-install-and-configure-postgresql-15-on-debian-11/" target="_blank" rel="noreferrer noopener">How to install and Configure PostgreSQL 15 on Debian 11</a>

 [1]: https://www.postgresql.org/files/documentation/pdf/14/postgresql-14-A4.pdf