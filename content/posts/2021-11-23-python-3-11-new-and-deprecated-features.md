---
title: Python 3.11 new and deprecated Features
author: Kipkoech Sang
type: post
date: 2021-11-23T09:35:22+00:00
url: /2021/11/23/python-3-11-new-and-deprecated-features/
entry_views:
  - 1
view_ip:
  - 'a:1:{i:0;s:37:"2a01:e0a:809:1750:24cf:e11a:a1d6:daed";}'
rank_math_news_sitemap_robots:
  - index
rank_math_robots:
  - 'a:1:{i:0;s:5:"index";}'
rank_math_analytic_object_id:
  - 89
rank_math_internal_links_processed:
  - 1
categories:
  - News

---
Python 3.11 has just been released, we can explore the new features which had been added and removed from the previous release.

Python programming language is an interpreted high-level general-purpose programming language. Its design philosophy emphasizes code readability with its use of significant code indentation.

## New features in Python 3.11 {.wp-block-heading}

These are the new features introduced to the new release of Python 3.11.

  * Introduction of **enhanced error locations in tracebacks**. Whenever you are printing tracebacks, the intepreter will now point to the exact expressions that caused the error instead of just the line. **Traceback** is a report containing the function calls made in your code at a specific point. When your program results in exception, Python will print the current traceback to help you know what went wrong. In Python it is best to read traceback from bottom up. This is very helpful if since the traceback is printed out and your terminal usually ends up at the bottom of the output, giving you the perfect place to start reading the traceback.
  * The introduction of column information for code objects. The information used by enhanced traceback feature is made available as a general API taht can be used to corelate byte code inmstaructions with source code. code objects are a low level detail of the CPython implementation. 
  * Asynchronous comprehensions are now allowed inside comprehensions in asynchronous functions. Outer comprehensions implicitly become asynchronous.
  * Introduction of support for PEP 515-style initialization of fraction from string. 
  * Addition of math.cbrt(): which return the cube root of x.
  * A new function **operator.call** has been added.
  * For Sqlite3, you can now disable the autorizer by passing None to set_authorizer().
  * Collation name create_collation() can now contain a unicode character. Collation names with invalid characters can now raise UnicodeEncodeError instead of sqlite3.programmingerror.
  * On Unix, time.sleep() now uses the clock_nanosleep() or nanosleep() function if available. 
  * The Unicode database have been updated to version 14.0.0
  * Method calls with keywords are now faster due to bytecode changes which avoid creating bound method instances.

## Deprecated functions  {.wp-block-heading}

  * The lib2to3 pacakge and tool 2to3 have been deprecated and may not be able to parse Python 3.10 or newer versions
  * Webbrowser MacOSx is deprecated and will be removed in Python 3.13.
  * The turtle.RawTurtle.settiltangle() is deprecated since python 3.1.

## What has been removed. {.wp-block-heading}

  * smtpd.MailmanProxy is now removed as it is unusable without an external module, mailman.
  * The binhex module, bdist_msi comand deprecated in Python 3.9 is now removed. 
  * Removed the deprecated split() method of _tkinter

All these are but few changes done, you can read more on [Python documentation.][1]

 [1]: https://docs.python.org/3.11/whatsnew/3.11.html